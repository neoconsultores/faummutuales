@extends('layout.base')
<?php
//dd($sec['estado']);

$today = date('Y-m-d');
//dd($datos);
?>

@section('content')
    <div class="content" style="position: relative;z-index: 100;">
        <div style="">
            <div class="jumbotron jumbotron-fluid" style="display:flex; padding-top:0px; padding-bottom:0px; margin-bottom:10px;" style="background-color: #F1F2F2">
                <div class="container" style="padding-bottom: 10px;padding-top: 10px;">
                    <img src="img/jumbo.png" class="float-right d-none d-lg-block" style="margin-left: -600px; height: 100%;"> {{--radial-gradient(circle, black 0%, transparent 85%);--}}
                    <h1 class="display-6">Tarjeta Prepaga y de Crédito Mutual</h1>
                    <p class="lead">MasterCard "FAUM Con Vos", Al resguardo del bien mutuo, FAUM CON VOS, siempre a tu lado.</p>
                                 
                </div>
            </div>
        </div>
    </div>

    <div class="container" style="position: relative;z-index: -100;" id="form">
        
        @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" data-toggle="tooltip" data-placement="top" title="Salir de la Notificación">
                    <i class='far fa-times-circle' style='font-size:24px;color:red'></i>
                </button>	
                <ul>
                    @foreach ($errors->all() as $err)
                    <li>{{ $err }}</li>
                    @endforeach
                </ul>
            </div>
            
        @endif

        <h4>Alta de Usuarios</h4>
        <div class="tab-content" style="background-color: #e9ecef;" id="usuario" style="width: 1036px" >
            <fieldset style="width:100%;" >
                <div class="row mrow" style="padding-top: 10px;">
            
                    <div class="col-md-4 col-sm-3 col-xs-2">
                        <label class="help-block text-muted small-font margen_bot">Mutual Adherente</label>
                        <select class="form-control form-control-sm margen_izq" id="id_mutual" name="id_mutual" required>
                            <option value="" selected>Seleccione</option>
                            @foreach ($mutuales as $p)
                                <option value="{{ $p->soc_numero_socio }}" @if(isset($mutuales->soc_numero_socio) && $mutuales->soc_numero_socio == $p->soc_numero_socio) selected @endif>({{ $p->soc_numero_socio }}) {{ $p->nombre }}</option>       
                            @endforeach
                        </select> 
                    </div>

                        
                    <div class="col-md-5 col-sm-4 col-xs-2">
                        <div>
                            <label for="" class="help-block text-muted small-font margen_bot">Correo</label>
                            <input type="email" class="form-control form-control-sm margen_izq" id="usuario_correo" name="usuario_correo" placeholder="Correo del Usuario" maxlength="50" value="">
                        </div>
                        
                    </div>

                    
                </div>

                <div class="row mrow">
                    <div class="col-md-4 col-sm-3 col-xs-2">
                        <div>
                            <label for="" class="help-block text-muted small-font margen_bot">Nombre Usuario</label>
                            <input type="text" class="form-control form-control-sm margen_izq" id="nombre_usuario" name="nombre_usuario" placeholder="Nombre del Usuario" maxlength="50" value="" required>
                        </div>
                        
                    </div>

                    <div class="col-md-3 col-sm-2 col-xs-2">
                        <div><label for="" class="help-block text-muted small-font margen_bot">Rol</label></div>
                        <div>
                            <select class="form-control form-control-sm margen_izq" id="rol" name="rol" required>
                            <option value="" selected>Seleccione</option>
                            <option value="1" >Administrador</option>
                            <option value="2" >Usuario</option>
                        </select>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <div><label for="" class="help-block text-muted small-font margen_bot">Estado</label></div>
                        <div>
                            <select class="form-control form-control-sm margen_izq" id="habilitado" name="habilitado" required>
                            <option value="" selected>Seleccione</option>
                            <option value="1" >Habilitado</option>
                            <option value="0" >Deshabilitado</option>
                        </select>
                        </div>
                    </div>
                </div>
                
            </fieldset>
            <div class="col-md-12 col-sm-8 col-xs-4" style="padding-top: 0px;text-align: end;padding-bottom: 5px;"> 
                <button type="button" class="btn boton-guardar guardar-datos-usuario" style="background-color:#268fa9;float: inline-end;color: aliceblue;">Guardar</button>
                <button type="button" class="btn boton-guardar update-datos-usuario" style="background-color:#268fa9;float: inline-end;color: aliceblue;display:none">Actualizar</button>
                <!--<button type="button" class="btn boton-guardar delete-datos-usuario" style="background-color:#268fa9;float: inline-end;color: aliceblue;display:none">Eliminar</button> -->
            </div>
                    
        </div>
    </div>
    
    
    
@endsection

<div class="modal fade" id="mensaje_usu">
    <div class="modal-dialog">
      <div class="modal-content">
  
        <!-- cabecera del diálogo -->
        <div class="modal-header">
          <h4 class="modal-title">Mensaje</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- cuerpo del diálogo -->
        <div class="modal-body">
            <div class="mensaje_s">
                <strong class="small-font" id="salida" style="color:rgb(75 191 101);background-color:#f7eeee;"></strong>
            </div>
          
            <div class="mensaje_e">
                <strong class="small-font" id="error" style="color:red;background-color:#f7eeee;"></strong>
            </div>
        </div>
  
        <!-- pie del diálogo -->
        <div class="modal-footer">
          <button type="button" class="btn boton-guardar" style="background-color:#268fa9;float: inline-end;color: aliceblue;" data-dismiss="modal">Cerrar</button>
        </div>
  
      </div>
    </div>
  </div> 


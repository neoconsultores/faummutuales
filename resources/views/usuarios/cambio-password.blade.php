@extends('layout.base')
<?php
//dd($sec['estado']);

$today = date('Y-m-d');
//dd($datos);
?>

@section('content')
    <div class="content" style="position: relative;z-index: 100;">
        <div style="">
            <div class="jumbotron jumbotron-fluid" style="display:flex; padding-top:0px; padding-bottom:0px; margin-bottom:10px;" style="background-color: #F1F2F2">
                <div class="container" style="padding-bottom: 10px;padding-top: 10px;">
                    <img src="img/jumbo.png" class="float-right d-none d-lg-block" style="margin-left: -600px; height: 100%;"> {{--radial-gradient(circle, black 0%, transparent 85%);--}}
                    <h1 class="display-6">Tarjeta Prepaga y de Crédito Mutual</h1>
                    <p class="lead">MasterCard "FAUM Con Vos", Al resguardo del bien mutuo, FAUM CON VOS, siempre a tu lado.</p>
                                 
                </div>
            </div>
        </div>
    </div>

    <div class="container" style="position: relative;z-index: -100;" id="form">
        
        @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" data-toggle="tooltip" data-placement="top" title="Salir de la Notificación">
                    <i class='far fa-times-circle' style='font-size:24px;color:red'></i>
                </button>	
                <ul>
                    @foreach ($errors->all() as $err)
                    <li>{{ $err }}</li>
                    @endforeach
                </ul>
            </div>
            
        @endif

        <h4>Cambio de Password</h4>
        <div class="tab-content" style="background-color: #e9ecef;" id="usuario" style="width: 1036px" >
            <fieldset style="width:100%;" >
            
                <div class="row mrow" style="padding-top: 10px;">
                             
                    <div class="col-md-4 col-sm-4 col-xs-2">
                        <div><label for="" class="help-block text-muted small-font margen_bot">Correo Usuario</label></div>
                        <div><input type="text" class="form-control form-control-sm margen_izq" id="usuario" name="usuario" readonly value="{{ $user }}"></div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-2">
                        <div>
                            <label for="" class="help-block text-muted small-font margen_bot">Nombre</label>
                            <input type="text" class="form-control form-control-sm margen_izq" id="nombre" name="nombre" readonly value="{{ $nombre }}"> 
                        </div>
                            
                    </div>
                </div>

                <div class="row mrow" style="padding-top: 10px;">
                             
                    <div class="col-md-4 col-sm-4 col-xs-2">
                        <div><label for="" class="help-block text-muted small-font margen_bot">Nuevo Password</label></div>
                        <div><input type="text" class="form-control form-control-sm margen_izq" id="password1" name="password1" placeholder="Nuevo Password"></div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-2">
                        <div><label for="" class="help-block text-muted small-font margen_bot">Confirme Password</label></div>
                        <div><input type="text" class="form-control form-control-sm margen_izq" id="password2" name="password2" placeholder="Repita Password"></div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-1"><i class="fa fa-question-circle-o" style="margin-top: 37px;" aria-hidden="true"  data-toggle="modal" data-target="#mensaje_pass" title="La contraseña debe tener al menos una mayúscula, una minúscula, un número y un símbolo de estos (@.,:;#+-), no acepta 3 numeros consecutivos ni las ñ/Ñ."></i></div>
                </div>
                <div class="" style="padding-top: 0px;text-align: end;padding-bottom: 5px;"> 
                    <button type="button" class="btn boton-guardar update-password" style="background-color:#268fa9;color: aliceblue;">Actualizar</button>
                </div> 
                                
            </fieldset>
           
                    
        </div>
        <br>
        <div class="row mrow">
            <div class="col-md-4 col-sm-3 col-xs-2">
                <a class=""  style="color: #73c6d9;" href="/admin/cargar"><i class="fal fa-undo"></i>Volver a Pagina Principal</a>
            </div>
    
            
        </div>
    </div>

    
    
    
    
@endsection

<div class="modal fade" id="mensaje_usu">
    <div class="modal-dialog">
      <div class="modal-content">
  
        <!-- cabecera del diálogo -->
        <div class="modal-header">
          <h4 class="modal-title">Mensaje</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- cuerpo del diálogo -->
        <div class="modal-body">
            <div class="mensaje_s">
                <strong class="small-font" id="salida" style="color:rgb(75 191 101);background-color:#f7eeee;"></strong>
            </div>
          
            <div class="mensaje_e">
                <strong class="small-font" id="error" style="color:red;background-color:#f7eeee;"></strong>
            </div>
        </div>
  
        <!-- pie del diálogo -->
        <div class="modal-footer">
          <button type="button" class="btn boton-guardar" style="background-color:#268fa9;float: inline-end;color: aliceblue;" data-dismiss="modal">Cerrar</button>
        </div>
  
      </div>
    </div>
  </div> 

  <div class="modal fade" id="mensaje_pass">
    <div class="modal-dialog">
      <div class="modal-content">
  
        <!-- cabecera del diálogo -->
        <div class="modal-header">
          <h4 class="modal-title">Mensaje</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- cuerpo del diálogo -->
        <div class="modal-body">
            <div class="mensaje_s">
                <strong class="small-font" id="salida" style="color:rgb(75 191 101);background-color:#f7eeee;">La contraseña debe tener al menos una mayúscula, una minúscula, un número y un símbolo de estos (@.,:;#+-), no acepta 3 numeros consecutivos ni las ñ/Ñ.</strong>
            </div>
    
        </div>
  
        <!-- pie del diálogo -->
        <div class="modal-footer">
          <button type="button" class="btn boton-guardar" style="background-color:#268fa9;float: inline-end;color: aliceblue;" data-dismiss="modal">Cerrar</button>
        </div>
  
      </div>
    </div>
  </div> 


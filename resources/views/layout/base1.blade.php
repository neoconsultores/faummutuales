<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Faum Adm</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/simple-sidebar.css" rel="stylesheet">

</head>

<body>

  <div class="d-flex" id="wrapper">


    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper"><img src="/img/tarjeta2.png" width="70">
      <div class="sidebar-heading">Menu Principal</div> 
      <div class="list-group list-group-flush">
        <a href="#" class="list-group-item list-group-item-action bg-light">Listar Mutuales</a>
        <a href="#" class="list-group-item list-group-item-action bg-light">Cargar Mutuales</a>
        <a href="#" class="list-group-item list-group-item-action bg-light">Agregar Comite Mutuales</a>
       
        <a href="#" class="list-group-item list-group-item-action bg-light">Mi Perfil</a>
        <a href="/admin/salir" class="list-group-item list-group-item-action bg-light">Cerrar Sesion</a>
        
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
                <a class="nav-link" >Al resguardo del bien mutuo, FAUM CON VOS, siempre a tu lado.</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" >Rol: {{ session()->get('rol') }}<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" >Usuario: {{ session()->get('apellido') }}, {{ session()->get('nombre') }} <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="/admin/salir">Cerrar Sesion <span class="sr-only">(current)</span></a>
            </li>
            
          </ul>
        </div>
      </nav>

      
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
      $("#menu-toggle").text('salir');
    });
  </script>

</body>

</html>

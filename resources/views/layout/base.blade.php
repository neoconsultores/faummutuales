<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Faum - Mutuales Adheridas</title>
        <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="css/aem.css">
        <!--<link rel="stylesheet" type="text/css" href="css/app.css">-->
        <link href="{{ asset('admin/css/app.css') }}" rel="stylesheet">
        <!-- Fonts -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
       


        <style>
        a.acceso-directo{
            color:#1ca4bd;

        }
        .acceso-directo h5{
            font-size: calc(8px + 0.8vw);
        }
        .card-body a{
            color:#17a2b8 !important;
        }
        .menu-lateral{
            position: fixed;
            width:15%; 
            height:90%; 
             background-image: linear-gradient(to right, #03121f, #1ca4bd); /* background-image: linear-gradient(to right, #03121f, #1ca4bd);*/
            float:left;
        }
        .mantenimiento h3, .mantenimiento h2{
            text-align: center;
        }
        .mantenimiento img{
            display: block;
            margin-left: auto;
            margin-right: auto;
            padding-top: 10%;
            max-width: 600px;
        }
        .neo-button{
            background-color:#5293ca;
            color: white;
        }
        a.neo-button:hover{
            color: white;
        }

        .menuli{
            background-color:transparent;
            color: white;
            border: 1px darkgray;
            border-style: solid none solid none;
            cursor: pointer; 
            padding-top:5px; 
            padding-bottom:5px; 
            border-top-left-radius: 0 !important;
            border-top-right-radius: 0 !important;
            border-bottom-left-radius: 0 !important;
            border-bottom-right-radius: 0 !important;
            -webkit-transition: background-color 0.3s;
            -moz-transition: background-color 0.3s;
            -o-transition: background-color 0.3s;
            transition: background-color 0.3s;
            color: white;
        }
        .menuli:hover{
            background-color: white;
            color: black;
        }
        .menuli-activo{
            background-color: white;
            color: black;
        }

        .menu-logo{
            height: 10%; 
            width: 100%;
            min-height: 60px;
        }

        .menu-logo img{
            margin-top: 5%;
        }
        .mrow{
            padding-bottom: 10px;
        }

        .jumbotron-fluid {
            height: 100px;
        }

        .nav-tabs .nav-link.active { /*para cambiar el estilo al dar click a la pestaña*/
        color: #eef2f7;
        background-color: #fff;
        border-color: #dee2e6 #dee2e6 #fff;
        background-color: #268fa9;
        }

        .nav-tabs {
            margin-left: -16px;
            border-bottom: 0px;
        }

        .nav-tabs .nav-link {
            border: 1px solid transparent;
            border-top-left-radius: 0.70rem;
            border-top-right-radius: 0.70rem;
            color: #212529;
        }

        .nav-tabs .nav-link {
            border: 1px solid lightgray;
        }

        .link-act {
            border: 1px solid transparent;
            border-top-left-radius: 0.70rem;
            border-top-right-radius: 0.70rem;
            border: 1px solid lightgray;
            
        }

        .table{
            width: 94%;
            
        }

        .table-doc{
            width: 98%;
            border: 1px solid !important;
        }
        tr, td, th{ border: 1px solid !important;}

        .btn:focus{
            box-shadow:none !important;
        }

        #success_message{ display: none;}

        .verm{position: absolute};

        /*input:valid {
            border:solid 1px green;
        }

        input:invalid {
            border:solid 1px red;
        }*/
        </style>
        <!-- Scripts -->
        <script type="text/javascript" src="js/aem.js"></script>

        <script src="http://kit.fontawesome.com/2286f8182e.js" crossorigin="anonymous"></script>
        <!--
        <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <script src="js/bootstrap-datetimepicker.min.js"></script>
        -->
    </head>
    <body style="color: #212529;overflow-y:hidden" >
        <div id="app" style="margin-top: 39px;">
            
            <div class="row" style="margin:0px;position:fixed;left:0;top:0;right:0;background-color: #268fa9;">
                <div class="col-4 burguer" style="top: 5px;">

                </div>

                <div class="col-4" style="text-align:center;padding-top: 6px;">
                    <span style="color: white;font-size: medium;">Federaciones Aliadas Unión Mutual</span>
                </div>
                <div class="col-4">
                    <div class="float-right" style="z-index: 11;"><span style="color: white;font-size: medium;">{{ session()->get('user')  }}</span>
                        <button type="button" class="btn btn-link text-dark"><img src="img/help.png" alt=""></button>
                        <!--<button type="button" class="btn btn-link text-dark" id="salir"><img src="img/logout.png" alt=""></button> -->
                        <a href="/admin/logout" class="" style="width:0%;"><img src="img/logout.png" title="Salir"></i></a>
                    </div>
                </div>
            </div>
            

            <div id="divTreeList" class="tree-content" >
                <div class="menu-lateral" style="min-width: 280px;">

                    <div class="menu-logo text-center"><img src="/img/tarjeta_1.png" width="80" class="img-responsive" alt=""/></div> <!-- img-thumbnail -->
                    <div class="text-center"><span style="width:100%;text-align: -webkit-center;color: white;"><i class=""></i> Menú Principal</span></div>
                    @if(session()->get('rol') == 'Administrador')
                        <a href="/admin/alta-mutual" class="list-group-item list-group-item-action menuli" style="width:100%;"><i class="fas fa-home"></i> Altas Mutuales</a>
                        <a href="/admin/up-sucursal" class="list-group-item list-group-item-action menuli" style="width:100%;"><i class="fas fa-warehouse"></i> Actualizar  Sucursales</a>
                        <a href="/admin/alta-usuario" class="list-group-item list-group-item-action menuli" style="width:100%;"><i class="fas fa-user-plus"></i> Altas Usuario</a>
                    @else
                        <a href="/admin/cargar" class="list-group-item list-group-item-action menuli" style="width:100%;"><i class="fas fa-clipboard-list"></i> Cargar Datos de Mutual</a>
                        <a href="/admin/aprobar" class="list-group-item list-group-item-action menuli" style="width:100%;"><i class="fas fa-clipboard-check"></i> Confirmar Datos Mutual</a>
                        
                    @endif
                    <a href="/admin/cambio-password" class="list-group-item list-group-item-action menuli" style="width:100%;"><i class="fas fa-user-edit"></i> Cambiar Password</a>
                    <a href="/admin/logout" class="list-group-item list-group-item-action menuli" style="width:100%;"><i class="fas fa-reply"></i> Salir</a>
                 
            
            
                </div>
                
            </div> 
           
            
            <div class="contenido">
                @yield('content')
            </div>
            
            
            
        </div>
        
    </body>
    
    <footer id="sticky-footer" class="py-2 text-white-50" style="background: #268fa9;position: relative;z-index: 11;" style="overflow-y:hidden">
        <div id="contenedor" class="container text-center">
            <small>Copyright &copy; NeoConsultores S.A</small>
        </div>
    </footer>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


    <script>
        function salir(){
            document.location.href="/"; //admin/login
        }
      
        /*$('#datepicker').datepicker({
            uiLibrary: 'bootstrap4',
            locale: 'es-es',
        });*/

        $("#filtro_postal_pre").click(function(e) {
            console.log('paso #filtro_postal_pre');
            if($("#filtro_postal_pre").val() == 1){
                $("#buscar_postal_pre").val('');
                $("#buscar_postal_pre").removeAttr('disabled').attr('placeholder','Nombre de la Localidad');
                        
                
            }else if($("#filtro_postal_pre").val() == 2){
                $("#buscar_postal_pre").val('');
                $("#buscar_postal_pre").removeAttr('disabled').attr('placeholder','Código Postal');
                
            }else if($("#filtro_postal_pre").val() == 3){
                $("#buscar_postal_pre").val('');
                $("#buscar_postal_pre").attr('disabled','disabled').attr('placeholder','Todas');
                
                        
            }else if($("#filtro_postal_pre").val() == 0){
                $("#buscar_postal_pre").attr('disabled','disabled').attr('placeholder',' ');
            }
        });

        $("#filtro_postal_sec").click(function(e) {
            console.log('paso #filtro_postal_sec');
            if($("#filtro_postal_sec").val() == 1){
                $("#buscar_postal_sec").val('');
                $("#buscar_postal_sec").removeAttr('disabled').attr('placeholder','Nombre de la Localidad');
                        
                
            }else if($("#filtro_postal_sec").val() == 2){
                $("#buscar_postal_sec").val('');
                $("#buscar_postal_sec").removeAttr('disabled').attr('placeholder','Código Postal');
                
            }else if($("#filtro_postal_sec").val() == 3){
                $("#buscar_postal_sec").val('');
                $("#buscar_postal_sec").attr('disabled','disabled').attr('placeholder','Todas');
                
                        
            }else if($("#filtro_postal_sec").val() == 0){
                $("#buscar_postal_sec").attr('disabled','disabled').attr('placeholder',' ');
            }
        });

        $("#filtro_postal_tes").click(function(e) {
            console.log('paso #filtro_postal_tes');
            if($("#filtro_postal_tes").val() == 1){
                $("#buscar_postal_tes").val('');
                $("#buscar_postal_tes").removeAttr('disabled').attr('placeholder','Nombre de la Localidad');
                        
                
            }else if($("#filtro_postal_tes").val() == 2){
                $("#buscar_postal_tes").val('');
                $("#buscar_postal_tes").removeAttr('disabled').attr('placeholder','Código Postal');
                
            }else if($("#filtro_postal_tes").val() == 3){
                $("#buscar_postal_tes").val('');
                $("#buscar_postal_tes").attr('disabled','disabled').attr('placeholder','Todas');
                
                        
            }else if($("#filtro_postal_tes").val() == 0){
                $("#buscar_postal_tes").attr('disabled','disabled').attr('placeholder',' ');
            }
        });

        $("#filtro_postal_apo").click(function(e) {
            console.log('paso #filtro_postal_apo');
            if($("#filtro_postal_apo").val() == 1){
                $("#buscar_postal_apo").val('');
                $("#buscar_postal_apo").removeAttr('disabled').attr('placeholder','Nombre de la Localidad');
                        
                
            }else if($("#filtro_postal_apo").val() == 2){
                $("#buscar_postal_apo").val('');
                $("#buscar_postal_apo").removeAttr('disabled').attr('placeholder','Código Postal');
                
            }else if($("#filtro_postal_apo").val() == 3){
                $("#buscar_postal_apo").val('');
                $("#buscar_postal_apo").attr('disabled','disabled').attr('placeholder','Todas');
                
                        
            }else if($("#filtro_postal_apo").val() == 0){
                $("#buscar_postal_apo").attr('disabled','disabled').attr('placeholder',' ');
            }
        });

        $("#filtro_postal").click(function(e) {
            console.log('paso #filtro_postal');
            if($("#filtro_postal").val() == 1){
                $("#buscar_postal").val('');
                $("#buscar_postal").removeAttr('disabled').attr('placeholder','Nombre de la Localidad');
                        
                
            }else if($("#filtro_postal").val() == 2){
                $("#buscar_postal").val('');
                $("#buscar_postal").removeAttr('disabled').attr('placeholder','Código Postal');
                
            }else if($("#filtro_postal").val() == 3){
                $("#buscar_postal").val('');
                $("#buscar_postal").attr('disabled','disabled').attr('placeholder','Todas');
                
                        
            }else if($("#filtro_postal").val() == 0){
                $("#buscar_postal").attr('disabled','disabled').attr('placeholder',' ');
            }
        });

        $("#filtro_postalsuc").click(function(e) {
            console.log('paso #filtro_postal');
            if($("#filtro_postalsuc").val() == 1){
                $("#buscar_postalsuc").val('');
                $("#buscar_postalsuc").removeAttr('disabled').attr('placeholder','Nombre de la Localidad');
                        
                
            }else if($("#filtro_postalsuc").val() == 2){
                $("#buscar_postalsuc").val('');
                $("#buscar_postalsuc").removeAttr('disabled').attr('placeholder','Código Postal');
                
            }else if($("#filtro_postalsuc").val() == 3){
                $("#buscar_postalsuc").val('');
                $("#buscar_postalsuc").attr('disabled','disabled').attr('placeholder','Todas');
                
                        
            }else if($("#filtro_postalsuc").val() == 0){
                $("#buscar_postalsuc").attr('disabled','disabled').attr('placeholder',' ');
            }
        });


        $(".buscar_codpostal_pre").click(function(e) {
             //si le quiero pasar los valores de un campo por jquery
            var op = $(".cargopre").val();
            $('#opcion').val(op);
        });

        $(".buscar_codpostal_sec").click(function(e) {
             //si le quiero pasar los valores de un campo por jquery
            var op = $(".cargosec").val();
            $('#opcion').val(op);
        });

        $(".buscar_codpostal_tes").click(function(e) {
             //si le quiero pasar los valores de un campo por jquery
            var op = $(".cargotes").val();
            $('#opcion').val(op);
        });

        $(".buscar_codpostal_apo").click(function(e) {
             //si le quiero pasar los valores de un campo por jquery
            var op = $(".cargoapo").val();
            $('#opcion').val(op);
        });

        $(".buscar_codpostal").click(function(e) {
             //si le quiero pasar los valores de un campo por jquery
            var op = $(".cargo").val();
            $('#opcion').val(op);
        });

        $(".buscar_codpostal").click(function(e) {
        console.log('entro por click buscar codpostal de alta de mutual');
        e.preventDefault();
        var opcion = $('#filtro_postal').val();
        var buscar = $('#buscar_postal').val();
        var tipo_cargo = $('#opcion').val();
        $('.error').text('');
        $('.resultado_postal_suc').html('');

        
        console.log('filtro_postal:', opcion);
        console.log('buscar_postal:', buscar);
          
        var error = false;


        if (!error) {
            var data = {filtro: opcion,buscar: buscar};
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: 'buscar-codpostal-modal',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log(data);
                        $('.resultado_postal').html(data.datos).show();
                                             
                    }else {
                        console.log('paso succ error');
                        $('.error').html(data.error).show();
                        
                    }
                    
                }
            });
        }
    });

        $(".buscar_codpostalpre").click(function(e) {
        console.log('entro por click buscar codpostal pre');
        e.preventDefault();
        var opcion = $('#filtro_postal_pre').val();
        var buscar = $('#buscar_postal_pre').val();
        var tipo_cargo = $('#opcion').val();
        $('.error').text('');

        
        console.log('filtro_postal pre:', opcion);
        console.log('buscar_postal pre:', buscar);
        console.log('cargo:', tipo_cargo);
   
        var error = false;


        if (!error) {
            var data = {filtro: opcion,buscar: buscar, tipo_cargo:tipo_cargo};
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: 'buscar-codpostal-modal',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log(data);
                        $('.resultado_postal').html(data.datos).show();
                                             
                    }else {
                        console.log('paso succ error');
                        $('.error').html(data.error).show();
                        
                    }
                    
                }
            });
        }
    });

    $(".buscar_codpostalsec").click(function(e) {
        console.log('entro por click buscar codpostal sec');
        e.preventDefault();
        var opcion = $('#filtro_postal_sec').val();
        var buscar = $('#buscar_postal_sec').val();
        var tipo_cargo = $('#opcion').val();
        $('.error').text('');

        
        console.log('filtro_postalsec:', opcion);
        console.log('buscar_postalsec:', buscar);
        console.log('cargo:', tipo_cargo);
   
        var error = false;


        if (!error) {
            var data = {filtro: opcion,buscar: buscar, tipo_cargo:tipo_cargo};
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: 'buscar-codpostal-modal',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log(data);
                        $('.resultado_postalsec').html(data.datos).show();
                     
                      
                    }else {
                        console.log('paso succ error');
                        $('.error').html(data.error).show();
                       

                    }
                    
                }
            });
        }
    });

    $(".buscar_codpostaltes").click(function(e) {
        console.log('entro por click buscar codpostal tes');
        e.preventDefault();
        var opcion = $('#filtro_postal_tes').val();
        var buscar = $('#buscar_postal_tes').val();
        var tipo_cargo = $('#opcion').val();
        $('.error').text('');

        
        console.log('filtro_postaltes:', opcion);
        console.log('buscar_postaltes:', buscar);
        console.log('cargo:', tipo_cargo);
   
        var error = false;


        if (!error) {
            var data = {filtro: opcion,buscar: buscar, tipo_cargo:tipo_cargo};
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: 'buscar-codpostal-modal',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log(data);
                        $('.resultado_postaltes').html(data.datos).show();
                     
                      
                    }else {
                        console.log('paso succ error');
                        $('.error').html(data.error).show();
                       

                    }
                    
                }
            });
        }
    });


    $(".buscar_codpostalapo").click(function(e) {
        console.log('entro por click buscar codpostal apo');
        e.preventDefault();
        var opcion = $('#filtro_postal_apo').val();
        var buscar = $('#buscar_postal_apo').val();
        var tipo_cargo = $('#opcion').val();
        $('.error').text('');

        
        console.log('filtro_postalapo:', opcion);
        console.log('buscar_postalapo:', buscar);
        console.log('cargo:', tipo_cargo);
   
        var error = false;


        if (!error) {
            var data = {filtro: opcion,buscar: buscar, tipo_cargo:tipo_cargo};
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: 'buscar-codpostal-modal',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log(data);
                        $('.resultado_postalapo').html(data.datos).show();
                     
                      
                    }else {
                        console.log('paso succ error');
                        $('.error').html(data.error).show();
                       

                    }
                    
                }
            });
        }
    });

    
    $('.nav-link').click(function(e) {
        $("#error").hide();
        $("#salida").hide();
    });


    $(".guardar-datos-generales").click(function(e) {
        $("#error").hide();
        $("#salida").hide();
        tipo_cuit      = $('#tipo_cuit').val();
        nro_cuit       = $('#nro_cuit').val();
        domicilio_calle   = $('#domicilio_calle').val();
        domicilio_nro      = $('#domicilio_nro').val();
        domicilio_piso           = $('#domicilio_piso').val();
        domicilio_depto      = $('#domicilio_depto').val();
        domicilio_adicional    = $('#domicilio_adicional').val();
        telefono_celular        = $('#telefono_celular').val();
        telefono1        = $('#telefono1').val();
        telefono2  = $('#telefono2').val();
        telefono3       = $('#telefono3').val();
        email  = $('#email').val();
        uif_actividades  = $('#uif_actividades').val();
        cond_iva  = $('#cond_iva').val();
        soc_ejercicio_economico  = $('#soc_ejercicio_economico').val();
        soc_jud_fecha_constitucion  = $('#soc_jud_fecha_constitucion').val();
        soc_jud_inscripcion_inaes  = $('#soc_jud_inscripcion_inaes').val();
        console.log('codigo_postal');
        console.log($('#codigo_postal').val());
        console.log('codigo_postal_pre');
        console.log($('#codigo_postal_pre').val());
        console.log('generales');
        console.log($('#form :input').serialize());
        todos = $('#form :input').serialize();
        event.preventDefault();

        var error = false;
        var err = false;

        /*var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

        if (!regex.test($('#email').val().trim())) {
                   
            //alert('Correo intytyvalidado');
            $("#error").text('Ingrese un mail valido para la Mutual.');
            console.log('paso false valor', 'error de correo');
            $("#error").fadeIn(); //alert alert-danger
            //$("#error").text('Ingrese el mail de la Mutual.');
            $("#mensaje").modal({
                fadeDuration: 100
            });
            return false;
        }*/

        /*$(".validate").each(function(i, elem) {
            console.log('VALIDAR CAMPOS:',elem);
            console.log($('#telefono_celular_pre').val().length);
            if ($('#telefono_celular_pre').val().length <= 0) {
                $(".response").html('<div class="alert alert-danger show">Debes completar todos los campos para continuar.</div>');
                err = true;
                return false;
            }
        });*/

              
                
        if (!error) {
            var data = todos;/*{tipo_cuit: tipo_cuit, nro_cuit:nro_cuit, domicilio_calle:domicilio_calle, domicilio_nro: domicilio_nro, domicilio_piso: domicilio_piso, domicilio_depto: domicilio_depto, domicilio_adicional: domicilio_adicional, 
                telefono1: telefono1, telefono2: telefono2, telefono_celular: telefono_celular, telefono3: telefono3, email: email, uif_actividades: uif_actividades, cond_iva: cond_iva,soc_ejercicio_economico:soc_ejercicio_economico, soc_jud_fecha_constitucion:soc_jud_fecha_constitucion, 
                soc_jud_inscripcion_inaes:soc_jud_inscripcion_inaes};*/
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/guardar-datos-generales',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('paso succ valor', data.salida);
                        //setTimeout(function() {
                        $("#salida").fadeIn();
                        $("#salida").css('class','alert alert-success');
                        $("#salida").text(data.salida);
                        //$("#salida").fadeOut(8000);
                        $("#mensaje").modal({
                            fadeDuration: 100
                        });
                        //},3000);
                        /*$(".mensaje_s").show();
                        $("#salida").text(data.salida);
                        //$(".mensaje_e").hide();
                        $("#mensaje_s").hide(8000);*/

                                                               
                    }else {
                        console.log('paso false valor', data.error);
                        $("#error").fadeIn(); //alert alert-danger
                        $("#error").text(data.error);
                        $("#mensaje").modal({
                            fadeDuration: 100
                        });
                        /*setTimeout(function() {
                            $("#error").fadeIn(); //alert alert-danger
                            $("#error").text(data.error);
                            $("#error").fadeOut(8000);
                        },3000);*/


                        /*setTimeout(function() {
                            $(".mensaje_e").fadeIn(500);
                        },3000);
                        $("#error").text(data.error);
                        $(".mensaje_s").hide();*/
                       
                    }
                    
                }
            });
        }
        
    });

    $(".guardar-datos-sucursales").click(function(e) {
        $("#error").hide();
        $("#salida").hide();
        tipo_cuit      = $('#tipo_cuit').val();
        nro_cuit       = $('#nro_cuit').val();
        domicilio_calle   = $('#domicilio_calle').val();
        domicilio_nro      = $('#domicilio_nro').val();
        domicilio_piso           = $('#domicilio_piso').val();
        domicilio_depto      = $('#domicilio_depto').val();
        domicilio_adicional    = $('#domicilio_adicional').val();
        telefono_celular        = $('#telefono_celular').val();
        telefono1        = $('#telefono1').val();
        telefono2  = $('#telefono2').val();
        telefono3       = $('#telefono3').val();
        email  = $('#email').val();
        uif_actividades  = $('#uif_actividades').val();
        cond_iva  = $('#cond_iva').val();
        soc_ejercicio_economico  = $('#soc_ejercicio_economico').val();
        soc_jud_fecha_constitucion  = $('#soc_jud_fecha_constitucion').val();
        soc_jud_inscripcion_inaes  = $('#soc_jud_inscripcion_inaes').val();
        console.log('codigo_postal');
        console.log($('#codigo_postal').val());
        console.log('codigo_postal_pre');
        console.log($('#codigo_postal_pre').val());
        console.log($('#form :input').serialize());
        todos = $('#form :input').serialize();
        event.preventDefault();

        var error = false;
        var err = false;

        /*$(".validate").each(function(i, elem) {
            console.log('VALIDAR CAMPOS:',elem);
            console.log($('#telefono_celular_pre').val().length);
            if ($('#telefono_celular_pre').val().length <= 0) {
                $(".response").html('<div class="alert alert-danger show">Debes completar todos los campos para continuar.</div>');
                err = true;
                return false;
            }
        });*/

                      
                
        if (!error) {
            var data = todos;/*{tipo_cuit: tipo_cuit, nro_cuit:nro_cuit, domicilio_calle:domicilio_calle, domicilio_nro: domicilio_nro, domicilio_piso: domicilio_piso, domicilio_depto: domicilio_depto, domicilio_adicional: domicilio_adicional, 
                telefono1: telefono1, telefono2: telefono2, telefono_celular: telefono_celular, telefono3: telefono3, email: email, uif_actividades: uif_actividades, cond_iva: cond_iva,soc_ejercicio_economico:soc_ejercicio_economico, soc_jud_fecha_constitucion:soc_jud_fecha_constitucion, 
                soc_jud_inscripcion_inaes:soc_jud_inscripcion_inaes};*/
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/guardar-datos-sucursales',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('paso succ valor', data.salida);
                        //setTimeout(function() {
                        $("#salida").fadeIn();
                        $("#salida").css('class','alert alert-success');
                        $("#salida").text(data.salida);
                        //$("#salida").fadeOut(8000);
                        $("#mensaje").modal({
                            fadeDuration: 100
                        });
                        //},3000);
                        /*$(".mensaje_s").show();
                        $("#salida").text(data.salida);
                        //$(".mensaje_e").hide();
                        $("#mensaje_s").hide(8000);*/

                                                               
                    }else {
                        console.log('paso false valor', data.error);
                        $("#error").fadeIn(); //alert alert-danger
                        $("#error").text(data.error);
                        $("#mensaje").modal({
                            fadeDuration: 100
                        });
                        /*setTimeout(function() {
                            $("#error").fadeIn(); //alert alert-danger
                            $("#error").text(data.error);
                            $("#error").fadeOut(8000);
                        },3000);*/


                        /*setTimeout(function() {
                            $(".mensaje_e").fadeIn(500);
                        },3000);
                        $("#error").text(data.error);
                        $(".mensaje_s").hide();*/
                       
                    }
                    
                }
            });
        }
        
    });

    $(".imprimir_so").click(function(e) {
        persona      = $('#nro_persona').val();
        event.preventDefault();

        var error = false;
        var err = false;

        if (!error) {
            var data = {persona:persona};
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/imprimir-so',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('paso succ valor', data.salida);
                        window.open(downloadUrl,'_blank');
                                                               
                    }else {
                        console.log('paso false valor', data.error);
                       
                       
                    }
                    
                }
            });
        }
        
    });

    $(".guardar-datos-presidente").click(function(e) {
        $("#error").hide();
        $("#salida").hide();
        tipo_cuit      = $('#tipo_cuit').val();
        nro_cuit       = $('#nro_cuit').val();
        domicilio_calle   = $('#domicilio_calle').val();
        domicilio_nro      = $('#domicilio_nro').val();
        domicilio_piso           = $('#domicilio_piso').val();
        domicilio_depto      = $('#domicilio_depto').val();
        domicilio_adicional    = $('#domicilio_adicional').val();
        telefono_celular        = $('#telefono_celular').val();
        telefono1        = $('#telefono1').val();
        telefono2  = $('#telefono2').val();
        telefono3       = $('#telefono3').val();
        email  = $('#email').val();
        uif_actividades  = $('#uif_actividades').val();
        cond_iva  = $('#cond_iva').val();
        soc_ejercicio_economico  = $('#soc_ejercicio_economico').val();
        soc_jud_fecha_constitucion  = $('#soc_jud_fecha_constitucion').val();
        soc_jud_inscripcion_inaes  = $('#soc_jud_inscripcion_inaes').val();
        console.log('codigo_postal');
        console.log($('#codigo_postal').val());
        console.log('codigo_postal_pre');
        console.log($('#codigo_postal_pre').val());
        console.log('todos presi');
        //console.log($('form').serialize());
        console.log($('#form :input').serialize());
        todos = $('#form :input').serialize();
        event.preventDefault();

        var error = false;
        var err = false;

        /*$(".validate").each(function(i, elem) {
            console.log('VALIDAR CAMPOS:',elem);
            console.log($('#telefono_celular_pre').val().length);
            if ($('#telefono_celular_pre').val().length <= 0) {
                $(".response").html('<div class="alert alert-danger show">Debes completar todos los campos para continuar.</div>');
                err = true;
                return false;
            }
        });*/

        /*var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

        if (!regex.test($('#email_pre').val().trim())) {
                   
            //alert('Correo intytyvalidado');
            $("#error").text('Correo Invalido.');
            console.log('paso false valor', 'error de correo');
            $("#error").fadeIn(); //alert alert-danger
            $("#error").text('Correo Invalido.');
            $("#mensaje").modal({
                fadeDuration: 100
            });
            return false;
        }*/

              
                
        if (!error) {
            var data = todos;/*{tipo_cuit: tipo_cuit, nro_cuit:nro_cuit, domicilio_calle:domicilio_calle, domicilio_nro: domicilio_nro, domicilio_piso: domicilio_piso, domicilio_depto: domicilio_depto, domicilio_adicional: domicilio_adicional, 
                telefono1: telefono1, telefono2: telefono2, telefono_celular: telefono_celular, telefono3: telefono3, email: email, uif_actividades: uif_actividades, cond_iva: cond_iva,soc_ejercicio_economico:soc_ejercicio_economico, soc_jud_fecha_constitucion:soc_jud_fecha_constitucion, 
                soc_jud_inscripcion_inaes:soc_jud_inscripcion_inaes};*/
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/guardar-datos-presidente',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('paso succ valor', data.salida);
                        //setTimeout(function() {
                        $("#salida").fadeIn();
                        $("#salida").css('class','alert alert-success');
                        $("#salida").text(data.salida);
                        //$("#salida").fadeOut(8000);
                        $("#mensaje").modal({
                            fadeDuration: 100
                        });
                        //},3000);
                        /*$(".mensaje_s").show();
                        $("#salida").text(data.salida);
                        //$(".mensaje_e").hide();
                        $("#mensaje_s").hide(8000);*/

                                                               
                    }else {
                        console.log('paso false valor', data.error);
                        $("#error").fadeIn(); //alert alert-danger
                        $("#error").text(data.error);
                        $("#mensaje").modal({
                            fadeDuration: 100
                        });
                        /*setTimeout(function() {
                            $("#error").fadeIn(); //alert alert-danger
                            $("#error").text(data.error);
                            $("#error").fadeOut(8000);
                        },3000);*/


                        /*setTimeout(function() {
                            $(".mensaje_e").fadeIn(500);
                        },3000);
                        $("#error").text(data.error);
                        $(".mensaje_s").hide();*/
                       
                    }
                    
                }
            });
        }
        
    });

    $(".guardar-datos-secretario").click(function(e) {
        $("#error").hide();
        $("#salida").hide();
        tipo_cuit      = $('#tipo_cuit').val();
        nro_cuit       = $('#nro_cuit').val();
        domicilio_calle   = $('#domicilio_calle').val();
        domicilio_nro      = $('#domicilio_nro').val();
        domicilio_piso           = $('#domicilio_piso').val();
        domicilio_depto      = $('#domicilio_depto').val();
        domicilio_adicional    = $('#domicilio_adicional').val();
        telefono_celular        = $('#telefono_celular').val();
        telefono1        = $('#telefono1').val();
        telefono2  = $('#telefono2').val();
        telefono3       = $('#telefono3').val();
        email  = $('#email').val();
        uif_actividades  = $('#uif_actividades').val();
        cond_iva  = $('#cond_iva').val();
        soc_ejercicio_economico  = $('#soc_ejercicio_economico').val();
        soc_jud_fecha_constitucion  = $('#soc_jud_fecha_constitucion').val();
        soc_jud_inscripcion_inaes  = $('#soc_jud_inscripcion_inaes').val();
        console.log('codigo_postal');
        console.log($('#codigo_postal').val());
        console.log('codigo_postal_pre');
        console.log($('#codigo_postal_pre').val());
        console.log('todos secre');
        //console.log($('form').serialize());
        console.log($('#form :input').serialize());
        todos = $('#form :input').serialize();

        var error = false;
        var err = false;

        /*$(".validate").each(function(i, elem) {
            console.log('VALIDAR CAMPOS:',elem);
            console.log($('#telefono_celular_pre').val().length);
            if ($('#telefono_celular_pre').val().length <= 0) {
                $(".response").html('<div class="alert alert-danger show">Debes completar todos los campos para continuar.</div>');
                err = true;
                return false;
            }
        });*/

        /*var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

        if (!regex.test($('#email_sec').val().trim())) {
                   
            //alert('Correo intytyvalidado');
            $("#error").text('Correo Invalido.');
            console.log('paso false valor', 'error de correo');
            $("#error").fadeIn(); //alert alert-danger
            $("#error").text('Correo Invalido.');
            $("#mensaje").modal({
                fadeDuration: 100
            });
            return false;
        }*/

              
                
        if (!error) {
            var data = todos;/*{tipo_cuit: tipo_cuit, nro_cuit:nro_cuit, domicilio_calle:domicilio_calle, domicilio_nro: domicilio_nro, domicilio_piso: domicilio_piso, domicilio_depto: domicilio_depto, domicilio_adicional: domicilio_adicional, 
                telefono1: telefono1, telefono2: telefono2, telefono_celular: telefono_celular, telefono3: telefono3, email: email, uif_actividades: uif_actividades, cond_iva: cond_iva,soc_ejercicio_economico:soc_ejercicio_economico, soc_jud_fecha_constitucion:soc_jud_fecha_constitucion, 
                soc_jud_inscripcion_inaes:soc_jud_inscripcion_inaes};*/
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/guardar-datos-secretario',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('paso succ valor', data.salida);
                        //setTimeout(function() {
                        $("#salida").fadeIn();
                        $("#salida").css('class','alert alert-success');
                        $("#salida").text(data.salida);
                        //$("#salida").fadeOut(8000);
                        $("#mensaje").modal({
                            fadeDuration: 100
                        });
                        //},3000);
                        /*$(".mensaje_s").show();
                        $("#salida").text(data.salida);
                        //$(".mensaje_e").hide();
                        $("#mensaje_s").hide(8000);*/

                                                               
                    }else {
                        console.log('paso false valor', data.error);
                        $("#error").fadeIn(); //alert alert-danger
                        $("#error").text(data.error);
                        $("#mensaje").modal({
                            fadeDuration: 100
                        });
                        /*setTimeout(function() {
                            $("#error").fadeIn(); //alert alert-danger
                            $("#error").text(data.error);
                            $("#error").fadeOut(8000);
                        },3000);*/


                        /*setTimeout(function() {
                            $(".mensaje_e").fadeIn(500);
                        },3000);
                        $("#error").text(data.error);
                        $(".mensaje_s").hide();*/
                       
                    }
                    
                }
            });
        }
        
    });

    $(".guardar-datos-tesorero").click(function(e) {
        $("#error").hide();
        $("#salida").hide();
        tipo_cuit      = $('#tipo_cuit').val();
        nro_cuit       = $('#nro_cuit').val();
        domicilio_calle   = $('#domicilio_calle').val();
        domicilio_nro      = $('#domicilio_nro').val();
        domicilio_piso           = $('#domicilio_piso').val();
        domicilio_depto      = $('#domicilio_depto').val();
        domicilio_adicional    = $('#domicilio_adicional').val();
        telefono_celular        = $('#telefono_celular').val();
        telefono1        = $('#telefono1').val();
        telefono2  = $('#telefono2').val();
        telefono3       = $('#telefono3').val();
        email  = $('#email').val();
        uif_actividades  = $('#uif_actividades').val();
        cond_iva  = $('#cond_iva').val();
        soc_ejercicio_economico  = $('#soc_ejercicio_economico').val();
        soc_jud_fecha_constitucion  = $('#soc_jud_fecha_constitucion').val();
        soc_jud_inscripcion_inaes  = $('#soc_jud_inscripcion_inaes').val();
        console.log('codigo_postal');
        console.log($('#codigo_postal').val());
        console.log('codigo_postal_pre');
        console.log($('#codigo_postal_pre').val());
        console.log('todos teso');
        //console.log($('form').serialize());
        console.log($('#form :input').serialize());
        todos = $('#form :input').serialize();
        event.preventDefault();

        var error = false;
        var err = false;

        /*$(".validate").each(function(i, elem) {
            console.log('VALIDAR CAMPOS:',elem);
            console.log($('#telefono_celular_pre').val().length);
            if ($('#telefono_celular_pre').val().length <= 0) {
                $(".response").html('<div class="alert alert-danger show">Debes completar todos los campos para continuar.</div>');
                err = true;
                return false;
            }
        });*/

        /*var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

        if (!regex.test($('#email_tes').val().trim())) {
                   
            //alert('Correo intytyvalidado');
            $("#error").text('Correo Invalido.');
            console.log('paso false valor', 'error de correo');
            $("#error").fadeIn(); //alert alert-danger
            $("#error").text('Correo Invalido.');
            $("#mensaje").modal({
                fadeDuration: 100
            });
            return false;
        }*/

              
                
        if (!error) {
            var data = todos;/*{tipo_cuit: tipo_cuit, nro_cuit:nro_cuit, domicilio_calle:domicilio_calle, domicilio_nro: domicilio_nro, domicilio_piso: domicilio_piso, domicilio_depto: domicilio_depto, domicilio_adicional: domicilio_adicional, 
                telefono1: telefono1, telefono2: telefono2, telefono_celular: telefono_celular, telefono3: telefono3, email: email, uif_actividades: uif_actividades, cond_iva: cond_iva,soc_ejercicio_economico:soc_ejercicio_economico, soc_jud_fecha_constitucion:soc_jud_fecha_constitucion, 
                soc_jud_inscripcion_inaes:soc_jud_inscripcion_inaes};*/
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/guardar-datos-tesorero',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('paso succ valor', data.salida);
                        //setTimeout(function() {
                        $("#salida").fadeIn();
                        $("#salida").css('class','alert alert-success');
                        $("#salida").text(data.salida);
                        //$("#salida").fadeOut(8000);
                        $("#mensaje").modal({
                            fadeDuration: 100
                        });
                        //},3000);
                        /*$(".mensaje_s").show();
                        $("#salida").text(data.salida);
                        //$(".mensaje_e").hide();
                        $("#mensaje_s").hide(8000);*/

                                                               
                    }else {
                        console.log('paso false valor', data.error);
                        $("#error").fadeIn(); //alert alert-danger
                        $("#error").text(data.error);
                        $("#mensaje").modal({
                            fadeDuration: 100
                        });
                        /*setTimeout(function() {
                            $("#error").fadeIn(); //alert alert-danger
                            $("#error").text(data.error);
                            $("#error").fadeOut(8000);
                        },3000);*/


                        /*setTimeout(function() {
                            $(".mensaje_e").fadeIn(500);
                        },3000);
                        $("#error").text(data.error);
                        $(".mensaje_s").hide();*/
                       
                    }
                    
                }
            });
        }
        
    });


    $(".guardar-datos-apoderado").click(function(e) {
        $("#error").hide();
        $("#salida").hide();
        /*tipo_cuit      = $('#tipo_cuit').val();
        nro_cuit       = $('#nro_cuit').val();
        domicilio_calle   = $('#domicilio_calle').val();
        domicilio_nro      = $('#domicilio_nro').val();
        domicilio_piso           = $('#domicilio_piso').val();
        domicilio_depto      = $('#domicilio_depto').val();
        domicilio_adicional    = $('#domicilio_adicional').val();
        telefono_celular        = $('#telefono_celular').val();
        telefono1        = $('#telefono1').val();
        telefono2  = $('#telefono2').val();
        telefono3       = $('#telefono3').val();
        email  = $('#email').val();
        uif_actividades  = $('#uif_actividades').val();
        cond_iva  = $('#cond_iva').val();
        soc_ejercicio_economico  = $('#soc_ejercicio_economico').val();
        soc_jud_fecha_constitucion  = $('#soc_jud_fecha_constitucion').val();
        soc_jud_inscripcion_inaes  = $('#soc_jud_inscripcion_inaes').val();
        console.log('codigo_postal');
        console.log($('#codigo_postal').val());
        console.log('codigo_postal_pre');
        console.log($('#codigo_postal_pre').val());
        console.log('todos');
        console.log($('form').serialize());*/
        console.log('todos apo');
        //console.log($('form').serialize());
        console.log($('#form :input').serialize());
        todos = $('#form :input').serialize();
        event.preventDefault();

        var error = false;
        var err = false;

        /*$(".validate").each(function(i, elem) {
            console.log('VALIDAR CAMPOS:',elem);
            console.log($('#telefono_celular_pre').val().length);
            if ($('#telefono_celular_pre').val().length <= 0) {
                $(".response").html('<div class="alert alert-danger show">Debes completar todos los campos para continuar.</div>');
                err = true;
                return false;
            }
        });*/

        /*var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

        if (!regex.test($('#email_apo').val().trim())) {
                   
            //alert('Correo intytyvalidado');
            $("#error").text('Correo Invalido.');
            console.log('paso false valor', 'error de correo');
            $("#error").fadeIn(); //alert alert-danger
            $("#error").text('Correo Invalido.');
            $("#mensaje").modal({
                fadeDuration: 100
            });
            return false;
        }*/

              
                
        if (!error) {
            var data = todos;/*{tipo_cuit: tipo_cuit, nro_cuit:nro_cuit, domicilio_calle:domicilio_calle, domicilio_nro: domicilio_nro, domicilio_piso: domicilio_piso, domicilio_depto: domicilio_depto, domicilio_adicional: domicilio_adicional, 
                telefono1: telefono1, telefono2: telefono2, telefono_celular: telefono_celular, telefono3: telefono3, email: email, uif_actividades: uif_actividades, cond_iva: cond_iva,soc_ejercicio_economico:soc_ejercicio_economico, soc_jud_fecha_constitucion:soc_jud_fecha_constitucion, 
                soc_jud_inscripcion_inaes:soc_jud_inscripcion_inaes};*/
            //console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/guardar-datos-apoderado',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('paso succ valor', data.salida);
                        //setTimeout(function() {
                        $("#salida").fadeIn();
                        $("#salida").css('class','alert alert-success');
                        $("#salida").text(data.salida);
                        //$("#salida").fadeOut(8000);
                        $("#mensaje").modal({
                            fadeDuration: 100
                        });
                        //},3000);
                        /*$(".mensaje_s").show();
                        $("#salida").text(data.salida);
                        //$(".mensaje_e").hide();
                        $("#mensaje_s").hide(8000);*/

                                                               
                    }else {
                        console.log('paso false valor', data.error);
                        $("#error").fadeIn(); //alert alert-danger
                        $("#error").text(data.error);
                        $("#mensaje").modal({
                            fadeDuration: 100
                        });
                        /*setTimeout(function() {
                            $("#error").fadeIn(); //alert alert-danger
                            $("#error").text(data.error);
                            $("#error").fadeOut(8000);
                        },3000);*/


                        /*setTimeout(function() {
                            $(".mensaje_e").fadeIn(500);
                        },3000);
                        $("#error").text(data.error);
                        $(".mensaje_s").hide();*/
                       
                    }
                    
                }
            });
        }
        
    });

    $("#rubro_pre").click(function(e) {
        console.log('paso #rubro_pre');
        console.log($("#rubro_pre").val());
     
        if($("#rubro_pre").val() > 0){
            $("#clase_rubro_pre").hide();
            $("#nuevo_rubro_pre").val('');

        }else if($("#rubro_pre").val() == ''){
            $("#clase_rubro_pre").hide();
            $("#nuevo_rubro_pre").val('');

        }else{
            $("#clase_rubro_pre").show();

        }
    });

    $("#rubro_sec").click(function(e) {
        console.log('paso #rubro_sec');
        console.log($("#rubro_sec").val());
        if($("#rubro_sec").val() > 0){
            $("#clase_rubro_sec").hide();
            $("#nuevo_rubro_sec").val('');

        }else if($("#rubro_sec").val() == ''){
            $("#clase_rubro_sec").hide();
            $("#nuevo_rubro_sec").val('');

        }else{
            $("#clase_rubro_sec").show();

        }
    });

    $("#rubro_tes").click(function(e) {
        console.log('paso #rubro_tes');
        console.log($("#rubro_tes").val());
        if($("#rubro_tes").val() > 0){
            $("#clase_rubro_tes").hide();
            $("#nuevo_rubro_tes").val('');

        }else if($("#rubro_tes").val() == ''){
            $("#clase_rubro_tes").hide();
            $("#nuevo_rubro_tes").val('');

        }else{
            $("#clase_rubro_tes").show();

        }
    });

    $("#rubro_apo").click(function(e) {
        console.log('paso #rubro_apo');
        console.log($("#rubro_apo").val());
        if($("#rubro_apo").val() > 0){
            $("#clase_rubro_apo").hide();
            $("#nuevo_rubro_apo").val('');

        }else if($("#rubro_apo").val() == ''){
            $("#clase_rubro_apo").hide();
            $("#nuevo_rubro_apo").val('');

        }else{
            $("#clase_rubro_apo").show();

        }
    });

    $('#archivo').click(function(e) {
        console.log('limpiar');
        $(".salida1").text('');
        $(".salida_error").text('');
        $("#salida_error1").text('');
    });

    $('#archivo').change(function(e){
        $('.guardar-datos-adjuntos').prop("disabled", this.files.length == 0);
    });
    
    $(".guardar-datos-adjuntos").click(function(e) {
        console.log('entro a insert - up archivo');
        console.log($('#cod').val());
        var formData = new FormData();
        var files = $('#archivo')[0].files[0];
        var cod = $('#cod').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    if(cod == 'FA00'){
                        data.salida = 'Se adjunto correctamente el documento de la Declaración Jurada';
                    }
                    
                    $(".salida1").text(data.salida);
                    $('#fa01').text(data.documento);
                    setTimeout(function() {
                        $(".salida1").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error1").text(data.error);
                   
                }
            }
        });
        return false;
    });

    $('#archivo2').click(function(e) {
        console.log('limpiar');
        $(".salida2").text('');
        $(".salida_error").text('');
        $("#salida_error2").text('');
    });

    $('#archivo2').change(function(e){
        $('.guardar-datos-adjuntos2').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos2").click(function(e) {
        console.log('entro a insert - up archivo 2');
        console.log($('#cod2').val());
        var formData = new FormData();
        var files = $('#archivo2')[0].files[0];
        var cod = $('#cod2').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida2").text(data.salida);
                    $('#fa02').text(data.documento);
                    setTimeout(function() {
                        $(".salida2").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error2").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo3').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $(".salida_error").text('');
        $("#salida_error3").text('');
    });

    $('#archivo3').change(function(e){
        $('.guardar-datos-adjuntos3').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos3").click(function(e) {
        console.log('entro a insert - up archivo 3');
        console.log($('#cod3').val());
        var formData = new FormData();
        var files = $('#archivo3')[0].files[0];
        var cod = $('#cod3').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida3").text(data.salida);
                    $('#fa03').text(data.documento);
                    setTimeout(function() {
                        $(".salida3").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error3").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo4').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $(".salida_error").text('');
        $("#salida_error4").text('');
    });

    $('#archivo4').change(function(e){
        $('.guardar-datos-adjuntos4').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos4").click(function(e) {
        console.log('entro a insert - up archivo 4');
        console.log($('#cod4').val());
        var formData = new FormData();
        var files = $('#archivo4')[0].files[0];
        var cod = $('#cod4').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida4").text(data.salida);
                    $('#fa04').text(data.documento);
                    setTimeout(function() {
                        $(".salida4").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error4").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo5').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error5").text('');
    });

    $('#archivo5').change(function(e){
        $('.guardar-datos-adjuntos5').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos5").click(function(e) {
        console.log('entro a insert - up archivo 5');
        console.log($('#cod5').val());
        var formData = new FormData();
        var files = $('#archivo5')[0].files[0];
        var cod = $('#cod5').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida5").text(data.salida);
                    $('#fa05').text(data.documento);
                    setTimeout(function() {
                        $(".salida5").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error5").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo6').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error6").text('');
    });

    $('#archivo6').change(function(e){
        $('.guardar-datos-adjuntos6').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos6").click(function(e) {
        console.log('entro a insert - up archivo 6');
        console.log($('#cod6').val());
        var formData = new FormData();
        var files = $('#archivo6')[0].files[0];
        var cod = $('#cod6').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida6").text(data.salida);
                    $('#fa06').text(data.documento);
                    setTimeout(function() {
                        $(".salida6").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error6").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo7').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error7").text('');
    });

    $('#archivo7').change(function(e){
        $('.guardar-datos-adjuntos7').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos7").click(function(e) {
        console.log('entro a insert - up archivo 7');
        console.log($('#cod7').val());
        var formData = new FormData();
        var files = $('#archivo7')[0].files[0];
        var cod = $('#cod7').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida7").text(data.salida);
                    $('#fa07').text(data.documento);
                    setTimeout(function() {
                        $(".salida7").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error7").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo8').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error8").text('');
    });

    $('#archivo8').change(function(e){
        $('.guardar-datos-adjuntos8').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos8").click(function(e) {
        console.log('entro a insert - up archivo 8');
        console.log($('#cod8').val());
        var formData = new FormData();
        var files = $('#archivo8')[0].files[0];
        var cod = $('#cod8').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida8").text(data.salida);
                    $('#fa08').text(data.documento);
                    setTimeout(function() {
                        $(".salida8").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error8").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo9').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error9").text('');
    });

    $('#archivo9').change(function(e){
        $('.guardar-datos-adjuntos9').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos9").click(function(e) {
        console.log('entro a insert - up archivo 9');
        console.log($('#cod9').val());
        var formData = new FormData();
        var files = $('#archivo9')[0].files[0];
        var cod = $('#cod9').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida9").text(data.salida);
                    $('#fa09').text(data.documento);
                    setTimeout(function() {
                        $(".salida9").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error9").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo9-a').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error9-a").text('');
    });

    $('#archivo9-a').change(function(e){
        $('.guardar-datos-adjuntos9-a').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos9-a").click(function(e) {
        console.log('entro a insert - up archivo 9-a');
        console.log($('#cod9-a').val());
        var formData = new FormData();
        var files = $('#archivo9-a')[0].files[0];
        var cod = $('#cod9-a').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida9-a").text(data.salida);
                    $('#fa09-a').text(data.documento);
                    setTimeout(function() {
                        $(".salida9-a").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error9-a").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo9-b').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error9-b").text('');
    });

    $('#archivo9-b').change(function(e){
        $('.guardar-datos-adjuntos9-b').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos9-b").click(function(e) {
        console.log('entro a insert - up archivo 9-b');
        console.log($('#cod9-b').val());
        var formData = new FormData();
        var files = $('#archivo9-b')[0].files[0];
        var cod = $('#cod9-b').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida9-b").text(data.salida);
                    $('#fa09-b').text(data.documento);
                    setTimeout(function() {
                        $(".salida9-b").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error9-b").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo9-c').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error9-c").text('');
    });

    $('#archivo9-c').change(function(e){
        $('.guardar-datos-adjuntos9-c').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos9-c").click(function(e) {
        console.log('entro a insert - up archivo 9-c');
        console.log($('#cod9-c').val());
        var formData = new FormData();
        var files = $('#archivo9-c')[0].files[0];
        var cod = $('#cod9-c').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida9-c").text(data.salida);
                    $('#fa09-c').text(data.documento);
                    setTimeout(function() {
                        $(".salida9-c").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error9-c").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo9-d').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error9-d").text('');
    });

    $('#archivo9-d').change(function(e){
        $('.guardar-datos-adjuntos9-d').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos9-d").click(function(e) {
        console.log('entro a insert - up archivo 9-d');
        console.log($('#cod9-d').val());
        var formData = new FormData();
        var files = $('#archivo9-d')[0].files[0];
        var cod = $('#cod9-d').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida9-d").text(data.salida);
                    $('#fa09-d').text(data.documento);
                    setTimeout(function() {
                        $(".salida9-d").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error9-d").text(data.error);
                }
            }
        });
        return false;
    });


    $('#archivo10').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error10").text('');
    });

    $('#archivo10').change(function(e){
        $('.guardar-datos-adjuntos10').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos10").click(function(e) {
        console.log('entro a insert - up archivo 10');
        console.log($('#cod10').val());
        var formData = new FormData();
        var files = $('#archivo10')[0].files[0];
        var cod = $('#cod10').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida10").text(data.salida);
                    $('#fa10').text(data.documento);
                    setTimeout(function() {
                        $(".salida10").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error10").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo10-a').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error10-a").text('');
    });

    $('#archivo10-a').change(function(e){
        $('.guardar-datos-adjuntos10-a').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos10-a").click(function(e) {
        console.log('entro a insert - up archivo 10-a');
        console.log($('#cod10-a').val());
        var formData = new FormData();
        var files = $('#archivo10-a')[0].files[0];
        var cod = $('#cod10-a').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida10-a").text(data.salida);
                    $('#fa10-a').text(data.documento);
                    setTimeout(function() {
                        $(".salida10-a").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error10-a").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo10-b').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error10-b").text('');
    });

    $('#archivo10-b').change(function(e){
        $('.guardar-datos-adjuntos10-b').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos10-b").click(function(e) {
        console.log('entro a insert - up archivo 10-b');
        console.log($('#cod10-b').val());
        var formData = new FormData();
        var files = $('#archivo10-b')[0].files[0];
        var cod = $('#cod10-b').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida10-b").text(data.salida);
                    $('#fa10-b').text(data.documento);
                    setTimeout(function() {
                        $(".salida10-b").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error10-b").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo10-c').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error10-c").text('');
    });

    $('#archivo10-c').change(function(e){
        $('.guardar-datos-adjuntos10-c').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos10-c").click(function(e) {
        console.log('entro a insert - up archivo 10-c');
        console.log($('#cod10-c').val());
        var formData = new FormData();
        var files = $('#archivo10-c')[0].files[0];
        var cod = $('#cod10-c').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida10-c").text(data.salida);
                    $('#fa10-c').text(data.documento);
                    setTimeout(function() {
                        $(".salida10-c").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error10-c").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo10-d').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error10-d").text('');
    });

    $('#archivo10-d').change(function(e){
        $('.guardar-datos-adjuntos10-d').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos10-d").click(function(e) {
        console.log('entro a insert - up archivo 10-d');
        console.log($('#cod10-d').val());
        var formData = new FormData();
        var files = $('#archivo10-d')[0].files[0];
        var cod = $('#cod10-d').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida10-d").text(data.salida);
                    $('#fa10-d').text(data.documento);
                    setTimeout(function() {
                        $(".salida10-d").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error10-d").text(data.error);
                }
            }
        });
        return false;
    });


    $('#archivo11').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error11").text('');
    });

    $('#archivo11').change(function(e){
        $('.guardar-datos-adjuntos11').prop("disabled", this.files.length == 0);
    });

    $(".guardar-datos-adjuntos11").click(function(e) {
        console.log('entro a insert - up archivo 11');
        console.log($('#cod11').val());
        var formData = new FormData();
        var files = $('#archivo11')[0].files[0];
        var cod = $('#cod11').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida11").text(data.salida);
                    $('#fa11').text(data.documento);
                    setTimeout(function() {
                        $(".salida11").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error11").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo12').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error12").text('');
    });

    $('#archivo12').change(function(e){
        $('.guardar-datos-adjuntos12').prop("disabled", this.files.length == 0);
    });
    
    $(".guardar-datos-adjuntos12").click(function(e) {
        console.log('entro a insert - up archivo 12');
        console.log($('#cod12').val());
        var formData = new FormData();
        var files = $('#archivo12')[0].files[0];
        var cod = $('#cod12').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida12").text(data.salida);
                    $('#fa12').text(data.documento);
                    setTimeout(function() {
                        $(".salida12").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error12").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo13').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error13").text('');
    });

    $('#archivo13').change(function(e){
        $('.guardar-datos-adjuntos13').prop("disabled", this.files.length == 0);
    });


    $(".guardar-datos-adjuntos13").click(function(e) {
        console.log('entro a insert - up archivo 13');
        console.log($('#cod13').val());
        var formData = new FormData();
        var files = $('#archivo13')[0].files[0];
        var cod = $('#cod13').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida13").text(data.salida);
                    $('#fa13').text(data.documento);
                    setTimeout(function() {
                        $(".salida13").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error13").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo13-a').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error13-a").text('');
    });

    $('#archivo13-a').change(function(e){
        $('.guardar-datos-adjuntos13-a').prop("disabled", this.files.length == 0);
    });


    $(".guardar-datos-adjuntos13-a").click(function(e) {
        console.log('entro a insert - up archivo 13-a');
        console.log($('#cod13-a').val());
        var formData = new FormData();
        var files = $('#archivo13-a')[0].files[0];
        var cod = $('#cod13-a').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida13-a").text(data.salida);
                    $('#fa13-a').text(data.documento);
                    setTimeout(function() {
                        $(".salida13-a").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error13-a").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo13-b').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error13-b").text('');
    });

    $('#archivo13-b').change(function(e){
        $('.guardar-datos-adjuntos13-b').prop("disabled", this.files.length == 0);
    });


    $(".guardar-datos-adjuntos13-b").click(function(e) {
        console.log('entro a insert - up archivo 13-b');
        console.log($('#cod13-b').val());
        var formData = new FormData();
        var files = $('#archivo13-b')[0].files[0];
        var cod = $('#cod13-b').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida13-b").text(data.salida);
                    $('#fa13-b').text(data.documento);
                    setTimeout(function() {
                        $(".salida13-b").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error13-b").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo14').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error14").text('');
    });

    $('#archivo14').change(function(e){
        $('.guardar-datos-adjuntos14').prop("disabled", this.files.length == 0);
    });


    $(".guardar-datos-adjuntos14").click(function(e) {
        console.log('entro a insert - up archivo 14');
        var cod = $('#cod14').val();
        console.log(cod);
        var formData = new FormData();
        var files = $('#archivo14')[0].files[0];
        console.log(files);
        
        var cod = $('#cod14').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida14").text(data.salida);
                    $('#fa14').text(data.documento);
                    setTimeout(function() {
                        $(".salida14").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error14").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo14-a').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error14-a").text('');
    });

    $('#archivo14-a').change(function(e){
        $('.guardar-datos-adjuntos14-a').prop("disabled", this.files.length == 0);
    });


    $(".guardar-datos-adjuntos14-a").click(function(e) {
        console.log('entro a insert - up archivo 14-a');
        var cod = $('#cod14-a').val();
        console.log(cod);
        var formData = new FormData();
        var files = $('#archivo14-a')[0].files[0];
        console.log(files);
        
        var cod = $('#cod14-a').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida14-a").text(data.salida);
                    $('#fa14-a').text(data.documento);
                    setTimeout(function() {
                        $(".salida14-a").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error14-a").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo14-b').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error14-b").text('');
    });

    $('#archivo14-b').change(function(e){
        $('.guardar-datos-adjuntos14-b').prop("disabled", this.files.length == 0);
    });


    $(".guardar-datos-adjuntos14-b").click(function(e) {
        console.log('entro a insert - up archivo 14-b');
        var cod = $('#cod14-b').val();
        console.log(cod);
        var formData = new FormData();
        var files = $('#archivo14-b')[0].files[0];
        console.log(files);
        
        var cod = $('#cod14-b').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida14-b").text(data.salida);
                    $('#fa14-b').text(data.documento);
                    setTimeout(function() {
                        $(".salida14-b").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error14-b").text(data.error);
                }
            }
        });
        return false;
    });

    $('#archivo15').click(function(e) {
        console.log('limpiar');
        $(".salida").text('');
        $("#salida_error15").text('');
    });

    $('#archivo15').change(function(e){
        $('.guardar-datos-adjuntos15').prop("disabled", this.files.length == 0);
    });


    $(".guardar-datos-adjuntos15").click(function(e) {
        console.log('entro a insert - up archivo 15');
        var cod = $('#cod15').val();
        console.log(cod);
        var formData = new FormData();
        var files = $('#archivo15')[0].files[0];
        console.log(files);
        
        var cod = $('#cod15').val();
        formData.append('archivo',files);
        formData.append('cod',cod);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/guardar-documento',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.documento);
                    $(".salida15").text(data.salida);
                    $('#fa15').text(data.documento);
                    setTimeout(function() {
                        $(".salida15").fadeOut(1500);
                    },5000);
                } else {
                    console.log('paso false valor', data.error);
                    $("#salida_error15").text(data.error);
                }
            }
        });
        return false;
    });

    $(".confirmar-datos").click(function(e) {
        console.log('entro a confirmar datos');
        var nro_persona = $('#nro_persona').val();
        console.log(nro_persona);
        data = {nro_persona : nro_persona};
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: '/admin/confirmar-datos',
            type: 'post',
            data: data,
            success: function(data) {
                if (data.success) {
                    console.log('paso succ valor', data.success);
                    console.log('paso succ valor', data.estado);
                    $("#salida").text(data.salida);
                    $(".confirmar-datos").attr('disabled',true);
                                                          
                    $("#estado-act").text(data.estado);
                    /*setTimeout(function() {
                        $(".nro_persona").fadeOut(1500);
                    },5000);*/
                } else {
                    console.log('paso false valor', data.success);
                    $("#error").text(data.salida);
                    $(".confirmar-datos").attr('disabled',false);
                    
                }
            }
        });
        return false;
    });

    $("#checkbox-ley").click(function() {  
        if($("#checkbox-ley").is(':checked')) { 
            $(".confirmar-datos").attr('disabled',false);  //background-color:#268fa9
            $(".confirmar-datos").css({'background-color':'#268fa9'});
            //alert("Está activado");  
        } else {  
            $(".confirmar-datos").attr('disabled',true);
            $(".confirmar-datos").css({'background-color':'#97a2a5'});
            //
            //alert("No está activado");  
        }  
    }); 

    $('#soc_numero_socio').blur(function() {
        soc_numero_socio = $('#soc_numero_socio').val();
        
        event.preventDefault();

        var error = false;
        var err = false;

        if (!error) {
            var data = {soc_numero_socio:soc_numero_socio};
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/buscar-mutual',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('paso succ valor', data.salida);
                        $("#estado_t").css('display','initial');
                        $("#estado_mutual").text(data.salida.estado_actual);
                        //setTimeout(function() {
                        if(data.salida.confirmada > 0){
                            $('#soc_numero_socio').val(data.salida.soc_numero_socio).attr('disabled',false);
                            $("#nombre").val(data.salida.nombre).attr('disabled',true);
                            $("#codigo_postal").val(data.salida.codigo_postal).attr('disabled',true);
                            $("#localidad_nombre_localidad").val(data.salida.localidad).attr('disabled',true);
                            $("#provincia_nombre_provincia").val(data.salida.provincia).attr('disabled',true);
                            $("#bt-buscar").attr('disabled',true);
                            $("#bt-buscarsuc").attr('disabled',true);
                            $(".update-datos-mutual").css('display','none');
                            $(".guardar-datos-mutual").css('display','none');
                            $(".delete-datos-mutual").css('display','none');
                            $(".agregar-sucursal").css('display','none');
                            $(".datos-suc").css('display','initial');
                            $(".datos-suc").html(data.datos_html);

                        }else{
                            $('#soc_numero_socio').val(data.salida.soc_numero_socio).attr('disabled',false);
                            $('#nombre').val(data.salida.nombre).attr('disabled',false);
                            $('#codigo_postal').val(data.salida.codigo_postal).attr('disabled',false);
                            $('#localidad_nombre_localidad').val(data.salida.localidad).attr('disabled',true);
                            $('#provincia_nombre_provincia').val(data.salida.provincia).attr('disabled',true);
                            $("#bt-buscar").attr('disabled',false);
                            $("#bt-buscarsuc").attr('disabled',false);
                            $(".update-datos-mutual").css('display','initial');
                            $(".agregar-sucursal").css('display','initial');
                            $(".guardar-datos-mutual").css('display','none');
                            $(".delete-datos-mutual").css('display','initial');
                            $(".datos-suc").css('display','initial');
                            $(".datos-suc").html(data.datos_html);
                        }
                        //$("#salida").fadeIn();
                        //$("#salida").css('class','alert alert-success');
                        //$("#salida").text(data.salida);
                        //$("#salida").fadeOut(8000);
                        //$("#mensaje_mut").modal({
                            //fadeDuration: 100
                        //});//
                        //},3000);
                        /*$(".mensaje_s").show();
                        $("#salida").text(data.salida);
                        //$(".mensaje_e").hide();
                        $("#mensaje_s").hide(8000);*/

                                                               
                    }else {
                        console.log('paso false valor', data.error);
                        $("#estado_t").css('display','none');
                        $("#estado_mutual").text('');
                        //$('#soc_numero_socio').val(data.salida.soc_numero_socio);
                        $('#nombre').val('').attr('disabled',false);
                        $('#codigo_postal').val('').attr('disabled',false);
                        $('#localidad_nombre_localidad').val('').attr('disabled',true);
                        $('#provincia_nombre_provincia').val('').attr('disabled',true);
                        $("#bt-buscar").attr('disabled',false);
                        $("#bt-buscarsuc").attr('disabled',false);
                        $(".update-datos-mutual").css('display','none');
                        $(".agregar-sucursal").css('display','none');
                        $(".guardar-datos-mutual").css('display','initial');
                        $(".delete-datos-mutual").css('display','none');
                        $(".datos-suc").css('display','none');

                    }
                        
                       
                        /*setTimeout(function() {
                            $("#error").fadeIn(); //alert alert-danger
                            $("#error").text(data.error);
                            $("#error").fadeOut(8000);
                        },3000);*/


                        /*setTimeout(function() {
                            $(".mensaje_e").fadeIn(500);
                        },3000);
                        $("#error").text(data.error);
                        $(".mensaje_s").hide();*/
                       
                    
                    
                }
            });
        }
    });

    $('.actualizar').click(function() {
        soc_numero_socio = $('#nro_mutual').val();
        console.log('refreca datos mutual');
        event.preventDefault();

        var error = false;
        var err = false;

        if (!error) {
            var data = {soc_numero_socio:soc_numero_socio};
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/buscar-mutual',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('paso succ valor', data.salida);
                        $("#estado_t").css('display','initial');
                        $("#estado_mutual").text(data.salida.estado_actual);
                        //setTimeout(function() {
                        if(data.salida.confirmada > 0){
                            $('#soc_numero_socio').val(data.salida.soc_numero_socio).attr('disabled',false);
                            $("#nombre").val(data.salida.nombre).attr('disabled',true);
                            $("#codigo_postal").val(data.salida.codigo_postal).attr('disabled',true);
                            $("#localidad_nombre_localidad").val(data.salida.localidad).attr('disabled',true);
                            $("#provincia_nombre_provincia").val(data.salida.provincia).attr('disabled',true);
                            $("#bt-buscar").attr('disabled',true);
                            $("#bt-buscarsuc").attr('disabled',true);
                            $(".update-datos-mutual").css('display','none');
                            $(".guardar-datos-mutual").css('display','none');
                            $(".delete-datos-mutual").css('display','none');
                            $(".agregar-sucursal").css('display','none');
                            $(".datos-suc").css('display','initial');
                            $(".datos-suc").html(data.datos_html);

                        }else{
                            $('#soc_numero_socio').val(data.salida.soc_numero_socio).attr('disabled',false);
                            $('#nombre').val(data.salida.nombre).attr('disabled',false);
                            $('#codigo_postal').val(data.salida.codigo_postal).attr('disabled',false);
                            $('#localidad_nombre_localidad').val(data.salida.localidad).attr('disabled',true);
                            $('#provincia_nombre_provincia').val(data.salida.provincia).attr('disabled',true);
                            $("#bt-buscar").attr('disabled',false);
                            $("#bt-buscarsuc").attr('disabled',false);
                            $(".update-datos-mutual").css('display','initial');
                            $(".agregar-sucursal").css('display','initial');
                            $(".guardar-datos-mutual").css('display','none');
                            $(".delete-datos-mutual").css('display','initial');
                            $(".datos-suc").css('display','initial');
                            $(".datos-suc").html(data.datos_html);
                        }
                        //$("#salida").fadeIn();
                        //$("#salida").css('class','alert alert-success');
                        //$("#salida").text(data.salida);
                        //$("#salida").fadeOut(8000);
                        //$("#mensaje_mut").modal({
                            //fadeDuration: 100
                        //});//
                        //},3000);
                        /*$(".mensaje_s").show();
                        $("#salida").text(data.salida);
                        //$(".mensaje_e").hide();
                        $("#mensaje_s").hide(8000);*/

                                                               
                    }else {
                        console.log('paso false valor', data.error);
                        $("#estado_t").css('display','none');
                        $("#estado_mutual").text('');
                        //$('#soc_numero_socio').val(data.salida.soc_numero_socio);
                        $('#nombre').val('').attr('disabled',false);
                        $('#codigo_postal').val('').attr('disabled',false);
                        $('#localidad_nombre_localidad').val('').attr('disabled',true);
                        $('#provincia_nombre_provincia').val('').attr('disabled',true);
                        $("#bt-buscar").attr('disabled',false);
                        $("#bt-buscarsuc").attr('disabled',false);
                        $(".update-datos-mutual").css('display','none');
                        $(".agregar-sucursal").css('display','initial');
                        $(".guardar-datos-mutual").css('display','initial');
                        $(".delete-datos-mutual").css('display','none');
                        $(".datos-suc").css('display','none');

                    }
                        
                       
                        /*setTimeout(function() {
                            $("#error").fadeIn(); //alert alert-danger
                            $("#error").text(data.error);
                            $("#error").fadeOut(8000);
                        },3000);*/


                        /*setTimeout(function() {
                            $(".mensaje_e").fadeIn(500);
                        },3000);
                        $("#error").text(data.error);
                        $(".mensaje_s").hide();*/
                       
                    
                    
                }
            });
        }
    });

    $(".guardar-datos-mutual").click(function(e) {
        $("#error").hide();
        $("#salida").hide();
       
        console.log($('#mutual :input').serialize());
        todos = $('#mutual :input').serialize();
        event.preventDefault();

        var error = false;
        var err = false;

        /*$(".validate").each(function(i, elem) {
            console.log('VALIDAR CAMPOS:',elem);
            console.log($('#telefono_celular_pre').val().length);
            if ($('#telefono_celular_pre').val().length <= 0) {
                $(".response").html('<div class="alert alert-danger show">Debes completar todos los campos para continuar.</div>');
                err = true;
                return false;
            }
        });*/

                      
                
        if (!error) {
            var data = todos;
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/alta-mutual',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('paso succ valor', data.salida);
                        //setTimeout(function() {
                        $("#salida").fadeIn();
                        $("#salida").css('class','alert alert-success');
                        $("#salida").text(data.salida);
                        //$("#salida").fadeOut(8000);
                        $("#mensaje_mut").modal({
                            fadeDuration: 100
                        });
                        //$('#nombre').val('').attr('disabled',false);
                        //$('#codigo_postal').val('').attr('disabled',false);
                        $('#localidad_nombre_localidad').attr('disabled',true);
                        $('#provincia_nombre_provincia').attr('disabled',true);
                        $("#bt-buscar").attr('disabled',false);
                        $("#bt-buscarsuc").attr('disabled',false);
                        $(".update-datos-mutual").css('display','initial');
                        $(".guardar-datos-mutual").css('display','none');
                        $(".delete-datos-mutual").css('display','initial');
                        $(".agregar-sucursal").css('display','initial');
                        //},3000);
                        /*$(".mensaje_s").show();
                        $("#salida").text(data.salida);
                        //$(".mensaje_e").hide();
                        $("#mensaje_s").hide(8000);*/

                                                               
                    }else {
                        console.log('paso false valor', data.error);
                        $("#error").fadeIn(); //alert alert-danger
                        $("#error").text(data.error);
                        $("#mensaje_mut").modal({
                            fadeDuration: 100
                        });
                        /*setTimeout(function() {
                            $("#error").fadeIn(); //alert alert-danger
                            $("#error").text(data.error);
                            $("#error").fadeOut(8000);
                        },3000);*/


                        /*setTimeout(function() {
                            $(".mensaje_e").fadeIn(500);
                        },3000);
                        $("#error").text(data.error);
                        $(".mensaje_s").hide();*/
                       
                    }
                    
                }
            });
        }
        
    });

    $(".update-datos-mutual").click(function(e) {
        $("#error").hide();
        $("#salida").hide();
       
        console.log($('#mutual :input').serialize());
        todos = $('#mutual :input').serialize();
        event.preventDefault();

        var error = false;
        var err = false;

                            
                
        if (!error) {
            var data = todos;
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/update-mutual',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('paso succ valor', data.salida);
                        $("#estado_t").css('display','initial');
                        $("#estado_mutual").text(data.salida.estado_actual);
                        //setTimeout(function() {
                        $("#salida").fadeIn();
                        $("#salida").css('class','alert alert-success');
                        $("#salida").text(data.salida);
                        //$("#salida").fadeOut(8000);
                        $("#mensaje_mut").modal({
                            fadeDuration: 100
                        });
                        //},3000);
                        /*$(".mensaje_s").show();
                        $("#salida").text(data.salida);
                        //$(".mensaje_e").hide();
                        $("#mensaje_s").hide(8000);*/

                                                               
                    }else {
                        console.log('paso false valor', data.error);
                        $("#error").fadeIn(); //alert alert-danger
                        $("#error").text(data.error);
                        $("#mensaje_mut").modal({
                            fadeDuration: 100
                        });
                        /*setTimeout(function() {
                            $("#error").fadeIn(); //alert alert-danger
                            $("#error").text(data.error);
                            $("#error").fadeOut(8000);
                        },3000);*/


                        /*setTimeout(function() {
                            $(".mensaje_e").fadeIn(500);
                        },3000);
                        $("#error").text(data.error);
                        $(".mensaje_s").hide();*/
                       
                    }
                    
                }
            });
        }
        
    });

    $(".delete-datos-mutual").click(function(e) {
        $("#error").hide();
        $("#salida").hide();

        /*$("#salida").fadeIn();
        $("#salida").css('class','alert alert-success');
        $("#salida").text('Esta seguro de Eliminar la Mutual? , se borraran todos sus datos cargados.');
        //$("#salida").fadeOut(8000);
        $("#modal-delete").modal({
            fadeDuration: 100
        });*/
        
        mensaje = 'Esta seguro de Eliminar la Mutual? , se borraran todos sus datos cargados.'

        if (confirm(mensaje)) {

        }else{
            return false;
        }
       
        console.log($('#mutual :input').serialize());
        todos = $('#mutual :input').serialize();
        event.preventDefault();

        var error = false;
        var err = false;

                            
                
        if (!error) {
            var data = todos;
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/delete-mutual',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('paso succ valor', data.salida);
                        $("#estado_t").css('display','none');
                        $("#estado_mutual").text('');
                        //setTimeout(function() {
                        $('#soc_numero_socio').val('').attr('disabled',false);
                        $('#nombre').val('').attr('disabled',false);
                        $('#codigo_postal').val('').attr('disabled',false);
                        $('#localidad_nombre_localidad').val('').attr('disabled',true);
                        $('#provincia_nombre_provincia').val('').attr('disabled',true);
                        $("#bt-buscar").attr('disabled',false);
                        $("#bt-buscarsuc").attr('disabled',false);
                        $(".update-datos-mutual").css('display','none');
                        $(".guardar-datos-mutual").css('display','initial');
                        $(".delete-datos-mutual").css('display','none');
                        $(".agregar-sucursal").css('display','none');
                        $(".datos-suc").css('display','none');
                        $("#salida").fadeIn();
                        $("#salida").css('class','alert alert-success');
                        $("#salida").text(data.salida);
                        //$("#salida").fadeOut(8000);
                        $("#mensaje_mut").modal({
                            fadeDuration: 100
                        });
                        //},3000);
                        /*$(".mensaje_s").show();
                        $("#salida").text(data.salida);
                        //$(".mensaje_e").hide();
                        $("#mensaje_s").hide(8000);*/

                                                               
                    }else {
                        console.log('paso false valor', data.error);
                        $("#error").fadeIn(); //alert alert-danger
                        $("#error").text(data.error);
                        $("#mensaje_mut").modal({
                            fadeDuration: 100
                        });
                        /*setTimeout(function() {
                            $("#error").fadeIn(); //alert alert-danger
                            $("#error").text(data.error);
                            $("#error").fadeOut(8000);
                        },3000);*/


                        /*setTimeout(function() {
                            $(".mensaje_e").fadeIn(500);
                        },3000);
                        $("#error").text(data.error);
                        $(".mensaje_s").hide();*/
                       
                    }
                    
                }
            });
        }
        
    });

    $(".guardar-datos-usuario").click(function(e) {
        $("#error").hide();
        $("#salida").hide();
       
        console.log($('#usuario :input').serialize());
        todos = $('#usuario :input').serialize();
        event.preventDefault();

        var error = false;
        var err = false;

        /*$(".validate").each(function(i, elem) {
            console.log('VALIDAR CAMPOS:',elem);
            console.log($('#telefono_celular_pre').val().length);
            if ($('#telefono_celular_pre').val().length <= 0) {
                $(".response").html('<div class="alert alert-danger show">Debes completar todos los campos para continuar.</div>');
                err = true;
                return false;
            }
        });*/

                      
                
        if (!error) {
            var data = todos;
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/alta-usuario',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('paso succ valor', data.salida);
                        //setTimeout(function() {
                        $("#salida").fadeIn();
                        $("#salida").css('class','alert alert-success');
                        $("#salida").text(data.salida);
                        //$("#salida").fadeOut(8000);
                        $("#mensaje_usu").modal({
                            fadeDuration: 100
                        });
                        //$('#nombre').val('').attr('disabled',false);
                        //$('#codigo_postal').val('').attr('disabled',false);
                        $(".update-datos-usuario").css('display','initial');
                        $(".guardar-datos-usuario").css('display','none');
                        $(".delete-datos-usuario").css('display','initial');
                        //},3000);
                        /*$(".mensaje_s").show();
                        $("#salida").text(data.salida);
                        //$(".mensaje_e").hide();
                        $("#mensaje_s").hide(8000);*/

                                                               
                    }else {
                        console.log('paso false valor', data.error);
                        $("#error").fadeIn(); //alert alert-danger
                        $("#error").text(data.error);
                        $("#mensaje_usu").modal({
                            fadeDuration: 100
                        });
                        /*setTimeout(function() {
                            $("#error").fadeIn(); //alert alert-danger
                            $("#error").text(data.error);
                            $("#error").fadeOut(8000);
                        },3000);*/


                        /*setTimeout(function() {
                            $(".mensaje_e").fadeIn(500);
                        },3000);
                        $("#error").text(data.error);
                        $(".mensaje_s").hide();*/
                       
                    }
                    
                }
            });
        }
        
    });

    $('#id_mutual').change(function() {
        $('#usuario_correo').val('');
        $('#nombre_usuario').val('');
        $('#rol').val('');
        $('#habilitado').val('');    
    });

    $('#usuario_correo').blur(function() {
        console.log('blur usuario');
        usuario = $('#usuario_correo').val();
        mutual = $('#id_mutual').val();
       
        event.preventDefault();

        var error = false;
        var err = false;

        if (!error) {
            var data = {mutual:mutual,usuario:usuario};
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/buscar-usuario',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('paso succ valor', data.salida);
                        //$('#soc_numero_socio').val(data.salida.soc_numero_socio).attr('disabled',false);
                        $('#nombre_usuario').val(data.salida.nombre);
                        $('#rol').val(data.salida.rol);
                        $('#habilitado').val(data.salida.habilitado);
                        $(".update-datos-usuario").css('display','initial');
                        $(".guardar-datos-usuario").css('display','none');
                        //$(".delete-datos-usuario").css('display','initial');
                                                                         
                    }else {
                        console.log('paso false valor', data.error);
                        $('#nombre_usuario').val('');
                        $('#rol').val('');
                        $('#habilitado').val('');                                           
                        $(".update-datos-usuario").css('display','none');
                        $(".guardar-datos-usuario").css('display','initial');
                        //$(".delete-datos-usuario").css('display','none');

                    }
         
                    
                }
            });
        }
    });

    $(".update-datos-usuario").click(function(e) {
        $("#error").hide();
        $("#salida").hide();
       
        console.log($('#usuario :input').serialize());
        todos = $('#usuario :input').serialize();
        event.preventDefault();

        var error = false;
        var err = false;

                            
                
        if (!error) {
            var data = todos;
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/update-usuario',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('paso succ valor', data.salida);
                        $("#salida").fadeIn();
                        $("#salida").css('class','alert alert-success');
                        $("#salida").text(data.salida);
                        $("#mensaje_usu").modal({
                            fadeDuration: 100
                        });
                       

                                                               
                    }else {
                        console.log('paso false valor', data.error);
                        $("#error").fadeIn(); //alert alert-danger
                        $("#error").text(data.error);
                        $("#mensaje_usu").modal({
                            fadeDuration: 100
                        });
                       
                       
                    }
                    
                }
            });
        }
        
    });

    $('.modal-postal-suc').click(function(e){
        $(".no-alta").html('');
        $("#myModal").modal("hide");
    });

    $('.agregar-sucursal').click(function(e) {
        var mutual = $("#nombre").val();
        var nro_mutual = $("#soc_numero_socio").val();
        $('#myModal').modal('hide');
       
        $("#nro_mutual").val(nro_mutual);
        $(".txt-mutual").text(mutual + '('+ nro_mutual+ ')');
        $(".no-alta").empty();
        $(".resultado-alta").empty();
        $("#soc_sucursal").val('');
        $("#nombre_suc").val('');
        $("#codigo_postal_suc").val('');
        $("#localidad_nombre_localidad_suc").val('');
        $("#provincia_nombre_provincia_suc").val('');
        console.log('modal_suc');
        console.log(mutual);
        $('.cerrar-suc').show();
        $('.guardar-sucursal').show();
        $('.alta-aceptar').hide();
        

    });

    /*$('.buscar_codpostall').click(function(e) {
        $('#modal-codpostal-suc').modal('toggle');
      

    });*/
    /*$(".agregar-sucursal").click(function(){
        $("#myModal").modal("hide");
    });*/
    
    $(".buscar_codpostal_suc").click(function(e) {
        console.log('entro por click buscar codpostal de alta de sucursal a una mutual');
        e.preventDefault();
        var opcion = $('#filtro_postalsuc').val();
        var buscar = $('#buscar_postalsuc').val();
        var suc = 99;
        $('.error').text('');
        $('.resultado_postal').html('');
        
        console.log('filtro_postal:', opcion);
        console.log('buscar_postal:', buscar);
          
        var error = false;


        if (!error) {
            var data = {filtro: opcion,buscar: buscar, suc:suc};
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: 'buscar-codpostal-modal-suc',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log(data);
                        $('.resultado_postal_suc').html(data.datos).show();
                                             
                    }else {
                        console.log('paso succ error');
                        $('.error').html(data.error).show();
                        
                    }
                    
                }
            });
        }
    });

    $(".guardar-sucursal").click(function(e) {
        console.log('entro a guardar sucursal');
       
        e.preventDefault();
        mutual          = $('#nro_mutual').val();
        nro_sucursal    = $('#soc_sucursal').val();
        nombre_sucursal = $('#nombre_suc').val();
        cod_postal      = $('#codigo_postal_suc').val();
        
       
        console.log('mutual:', mutual);
        console.log('nro_sucursal:', nro_sucursal);
        console.log('nombre_sucursal:', nombre_sucursal);
        console.log('cod_postal:', cod_postal);
        var error = false;
        $('.adicional-js').slideDown();
        
        if (!error) {
            var data = {mutual: mutual, nro_sucursal:nro_sucursal, nombre_sucursal:nombre_sucursal, cod_postal: cod_postal};
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/agregar-sucursal',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('valor:', data.embozado);
                        $(".resultado-alta").html(data.salida);
                       
                        $("#success-back").slideDown();
                        $('.adicional-js').slideDown();
                        $(".no-alta").empty();
                        $('.cerrar-suc').hide();
                        $('.alta-aceptar').show();
                        $('.guardar-sucursal').hide();
                       
                                                    
                                            
                    }else {
                        console.log('paso succ valor', data.erro);
                        $(".no-alta").html(data.error); 
                        $('.adicional-js').hide();
                        $('.cerrar-suc').show();
                        $('.guardar-sucursal').show();
                        $(".resultado-alta").empty();
                    }
                    
                }
            });
        }
    });

    $("#id_sucursal").change(function(e) {
        $(".resultado-buscar").html('');
        $('.update-datos-suc').hide();
    });

    $(".mutual").change(function(e) {
       console.log('paso por change id_mutual');
       $(".resultado-buscar").html('');
       $('.update-datos-suc').hide();
       $("#estado_t").css('display','none');
       e.preventDefault();
       id_mutual = $('#id_mutual').val();
             
       console.log('id_mutual:', id_mutual);
       if($('#id_mutual').val() == 0){
           $('#id_mutual').html('<option value="">Seleccione</option>');
       }
        var error = false;


       if (!error) {
        var data = {id_mutual: id_mutual};
        console.log('paso :', data);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            type: 'POST',
            url: '/admin/buscar-suc-mutual',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
               
            },
            success: function(data) {
                if (data.success) {
                    console.log('datos: true');
                    var select = '<option value="">Seleccione</option>';
                    for(var i=0; i<data.sel.length;i++){
                        select+='<option value="'+data.sel[i].soc_sucursal+'">'+data.sel[i].nombre+'</option>';
                        $('#id_sucursal').html(select);
                    }
                                  
                }else {
                    console.log('paso succ false');
                    $('#id_sucursal').html('<option value="">Seleccione</option>');
                     

                }
                    
                
            }
        });
       }

    });

    $(".buscar-suc").click(function(e) {
        console.log('entro a buscar sucursal');
       
        e.preventDefault();
        mutual          = $('#id_mutual').val();
        nro_sucursal    = $('#id_sucursal').val();
               
        console.log('mutual:', mutual);
        console.log('nro_sucursal:', nro_sucursal);

              
        var error = false;
               
        if (!error) {
            var data = {mutual: mutual, nro_sucursal:nro_sucursal};
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/buscar-suc',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        $("#estado_t").css('display','initial');
                        $("#estado_mutual").text(data.salida.estado_actual);
                        console.log('valor:', data);
                        if(data.salida.confirmada > 0){
                            $(".resultado-buscar").html(data.datos);
                            $("#nombre").attr('disabled',true);
                            $("#habilitado").attr('disabled',true);
                            $(".b-s").attr('disabled',true);
                            $(".no-buscar").html(''); 
                            $('.update-datos-suc').hide();
                        }else{
                            $(".resultado-buscar").html(data.datos);
                            $(".no-buscar").html(''); 
                            $('.update-datos-suc').show();
                        }
                                                 
                                            
                    }else {
                        console.log('paso succ valor', data.error);
                        $("#estado_t").css('display','none');
                        $("#estado_mutual").text('');
                        $(".no-buscar").html(data.error); 
                        $(".resultado-buscar").html('<div class="alert alert-danger show">No existe Sucursal para buscar.</div>');
                        $('.update-datos-suc').hide();
                    }
                    
                }
            });
        }
    });

    $("#filtro_postal_s").click(function(e) {
            console.log('paso #filtro_postal_s');
            if($("#filtro_postal_s").val() == 1){
                $("#buscar_postal_s").val('');
                $("#buscar_postal_s").removeAttr('disabled').attr('placeholder','Nombre de la Localidad');
                        
                
            }else if($("#filtro_postal_s").val() == 2){
                $("#buscar_postal_s").val('');
                $("#buscar_postal_s").removeAttr('disabled').attr('placeholder','Código Postal');
                
            }else if($("#filtro_postal_s").val() == 3){
                $("#buscar_postal_s").val('');
                $("#buscar_postal_s").attr('disabled','disabled').attr('placeholder','Todas');
                
                        
            }else if($("#filtro_postal_s").val() == 0){
                $("#buscar_postal_s").attr('disabled','disabled').attr('placeholder',' ');
            }
        });
        

        $(".buscar_codpostal_s").click(function(e) {
        console.log('entro por click buscar codpostal de alta de sucursal() ');
        e.preventDefault();
        var opcion = $('#filtro_postal').val();
        var buscar = $('#buscar_postal').val();
        
        $('.error').text('');
        $('.resultado_postal').html('');
        
        console.log('filtro_postal:', opcion);
        console.log('buscar_postal:', buscar);
          
        var error = false;


        if (!error) {
            var data = {filtro: opcion,buscar: buscar};
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: 'buscar-codpostal-modal-suc',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log(data);
                        $('.resultado_postal').html(data.datos).show();
                        
                                             
                    }else {
                        console.log('paso succ error');
                        $('.error').html(data.error).show();
                       
                        
                    }
                    
                }
            });
        }
    });

    $(".update-datos-suc").click(function(e) {
        $("#error").hide();
        $("#salida").hide();
       
        //console.log($('#usuario :input').serialize());
        todos = $('#form :input').serialize();
        event.preventDefault();

        var error = false;
        var err = false;

                            
                
        if (!error) {
            var data = todos;
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/up-sucursal',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('paso succ valor', data.salida);
                        $("#salida").fadeIn();
                        $("#salida").css('class','alert alert-success');
                        $("#salida").text(data.salida);
                        $("#mensaje_usu").modal({
                            fadeDuration: 100
                        });
                       

                                                               
                    }else {
                        console.log('paso false valor', data.error);
                        $("#error").fadeIn(); //alert alert-danger
                        $("#error").text(data.error);
                        $("#mensaje_usu").modal({
                            fadeDuration: 100
                        });
                       
                       
                    }
                    
                }
            });
        }
        
    });

    $(".update-password").click(function(e) {
        $("#error").hide();
        $("#salida").hide();
       
        //console.log($('#usuario :input').serialize());
        todos = $('#usuario :input').serialize();
        event.preventDefault();

        var error = false;
        var err = false;

                            
                
        if (!error) {
            var data = todos;
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: '/admin/cambio-password',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log('paso succ valor', data.salida);
                        $("#password1").val('');
                        $("#password2").val('');
                        $("#salida").fadeIn();
                        $("#salida").css('class','alert alert-success');
                        $("#salida").text(data.salida);
                        $("#mensaje_usu").modal({
                            fadeDuration: 100
                        });
                       

                                                               
                    }else {
                        console.log('paso false valor', data.error);
                        $("#error").fadeIn(); //alert alert-danger
                        $("#error").text(data.error);
                        $("#mensaje_usu").modal({
                            fadeDuration: 100
                        });
                       
                       
                    }
                    
                }
            });
        }
        
    });

    /*$(".modal-suc").click(function(e) {
        console.log('nueva js--');
        //si le quiero pasar los valores de un campo por jquery
        $("#myModalsuc" + $(this).attr("id")).modal('show');
        $("#.verm").css('position','absolute'); // le concateno el id del boton para generar los modales dinamicos
       
    });*/

   
      
    

    </script>
</html>

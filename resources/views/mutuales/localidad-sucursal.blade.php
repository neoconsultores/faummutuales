<?php //dd($datos);
  
?>

    @if(isset($datos))
      @if(count($datos) > 0)
        @if(is_array($datos))
        <table class="table table-bordered table-hover" style="font: 12px Helvetica, Arial, san-serif;">
          <thead>
            <tr class="text-center">
              <th>Código Postal</th>
              <th>Localidad</th>
              <th>Provincia</th>
                           
            </tr>
          </thead>
          <tbody>
          <?php $count = 1; 
            //dd($datos);?>
          @foreach($datos as $i => $valor)
          <?php $count = 1; 
            //dd($valor->cod_postal);?>
            <tr class="text-center">
             
              <td class="text-center" ><a href="#" style="color: blue;cursor-p" class="seleccionar" id="{{ $loop->index }}">{{ $valor->cod_postal}}</u></td>
                <input type="hidden" id="cod_postal{{ $loop->index }}" value="{{ $valor->cod_postal}}" name="cod_postal">
              <td class="text-center">{{ $valor->localidad }}</td>
              <input type="hidden" id="localidad{{ $loop->index }}" value="{{ $valor->localidad}}" name="localidad">
              <td>{{ $valor->provincia }}</td>
              <input type="hidden" id="prov{{ $loop->index }}" value="{{ $valor->provincia}}" name="prov">
                                
              
            </tr>
            <?php $count++; ?>
          @endforeach
        @endif
      @endif
    @else
        
      <h5 style="color:crimson; text-align: center;">Seleccione una opción de busqueda.</h5>
         
    @endif   
    </tbody>
</table>
<script>
  $(document).ready(function() {
    //$('#myModal').modal('hide');
    $('.seleccionar').click(function(){
        cod_postal = $('#cod_postal'+ $(this).attr("id")).val();
        localidad = $('#localidad'+ $(this).attr("id")).val();
        provincia = $('#prov'+ $(this).attr("id")).val();
        dato ={cod_postal:cod_postal,localidad:localidad,provincia:provincia};
        console.log('buscar codpostal:',dato);

        
        $('#codigo_postal_suc').val(cod_postal);//.attr('disabled','disabled');
        $('#provincia_nombre_provincia_suc').val(provincia);//.attr('disabled','disabled'),
        $('#localidad_nombre_localidad_suc').val(localidad);//.attr('disabled','disabled');
        $('#myModal').modal('toggle');
        $('#myModal').modal('hide');
        console.log('salio codigo postal suc');
    
        
      
  });
});

</script>
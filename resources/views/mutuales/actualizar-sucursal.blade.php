<?php //dd($sucursal)?>
<form action="" id="form" name="form" class="form" autocomplete="off">
    @if(isset($sucursal))
        
            <hr>
            <legend class="margen_izq">Sucursales Adheridas</Canvas></legend>
                       
           
            @foreach($sucursal as $i => $valor)
                <?php //dd($sucursal); ?>
                <div class="row mrow" style="padding-top: 10px;">
                             
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <div><label for="" class="help-block text-muted small-font margen_bot">N° Sucursal</label></div>
                        <div><input type="text" class="form-control form-control-sm margen_izq" id="soc_sucursal" name="soc_sucursal" placeholder="N° de Sucursal" value="{{$valor->soc_sucursal}}" maxlength="5" readonly></div>
                    </div>
                    <div class="col-md-7 col-sm-4 col-xs-2">
                        <div>
                            <label for="" class="help-block text-muted small-font margen_bot">Nombre</label>
                            <input type="text" class="form-control form-control-sm margen_izq" id="nombre" name="nombre" placeholder="Nombre de la Sucursal" maxlength="50" value="{{ $valor->nombre }}" > 
                        </div>
                   
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <div><label for="" class="help-block text-muted small-font margen_bot">Habilitado</label></div>
                        <div>
                            <select class="form-control form-control-sm margen_izq" id="habilitado" name="habilitado" required>
                            <option value="" selected>Seleccione</option>
                            <option value="0" @if(isset($valor->soc_codigo_baja) && $valor->soc_codigo_baja == 0) selected @endif>Habilitado</option>
                            <option value="1" @if(isset($valor->soc_codigo_baja) && $valor->soc_codigo_baja == 1) selected @endif>Deshabilitado</option>
                                             
                        </select>
                        </div>
                    </div>
                </div>

                <div class="row  mrow" style="padding-top: 10px;">
                    
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <div><label for="" class="help-block text-muted small-font margen_bot">Código Postal</label></div>
                        <div><input type="text" class="form-control form-control-sm margen_izq codigo_postal_suc" id="codigo_postal_suc" name="codigo_postal_suc" placeholder="Código Postal" value="{{$valor->codigo_postal }}" maxlength="8"  required readonly></div>
                    </div>

                    <div class="col-md-1 col-sm-1 col-xs-1">
                        <div style="margin-bottom: 20px"><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                        <div><button type="button" class="btn btn-default b-s"  data-toggle="modal" data-target="#myModal"> <!-- data-toggle="modal" data-target="#myModalsuc falta las llaves y {$i}" -->
                            <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                        <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                    </div>

                    <div class="col-md-3 col-sm-2 col-xs-2">
                        <div>
                            <label for="" class="help-block text-muted small-font margen_bot">Localidad</label>
                            <input type="text" class="form-control form-control-sm margen_izq localidad_nombre_localidad_suc" id="localidad_nombre_localidad_suc" name="localidad_nombre_localidad_suc" placeholder="" maxlength="60" value="{{$valor->localidad  }}" readonly> 
                            <input id="provincia_doc" name="provincia_doc" type="hidden" value="0">
                        </div>
                                          
                    </div>
                
                    <div class="col-md-3 col-sm-2 col-xs-2">
                        <div>
                            <label for="" class="help-block text-muted small-font margen_bot">Provincia</label>
                            <input type="text" class="form-control form-control-sm margen_izq provincia_nombre_provincia_suc" id="provincia_nombre_provincia_suc" name="provincia_nombre_provincia_suc" placeholder="" maxlength="60" value="{{$valor->provincia}}" readonly> 
                        </div>
                                                
                    </div>
                </div>
                <hr>
               
            @endforeach
         
        
    @endif
</form>
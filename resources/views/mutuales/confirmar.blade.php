@extends('layout.base')
<?php
//dd($sec['estado']);
if(($datos_doc['estado'] == 0) || ($pre['estado'] == 0) || ($sec['estado'] == 0) || ($tes['estado'] == 0) || ($mut['estado'] == 0)){ //|| ($apo['estado'] == 0)
    $ver = 'none';
    $leyenda = '';
}elseif($datos->confirmada == 0 || $datos->confirmada == 3){
    $ver = 'inline';
    $leyenda = 'Declaro bajo juramento que los datos aquí consignados son correctos, 
                completos y fiel expresión de la verdad, obligándome a informar a Federaciones Aliadas Unión Mutual (FAUM) cualquier modificación que se produzca, 
                dentro de los treinta (30) días de ocurrida.';
}else{
    $ver = 'none';
    $leyenda = '';
}

if($datos->confirmada == 3){
    $color = 'red';
}else{
    $color = 'rgb(105, 191, 105)';
}
$today = date('Y-m-d');
//dd($datos);
?>

@section('content')
    <div class="content" style="position: relative;z-index: 100;">
        <div style="">
            <div class="jumbotron jumbotron-fluid" style="display:flex; padding-top:0px; padding-bottom:0px; margin-bottom:10px;" style="background-color: #F1F2F2">
                <div class="container" style="padding-bottom: 10px;padding-top: 10px;">
                    <img src="img/jumbo.png" class="float-right d-none d-lg-block" style="margin-left: -600px; height: 100%;"> {{--radial-gradient(circle, black 0%, transparent 85%);--}}
                    <h1 class="display-6">Tarjeta Prepaga y de Crédito Mutual</h1>
                    <p class="lead">MasterCard "FAUM Con Vos", Al resguardo del bien mutuo, FAUM CON VOS, siempre a tu lado.</p>
                                 
                </div>
            </div>
        </div>
    </div>

    <div class="container" style="position: relative;z-index: -100;" id="form">
        
        @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" data-toggle="tooltip" data-placement="top" title="Salir de la Notificación">
                    <i class='far fa-times-circle' style='font-size:24px;color:red'></i>
                </button>	
                <ul>
                    @foreach ($errors->all() as $err)
                    <li>{{ $err }}</li>
                    @endforeach
                </ul>
            </div>
            
        @endif

        <h4>Confirmar Datos</h4>
        <div class="tab-content" style="background-color: #e9ecef;" style="width: 1036px">
            <fieldset style="width:100%;" >
                @if(isset($datos))    
                    <div class="row mrow" style="padding-top: 10px;">
                        <div class="col-md-6 col-sm-4 col-xs-2">
                            <div><label for="" class="help-block text-muted small-font margen_bot" >Estado Actual: <strong style="color:{{ $color }}" id="estado-act">{{ $datos->estado_actual }}</strong></label></div>
                        </div>                                
                    </div>

                    <div class="row mrow" style="padding-top: 10px;">
                        
                    
                        <div class="col-md-2 col-sm-2 col-xs-2">
                        <div><label for="" class="help-block text-muted small-font margen_bot">N° Adherente</label></div>
                            <div><input type="text" class="form-control form-control-sm margen_izq" id="soc_numero_socio" name="soc_numero_socio" placeholder="N° de Socio" value="{{$datos->soc_numero_socio  }}" maxlength="5" readonly></div>
                            <input type="hidden" id="nro_persona" name="nro_persona" value="{{ $datos->nro_persona }}">
                        </div>

                            
                        <div class="col-md-5 col-sm-4 col-xs-2">
                            <div>
                                <label for="" class="help-block text-muted small-font margen_bot">Nombre</label>
                                <input type="text" class="form-control form-control-sm margen_izq" id="nombre" name="nombre" placeholder="Nombre del Socio" maxlength="50" value="{{$datos->nombre  }}" disabled> 
                            </div>
                            
                        </div>

                        <div class="col-md-2 col-sm-1 col-xs-1">
                            <div><label for="state_id" class="control-label small-font margen_bot">CUIT N°</label></div>
                            <div><input type="text" class="form-control form-control-sm margen_izq validar_numero" id="nro_cuit" name="nro_cuit" placeholder="Nro de documento" maxlength="15" value="{{$datos->nro_cuit }}" maxlength="20" disabled></div>
                        </div>

                    </div>

            

                    @if(($datos_doc['estado'] == 0) || ($pre['estado'] == 0) || ($sec['estado'] == 0) || ($tes['estado'] == 0) || ($mut['estado'] == 0)) <!--|| ($apo['estado'] == 0) -->
                        <div class="row mrow" style="padding-top: 10px;text-align: center;">
                            <div class="col-md-12 col-sm-8 col-xs-4" style="color: red">Cargue toda la información requerida para confirmar los datos.</div>
                        </div>
                        <div class="row mrow">
                            <ul>
                                @if(isset($mut) && $mut['estado']  == 0)
                                    <li style="color: red">{{ $mut['mensaje'] }}</li>
                                @endif   
                                @if(isset($pre) && $pre['estado']  == 0)
                                    <li style="color: red">{{ $pre['mensaje'] }}</li>
                                @endif    
                                @if(isset($sec) && $sec['estado']  == 0)
                                    <li style="color: red">{{ $sec['mensaje'] }}</li>
                                @endif 
                                @if(isset($tes) && $tes['estado']  == 0)
                                    <li style="color: red">{{ $tes['mensaje'] }}</li>
                                @endif 
                                @if(isset($apo) && $apo['estado']  == 0)
                                    <li style="color: red">{{ $apo['mensaje'] }}</li>
                                @endif 
                                @if(isset($datos_doc) && $datos_doc['estado']  == 0)
                                    <li style="color: red">{{ $datos_doc['mensaje'] }}</li>
                                @endif
                            </ul>
                        </div>
                    
                    @else
                        <div class="row mrow" style="padding-top: 10px;">
                                            
                            @if(isset($datos->nro_cuit) && $datos->nro_cuit <> '')
                                <div class="col-md-2 col-sm-2 col-xs-2" style="">
                                    <label class="control-label text-muted small-font margen_bot margen_izq">Declaración Jurada</label>
                                    <form action="/admin/imprimir-jur" method="POST" autocomplete="off">
                                        @csrf
                                        <input type="hidden" id="nro_persona" name="nro_persona" value="{{$datos->nro_persona}}">
                                        <button type="submit" class="btn" style="margin-top: -63px;margin-left: 114px;" data-toggle="tooltip" data-placement="top" title="Declaración Jurada"><i class="fa fa-file-pdf-o"></i></button>
                                    </form>
                                        
                                </div>
                                    
                            @endif
                        

                            <div class="col-md-5 col-sm-4 col-xs-2" style="display:{{ $ver }}"> 
                                <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo" style="margin-left: -28px;font-size: 14px;" name="archivo"  required value="">
                                <input type="hidden" id="cod" name="cod" value ="FA00">
                                <div  style="text-align: end;font: caption;color: red;"><span id="salida_error1"></span></div>
                                <div  style="text-align: center;font: initial;color:rgb(75 191 101);margin-top: 15px;"><span class="salida1"></span></div>
                            </div>
                            <div class="col-md-1 col-sm-1 col-xs-1"> 
                                <button type="button" class="btn btn-sm guardar-datos-adjuntos" style="background-color:#268fa9;float: inline-end;margin-left: -85px;margin-top: 4px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                            </div>
                        </div>
                    @endif
                    <div class="row mrow" style="padding-top: 10px;">

                        <div class="col-md-10 col-sm-6 col-xs-4" style="display:{{ $ver }}">
                            <div class="form-check margen_izq" style="padding-top: 30px;">
                                <input class="form-check-input margen_izq" type="checkbox" id="checkbox-ley" name="checkbox-ley" style="margin-top: 15px;" > <!--disabled -->
                                <label class="form-check-label" for="" style="font-family: auto;padding-top: 10px;">{{ $leyenda }}</label>
                                
                            </div>
                    
                            
                        </div>
                                                            
                    </div>
                    @if(isset($datos->confirmada) && $datos->confirmada > 1 )
                        <div class="row mrow" style="padding-top: 10px;">
                            <div class="col-md-7 col-sm-4 col-xs-2">
                                <div><label class="help-block text-muted small-font margen_bot">Observaciones</label></div>
                                <div><textarea class="form-control margen_izq" rows="2" cols="10" id="comentario"  readonly >@if($datos->com_observaciones <> ''){{$datos->com_observaciones}}@else Sin Observaciones @endif</textarea></div>
                            </div>
                        </div>
                    @endif
                @endif
            </fieldset>
                    
        </div>
        <br>
        <div class="row mrow">
            <div class="col-md-4 col-sm-3 col-xs-2">
                <a class=""  style="color: #73c6d9;" href="/admin/cargar"><i class="fal fa-undo"></i>Volver a Pagina Principal</a>
            </div>
    
            
        </div>
    </div>
    <div class="mensaje_s" style="text-align: center;">
        <strong class="small-font" id="salida" style="color:rgb(75 191 101);"></strong>
    </div>
    
    <div class="mensaje_e" style="text-align: center;">
        <strong class="small-font" id="error" style="color:red;"></strong>
    </div>

    <div class="row mrow" style="padding-top: 10px;">
                                       
        <div class="col-md-12 col-sm-8 col-xs-4" style="padding-top: 5px;text-align: right;"> 
            <button type="button" class="btn btn-sm boton-guardar confirmar-datos" style="background-color:#97a2a5;float: inline-end;color: aliceblue;display:{{ $ver }}" disabled >Confirmar</button>
            <input id="nro_persona" name="nro_persona" type="hidden" value="{{ $datos->soc_numero_socio }}">
        </div>
        
    </div>

        
    
@endsection
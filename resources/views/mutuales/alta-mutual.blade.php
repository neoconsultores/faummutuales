@extends('layout.base')
<?php
//dd($sec['estado']);

$today = date('Y-m-d');
//dd($datos);
?>

@section('content')
    <div class="content" style="position: relative;z-index: 100;">
        <div style="">
            <div class="jumbotron jumbotron-fluid" style="display:flex; padding-top:0px; padding-bottom:0px; margin-bottom:10px;" style="background-color: #F1F2F2">
                <div class="container" style="padding-bottom: 10px;padding-top: 10px;">
                    <img src="img/jumbo.png" class="float-right d-none d-lg-block" style="margin-left: -600px; height: 100%;"> {{--radial-gradient(circle, black 0%, transparent 85%);--}}
                    <h1 class="display-6">Tarjeta Prepaga y de Crédito Mutual</h1>
                    <p class="lead">MasterCard "FAUM Con Vos", Al resguardo del bien mutuo, FAUM CON VOS, siempre a tu lado.</p>
                                 
                </div>
            </div>
        </div>
    </div>

    <div class="container" style="position: relative;z-index: -100;" id="form">
        
        @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" data-toggle="tooltip" data-placement="top" title="Salir de la Notificación">
                    <i class='far fa-times-circle' style='font-size:24px;color:red'></i>
                </button>	
                <ul>
                    @foreach ($errors->all() as $err)
                    <li>{{ $err }}</li>
                    @endforeach
                </ul>
            </div>
            
        @endif

        <h4>Alta de Mutuales Adherentes</h4><div id="estado_t" style="display: none">Estado Actual: <strong id="estado_mutual" style="color: rgb(105, 191, 105);"></strong></div>
        <div class="tab-content" style="background-color: #e9ecef;" id="mutual" style="width: 1036px" >
            <fieldset style="width:100%;" >
                <div class="row mrow" style="padding-top: 10px;">
            
                    <div class="col-md-2 col-sm-2 col-xs-2">
                    <div><label for="" class="help-block text-muted small-font margen_bot">N° Adherente</label></div>
                        <div><input type="text" class="form-control form-control-sm margen_izq" id="soc_numero_socio" name="soc_numero_socio" placeholder="N° de Mutual" value="" maxlength="5" ></div>
                        
                    </div>

                        
                    <div class="col-md-7 col-sm-4 col-xs-2">
                        <div>
                            <label for="" class="help-block text-muted small-font margen_bot">Nombre</label>
                            <input type="text" class="form-control form-control-sm margen_izq" id="nombre" name="nombre" placeholder="Nombre del la Mutual" maxlength="50" value="" required>
                            <input class="cargo" type="hidden" value="0">
                        </div>
                        
                    </div>
                </div>

                <div class="row mrow">
                                    
                               
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <div><label for="" class="help-block text-muted small-font margen_bot">Código Postal</label></div>
                        <div><input type="text" class="form-control form-control-sm margen_izq" id="codigo_postal" name="codigo_postal" placeholder="Código Postal" value="" maxlength="8" readonly></div>
                    </div>
                    

                    <div class="col-md-1 col-sm-1 col-xs-1">
                        <div style="margin-bottom: 20px"><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                        <div><button type="button" class="btn btn-default buscar_codpostall" style="" id="bt-buscar" data-toggle="modal" data-target="#myModal">
                            <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                        <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                    </div>
                    

                    <div class="col-md-3 col-sm-2 col-xs-2">
                        <div>
                            <label for="" class="help-block text-muted small-font margen_bot">Localidad</label>
                            <input type="text" class="form-control form-control-sm margen_izq" id="localidad_nombre_localidad" name="localidad_nombre_localidad" placeholder="" maxlength="60" value="" disabled> 
                            
                        </div>
                        
                    </div>

                    <div class="col-md-3 col-sm-2 col-xs-2">
                        <div>
                            <label for="" class="help-block text-muted small-font margen_bot">Provincia</label>
                            <input type="text" class="form-control form-control-sm margen_izq" id="provincia_nombre_provincia" name="provincia_nombre_provincia" placeholder="" maxlength="60" value="" disabled> 
                        </div>
                        
                    </div>
                </div>
                <div class="row mrow datos-suc" style="display: none"></div>

                <div class="" style="padding-top: 0px;text-align: end;padding-bottom: 10px;"> 
                    <button type="button" class="btn boton-guardar agregar-sucursal" style="background-color:#268fa9;color: aliceblue;display: none;" data-toggle="modal" data-target="#modal-sucursal">Sucursal (+)</button>
                    <button type="button" class="btn boton-guardar guardar-datos-mutual" style="background-color:#268fa9;color: aliceblue;">Guardar</button>
                    <button type="button" class="btn boton-guardar update-datos-mutual" style="background-color:#268fa9;color: aliceblue;display:none">Actualizar</button>
                    <button type="button" class="btn boton-guardar delete-datos-mutual" style="background-color:#268fa9;color: aliceblue;display:none">Eliminar</button>
                </div>
                
            </fieldset>
            
                    
        </div>
    </div>
    
    
    
    
@endsection

<!-- Modal codigo postal-->
<div class="modal" id="myModal">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
        <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Buscar Localidad de la Mutual</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
                     
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row sm-9" style="background-color: #e9ecef; padding-top: 15px;padding-bottom: 20px;">
                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot">Buscar por</label>
                        <input id="opcion" name="opcion" type="hidden">
                            <select class="form-control form-control-sm"  name="filtro_postal" id="filtro_postal">
                            <option selected value="" >Seleccione</option>
                            <option value="1">Nombre de Localidad</option>
                            <option value="2">Código Postal</p></option>
                            <option value="3">Todas</p></option>
                            
                        </select>
                    </div>

                    

                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot" id="titulo">Opción de busqueda</label>
                        <input type="text" class="form-control form-control-sm" id="buscar_postal" name="buscar_postal" disabled>
                      
                    </div>
        
                
                    
                    <div class="col-sm-2" style="margin-top: 15px">
                        <div><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                        <div><button type="button" class="btn btn-default buscar_codpostal" style="margin-bottom: -47px; margin-left: -16px;">
                            <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                        <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                    </div>
        
                </div>
                <br>
                <div class="resultado_postal"></div>
                <div class="error" style="color:crimson; text-align: center;"></div>
                
            </div>
                
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-info" data-dismiss="modal" style="color: #73c6d9;">Salir</button>
            </div>
                            
        </div>
    </div>
</div>

<div class="modal fade" id="mensaje_mut">
    <div class="modal-dialog">
      <div class="modal-content">
  
        <!-- cabecera del diálogo -->
        <div class="modal-header">
          <h4 class="modal-title">Mensaje</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- cuerpo del diálogo -->
        <div class="modal-body">
            <div class="mensaje_s">
                <strong class="small-font" id="salida" style="color:rgb(75 191 101);background-color:#f7eeee;"></strong>
            </div>
          
            <div class="mensaje_e">
                <strong class="small-font" id="error" style="color:red;background-color:#f7eeee;"></strong>
            </div>
        </div>
  
        <!-- pie del diálogo -->
        <div class="modal-footer">
          <button type="button" class="btn boton-guardar" style="background-color:#268fa9;float: inline-end;color: aliceblue;" data-dismiss="modal">Cerrar</button>
        </div>
  
      </div>
    </div>
  </div> 
  

  <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal-delete">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Confirmar</h4>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="modal-btn-si">Aceptar</button>
          <button type="button" class="btn btn-primary" id="modal-btn-no">Cancelar</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal agrgar sucursales -->
<div class="modal" id="modal-sucursal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="#" method="POST" autocomplete="off">
                <input type="hidden" id="nro_mutual" name="nro_mutual" >
                
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Sucursal</h5><br>
                                       
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    
                </div>
                <h5 class="modal-title" id="exampleModalLabel" style="margin-left: 16px;"><strong class="txt-mutual"></strong></h5>
                    <div class="modal-body">
                     
                        <div style="border-style: dotted; border-width: 1px;">
                            <fieldset>
                                <div class="row mrow" style="padding-top: 5px;">
                             
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <div><label for="" class="help-block text-muted small-font margen_bot">N° Sucursal</label></div>
                                        <div><input type="text" class="form-control form-control-sm margen_izq" id="soc_sucursal" name="soc_sucursal" placeholder="N° Sucursal" value="" maxlength="5" ></div>
                                    </div>
        
                                        
                                    <div class="col-md-7 col-sm-5 col-xs-2">
                          
                                        <div>
                                            <label for="" class="help-block text-muted small-font margen_bot">Nombre</label>
                                            <input type="text" class="form-control form-control-sm margen_izq" id="nombre_suc" name="nombre_suc" placeholder="Nombre de la Sucursal" maxlength="50" value="" > 
                                        </div>

                                        
                                    </div>
                                </div>

                                <div class="row  mrow" style="padding-top: 10px;">
                
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <div><label for="" class="help-block text-muted small-font margen_bot">Código Postal</label></div>
                                        <div><input type="text" class="form-control form-control-sm margen_izq" id="codigo_postal_suc" name="codigo_postal_suc" placeholder="Código Postal" value="" maxlength="8"  readonly></div>
                                    </div>

                                    <div class="col-md-1 col-sm-1 col-xs-1">
                                        <div style="margin-bottom: 20px"><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                                        <div><button type="button" class="btn btn-default modal-postal-suc" style=""  data-toggle="modal" data-target="#modal-codpostal-suc">
                                            <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                                        <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                                    </div>

                                    <div class="col-md-3 col-sm-2 col-xs-2">
                                        <div>
                                            <label for="" class="help-block text-muted small-font margen_bot">Localidad</label>
                                            <input type="text" class="form-control form-control-sm margen_izq" id="localidad_nombre_localidad_suc" name="localidad_nombre_localidad_suc" placeholder="" maxlength="60" value="" disabled> 
                                            <input id="provincia_doc_suc" name="provincia_doc_suc" type="hidden" value="0">
                                        </div>
                                      
                                    </div>
            
                                    <div class="col-md-3 col-sm-2 col-xs-2">
                                        <div>
                                            <label for="" class="help-block text-muted small-font margen_bot">Provincia</label>
                                            <input type="text" class="form-control form-control-sm margen_izq" id="provincia_nombre_provincia_suc" name="provincia_nombre_provincia_suc" placeholder="" maxlength="60" value="" disabled> 
                                        </div>
                                            
                                    </div>
                                </div>
                                    <br>  
                                                
                            </fieldset>
                        </div>
                        <br>
                        <h5 class="resultado-alta" style="color: green;"></h5>
                        <h5 class="no-alta" style="color:red;"></h5>
                        <div id="es_adicional" style="display:none;">
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close" title="Salir" data-toggle="tooltip" data-placement="top"><span aria-hidden="true">&times;</span></button>
                                <a href="/registro-persona"  style="color: red;"><i class="fa fa-check"></i> Esta Sucursal ya existe, con este numero.</a>
                            </div>
                                
                        </div>
  
                    
                    
                </div>
                
                <div class="modal-footer alta-guardar">
                    <button type="button" class="btn btn-secondary cerrar-suc" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn boton-guardar guardar-sucursal" style="background-color:#268fa9;float: inline-end;color: aliceblue;" title="Alta de Sucursal" data-toggle="tooltip" data-placement="top">Guardar</button>
                    <button type="button" class="btn boton-guardar actualizar alta-aceptar" style="background-color:#268fa9;float: inline-end;color: aliceblue;" title="Salir" data-toggle="tooltip" data-placement="top" data-dismiss="modal">Salir</button>
                    
                </div>
                <div class="modal-footer " id="success-back" style="display:none;">
                   
                </div>
            </form>
        </div>
    </div>
</div>
<!-- fin modal  -->

  <!-- Modal codigo postal sucursal-->
<div class="modal" id="modal-codpostal-suc">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
      
            <div class="modal-header">
                <h4 class="modal-title">Buscar Localidad de la sucursal</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
                     
         
            <div class="modal-body">
                <div class="row sm-9" style="background-color: #e9ecef; padding-top: 15px;padding-bottom: 20px;">
                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot">Buscar por</label>
                            <select class="form-control form-control-sm"  name="filtro_postalsuc" id="filtro_postalsuc">
                            <option selected value="" >Seleccione</option>
                            <option value="1">Nombre de Localidad</option>
                            <option value="2">Código Postal</p></option>
                            <option value="3">Todas</p></option>
                            
                        </select>
                    </div>

                    

                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot" id="titulo">Opción de busqueda</label>
                        <input type="text" class="form-control form-control-sm" id="buscar_postalsuc" name="buscar_postalsuc" disabled>
                      
                    </div>
        
                
                    
                    <div class="col-sm-2" style="margin-top: 15px">
                        <div><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                        <div><button type="button" class="btn btn-default buscar_codpostal_suc" style="margin-bottom: -47px; margin-left: -16px;">
                            <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                        <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                    </div>
        
                </div>
                <br>
                <div class="resultado_postal_suc"></div>
                <div class="error" style="color:crimson; text-align: center;"></div>
                
            </div>
                
          
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-info" data-dismiss="modal" style="background-color:#268fa9;color: aliceblue;">Salir</button>
            </div>
                            
        </div>
    </div>
</div>

@extends('layout.base')
<?php
//dd(date('Y-m-d', strtotime($presidente->soc_fecha_nacimiento)));
$today = date('Y-m-d');
//dd($documentos);
if(isset($documentos)){
    foreach($documentos as $i => $valor){
        if($documentos[$i]->cod_doc == 'FA01'){
            //dd($documentos[$i]->descripcion);
        }
    }
}
if(isset($datos) && $datos->confirmada == 0){
    $ver = 'inline';
    $color = 'rgb(105, 191, 105)';
}elseif($datos->confirmada == 3){
    $ver = 'inline';
    $color = 'red';
}else{
    $ver = 'none';
    $color = 'rgb(105, 191, 105)';
}
///dd($documentos);
use Carbon\Carbon;
?>

@section('content')
    <div class="content" style="position: relative;z-index: 100;">
        <div style="">
            <div class="jumbotron jumbotron-fluid" style="display:flex; padding-top:0px; padding-bottom:0px; margin-bottom:10px;" style="background-color: #F1F2F2">
                <div class="container" style="padding-bottom: 10px;padding-top: 10px;">
                    <img src="img/jumbo.png" class="float-right d-none d-lg-block" style="margin-left: -600px; height: 100%;"> {{--radial-gradient(circle, black 0%, transparent 85%);--}}
                    <h1 class="display-6">Tarjeta Prepaga y de Crédito Mutual</h1>
                    <p class="lead">MasterCard "FAUM Con Vos", Al resguardo del bien mutuo, FAUM CON VOS, siempre a tu lado.</p>
                                 
                </div>
            </div>
        </div>
    </div>

    <div class="container" style="position: relative;z-index: -100;" id="form">
        
        @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" data-toggle="tooltip" data-placement="top" title="Salir de la Notificación">
                    <i class='far fa-times-circle' style='font-size:24px;color:red'></i>
                </button>	
                <ul>
                    @foreach ($errors->all() as $err)
                    <li>{{ $err }}</li>
                    @endforeach
                </ul>
            </div>
            
        @endif
    
               
        <h4>Alta de Mutual Adherente (<label style="font-size: initial;"> Estado Actual: <strong style="color:{{ $color }}" id="estado-act">{{ $datos->estado_actual }}</strong></label>)</h4>
        <div class="mensaje_s">
            <strong class="small-font" id="salida" style="color:rgb(75 191 101);background-color:#f7eeee;"></strong>
        </div>
        
        <div class="response"></div>
        

        <div class="mensaje_e">
            <strong class="small-font" id="error" style="color:red;background-color:#f7eeee;"></strong>
        </div>
                    
        <!--<form action="#" method="POST" autocomplete="off"> -->
                
            @csrf
           <div class="container" >
                <ul class="nav nav-tabs" id="myTab" role="tablist" >
                    <li class="nav-item">
                        <a href="#personal" data-toggle="tab" class="nav-link active link-act" role="tab" aria-controls="personal" aria-selected="true" id="personal-tab" style="">Datos Generales</a>
                    </li>
                        <li class="nav-item">
                            <a href="#sucursales" data-toggle="tab" class="nav-link" role="tab" aria-controls="sucursales" aria-selected="false" id="sucursales-tab" style="">Sucursales</a>
                        </li>
                        <li class="nav-item">
                            <a href="#presidente" data-toggle="tab" class="nav-link" role="tab" aria-controls="presidente" aria-selected="false" id="presidente-tab">Presidente</a>
                        </li>
                        <li class="nav-item">
                            <a href="#secretario" data-toggle="tab" class="nav-link" role="tab" aria-controls="secretario" aria-selected="false" id="secretario-tab">Secretario</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tesorero" data-toggle="tab" class="nav-link" role="tab" aria-controls="tesorero" aria-selected="false" id="tesorero-tab">Tesorero</a>
                        </li>
                        <li class="nav-item">
                            <a href="#apoderado"data-toggle="tab" class="nav-link" role="tab" aria-controls="apoderado" aria-selected="false" id="apoderado-tab">Apoderado</a>
                        </li>
                        <li class="nav-item">
                            <a href="#documentos"data-toggle="tab" class="nav-link" role="tab" aria-controls="documentos" aria-selected="false" id="documentos-tab">Adjuntar Documentos</a>
                        </li>
                        
                    </ul>
                        
                </div>
                            
                <div class="tab-content" id="myTabContent" style="width: 1036px"> <!--93% -->

                    <div class="tab-pane fade show active" style="background-color: #e9ecef;" id="personal" role="tabpanel" aria-labelledby="personal-tab">
                        <fieldset style="width:100%;">
                            <div class="row mrow" style="padding-top: 10px;">
                                
                            
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                <div><label for="" class="help-block text-muted small-font margen_bot">N° Adherente</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq" id="soc_numero_socio" name="soc_numero_socio" placeholder="N° de Socio" value="{{$datos->soc_numero_socio  }}" maxlength="5" readonly></div>
                                    <input type="hidden" id="soc_sucursal" name="soc_sucursal" value="">
                                </div>
    
                                    
                                <div class="col-md-5 col-sm-4 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Nombre</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="nombre" name="nombre" placeholder="Nombre del Socio" maxlength="50" value="{{$datos->nombre  }}" disabled> 
                                    </div>
                                    
                                </div>

                                <div class="col-md-2 col-sm-1 col-xs-1">
                                    <div><label for="state_id" class="control-label small-font margen_bot">CUIT</label></div>
                                    <div><select class="form-control form-control-sm margen_izq" id="tipo_cuit" name="tipo_cuit" readonly>
                                         <option value="8" @if(isset($datos->tipo_cuit) && $datos->tipo_cuit == 8) selected @endif>CUIT</option>>
                                        
                                    </select></div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">N°</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq validar_numero" id="nro_cuit" name="nro_cuit" placeholder="Nro de documento" maxlength="15" value="{{$datos->nro_cuit }}" maxlength="20" required></div>
                                   
                                    
                                </div>
                            </div>
                        
                            <div class="row mrow">
                                    
                               
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">Código Postal</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq" id="codigo_postal" name="codigo_postal" placeholder="Código Postal" value="{{$datos->codigo_postal }}" maxlength="8" disabled></div>
                                </div>
                                
    
                                <!--<div class="col-md-1 col-sm-1 col-xs-1">
                                    <div style="margin-bottom: 20px"><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                                    <div><button type="button" class="btn btn-default buscar_codpostall" style="" data-toggle="modal" data-target="#myModal">
                                        <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                                    <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                                </div>-->
                                
    
                                <div class="col-md-3 col-sm-2 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Localidad</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="localidad_nombre_localidad" name="localidad_nombre_localidad" placeholder="" maxlength="60" value="{{$datos->localidad  }}" disabled> 
                                        
                                    </div>
                                    
                                </div>
    
                                <div class="col-md-3 col-sm-2 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Provincia</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="provincia_nombre_provincia" name="provincia_nombre_provincia" placeholder="" maxlength="60" value="{{$datos->provincia}}" disabled> 
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="row mrow">
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <label for="" class="help-block text-muted small-font margen_bot">Domicilio</label>
                                    <input type="text" class="form-control form-control-sm margen_izq" id="domicilio_calle" name="domicilio_calle" placeholder="Domicilio Socio" maxlength="50" value="{{$datos->domicilio_calle}}" required> 
                                </div>
        
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label for="" class="help-block text-muted small-font margen_bot">N°</label>
                                    <input type="text" class="form-control form-control-sm margen_izq" id="domicilio_nro" name="domicilio_nro" placeholder="Nro Domicilio" maxlength="10" value="{{$datos->domicilio_nro}}" required> 
                                </div>
    
                                <div class="col-md-1 col-sm-1 col-xs-1">
                                    <label for="" class="help-block text-muted small-font margen_bot">Piso</label>
                                    <input type="text" class="form-control form-control-sm margen_izq" id="domicilio_piso" name="domicilio_piso" placeholder="Piso" maxlength="5" value="{{$datos->domicilio_piso}}"> 
                                </div>
    
                                <div class="col-md-1 col-sm-1 col-xs-1">
                                    <label for="" class="help-block text-muted small-font margen_bot">Depto</label>
                                    <input type="text" class="form-control form-control-sm margen_izq" id="domicilio_depto" name="domicilio_depto" placeholder="Depto" maxlength="3" value="{{$datos->domicilio_depto}}"> 
                                </div>
                                
                            </div>
        
                            <div class="row mrow">
                                   
                                <div class="col-md-7 col-sm-7 col-xs-7">
                                    <label for="" class="help-block text-muted small-font margen_bot">Adicional Domicilio</label>
                                    <input type="text" class="form-control form-control-sm margen_izq" id="domicilio_adicional" name="domicilio_adicional" placeholder="Adicional Domicilio" maxlength="200" value="{{$datos->domicilio_adicional}}"> 
                                </div>
                                    
                            </div>
                            
                            <div class="row mrow">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Tel Oficina</label>
                                    <input type="text" class="form-control form-control-sm margen_izq validar_numero" id="telefono1" name="telefono1" placeholder="" value="@if(isset($datos->telefono1)){{$datos->telefono1}}@endif" maxlength="18" required> 
                                </div>
    
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Tel Celular</label>
                                    <input type="text" class="form-control form-control-sm margen_izq validar_numero" id="telefono_celular" name="telefono_celular" placeholder="" value="{{$datos->telefono_celular}}" maxlength="18" required>
                                </div>
    
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Teléfono 2</label>
                                    <input type="text" class="form-control form-control-sm margen_izq validar_numero" id="telefono2" name="telefono2" placeholder="" value="{{$datos->telefono2}}" maxlength="18"> 
                                </div>
    
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Teléfono 3</label>
                                    <input type="text" class="form-control form-control-sm margen_izq validar_numero" id="telefono3" name="telefono3" placeholder="" value="{{$datos->telefono3}}" maxlength="18"> 
                                </div>
    
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">E-mail</label></div>
                                    <div><input type="email" class="form-control form-control-sm margen_izq" id="email" name="email" placeholder="" value="{{$datos->email}}" maxlength="40" required></div>
                                </div>
    
                              
                            </div>

                            <div class="row mrow">
                                   
                                <div class="col-md-4 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Actividad s/UIF</label>
                                    <select class="form-control form-control-sm margen_izq" id="uif_actividades" name="uif_actividades" disabled>
                                        <option value="" selected>Seleccione</option>
                                        @foreach ($uif_act as $p)
                                            <option value="{{ $p->codigo }}" @if($p->codigo == 51) selected @endif>{{ $p->nombre }}</option>       
                                        @endforeach
                                    </select> 
                                </div>
                                    
                            </div>


                            <div class="row mrow">
                                <div class="col-md-3 col-sm-2 col-xs-1">
                                    <label for="" class="help-block text-muted small-font margen_bot">Condición IVA</label>
                                    <select class="form-control form-control-sm margen_izq" id="cond_iva" name="cond_iva" required>
                                        <option value=""   @if(isset($datos->cond_iva) && $datos->cond_iva == 'NULL') selected @endif)>Seleccione</option>
                                        <option value="1"  @if(isset($datos->cond_iva) && $datos->cond_iva == 1) selected @endif)>Responsable Inscripto</option>
                                        <option value="2"  @if(isset($datos->cond_iva) && $datos->cond_iva == 2) selected @endif)>Responsable No Inscripto</option>
                                        <option value="3"  @if(isset($datos->cond_iva) && $datos->cond_iva == 3) selected @endif)>No Responsable</option>
                                        <option value="4"  @if(isset($datos->cond_iva) && $datos->cond_iva == 4) selected @endif)>Consumidor Final</option>
                                        <option value="5"  @if(isset($datos->cond_iva) && $datos->cond_iva == 5) selected @endif)>Exento</option>
                                        <option value="6"  @if(isset($datos->cond_iva) && $datos->cond_iva == 6) selected @endif)>Monotributista</option>
                                        <option value="7"  @if(isset($datos->cond_iva) && $datos->cond_iva == 7) selected @endif)>No Recategorizado</option>
                                    </select> 
                                </div>
    
                                   
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label for="" class="help-block text-muted small-font margen_bot">Inicio Ejerccio</label>
                                    <select class="form-control form-control-sm margen_izq" id="soc_ejercicio_economico" name="soc_ejercicio_economico">
                                        <option value="" selected>Seleccione</option>
                                        <option value="1" @if(isset($datos->soc_ejercicio_economico) && $datos->soc_ejercicio_economico == 1) selected @endif)>Enero</option>
                                        <option value="2" @if(isset($datos->soc_ejercicio_economico) && $datos->soc_ejercicio_economico == 2) selected @endif)>Febrero</option>
                                        <option value="3" @if(isset($datos->soc_ejercicio_economico) && $datos->soc_ejercicio_economico == 3) selected @endif)>Marzo</option>
                                        <option value="4" @if(isset($datos->soc_ejercicio_economico) && $datos->soc_ejercicio_economico == 4) selected @endif)>Abril</option>
                                        <option value="5" @if(isset($datos->soc_ejercicio_economico) && $datos->soc_ejercicio_economico == 5) selected @endif)>Mayo</option>
                                        <option value="6" @if(isset($datos->soc_ejercicio_economico) && $datos->soc_ejercicio_economico == 6) selected @endif)>Junio</option>
                                        <option value="7" @if(isset($datos->soc_ejercicio_economico) && $datos->soc_ejercicio_economico == 7) selected @endif)>Julio</option>
                                        <option value="8" @if(isset($datos->soc_ejercicio_economico) && $datos->soc_ejercicio_economico == 8) selected @endif)>Agosto</option>
                                        <option value="9" @if(isset($datos->soc_ejercicio_economico) && $datos->soc_ejercicio_economico == 9) selected @endif)>Septiembre</option>
                                        <option value="10" @if(isset($datos->soc_ejercicio_economico) && $datos->soc_ejercicio_economico == 10) selected @endif)>Octubre</option>
                                        <option value="11" @if(isset($datos->soc_ejercicio_economico) && $datos->soc_ejercicio_economico == 11) selected @endif)>Noviembre</option>
                                        <option value="12" @if(isset($datos->soc_ejercicio_economico) && $datos->soc_ejercicio_economico == 12) selected @endif)>Diciembre</option>
                                    </select> 
                                </div>

                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Fecha Constitución</label>
                                    <input type="date" class="form-control form-control-sm form-control-sm margen_izq" id="soc_jud_fecha_constitucion" name="soc_jud_fecha_constitucion" placeholder="" value="@if(isset($datos->soc_jud_fecha_constitucion)){{date('Y-m-d', strtotime($datos->soc_jud_fecha_constitucion))}}@else{{ $today }}@endif">
                                </div>                                                                                                                                                                       

                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Matrícula  INAES</label>
                                    <input type="text" class="form-control form-control-sm form-control-sm margen_izq" id="soc_jud_inscripcion_inaes" name="soc_jud_inscripcion_inaes" placeholder="" value="{{$datos->soc_jud_nro_inscripcion}}">
                                </div>
                                            
                            </div>
                            @if(isset($datos->datos_completos) && $datos->datos_completos > 0)
                                <div class="row mrow">
                                    <div class="col-md-11 col-sm-8 col-xs-4" style="text-align: right;">
                                        <label class="help-block text-muted small-font margen_bot margen_izq">Descargar Solicitud FA01</label>
                                        <form action="/admin/imprimir-datos-generales" method="POST" autocomplete="off">
                                            @csrf
                                            <div class="col-md-11 col-sm-8 col-xs-4" style="text-align: right;"> 
                                                <input type="hidden" id="nro_persona_m" name="nro_persona_m" value="{{ $datos->nro_persona }}">
                                                <button type="submit" class="btn margen_izq" style="margin-top: -34px;margin-right: -125px;" data-toggle="tooltip" data-placement="top" title="Datos Generales de la Mutual"><i class="fa fa-file-pdf-o"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            @endif
                                        
                        </fieldset>
                        <div class="col-md-12 col-sm-8 col-xs-4" style="padding-top: 15px;text-align: end;padding-bottom: 5px;"> 
                            <button type="button" class="btn boton-guardar guardar-datos-generales" style="background-color:#268fa9;float: inline-end;color: aliceblue;display:{{ $ver }}">Guardar</button>
                        </div>
                    </div>
                    <div class="tab-pane fade suc" style="background-color: #e9ecef;" id="sucursales" role="tabpanel" aria-labelledby="sucursales-tab">
                        <fieldset style="width:100%;">
                            <?php //dd(count($sucursales))
                                    $c = 0;
                                ?>
                            @if(isset($sucursales) && count($sucursales) > 0)
                                @foreach($sucursales as $i => $valor)
                                    <?php $c = $c + 1; ?>
                                    <div class="row mrow" style="padding-top: 10px;">
                             
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <div><label for="" class="help-block text-muted small-font margen_bot">N° Sucursal Adherente</label></div>
                                            <div><input type="text" class="form-control form-control-sm margen_izq" id="soc_sucursal{{ $i }}" name="soc_sucursal{{ $i }}" placeholder="N° de Sucursal" value="{{$valor->soc_sucursal}}" maxlength="5" readonly></div>
                                        </div>
            
                                            
                                        <div class="col-md-6 col-sm-4 col-xs-2">
                              
                                            <div>
                                                <label for="" class="help-block text-muted small-font margen_bot">Nombre</label>
                                                <input type="text" class="form-control form-control-sm margen_izq" id="nombre{{ $i }}" name="nombre{{ $i }}" placeholder="Nombre de la Sucursal" maxlength="50" value="{{ $valor->nombre }}" readonly> 
                                            </div>

                                            
                                        </div>
                                    </div>

                                    <div class="row  mrow" style="padding-top: 10px;">
                    
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <div><label for="" class="help-block text-muted small-font margen_bot">Código Postal</label></div>
                                            <div><input type="text" class="form-control form-control-sm margen_izq" id="codigo_postal{{ $i }}" name="codigo_postal{{ $i }}" placeholder="Código Postal" value="{{$valor->codigo_postal }}" maxlength="8" disabled required></div>
                                        </div>

                                        <div class="col-md-3 col-sm-2 col-xs-2">
                                            <div>
                                                <label for="" class="help-block text-muted small-font margen_bot">Localidad</label>
                                                <input type="text" class="form-control form-control-sm margen_izq" id="localidad_nombre_localidad{{ $i }}" name="localidad_nombre_localidad{{ $i }}" placeholder="" maxlength="60" value="{{$valor->localidad  }}" disabled> 
                                                <input id="provincia_doc{{ $i }}" name="provincia_doc{{ $i }}" type="hidden" value="0">
                                            </div>
                                          
                                        </div>
                
                                        <div class="col-md-3 col-sm-2 col-xs-2">
                                            <div>
                                                <label for="" class="help-block text-muted small-font margen_bot">Provincia</label>
                                                <input type="text" class="form-control form-control-sm margen_izq" id="provincia_nombre_provincia{{ $i }}" name="provincia_nombre_provincia{{ $i }}" placeholder="" maxlength="60" value="{{$valor->provincia}}" disabled> 
                                            </div>
                                                
                                        </div>
                                    </div>

                                    <div class="row  mrow" style="padding-top: 10px;">
                                        <div class="col-md-5 col-sm-5 col-xs-5">
                                            <label for="" class="help-block text-muted small-font margen_bot">Domicilio</label>
                                            <input type="text" class="form-control form-control-sm margen_izq" id="domicilio_calle{{ $i }}" name="domicilio_calle{{ $i }}" placeholder="Domicilio Sucursal" value="{{$valor->domicilio_calle}}" required> 
                                        </div>
                 
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <label for="" class="help-block text-muted small-font margen_bot">N°</label>
                                            <input type="text" class="form-control form-control-sm margen_izq" id="domicilio_nro{{ $i }}" name="domicilio_nro{{ $i }}" placeholder="Nro Domicilio" maxlength="10" value="{{$valor->domicilio_nro}}" required> 
                                        </div>
                
                                        <div class="col-md-1 col-sm-1 col-xs-1">
                                            <label for="" class="help-block text-muted small-font margen_bot">Piso</label>
                                            <input type="text" class="form-control form-control-sm margen_izq" id="domicilio_piso{{ $i }}" name="domicilio_piso{{ $i }}" placeholder="Piso" maxlength="5" value="{{$valor->domicilio_piso}}"> 
                                        </div>
                
                                        <div class="col-md-1 col-sm-1 col-xs-1">
                                            <label for="" class="help-block text-muted small-font margen_bot">Depto</label>
                                            <input type="text" class="form-control form-control-sm margen_izq" id="domicilio_depto{{ $i }}" name="domicilio_depto{{ $i }}" placeholder="Depto" maxlength="3" value="{{$valor->domicilio_depto}}"> 
                                        </div>
                                            
                                    </div>
                    
                                    <div class="row mrow" style="padding-top: 10px;">
                                               
                                        <div class="col-md-7 col-sm-7 col-xs-7">
                                            <label for="" class="help-block text-muted small-font margen_bot">Adicional Domicilio</label>
                                            <input type="text" class="form-control form-control-sm margen_izq" id="domicilio_adicional{{ $i }}" name="domicilio_adicional{{ $i }}" placeholder="Adicional Domicilio Sucursal" maxlength="200" value="{{$valor->domicilio_adicional}}"> 
                                        </div>
                                                
                                    </div>
                                        
                                    <div class="row  mrow" style="padding-top: 10px;">
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <label class="help-block text-muted small-font margen_bot">Tel Oficina</label>
                                            <input type="text" class="form-control form-control-sm margen_izq validar_numero" id="telefono1{{ $i }}" name="telefono1{{ $i }}" placeholder="" value="{{$valor->telefono1}}" maxlength="18" required> 
                                        </div>
                
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <label class="help-block text-muted small-font margen_bot">Tel Celular</label>
                                            <input type="text" class="form-control form-control-sm margen_izq validar_numero" id="telefono_celular{{ $i }}" name="telefono_celular{{ $i }}" placeholder="" value="{{$valor->telefono_celular}}" maxlength="18" required> 
                                        </div>
                                                                            
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <div><label for="" class="help-block text-muted small-font margen_bot">E-mail</label></div>
                                            <div><input type="email" class="form-control form-control-sm margen_izq" id="email{{ $i }}" name="email{{ $i }}" placeholder="" value="{{$valor->email}}" maxlength="40" required></div>
                                        </div>
                                        
                                    </div>
                                   
                                    <hr>
                                   
                                
                                        
                                @endforeach
                                
                                
                            @else 
                                <div style="padding-top: 10px;" >
                                    <strong class="" style="margin-left: 364px;font: inherit;color:red" id="error">Esta Mutual no posse Sucursales Adherente.</strong>
                                </div><br>
                            @endif
                          

                        </fieldset>
                        @if(isset($sucursales) && count($sucursales) > 0)
                            <div class="col-md-12 col-sm-8 col-xs-4" style="padding-top: 15px;text-align: end;padding-bottom: 5px;"> 
                                <button type="button" class="btn boton-guardar guardar-datos-sucursales" style="background-color:#268fa9;float: inline-end;color: aliceblue;display:{{ $ver }}">Guardar</button>
                            </div>
                        @endif
                    </div> 
                    
                    <div class="tab-pane fade" style="background-color: #e9ecef;" id="presidente" role="tabpanel" aria-labelledby="presidente-tab"> <!--presidente -->
                        <fieldset style="width:100%;">
                            <div class="row mrow" style="padding-top: 10px;">
                                <div class="col-md-5 col-sm-4 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Apellido y Nombre</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="nombre_pre" name="nombre_pre" placeholder="Nombre del Presidente" maxlength="50" value="@if(isset($presidente->nombre)) {{$presidente->nombre}} @endif" required> 
                                    </div>
                                    
                                </div>
                                <div class="col-md-2 col-sm-1 col-xs-1">
                                    <div><label for="state_id" class="control-label text-muted small-font margen_bot">CUIT/CUIL</label></div>
                                    <div>
                                        <select class="form-control form-control-sm margen_izq" id="tipo_cuit_pre" name="tipo_cuit_pre" required>
                                            <option value="7" @if(isset($presidente->tipo_cuit) && $presidente->tipo_cuit == 7) selected @endif>CUIL</option>>
                                            <option value="8" @if(isset($presidente->tipo_cuit) && $presidente->tipo_cuit == 8) selected @endif>CUIT</option>>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">N°</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq validar_numero" id="nro_cuit_pre" name="nro_cuit_pre" placeholder="Nro CUIL/CUIT" maxlength="15" value="@if(isset($presidente->nro_cuit)) {{$presidente->nro_cuit}} @endif" maxlength="20" required></div>
                                   
                                    
                                </div>
                               
                       
                                
                            </div>
                            <div class="row mrow" >
                                <div class="col-md-3 col-sm-2 col-xs-1">
                                    <div><label for="state_id" class="control-label text-muted small-font margen_bot">Tipo de Documento</label></div>
                                    <div><select class="form-control form-control-sm margen_izq" id="tipo_doc_pre" name="tipo_doc_pre" required>
                                        <option value="" selected>Seleccione</option>
                                        <option value="0"  @if(isset($presidente->tipo_doc) && $presidente->tipo_doc == 0) selected @endif>No Informado</option>
                                        <option value="1"  @if(isset($presidente->tipo_doc) && $presidente->tipo_doc == 1) selected @endif>D.N.I</option>
                                        <option value="2"  @if(isset($presidente->tipo_doc) && $presidente->tipo_doc == 2) selected @endif>CI</option>
                                        <option value="4"  @if(isset($presidente->tipo_doc) && $presidente->tipo_doc == 4) selected @endif>PASAPORTE</option>
                                        <option value="6"  @if(isset($presidente->tipo_doc) && $presidente->tipo_doc == 6) selected @endif>RUT</option>
                                        <option value="7"  @if(isset($presidente->tipo_doc) && $presidente->tipo_doc == 7) selected @endif>CUIL</option>
                                        <option value="8"  @if(isset($presidente->tipo_doc) && $presidente->tipo_doc == 8) selected @endif>CUIT</option>
                                        <option value="9"  @if(isset($presidente->tipo_doc) && $presidente->tipo_doc == 9) selected @endif>OTRO</option>
                                        
                                    </select></div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">N°</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq validar_numero" id="nro_doc_pre" name="nro_doc_pre" placeholder="Nro de documento" maxlength="15" value="@if(isset($presidente->nro_doc)) {{$presidente->nro_doc}} @endif" maxlength="20" required></div>
                                                                    
                                </div>         

                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">Sexo</label></div>
                                    <div>
                                        <select class="form-control form-control-sm margen_izq" id="sexo_pre" name="sexo_pre" required>
                                        <option value="" selected>Seleccione</option>
                                        <option value="2" @if(isset($presidente->sexo) && $presidente->sexo == 2) selected @endif>Femenino</option>
                                        <option value="1" @if(isset($presidente->sexo) && $presidente->sexo == 1) selected @endif>Masculino</option>
                                    </select>
                                    </div>
                                </div>

                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Fecha Nac</label>
                                    <input type="date" class="form-control form-control-sm form-control-sm margen_izq" id="fecha_nac_pre" name="fecha_nac_pre" placeholder=""  value="@if(isset($presidente->soc_fecha_nacimiento)){{$presidente->soc_fecha_nacimiento}}@else{{ $today }}@endif">
                                </div>

                            </div>

                            <div class="row mrow" >
                                <div class="col-md-5 col-sm-4 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Lugar de Nacimiento</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="lugar_nac_pre" name="lugar_nac_pre" placeholder="Lugar de Nacimiento" maxlength="50" value="@if(isset($presidente->soc_lugar_nacimiento)) {{$presidente->soc_lugar_nacimiento}} @endif"> 
                                    </div>
                                    
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">Nacionalidad</label></div>
                                    <div>
                                        <select class="form-control form-control-sm margen_izq" id="nacionalidad_pre" name="nacionalidad_pre" required>
                                            <option value="" selected>Seleccione</option>
                                            <option value="32" @if(isset($presidente->cod_nacionalidad) && $presidente->cod_nacionalidad == 32) selected @endif>Argentina</option>
                                            <option value="1"  @if(isset($presidente->cod_nacionalidad) && $presidente->cod_nacionalidad == 1) selected @endif>Otra</option>
                                        </select>
                                    </div>
                                </div>
                            
                            </div>

                            <div class="row mrow">
                                                          
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">Código Postal</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq" id="codigo_postal_pre" name="codigo_postal_pre" placeholder="Código Postal"  maxlength="8" readonly value="@if(isset($presidente->codigo_postal)) {{$presidente->codigo_postal}} @endif" required></div>
                                    <input class="cargopre" type="hidden" value="8">
                                </div>
                                
    
                                <div class="col-md-1 col-sm-1 col-xs-1">
                                    <div style="margin-bottom: 20px"><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                                    <div><button type="button" class="btn btn-default buscar_codpostal_pre" style="" data-toggle="modal" data-target="#myModalpre">
                                        <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                                    <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                                </div>
                                
    
                                <div class="col-md-3 col-sm-2 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Localidad</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="localidad_nombre_localidad_pre" name="localidad_nombre_localidad_pre" placeholder="" maxlength="60" disabled value="@if(isset($presidente->localidad)) {{$presidente->localidad}} @endif"> 
                                        
                                    </div>
                                    
                                </div>
    
                                <div class="col-md-3 col-sm-2 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Provincia</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="provincia_nombre_provincia_pre" name="provincia_nombre_provincia_pre" placeholder="" maxlength="60"  disabled value="@if(isset($presidente->provincia)) {{$presidente->provincia}} @endif">  
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="row  mrow" style="">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Tel Celular</label>
                                    <input type="text" class="form-control form-control-sm margen_izq validar_numero validate" id="telefono_celular_pre" name="telefono_celular_pre" placeholder="" value="@if(isset($presidente->telefono_celular)) {{$presidente->telefono_celular}} @endif" maxlength="18" required> 
                                </div>

                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Teléfono</label>
                                    <input type="text" class="form-control form-control-sm margen_izq validar_numero" id="telefono1_pre" name="telefono1_pre" placeholder="" value="@if(isset($presidente->telefono1)) {{$presidente->telefono1}} @endif" maxlength="18" required> 
                                </div>
                                                                    
                                <div class="col-md-5 col-sm-3 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">E-mail</label></div>
                                    <div><input type="email" class="form-control form-control-sm margen_izq" id="email_pre" name="email_pre" placeholder="" value="@if(isset($presidente->email)) {{$presidente->email}} @endif" maxlength="40" required></div>
                                </div>
                                
                            </div>

                            <div class="row mrow" >
                                <div class="col-md-4 col-sm-3 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Empresa donde trabaja</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="empresa_pre" name="empresa_pre" placeholder="Nombre de Empresa" maxlength="50" value="@if(isset($presidente->soc_lugar_trabajo)) {{$presidente->soc_lugar_trabajo}} @endif" required> 
                                    </div>
                                    
                                </div>

                                <div class="col-md-5 col-sm-3 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Función / Cargo</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="cargo_pre" name="cargo_pre" placeholder="Nombre de Empresa" maxlength="50" value="@if(isset($presidente->soc_jud_actividad_principal)) {{$presidente->soc_jud_actividad_principal}} @endif" required> 
                                        <!--<div>
                                            <select class="form-control form-control-sm margen_izq" id="cargo_pre" name="cargo_pre">
                                                <option value="" selected>Seleccione</option>
                                                <option value="8"> Presidente</option>
                                                <option value="6"> Secretario</option>
                                                <option value="7"> Tesorero</option>
                                                <option value="1"> Apoderado</option>
                                            </select>
                                        </div>-->
                                    </div>
                                    
                                </div>
                            </div>

                            <hr>
                            <div class="row mrow">
                                <div>
                                    <h2  style="font-size: 14px;margin-left: 42px;margin-left: 26px;margin-top: revert;">Declaro bajo juramento que mi actividad / profesional es:</h2>
                                </div>
                            </div>

                            <div class="row mrow">

                                <div class="col-md-4 col-sm-3 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Actividad</label>
                                    <select class="form-control form-control-sm margen_izq" id="uif_actividades_pre" name="uif_actividades_pre" required>
                                        <option value="" selected>Seleccione</option>
                                        @foreach ($uif_act as $p)
                                            <option value="{{ $p->codigo }}" @if(isset($presidente->soc_actividad_uif) && $presidente->soc_actividad_uif == $p->codigo) selected @endif>{{ $p->nombre }}</option>       
                                        @endforeach
                                    </select> 
                                </div>

                                <div class="col-md-4 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Rubro</label>
                                    <select class="form-control form-control-sm margen_izq" id="rubro_pre" name="rubro_pre">
                                        <option value="" selected>Seleccione</option>
                                        @foreach ($rubros as $p)
                                            <option value="{{ $p->id_rubro }}" @if(isset($presidente->com_rubro) && $presidente->com_rubro == $p->id_rubro) selected @endif>{{ $p->descripcion }}</option>       
                                        @endforeach
                                    </select> 
                                </div>
                                <div class="col-md-3 col-sm-2 col-xs-2" style="display: none" id="clase_rubro_pre">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Nuevo Rubro</label>
                                        <input type="text" class="form-control form-control-sm" id="nuevo_rubro_pre" name="nuevo_rubro_pre" placeholder="Escriba el nuevo rubro" maxlength="50" value=""> 
                                    </div>
                                    
                                </div>
                            </div>
                            <hr>
                            <div class="row mrow">
                                <div>
                                    <h2  style="font-size: 14px;margin-left: 42px;margin-left: 26px;margin-top: revert;">Declaro bajo juramento que mi estado civil es:</h2>
                                </div>
                            </div>

                            <div class="row mrow">
                                <div class="col-md-2 col-sm-2 col-xs-1">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">Estado Civil</label></div>
                                    <div>
                                        <select class="form-control form-control-sm margen_izq" id="edo_civil_pre" name="edo_civil_pre" required>
                                            <option value="" selected>Seleccione</option>
                                            <option value="0" @if(isset($presidente->estado_civil) && $presidente->estado_civil == 0) selected @endif>No Informado</option>
                                            <option value="1" @if(isset($presidente->estado_civil) && $presidente->estado_civil == 1) selected @endif>Soltero/a</option>
                                            <option value="2" @if(isset($presidente->estado_civil) && $presidente->estado_civil == 2) selected @endif>Casado/a</option>
                                            <option value="3" @if(isset($presidente->estado_civil) && $presidente->estado_civil == 3) selected @endif>Viudo/a</option>
                                            <option value="5" @if(isset($presidente->estado_civil) && $presidente->estado_civil == 5) selected @endif>Divorciado/a</option>
                                            <option value="6" @if(isset($presidente->estado_civil) && $presidente->estado_civil == 6) selected @endif>Unión Libre</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-3 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Apellido y Nombre Conyugue</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="conyugue_pre" name="conyugue_pre" placeholder="Nombre de Conyugue" maxlength="50" value="@if(isset($presidente->soc_conyuge_nombre)) {{$presidente->soc_conyuge_nombre}} @endif"> 
                                    </div>
                                    
                                </div>

                                <div class="col-md-2 col-sm-2 col-xs-1">
                                    <div><label for="state_id" class="control-label text-muted small-font margen_bot">Tipo de Documento</label></div>
                                    <div><select class="form-control form-control-sm margen_izq" id="tipo_doc_conyugue_pre" name="tipo_doc_conyugue_pre">
                                        <option value="" selected>Seleccione</option>
                                        <option value="0"  @if(isset($presidente->soc_conyuge_tipo_doc) && $presidente->soc_conyuge_tipo_doc == 0) selected @endif>No Informado</option>
                                        <option value="1"  @if(isset($presidente->soc_conyuge_tipo_doc) && $presidente->soc_conyuge_tipo_doc == 1) selected @endif>D.N.I</option>
                                        <option value="2"  @if(isset($presidente->soc_conyuge_tipo_doc) && $presidente->soc_conyuge_tipo_doc == 2) selected @endif>CI</option>
                                        <option value="4"  @if(isset($presidente->soc_conyuge_tipo_doc) && $presidente->soc_conyuge_tipo_doc == 4) selected @endif>PASAPORTE</option>
                                        <option value="6"  @if(isset($presidente->soc_conyuge_tipo_doc) && $presidente->soc_conyuge_tipo_doc == 6) selected @endif>RUT</option>
                                        <option value="7"  @if(isset($presidente->soc_conyuge_tipo_doc) && $presidente->soc_conyuge_tipo_doc == 7) selected @endif>CUIL</option>
                                        <option value="8"  @if(isset($presidente->soc_conyuge_tipo_doc) && $presidente->soc_conyuge_tipo_doc == 8) selected @endif>CUIT</option>
                                        <option value="9"  @if(isset($presidente->soc_conyuge_tipo_doc) && $presidente->soc_conyuge_tipo_doc == 9) selected @endif>OTRO</option>
                                        
                                    </select></div>
                                </div>

                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">N°</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq validar_numero" id="nro_doc_conyugue_pre" name="nro_doc_conyugue_pre" placeholder="Nro de documento" maxlength="15" value="@if(isset($presidente->soc_conyuge_nro_doc)) {{$presidente->soc_conyuge_nro_doc}} @endif" maxlength="20"></div>
                                                                    
                                </div>

                            </div>
                            <hr>
                            <div class="row mrow">
                                <!--<div class="col-md-4 col-sm-3 col-xs-2">
                                    <label class="control-label text-muted small-font margen_bot">Es Sujeto Obligado Prevención Lavado</label>
                                    <select class="form-control form-control-sm margen_izq" id="lavado_pre" name="lavado_pre" required>
                                        <option value="" selected>Seleccione</option>
                                        <option value="1" @if(isset($presidente->sujeto_obligado) && $presidente->sujeto_obligado == 1) selected @endif>Sí</option>
                                        <option value="0" @if(isset($presidente->sujeto_obligado) && $presidente->sujeto_obligado == 0) selected @endif>No</option>
                                    </select> 
                                </div>-->

                                @if(isset($presidente->sujeto_obligado) && $presidente->sujeto_obligado >= 0)
                                    <form action="/admin/imprimir-so" method="POST" autocomplete="off">
                                        @csrf
                                        <!--<div class="col-md-2 col-sm-2 col-xs-1"> 
                                                <input type="hidden" id="nro_personaso" name="nro_personaso" value="{{$presidente->nro_persona}}">
                                               
                                        
                                        </div>-->
                                    </form>
                                @endif

                                <div class="col-md-3 col-sm-2 col-xs-1">
                                    <label class="control-label text-muted small-font margen_bot">Reviste Calidad de Pep´s</label>
                                    <select class="form-control form-control-sm margen_izq" id="pep_pre" name="pep_pre" required>
                                        <option value="" selected>Seleccione</option>
                                        <option value="1" @if(isset($presidente->persona_pep) && $presidente->persona_pep == 1) selected @endif>Sí</option>
                                        <option value="0" @if(isset($presidente->persona_pep) && $presidente->persona_pep == 0) selected @endif>No</option>
                                    </select> 
                                </div>

                                @if(isset($presidente->persona_pep) && $presidente->persona_pep >= 0)
                                    <form action="/admin/imprimir-pep" method="POST" autocomplete="off">
                                        @csrf
                                        <div class="col-md-2 col-sm-2 col-xs-1"> 
                                                <input type="hidden" id="nro_personapepp" name="nro_personapepp" value="{{$presidente->nro_persona}}">
                                                <input type="hidden" id="p" name="p" value="p">
                                                <button type="submit" class="btn" style="margin-top: 31px;margin-left: -20px;" data-toggle="tooltip" data-placement="top" title="Persona Expuesta Políticamente"><i class="fa fa-file-pdf-o"></i></button>
                                        
                                        </div>
                                    </form>
                                @endif
                            </div>

                        </fieldset>
                        <div class="col-md-12 col-sm-8 col-xs-4" style="padding-top: 15px;text-align: end;padding-bottom: 5px;"> 
                            <button type="button" class="btn boton-guardar guardar-datos-presidente" style="background-color:#268fa9;float: inline-end;color: aliceblue;display:{{ $ver }}">Guardar</button>
                        </div>
                    </div> 
                    
                    
                    <div class="tab-pane fade" style="background-color: #e9ecef;" id="secretario" role="tabpanel" aria-labelledby="Secretario-tab">
                        <fieldset style="width:100%;">
                            <div class="row mrow" style="padding-top: 10px;">
                                <div class="col-md-5 col-sm-4 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Apellido y Nombre</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="nombre_sec" name="nombre_sec" placeholder="Nombre del secretario" maxlength="50" value="@if(isset($secretario->nombre)) {{$secretario->nombre}} @endif" required> 
                                        <!--<small class="form-text text-danger margen_izq">Error de nombre.</small> -->
                                    </div>
                                    
                                </div>
                                <div class="col-md-2 col-sm-1 col-xs-1">
                                    <div><label for="state_id" class="control-label text-muted small-font margen_bot">CUIT/CUIL</label></div>
                                    <div>
                                        <select class="form-control form-control-sm margen_izq" id="tipo_cuit_sec" name="tipo_cuit_sec" required>
                                            <option value="7" @if(isset($secretario->tipo_cuit) && $secretario->tipo_cuit == 7) selected @endif>CUIL</option>>
                                            <option value="8" @if(isset($secretario->tipo_cuit) && $secretario->tipo_cuit == 8) selected @endif>CUIT</option>>
                                        </select>
                                        <!--<small class="form-text text-danger margen_izq">Error de nombre.</small>-->
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">N°</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq validar_numero" id="nro_cuit_sec" name="nro_cuit_sec" placeholder="Nro CUIL/CUIT" maxlength="15" value="@if(isset($secretario->nro_cuit)) {{$secretario->nro_cuit}} @endif" maxlength="20" required></div>
                                   
                                    
                                </div>
                               
                        
                                
                            </div>
                            <div class="row mrow" >
                                <div class="col-md-3 col-sm-2 col-xs-1">
                                    <div><label for="state_id" class="control-label text-muted small-font margen_bot">Tipo de Documento</label></div>
                                    <div><select class="form-control form-control-sm margen_izq" id="tipo_doc_sec" name="tipo_doc_sec" required>
                                        <option value="" selected>Seleccione</option>
                                        <option value="0"  @if(isset($secretario->tipo_doc) && $secretario->tipo_doc == 0) selected @endif>No Informado</option>
                                        <option value="1"  @if(isset($secretario->tipo_doc) && $secretario->tipo_doc == 1) selected @endif>D.N.I</option>
                                        <option value="2"  @if(isset($secretario->tipo_doc) && $secretario->tipo_doc == 2) selected @endif>CI</option>
                                        <option value="4"  @if(isset($secretario->tipo_doc) && $secretario->tipo_doc == 4) selected @endif>PASAPORTE</option>
                                        <option value="6"  @if(isset($secretario->tipo_doc) && $secretario->tipo_doc == 6) selected @endif>RUT</option>
                                        <option value="7"  @if(isset($secretario->tipo_doc) && $secretario->tipo_doc == 7) selected @endif>CUIL</option>
                                        <option value="8"  @if(isset($secretario->tipo_doc) && $secretario->tipo_doc == 8) selected @endif>CUIT</option>
                                        <option value="9"  @if(isset($secretario->tipo_doc) && $secretario->tipo_doc == 9) selected @endif>OTRO</option>
                                        
                                    </select></div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">N°</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq validar_numero" id="nro_doc_sec" name="nro_doc_sec" placeholder="Nro de documento" maxlength="15" value="@if(isset($secretario->nro_doc)) {{$secretario->nro_doc}} @endif" maxlength="20"></div>
                                                                    
                                </div>         
                        
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">Sexo</label></div>
                                    <div>
                                        <select class="form-control form-control-sm margen_izq" id="sexo_sec" name="sexo_sec">
                                        <option value="" selected>Seleccione</option>
                                        <option value="2" @if(isset($secretario->sexo) && $secretario->sexo == 2) selected @endif>Femenino</option>
                                        <option value="1" @if(isset($secretario->sexo) && $secretario->sexo == 1) selected @endif>Masculino</option>
                                    </select>
                                    </div>
                                </div>
                        
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Fecha Nac</label>
                                    <input type="date" class="form-control form-control-sm form-control-sm margen_izq" id="fecha_nac_sec" name="fecha_nac_sec" placeholder=""  value="@if(isset($secretario->soc_fecha_nacimiento)){{$secretario->soc_fecha_nacimiento}}@else{{ $today }}@endif">
                                </div>
                        
                            </div>
                        
                            <div class="row mrow" >
                                <div class="col-md-5 col-sm-4 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Lugar de Nacimiento</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="lugar_nac_sec" name="lugar_nac_sec" placeholder="Lugar de Nacimiento" maxlength="50" value="@if(isset($secretario->soc_lugar_nacimiento)) {{$secretario->soc_lugar_nacimiento}} @endif"> 
                                    </div>
                                    
                                </div>
                        
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">Nacionalidad</label></div>
                                    <div>
                                        <select class="form-control form-control-sm margen_izq" id="nacionalidad_sec" name="nacionalidad_sec">
                                            <option value="" selected>Seleccione</option>
                                            <option value="32" @if(isset($secretario->cod_nacionalidad) && $secretario->cod_nacionalidad == 32) selected @endif>Argentina</option>
                                            <option value="1"  @if(isset($secretario->cod_nacionalidad) && $secretario->cod_nacionalidad == 1) selected @endif>Otra</option>
                                        </select>
                                    </div>
                                </div>
                            
                            </div>
                        
                            <div class="row mrow">
                                                          
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">Código Postal</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq" id="codigo_postal_sec" name="codigo_postal_sec" placeholder="Código Postal"  maxlength="8" readonly value="@if(isset($secretario->codigo_postal)) {{$secretario->codigo_postal}} @endif"></div>
                                    <input class="cargosec" type="hidden" value="6">
                                </div>
                                
                        
                                <div class="col-md-1 col-sm-1 col-xs-1">
                                    <div style="margin-bottom: 20px"><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                                    <div><button type="button" class="btn btn-default buscar_codpostal_sec" style="" data-toggle="modal" data-target="#myModalsec">
                                        <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                                    <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                                </div>
                                
                        
                                <div class="col-md-3 col-sm-2 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Localidad</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="localidad_nombre_localidad_sec" name="localidad_nombre_localidad_sec" placeholder="" maxlength="60" disabled value="@if(isset($secretario->localidad)) {{$secretario->localidad}} @endif"> 
                                        
                                    </div>
                                    
                                </div>
                        
                                <div class="col-md-3 col-sm-2 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Provincia</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="provincia_nombre_provincia_sec" name="provincia_nombre_provincia_sec" placeholder="" maxlength="60"  disabled value="@if(isset($secretario->provincia)) {{$secretario->provincia}} @endif">  
                                    </div>
                                    
                                </div>
                            </div>
                        
                            <div class="row  mrow" style="">

                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Tel Celular</label>
                                    <input type="text" class="form-control form-control-sm margen_izq validar_numero validate" id="telefono_celular_sec" name="telefono_celular_sec" placeholder="" value="@if(isset($secretario->telefono_celular)) {{$secretario->telefono_celular}} @endif" maxlength="18"> 
                                </div>

                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Teléfono</label>
                                    <input type="text" class="form-control form-control-sm margen_izq validar_numero" id="telefono1_sec" name="telefono1_sec" placeholder="" value="@if(isset($secretario->telefono1)) {{$secretario->telefono1}} @endif" maxlength="18"> 
                                </div>
                        
                                
                                                                    
                                <div class="col-md-5 col-sm-3 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">E-mail</label></div>
                                    <div><input type="email" class="form-control form-control-sm margen_izq" id="email_sec" name="email_sec" placeholder="" value="@if(isset($secretario->email)) {{$secretario->email}} @endif" maxlength="40"></div>
                                </div>
                                
                            </div>
                        
                            <div class="row mrow" >
                                <div class="col-md-4 col-sm-3 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Empresa donde trabaja</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="empresa_sec" name="empresa_sec" placeholder="Nombre de Empresa" maxlength="50" value="@if(isset($secretario->soc_lugar_trabajo)) {{$secretario->soc_lugar_trabajo}} @endif"> 
                                    </div>
                                    
                                </div>
                        
                                <div class="col-md-5 col-sm-3 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Función / Cargo</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="cargo_sec" name="cargo_sec" placeholder="Nombre de Empresa" maxlength="50" value="@if(isset($secretario->soc_jud_actividad_principal)) {{$secretario->soc_jud_actividad_principal}} @endif"> 
                                        <!--<div>
                                            <select class="form-control form-control-sm margen_izq" id="cargo_sec" name="cargo_sec">
                                                <option value="" selected>Seleccione</option>
                                                <option value="8"> secretario</option>
                                                <option value="6"> Secretario</option>
                                                <option value="7"> Tesorero</option>
                                                <option value="1"> Apoderado</option>
                                            </select>
                                        </div>-->
                                    </div>
                                    
                                </div>
                            </div>
                        
                            <hr>
                            <div class="row mrow">
                                <div>
                                    <h2  style="font-size: 14px;margin-left: 42px;margin-left: 26px;margin-top: revert;">Declaro bajo juramento que mi actividad / profesional es:</h2>
                                </div>
                            </div>
                        
                            <div class="row mrow">
                        
                                <div class="col-md-4 col-sm-3 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Actividad</label>
                                    <select class="form-control form-control-sm margen_izq" id="uif_actividades_sec" name="uif_actividades_sec" >
                                        <option value="" selected>Seleccione</option>
                                        @foreach ($uif_act as $p)
                                            <option value="{{ $p->codigo }}" @if(isset($secretario->soc_actividad_uif) && $secretario->soc_actividad_uif == $p->codigo) selected @endif>{{ $p->nombre }}</option>       
                                        @endforeach
                                    </select> 
                                </div>
                        
                                <div class="col-md-4 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Rubro</label>
                                    <select class="form-control form-control-sm margen_izq" id="rubro_sec" name="rubro_sec" >
                                        <option value="" selected>Seleccione</option>
                                        @foreach ($rubros as $p)
                                            <option value="{{ $p->id_rubro }}" @if(isset($secretario->com_rubro) && $secretario->com_rubro == $p->id_rubro) selected @endif>{{ $p->descripcion }}</option>       
                                        @endforeach
                                    </select> 
                                </div>

                                <div class="col-md-3 col-sm-2 col-xs-2" style="display: none" id="clase_rubro_sec">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Nuevo Rubro</label>
                                        <input type="text" class="form-control form-control-sm" id="nuevo_rubro_sec" name="nuevo_rubro_sec" placeholder="Escriba el nuevo rubro" maxlength="50" value=""> 
                                    </div>
                                    
                                </div>
                            </div>
                            <hr>
                            <div class="row mrow">
                                <div>
                                    <h2  style="font-size: 14px;margin-left: 42px;margin-left: 26px;margin-top: revert;">Declaro bajo juramento que mi estado civil es:</h2>
                                </div>
                            </div>
                        
                            <div class="row mrow">
                                <div class="col-md-2 col-sm-2 col-xs-1">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">Estado Civil</label></div>
                                    <div>
                                        <select class="form-control form-control-sm margen_izq" id="edo_civil_sec" name="edo_civil_sec">
                                            <option value="" selected>Seleccione</option>
                                            <option value="0" @if(isset($secretario->estado_civil) && $secretario->estado_civil == 0) selected @endif>No Informado</option>
                                            <option value="1" @if(isset($secretario->estado_civil) && $secretario->estado_civil == 1) selected @endif>Soltero/a</option>
                                            <option value="2" @if(isset($secretario->estado_civil) && $secretario->estado_civil == 2) selected @endif>Casado/a</option>
                                            <option value="3" @if(isset($secretario->estado_civil) && $secretario->estado_civil == 3) selected @endif>Viudo/a</option>
                                            <option value="5" @if(isset($secretario->estado_civil) && $secretario->estado_civil == 5) selected @endif>Divorciado/a</option>
                                            <option value="6" @if(isset($secretario->estado_civil) && $secretario->estado_civil == 6) selected @endif>Unión Libre</option>
                                        </select>
                                    </div>
                                </div>
                        
                                <div class="col-md-4 col-sm-3 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Apellido y Nombre Conyugue</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="conyugue_sec" name="conyugue_sec" placeholder="Nombre de Conyugue" maxlength="50" value="@if(isset($secretario->soc_conyuge_nombre)) {{$secretario->soc_conyuge_nombre}} @endif"> 
                                    </div>
                                    
                                </div>
                        
                                <div class="col-md-2 col-sm-2 col-xs-1">
                                    <div><label for="state_id" class="control-label text-muted small-font margen_bot">Tipo de Documento</label></div>
                                    <div><select class="form-control form-control-sm margen_izq" id="tipo_doc_conyugue_sec" name="tipo_doc_conyugue_sec">
                                        <option value="" selected>Seleccione</option>
                                        <option value="0"  @if(isset($secretario->soc_conyuge_tipo_doc) && $secretario->soc_conyuge_tipo_doc == 0) selected @endif>No Informado</option>
                                        <option value="1"  @if(isset($secretario->soc_conyuge_tipo_doc) && $secretario->soc_conyuge_tipo_doc == 1) selected @endif>D.N.I</option>
                                        <option value="2"  @if(isset($secretario->soc_conyuge_tipo_doc) && $secretario->soc_conyuge_tipo_doc == 2) selected @endif>CI</option>
                                        <option value="4"  @if(isset($secretario->soc_conyuge_tipo_doc) && $secretario->soc_conyuge_tipo_doc == 4) selected @endif>PASAPORTE</option>
                                        <option value="6"  @if(isset($secretario->soc_conyuge_tipo_doc) && $secretario->soc_conyuge_tipo_doc == 6) selected @endif>RUT</option>
                                        <option value="7"  @if(isset($secretario->soc_conyuge_tipo_doc) && $secretario->soc_conyuge_tipo_doc == 7) selected @endif>CUIL</option>
                                        <option value="8"  @if(isset($secretario->soc_conyuge_tipo_doc) && $secretario->soc_conyuge_tipo_doc == 8) selected @endif>CUIT</option>
                                        <option value="9"  @if(isset($secretario->soc_conyuge_tipo_doc) && $secretario->soc_conyuge_tipo_doc == 9) selected @endif>OTRO</option>
                                        
                                    </select></div>
                                </div>
                        
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">N°</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq validar_numero" id="nro_doc_conyugue_sec" name="nro_doc_conyugue_sec" placeholder="Nro de documento" maxlength="15" value="@if(isset($secretario->soc_conyuge_nro_doc)) {{$secretario->soc_conyuge_nro_doc}} @endif" maxlength="20"></div>
                                                                    
                                </div>
                        
                            </div>
                            <hr>
                            <div class="row mrow">
                                <!--<div class="col-md-4 col-sm-3 col-xs-2">
                                    <label class="control-label text-muted small-font margen_bot">Es Sujeto Obligado Prevención Lavado</label>
                                    <select class="form-control form-control-sm margen_izq" id="lavado_sec" name="lavado_sec">
                                        <option value="" selected>Seleccione</option>
                                        <option value="1" @if(isset($secretario->sujeto_obligado) && $secretario->sujeto_obligado == 1) selected @endif>Sí</option>
                                        <option value="0" @if(isset($secretario->sujeto_obligado) && $secretario->sujeto_obligado == 0) selected @endif>No</option>
                                    </select> 
                                </div>-->

                                @if(isset($secretario->sujeto_obligado) && $secretario->sujeto_obligado >= 0)
                                    <form action="/admin/imprimir-so" method="POST" autocomplete="off">
                                        @csrf
                                        <!--<div class="col-md-2 col-sm-2 col-xs-1"> 
                                                <input type="hidden" id="nro_personaso" name="nro_personaso" value="{{$secretario->nro_persona}}">
                                                
                                        
                                        </div>-->
                                    </form>
                                @endif
                        
                                <div class="col-md-3 col-sm-2 col-xs-1">
                                    <label class="control-label text-muted small-font margen_bot">Reviste Calidad de Pep´s</label>
                                    <select class="form-control form-control-sm margen_izq" id="pep_sec" name="pep_sec">
                                        <option value="" selected>Seleccione</option>
                                        <option value="1" @if(isset($secretario->persona_pep) && $secretario->persona_pep == 1) selected @endif>Sí</option>
                                        <option value="0" @if(isset($secretario->persona_pep) && $secretario->persona_pep == 0) selected @endif>No</option>
                                    </select> 
                                </div>

                                @if(isset($secretario->persona_pep) && $secretario->persona_pep >= 0)
                                    <form action="/admin/imprimir-pep" method="POST" autocomplete="off">
                                        @csrf
                                        <div class="col-md-2 col-sm-2 col-xs-1"> 
                                                <input type="hidden" id="nro_personapeps" name="nro_personapeps" value="{{$secretario->nro_persona}}">
                                                <input type="hidden" id="s" name="s" value="s">
                                                <button type="submit" class="btn" style="margin-top: 31px;margin-left: -20px;" data-toggle="tooltip" data-placement="top" title="Persona Expuesta Políticamente"><i class="fa fa-file-pdf-o"></i></button>
                                        
                                        </div>
                                    </form>
                                @endif
                            </div>
                        
                        </fieldset>
                        <div class="col-md-12 col-sm-8 col-xs-4" style="padding-top: 15px;text-align: end;padding-bottom: 5px;"> 
                            <button type="button" class="btn boton-guardar guardar-datos-secretario" style="background-color:#268fa9;float: inline-end;color: aliceblue;display:{{ $ver }}">Guardar</button>
                        </div>
                    </div>

                    <div class="tab-pane fade" style="background-color: #e9ecef;" id="tesorero" role="tabpanel" aria-labelledby="tesorero-tab">
                        <fieldset style="width:100%;">
                            <div class="row mrow" style="padding-top: 10px;">
                                <div class="col-md-5 col-sm-4 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Apellido y Nombre</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="nombre_tes" name="nombre_tes" placeholder="Nombre del tesorero" maxlength="50" value="@if(isset($tesorero->nombre)) {{$tesorero->nombre}} @endif"> 
                                    </div>
                                    
                                </div>
                                <div class="col-md-2 col-sm-1 col-xs-1">
                                    <div><label for="state_id" class="control-label text-muted small-font margen_bot">CUIT/CUIL</label></div>
                                    <div>
                                        <select class="form-control form-control-sm margen_izq" id="tipo_cuit_tes" name="tipo_cuit_tes">
                                            <option value="7" @if(isset($tesorero->tipo_cuit) && $tesorero->tipo_cuit == 7) selected @endif>CUIL</option>>
                                            <option value="8" @if(isset($tesorero->tipo_cuit) && $tesorero->tipo_cuit == 8) selected @endif>CUIT</option>>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">N°</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq validar_numero" id="nro_cuit_tes" name="nro_cuit_tes" placeholder="Nro CUIL/CUIT" maxlength="15" value="@if(isset($tesorero->nro_cuit)) {{$tesorero->nro_cuit}} @endif" maxlength="20"></div>
                                   
                                    
                                </div>
                               
                        
                                
                            </div>
                            <div class="row mrow" >
                                <div class="col-md-3 col-sm-2 col-xs-1">
                                    <div><label for="state_id" class="control-label text-muted small-font margen_bot">Tipo de Documento</label></div>
                                    <div><select class="form-control form-control-sm margen_izq" id="tipo_doc_tes" name="tipo_doc_tes">
                                        <option value="" selected>Seleccione</option>
                                        <option value="0"  @if(isset($tesorero->tipo_doc) && $tesorero->tipo_doc == 0) selected @endif>No Informado</option>
                                        <option value="1"  @if(isset($tesorero->tipo_doc) && $tesorero->tipo_doc == 1) selected @endif>D.N.I</option>
                                        <option value="2"  @if(isset($tesorero->tipo_doc) && $tesorero->tipo_doc == 2) selected @endif>CI</option>
                                        <option value="4"  @if(isset($tesorero->tipo_doc) && $tesorero->tipo_doc == 4) selected @endif>PASAPORTE</option>
                                        <option value="6"  @if(isset($tesorero->tipo_doc) && $tesorero->tipo_doc == 6) selected @endif>RUT</option>
                                        <option value="7"  @if(isset($tesorero->tipo_doc) && $tesorero->tipo_doc == 7) selected @endif>CUIL</option>
                                        <option value="8"  @if(isset($tesorero->tipo_doc) && $tesorero->tipo_doc == 8) selected @endif>CUIT</option>
                                        <option value="9"  @if(isset($tesorero->tipo_doc) && $tesorero->tipo_doc == 9) selected @endif>OTRO</option>
                                        
                                    </select></div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">N°</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq validar_numero" id="nro_doc_tes" name="nro_doc_tes" placeholder="Nro de documento" maxlength="15" value="@if(isset($tesorero->nro_doc)) {{$tesorero->nro_doc}} @endif" maxlength="20"></div>
                                                                    
                                </div>         
                        
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">Sexo</label></div>
                                    <div>
                                        <select class="form-control form-control-sm margen_izq" id="sexo_tes" name="sexo_tes">
                                        <option value="" selected>Seleccione</option>
                                        <option value="2" @if(isset($tesorero->sexo) && $tesorero->sexo == 2) selected @endif>Femenino</option>
                                        <option value="1" @if(isset($tesorero->sexo) && $tesorero->sexo == 1) selected @endif>Masculino</option>
                                    </select>
                                    </div>
                                </div>
                        
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Fecha Nac</label>
                                    <input type="date" class="form-control form-control-sm form-control-sm margen_izq" id="fecha_nac_tes" name="fecha_nac_tes" placeholder=""  value="@if(isset($tesorero->soc_fecha_nacimiento)){{$tesorero->soc_fecha_nacimiento}}@else{{ $today }}@endif">
                                </div>
                        
                            </div>
                        
                            <div class="row mrow" >
                                <div class="col-md-5 col-sm-4 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Lugar de Nacimiento</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="lugar_nac_tes" name="lugar_nac_tes" placeholder="Lugar de Nacimiento" maxlength="50" value="@if(isset($tesorero->soc_lugar_nacimiento)) {{$tesorero->soc_lugar_nacimiento}} @endif"> 
                                    </div>
                                    
                                </div>
                        
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">Nacionalidad</label></div>
                                    <div>
                                        <select class="form-control form-control-sm margen_izq" id="nacionalidad_tes" name="nacionalidad_tes">
                                            <option value="" selected>Seleccione</option>
                                            <option value="32" @if(isset($tesorero->cod_nacionalidad) && $tesorero->cod_nacionalidad == 32) selected @endif>Argentina</option>
                                            <option value="1"  @if(isset($tesorero->cod_nacionalidad) && $tesorero->cod_nacionalidad == 1) selected @endif>Otra</option>
                                        </select>
                                    </div>
                                </div>
                            
                            </div>
                        
                            <div class="row mrow">
                                                          
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">Código Postal</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq" id="codigo_postal_tes" name="codigo_postal_tes" placeholder="Código Postal"  maxlength="8" readonly value="@if(isset($tesorero->codigo_postal)) {{$tesorero->codigo_postal}} @endif"></div>
                                    <input class="cargotes" type="hidden" value="7">
                                </div>
                                
                        
                                <div class="col-md-1 col-sm-1 col-xs-1">
                                    <div style="margin-bottom: 20px"><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                                    <div><button type="button" class="btn btn-default buscar_codpostal_tes" style="" data-toggle="modal" data-target="#myModaltes">
                                        <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                                    <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                                </div>
                                
                        
                                <div class="col-md-3 col-sm-2 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Localidad</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="localidad_nombre_localidad_tes" name="localidad_nombre_localidad_tes" placeholder="" maxlength="60" disabled value="@if(isset($tesorero->localidad)) {{$tesorero->localidad}} @endif"> 
                                        
                                    </div>
                                    
                                </div>
                        
                                <div class="col-md-3 col-sm-2 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Provincia</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="provincia_nombre_provincia_tes" name="provincia_nombre_provincia_tes" placeholder="" maxlength="60"  disabled value="@if(isset($tesorero->provincia)) {{$tesorero->provincia}} @endif">  
                                    </div>
                                    
                                </div>
                            </div>
                        
                            <div class="row  mrow" style="">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Tel Celular</label>
                                    <input type="text" class="form-control form-control-sm margen_izq validar_numero validate" id="telefono_celular_tes" name="telefono_celular_tes" placeholder="" value="@if(isset($tesorero->telefono_celular)) {{$tesorero->telefono_celular}} @endif" maxlength="18"> 
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Teléfono</label>
                                    <input type="text" class="form-control form-control-sm margen_izq validar_numero" id="telefono1_tes" name="telefono1_tes" placeholder="" value="@if(isset($tesorero->telefono1)) {{$tesorero->telefono1}} @endif" maxlength="18"> 
                                </div>
                        
                               
                                                                    
                                <div class="col-md-5 col-sm-3 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">E-mail</label></div>
                                    <div><input type="email" class="form-control form-control-sm margen_izq" id="email_tes" name="email_tes" placeholder="" value="@if(isset($tesorero->email)) {{$tesorero->email}} @endif" maxlength="40"></div>
                                </div>
                                
                            </div>
                        
                            <div class="row mrow" >
                                <div class="col-md-4 col-sm-3 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Empresa donde trabaja</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="empresa_tes" name="empresa_tes" placeholder="Nombre de Empresa" maxlength="50" value="@if(isset($tesorero->soc_lugar_trabajo)) {{$tesorero->soc_lugar_trabajo}} @endif"> 
                                    </div>
                                    
                                </div>
                        
                                <div class="col-md-5 col-sm-3 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Función / Cargo</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="cargo_tes" name="cargo_tes" placeholder="Nombre de Empresa" maxlength="50" value="@if(isset($tesorero->soc_jud_actividad_principal)) {{$tesorero->soc_jud_actividad_principal}} @endif"> 
                                        <!--<div>
                                            <select class="form-control form-control-sm margen_izq" id="cargo_tes" name="cargo_tes">
                                                <option value="" selected>Seleccione</option>
                                                <option value="8"> tesorero</option>
                                                <option value="6"> tesorero</option>
                                                <option value="7"> Tesorero</option>
                                                <option value="1"> Apoderado</option>
                                            </select>
                                        </div>-->
                                    </div>
                                    
                                </div>
                            </div>
                        
                            <hr>
                            <div class="row mrow">
                                <div>
                                    <h2  style="font-size: 14px;margin-left: 42px;margin-left: 26px;margin-top: revert;">Declaro bajo juramento que mi actividad / profesional es:</h2>
                                </div>
                            </div>
                        
                            <div class="row mrow">
                        
                                <div class="col-md-4 col-sm-3 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Actividad</label>
                                    <select class="form-control form-control-sm margen_izq" id="uif_actividades_tes" name="uif_actividades_tes" >
                                        <option value="" selected>Seleccione</option>
                                        @foreach ($uif_act as $p)
                                            <option value="{{ $p->codigo }}" @if(isset($tesorero->soc_actividad_uif) && $tesorero->soc_actividad_uif == $p->codigo) selected @endif>{{ $p->nombre }}</option>       
                                        @endforeach
                                    </select> 
                                </div>
                        
                                <div class="col-md-4 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Rubro</label>
                                    <select class="form-control form-control-sm margen_izq" id="rubro_tes" name="rubro_tes" >
                                        <option value="" selected>Seleccione</option>
                                        @foreach ($rubros as $p)
                                            <option value="{{ $p->id_rubro }}" @if(isset($tesorero->com_rubro) && $tesorero->com_rubro == $p->id_rubro) selected @endif>{{ $p->descripcion }}</option>       
                                        @endforeach
                                    </select> 
                                </div>

                                <div class="col-md-3 col-sm-2 col-xs-2" style="display: none" id="clase_rubro_tes">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Nuevo Rubro</label>
                                        <input type="text" class="form-control form-control-sm" id="nuevo_rubro_tes" name="nuevo_rubro_tes" placeholder="Escriba el nuevo rubro" maxlength="50" value=""> 
                                    </div>
                                    
                                </div>
                            </div>
                            <hr>
                            <div class="row mrow">
                                <div>
                                    <h2  style="font-size: 14px;margin-left: 42px;margin-left: 26px;margin-top: revert;">Declaro bajo juramento que mi estado civil es:</h2>
                                </div>
                            </div>
                        
                            <div class="row mrow">
                                <div class="col-md-2 col-sm-2 col-xs-1">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">Estado Civil</label></div>
                                    <div>
                                        <select class="form-control form-control-sm margen_izq" id="edo_civil_tes" name="edo_civil_tes">
                                            <option value="" selected>Seleccione</option>
                                            <option value="0" @if(isset($tesorero->estado_civil) && $tesorero->estado_civil == 0) selected @endif>No Informado</option>
                                            <option value="1" @if(isset($tesorero->estado_civil) && $tesorero->estado_civil == 1) selected @endif>Soltero/a</option>
                                            <option value="2" @if(isset($tesorero->estado_civil) && $tesorero->estado_civil == 2) selected @endif>Casado/a</option>
                                            <option value="3" @if(isset($tesorero->estado_civil) && $tesorero->estado_civil == 3) selected @endif>Viudo/a</option>
                                            <option value="5" @if(isset($tesorero->estado_civil) && $tesorero->estado_civil == 5) selected @endif>Divorciado/a</option>
                                            <option value="6" @if(isset($tesorero->estado_civil) && $tesorero->estado_civil == 6) selected @endif>Unión Libre</option>
                                        </select>
                                    </div>
                                </div>
                        
                                <div class="col-md-4 col-sm-3 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Apellido y Nombre Conyugue</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="conyugue_tes" name="conyugue_tes" placeholder="Nombre de Conyugue" maxlength="50" value="@if(isset($tesorero->soc_conyuge_nombre)) {{$tesorero->soc_conyuge_nombre}} @endif"> 
                                    </div>
                                    
                                </div>
                        
                                <div class="col-md-2 col-sm-2 col-xs-1">
                                    <div><label for="state_id" class="control-label text-muted small-font margen_bot">Tipo de Documento</label></div>
                                    <div><select class="form-control form-control-sm margen_izq" id="tipo_doc_conyugue_tes" name="tipo_doc_conyugue_tes">
                                        <option value="" selected>Seleccione</option>
                                        <option value="0"  @if(isset($tesorero->soc_conyuge_tipo_doc) && $tesorero->soc_conyuge_tipo_doc == 0) selected @endif>No Informado</option>
                                        <option value="1"  @if(isset($tesorero->soc_conyuge_tipo_doc) && $tesorero->soc_conyuge_tipo_doc == 1) selected @endif>D.N.I</option>
                                        <option value="2"  @if(isset($tesorero->soc_conyuge_tipo_doc) && $tesorero->soc_conyuge_tipo_doc == 2) selected @endif>CI</option>
                                        <option value="4"  @if(isset($tesorero->soc_conyuge_tipo_doc) && $tesorero->soc_conyuge_tipo_doc == 4) selected @endif>PASAPORTE</option>
                                        <option value="6"  @if(isset($tesorero->soc_conyuge_tipo_doc) && $tesorero->soc_conyuge_tipo_doc == 6) selected @endif>RUT</option>
                                        <option value="7"  @if(isset($tesorero->soc_conyuge_tipo_doc) && $tesorero->soc_conyuge_tipo_doc == 7) selected @endif>CUIL</option>
                                        <option value="8"  @if(isset($tesorero->soc_conyuge_tipo_doc) && $tesorero->soc_conyuge_tipo_doc == 8) selected @endif>CUIT</option>
                                        <option value="9"  @if(isset($tesorero->soc_conyuge_tipo_doc) && $tesorero->soc_conyuge_tipo_doc == 9) selected @endif>OTRO</option>
                                        
                                    </select></div>
                                </div>
                        
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">N°</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq validar_numero" id="nro_doc_conyugue_tes" name="nro_doc_conyugue_tes" placeholder="Nro de documento" maxlength="15" value="@if(isset($tesorero->soc_conyuge_nro_doc)) {{$tesorero->soc_conyuge_nro_doc}} @endif" maxlength="20"></div>
                                                                    
                                </div>
                        
                            </div>
                            <hr>
                            <div class="row mrow">
                                <!--<div class="col-md-4 col-sm-3 col-xs-2">
                                    <label class="control-label text-muted small-font margen_bot">Es Sujeto Obligado Prevención Lavado</label>
                                    <select class="form-control form-control-sm margen_izq" id="lavado_tes" name="lavado_tes">
                                        <option value="" selected>Seleccione</option>
                                        <option value="1" @if(isset($tesorero->sujeto_obligado) && $tesorero->sujeto_obligado == 1) selected @endif>Sí</option>
                                        <option value="0" @if(isset($tesorero->sujeto_obligado) && $tesorero->sujeto_obligado == 0) selected @endif>No</option>
                                    </select> 
                                </div>-->

                                @if(isset($tesorero->sujeto_obligado) && $tesorero->sujeto_obligado >= 0)
                                    <form action="/admin/imprimir-so" method="POST" autocomplete="off">
                                        @csrf
                                        <!--<div class="col-md-2 col-sm-2 col-xs-1"> 
                                                <input type="hidden" id="nro_personaso" name="nro_personaso" value="{{$tesorero->nro_persona}}">
                                              
                                        
                                        </div> -->
                                    </form>
                                @endif
                        
                                <div class="col-md-3 col-sm-2 col-xs-1">
                                    <label class="control-label text-muted small-font margen_bot">Reviste Calidad de Pep´s</label>
                                    <select class="form-control form-control-sm margen_izq" id="pep_tes" name="pep_tes">
                                        <option value="" selected>Seleccione</option>
                                        <option value="1" @if(isset($tesorero->persona_pep) && $tesorero->persona_pep == 1) selected @endif>Sí</option>
                                        <option value="0" @if(isset($tesorero->persona_pep) && $tesorero->persona_pep == 0) selected @endif>No</option>
                                    </select> 
                                </div>

                                @if(isset($tesorero->persona_pep) && $tesorero->persona_pep >= 0)
                                    <form action="/admin/imprimir-pep" method="POST" autocomplete="off">
                                        @csrf
                                        <div class="col-md-2 col-sm-2 col-xs-1"> 
                                                <input type="hidden" id="nro_personapept" name="nro_personapept" value="{{$tesorero->nro_persona}}">
                                                <input type="hidden" id="t" name="t" value="t">
                                                <button type="submit" class="btn" style="margin-top: 31px;margin-left: -20px;" data-toggle="tooltip" data-placement="top" title="Persona Expuesta Políticamente"><i class="fa fa-file-pdf-o"></i></button>
                                        
                                        </div>
                                    </form>
                                @endif
                            </div>
                        
                        </fieldset>
                        <div class="col-md-12 col-sm-8 col-xs-4" style="padding-top: 15px;text-align: end;padding-bottom: 5px;"> 
                            <button type="button" class="btn boton-guardar guardar-datos-tesorero" style="background-color:#268fa9;float: inline-end;color: aliceblue;display:{{ $ver }}">Guardar</button>
                        </div>
                    </div>


                    <div class="tab-pane fade" style="background-color: #e9ecef;" id="apoderado" role="tabpanel" aria-labelledby="apoderado-tab">
                        <fieldset style="width:100%;">
                            <div class="row mrow" style="padding-top: 10px;">
                                <div class="col-md-5 col-sm-4 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Apellido y Nombre</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="nombre_apo" name="nombre_apo" placeholder="Nombre del apoderado" maxlength="50" value="@if(isset($apoderado->nombre)) {{$apoderado->nombre}} @endif" required> 
                                    </div>
                                    
                                </div>
                                <div class="col-md-2 col-sm-1 col-xs-1">
                                    <div><label for="state_id" class="control-label text-muted small-font margen_bot">CUIT/CUIL</label></div>
                                    <div>
                                        <select class="form-control form-control-sm margen_izq" id="tipo_cuit_apo" name="tipo_cuit_apo">
                                            <option value="7" @if(isset($apoderado->tipo_cuit) && $apoderado->tipo_cuit == 7) selected @endif>CUIL</option>>
                                            <option value="8" @if(isset($apoderado->tipo_cuit) && $apoderado->tipo_cuit == 8) selected @endif>CUIT</option>>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">N°</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq validar_numero" id="nro_cuit_apo" name="nro_cuit_apo" placeholder="Nro CUIL/CUIT" maxlength="15" value="@if(isset($apoderado->nro_cuit)) {{$apoderado->nro_cuit}} @endif" maxlength="20"></div>
                                   
                                    
                                </div>
                               
                        
                                
                            </div>
                            <div class="row mrow" >
                                <div class="col-md-3 col-sm-2 col-xs-1">
                                    <div><label for="state_id" class="control-label text-muted small-font margen_bot">Tipo de Documento</label></div>
                                    <div><select class="form-control form-control-sm margen_izq" id="tipo_doc_apo" name="tipo_doc_apo">
                                        <option value="" selected>Seleccione</option>
                                        <option value="0"  @if(isset($apoderado->tipo_doc) && $apoderado->tipo_doc == 0) selected @endif>No Informado</option>
                                        <option value="1"  @if(isset($apoderado->tipo_doc) && $apoderado->tipo_doc == 1) selected @endif>D.N.I</option>
                                        <option value="2"  @if(isset($apoderado->tipo_doc) && $apoderado->tipo_doc == 2) selected @endif>CI</option>
                                        <option value="4"  @if(isset($apoderado->tipo_doc) && $apoderado->tipo_doc == 4) selected @endif>PASAPORTE</option>
                                        <option value="6"  @if(isset($apoderado->tipo_doc) && $apoderado->tipo_doc == 6) selected @endif>RUT</option>
                                        <option value="7"  @if(isset($apoderado->tipo_doc) && $apoderado->tipo_doc == 7) selected @endif>CUIL</option>
                                        <option value="8"  @if(isset($apoderado->tipo_doc) && $apoderado->tipo_doc == 8) selected @endif>CUIT</option>
                                        <option value="9"  @if(isset($apoderado->tipo_doc) && $apoderado->tipo_doc == 9) selected @endif>OTRO</option>
                                        
                                    </select></div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">N°</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq validar_numero" id="nro_doc_apo" name="nro_doc_apo" placeholder="Nro de documento" maxlength="15" value="@if(isset($apoderado->nro_doc)) {{$apoderado->nro_doc}} @endif" maxlength="20"></div>
                                                                    
                                </div>         
                        
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">Sexo</label></div>
                                    <div>
                                        <select class="form-control form-control-sm margen_izq" id="sexo_apo" name="sexo_apo">
                                        <option value="" selected>Seleccione</option>
                                        <option value="2" @if(isset($apoderado->sexo) && $apoderado->sexo == 2) selected @endif>Femenino</option>
                                        <option value="1" @if(isset($apoderado->sexo) && $apoderado->sexo == 1) selected @endif>Masculino</option>
                                    </select>
                                    </div>
                                </div>
                        
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Fecha Nac</label>
                                    <input type="date" class="form-control form-control-sm form-control-sm margen_izq" id="fecha_nac_apo" name="fecha_nac_apo" placeholder=""  value="@if(isset($apoderado->soc_fecha_nacimiento)){{$apoderado->soc_fecha_nacimiento}}@else{{ $today }}@endif">
                                </div>
                        
                            </div>
                        
                            <div class="row mrow" >
                                <div class="col-md-5 col-sm-4 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Lugar de Nacimiento</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="lugar_nac_apo" name="lugar_nac_apo" placeholder="Lugar de Nacimiento" maxlength="50" value="@if(isset($apoderado->soc_lugar_nacimiento)) {{$apoderado->soc_lugar_nacimiento}} @endif"> 
                                    </div>
                                    
                                </div>
                        
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">Nacionalidad</label></div>
                                    <div>
                                        <select class="form-control form-control-sm margen_izq" id="nacionalidad_apo" name="nacionalidad_apo">
                                            <option value="" selected>Seleccione</option>
                                            <option value="32" @if(isset($apoderado->cod_nacionalidad) && $apoderado->cod_nacionalidad == 32) selected @endif>Argentina</option>
                                            <option value="1"  @if(isset($apoderado->cod_nacionalidad) && $apoderado->cod_nacionalidad == 1) selected @endif>Otra</option>
                                        </select>
                                    </div>
                                </div>
                            
                            </div>
                        
                            <div class="row mrow">
                                                          
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">Código Postal</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq" id="codigo_postal_apo" name="codigo_postal_apo" placeholder="Código Postal"  maxlength="8" readonly value="@if(isset($apoderado->codigo_postal)) {{$apoderado->codigo_postal}} @endif"></div>
                                    <input class="cargoapo" type="hidden" value="1">
                                </div>
                                
                        
                                <div class="col-md-1 col-sm-1 col-xs-1">
                                    <div style="margin-bottom: 20px"><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                                    <div><button type="button" class="btn btn-default buscar_codpostal_apo" style="" data-toggle="modal" data-target="#myModalapo">
                                        <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                                    <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                                </div>
                                
                        
                                <div class="col-md-3 col-sm-2 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Localidad</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="localidad_nombre_localidad_apo" name="localidad_nombre_localidad_apo" placeholder="" maxlength="60" disabled value="@if(isset($apoderado->localidad)) {{$apoderado->localidad}} @endif"> 
                                        
                                    </div>
                                    
                                </div>
                        
                                <div class="col-md-3 col-sm-2 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Provincia</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="provincia_nombre_provincia_apo" name="provincia_nombre_provincia_apo" placeholder="" maxlength="60"  disabled value="@if(isset($apoderado->provincia)) {{$apoderado->provincia}} @endif">  
                                    </div>
                                    
                                </div>
                            </div>
                        
                            <div class="row  mrow" style="">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Tel Celular</label>
                                    <input type="text" class="form-control form-control-sm margen_izq validar_numero validate" id="telefono_celular_apo" name="telefono_celular_apo" placeholder="" value="@if(isset($apoderado->telefono_celular)) {{$apoderado->telefono_celular}} @endif" maxlength="18" required> 
                                </div>

                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Teléfono</label>
                                    <input type="text" class="form-control form-control-sm margen_izq validar_numero" id="telefono1_apo" name="telefono1_apo" placeholder="" value="@if(isset($apoderado->telefono1)) {{$apoderado->telefono1}} @endif" maxlength="18" required> 
                                </div>
                        
                                
                                                                    
                                <div class="col-md-5 col-sm-3 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">E-mail</label></div>
                                    <div><input type="email" class="form-control form-control-sm margen_izq" id="email_apo" name="email_apo" placeholder="" value="@if(isset($apoderado->email)) {{$apoderado->email}} @endif" maxlength="40" required></div>
                                </div>
                                
                            </div>
                        
                            <div class="row mrow" >
                                <div class="col-md-4 col-sm-3 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Empresa donde trabaja</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="empresa_apo" name="empresa_apo" placeholder="Nombre de Empresa" maxlength="50" value="@if(isset($apoderado->soc_lugar_trabajo)) {{$apoderado->soc_lugar_trabajo}} @endif"> 
                                    </div>
                                    
                                </div>
                        
                                <div class="col-md-5 col-sm-3 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Función / Cargo</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="cargo_apo" name="cargo_apo" placeholder="Nombre de Empresa" maxlength="50" value="@if(isset($apoderado->soc_jud_actividad_principal)) {{$apoderado->soc_jud_actividad_principal}} @endif"> 
                                        <!--<div>
                                            <select class="form-control form-control-sm margen_izq" id="cargo_apo" name="cargo_apo">
                                                <option value="" selected>Seleccione</option>
                                                <option value="8"> apoderado</option>
                                                <option value="6"> apoderado</option>
                                                <option value="7"> apoderado</option>
                                                <option value="1"> Apoderado</option>
                                            </select>
                                        </div>-->
                                    </div>
                                    
                                </div>
                            </div>
                        
                            <hr>
                            <div class="row mrow">
                                <div>
                                    <h2  style="font-size: 14px;margin-left: 42px;margin-left: 26px;margin-top: revert;">Declaro bajo juramento que mi actividad / profesional es:</h2>
                                </div>
                            </div>
                        
                            <div class="row mrow">
                        
                                <div class="col-md-4 col-sm-3 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Actividad</label>
                                    <select class="form-control form-control-sm margen_izq" id="uif_actividades_apo" name="uif_actividades_apo" >
                                        <option value="" selected>Seleccione</option>
                                        @foreach ($uif_act as $p)
                                            <option value="{{ $p->codigo }}" @if(isset($apoderado->soc_actividad_uif) && $apoderado->soc_actividad_uif == $p->codigo) selected @endif>{{ $p->nombre }}</option>       
                                        @endforeach
                                    </select> 
                                </div>
                        
                                <div class="col-md-4 col-sm-2 col-xs-2">
                                    <label class="help-block text-muted small-font margen_bot">Rubro</label>
                                    <select class="form-control form-control-sm margen_izq" id="rubro_apo" name="rubro_apo" >
                                        <option value="" selected>Seleccione</option>
                                        @foreach ($rubros as $p)
                                            <option value="{{ $p->id_rubro }}" @if(isset($apoderado->com_rubro) && $apoderado->com_rubro == $p->id_rubro) selected @endif>{{ $p->descripcion }}</option>       
                                        @endforeach
                                    </select> 
                                </div>

                                <div class="col-md-3 col-sm-2 col-xs-2" style="display: none" id="clase_rubro_apo">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Nuevo Rubro</label>
                                        <input type="text" class="form-control form-control-sm" id="nuevo_rubro_apo" name="nuevo_rubro_apo" placeholder="Escriba el nuevo rubro" maxlength="50" value=""> 
                                    </div>
                                    
                                </div>

                            </div>
                            <hr>
                            <div class="row mrow">
                                <div>
                                    <h2  style="font-size: 14px;margin-left: 42px;margin-left: 26px;margin-top: revert;">Declaro bajo juramento que mi estado civil es:</h2>
                                </div>
                            </div>
                        
                            <div class="row mrow">
                                <div class="col-md-2 col-sm-2 col-xs-1">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">Estado Civil</label></div>
                                    <div>
                                        <select class="form-control form-control-sm margen_izq" id="edo_civil_apo" name="edo_civil_apo">
                                            <option value="" selected>Seleccione</option>
                                            <option value="0" @if(isset($apoderado->estado_civil) && $apoderado->estado_civil == 0) selected @endif>No Informado</option>
                                            <option value="1" @if(isset($apoderado->estado_civil) && $apoderado->estado_civil == 1) selected @endif>Soltero/a</option>
                                            <option value="2" @if(isset($apoderado->estado_civil) && $apoderado->estado_civil == 2) selected @endif>Casado/a</option>
                                            <option value="3" @if(isset($apoderado->estado_civil) && $apoderado->estado_civil == 3) selected @endif>Viudo/a</option>
                                            <option value="5" @if(isset($apoderado->estado_civil) && $apoderado->estado_civil == 5) selected @endif>Divorciado/a</option>
                                            <option value="6" @if(isset($apoderado->estado_civil) && $apoderado->estado_civil == 6) selected @endif>Unión Libre</option>
                                        </select>
                                    </div>
                                </div>
                        
                                <div class="col-md-4 col-sm-3 col-xs-2">
                                    <div>
                                        <label for="" class="help-block text-muted small-font margen_bot">Apellido y Nombre Conyugue</label>
                                        <input type="text" class="form-control form-control-sm margen_izq" id="conyugue_apo" name="conyugue_apo" placeholder="Nombre de Conyugue" maxlength="50" value="@if(isset($apoderado->soc_conyuge_nombre)) {{$apoderado->soc_conyuge_nombre}} @endif"> 
                                    </div>
                                    
                                </div>
                        
                                <div class="col-md-2 col-sm-2 col-xs-1">
                                    <div><label for="state_id" class="control-label text-muted small-font margen_bot">Tipo de Documento</label></div>
                                    <div><select class="form-control form-control-sm margen_izq" id="tipo_doc_conyugue_apo" name="tipo_doc_conyugue_apo">
                                        <option value="" selected>Seleccione</option>
                                        <option value="0"  @if(isset($apoderado->soc_conyuge_tipo_doc) && $apoderado->soc_conyuge_tipo_doc == 0) selected @endif>No Informado</option>
                                        <option value="1"  @if(isset($apoderado->soc_conyuge_tipo_doc) && $apoderado->soc_conyuge_tipo_doc == 1) selected @endif>D.N.I</option>
                                        <option value="2"  @if(isset($apoderado->soc_conyuge_tipo_doc) && $apoderado->soc_conyuge_tipo_doc == 2) selected @endif>CI</option>
                                        <option value="4"  @if(isset($apoderado->soc_conyuge_tipo_doc) && $apoderado->soc_conyuge_tipo_doc == 4) selected @endif>PASAPORTE</option>
                                        <option value="6"  @if(isset($apoderado->soc_conyuge_tipo_doc) && $apoderado->soc_conyuge_tipo_doc == 6) selected @endif>RUT</option>
                                        <option value="7"  @if(isset($apoderado->soc_conyuge_tipo_doc) && $apoderado->soc_conyuge_tipo_doc == 7) selected @endif>CUIL</option>
                                        <option value="8"  @if(isset($apoderado->soc_conyuge_tipo_doc) && $apoderado->soc_conyuge_tipo_doc == 8) selected @endif>CUIT</option>
                                        <option value="9"  @if(isset($apoderado->soc_conyuge_tipo_doc) && $apoderado->soc_conyuge_tipo_doc == 9) selected @endif>OTRO</option>
                                        
                                    </select></div>
                                </div>
                        
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <div><label for="" class="help-block text-muted small-font margen_bot">N°</label></div>
                                    <div><input type="text" class="form-control form-control-sm margen_izq validar_numero" id="nro_doc_conyugue_apo" name="nro_doc_conyugue_apo" placeholder="Nro de documento" maxlength="15" value="@if(isset($apoderado->soc_conyuge_nro_doc)) {{$apoderado->soc_conyuge_nro_doc}} @endif" maxlength="20"></div>
                                                                    
                                </div>
                        
                            </div>
                            <hr>
                            <div class="row mrow">
                                <!--<div class="col-md-4 col-sm-3 col-xs-2">
                                    <label class="control-label text-muted small-font margen_bot">Es Sujeto Obligado Prevención Lavado</label>
                                    <select class="form-control form-control-sm margen_izq" id="lavado_apo" name="lavado_apo">
                                        <option value="" selected>Seleccione</option>
                                        <option value="1" @if(isset($apoderado->sujeto_obligado) && $apoderado->sujeto_obligado == 1) selected @endif>Sí</option>
                                        <option value="0" @if(isset($apoderado->sujeto_obligado) && $apoderado->sujeto_obligado == 0) selected @endif>No</option>
                                    </select> 
                                </div> -->

                                @if(isset($apoderado->sujeto_obligado) && $apoderado->sujeto_obligado >= 0)
                                    <form action="/admin/imprimir-so" method="POST" autocomplete="off">
                                        @csrf
                                        <!--<div class="col-md-2 col-sm-2 col-xs-1"> 
                                                <input type="hidden" id="nro_personaso" name="nro_personaso" value="{{$apoderado->nro_persona}}">
                                                
                                        
                                        </div>-->
                                    </form>
                                @endif
                        
                                <div class="col-md-3 col-sm-2 col-xs-1">
                                    <label class="control-label text-muted small-font margen_bot">Reviste Calidad de Pep´s</label>
                                    <select class="form-control form-control-sm margen_izq" id="pep_apo" name="pep_apo">
                                        <option value="" selected>Seleccione</option>
                                        <option value="1" @if(isset($apoderado->persona_pep) && $apoderado->persona_pep == 1) selected @endif>Sí</option>
                                        <option value="0" @if(isset($apoderado->persona_pep) && $apoderado->persona_pep == 0) selected @endif>No</option>
                                    </select> 
                                </div>

                                @if(isset($apoderado->persona_pep) && $apoderado->persona_pep >= 0)
                                    <form action="/admin/imprimir-pep" method="POST" autocomplete="off">
                                        @csrf
                                        <div class="col-md-2 col-sm-2 col-xs-1"> 
                                                <input type="hidden" id="nro_personapepa" name="nro_personapepa" value="{{$apoderado->nro_persona}}">
                                                <input type="hidden" id="a" name="a" value="a">
                                                <button type="submit" class="btn" style="margin-top: 31px;margin-left: -20px;" data-toggle="tooltip" data-placement="top" title="Persona Expuesta Políticamente"><i class="fa fa-file-pdf-o"></i></button>
                                        
                                        </div>
                                    </form>
                                @endif
                            </div>
                        
                        </fieldset>
                        <div class="col-md-12 col-sm-8 col-xs-4" style="padding-top: 15px;text-align: end;padding-bottom: 5px;"> 
                            <button type="button" class="btn boton-guardar guardar-datos-apoderado" style="background-color:#268fa9;float: inline-end;color: aliceblue;display:{{ $ver }}">Guardar</button>
                        </div>
                    </div>

                    <div class="tab-pane fade" style="background-color: #e9ecef;" id="documentos" role="tabpanel" aria-labelledby="documentos-tab">
                        <div>
                            <div class="panel panel-default">
                                <div class="panel-heading margen_izq" style="padding-top: 15px;text-align: left;padding-bottom: 5px;"><b>Modelos de documentos para descargar:</b>
                                    <div class="row">
                                        <div class="col-md-5 col-sm-4 col-xs-2" style="font-size: 14px;">- Declaración Jurada sobre conformación de Ahorros (FA13-A)
                                            <form action="/admin/imprimir/declara-ahorro" method="get" autocomplete="off">
                                                @csrf
                                                <button type="submit" class="btn" style="margin-top: -25px;margin-left: 377px;" data-toggle="tooltip" data-placement="top" title="Declaración Jurada sobre conformación de Ahorros (FA13-A)"><i class="fa fa-file-pdf-o"></i></button>
                                            </form>
                                        </div>
                                                                                                                                                                
                                        <div class="col-md-7 col-sm-4 col-xs-2" style="font-size: 14px;">- Certificación sobre conformación  de Ahorros (FA13-B)
                                            <form action="/admin/imprimir/certificacion-ahorro" method="get" autocomplete="off">
                                                @csrf
                                                <button type="submit" class="btn" style="margin-top: -25px;margin-left: 336px;" data-toggle="tooltip" data-placement="top" title="Certificación sobre conformación  de Ahorros (FA13-B)"><i class="fa fa-file-pdf-o"></i></button>
                                            </form>
                                        </div>
                                                    
                                    </div>
                                    <div class="row">    
                                        <div class="col-md-5 col-sm-4 col-xs-2" style="font-size: 14px">- Declaración Jurada del Servicio de Ayuda Economica con Fondos Propios (FA14-A)
                                            <form action="/admin/imprimir/declara-ayuda-economica" method="get" autocomplete="off">
                                                @csrf
                                                <button type="submit" class="btn" style="margin-top: -25px;margin-left: 98px;" data-toggle="tooltip" data-placement="top" title="Declaración Jurada del Servicio de Ayuda Economica con Fondos Propios (FA14-A)"><i class="fa fa-file-pdf-o"></i></button>
                                            </form>
                                        </div>
                                                                            
                                        <div class="col-md-7 col-sm-4 col-xs-2" style="font-size: 14px">- Certificación del Servicio de Ayuda Economica con Fondos Propios (FA14-B)
                                            <form action="/admin/imprimir/certificacion-ayuda-economica" method="get" autocomplete="off">
                                                @csrf
                                                <button type="submit" class="btn" style="margin-top: -25px;margin-left: 471px;" data-toggle="tooltip" data-placement="top" title="Certificación del Servicio de Ayuda Economica con Fondos Propios (FA14-B)"><i class="fa fa-file-pdf-o"></i></button>
                                            </form>
                                        </div>
                                                                                
                                    </div>
                                </div>
                                <div class="panel-heading margen_izq" style="padding-top: 15px;text-align: left;padding-bottom: 5px;">Adjuntar los siguientes documentos</div>
                                <div class="salida" style="text-align: center;font: initial;color:rgb(75 191 101);"></div>
                                <div class="salida_error" style="text-align: center;font: initial;color:red;"></div>
                                    <table class="table-doc table-bordered text-muted small-font margen_bot">
                                        <thead>
                                            <tr style="text-align: center;">
                                                <th style="width: 30px">Documento</th>
                                                <th>Descripción</th>
                                                <th>Acción</th>
                                                <th></th>
                                                
                                            </tr>
                                        </thead>

                                        <tbody class="magen_izq">
                                            <tr>
                                                <td>FA01</td>
                                                <td>Formulario de Solicitud de Adhesión de Persona Jurídica</td>
                                                
                                                <form id="formad" method="post" action="#" enctype="multipart/form-data"> <!--/admin/guardar-documento -->
                                                    @csrf
                                                    <td>
                                                        @if(isset($documentos))
                                                            @foreach($documentos as $i => $valor)
                                                                @if($documentos[$i]->cod_doc == 'FA01')
                                                                    <div  style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa01" style="text-align: end;font: caption;color: #7ed47e;">@if($valor->cod_doc == 'FA01'){{ $documentos[$i]->descripcion }}@else Sin Archivo @endif</span></div>
                                                                
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                        <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo" style="margin-top: 6px;font-size: 14px;" name="archivo"  required value="">
                                                        <input type="hidden" id="cod" name="cod" value ="FA01">
                                                        <div  style="text-align: end;font: caption;color: red;"><span id="salida_error1"></span></div>
                                                        <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida1"></span></div>
                                                        
                                                        
                                                        
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm guardar-datos-adjuntos" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                        
                                                    </td>
                                                    
                                                </form>
                                                
                                            </tr>
                                            <tr>
                                                <td>FA02</td>
                                                <td>Planilla de Datos de Representantes Legales y Apoderados</td>
                                                <td style="text-align: center"><b>Solo cargar en la página web</b></td>
                                                <td></td>
                                                <!--
                                                <td>
                                                    {{-- @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA02')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa02" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif --}}
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo2" style="margin-top: 6px;font-size: 14px;" name="archivo2"  required>
                                                    <input type="hidden" id="cod2" name="cod2" value ="FA02">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error2"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida2"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos2" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td> -->
                                                
                                                
                                            </tr>
                                            <tr>
                                                <td>FA03</td>
                                                <td>Copia del Estatuto aprobado e inscripto en el INAES</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA03')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa03" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo3" style="margin-top: 6px;font-size: 14px;" name="archivo3"  required>
                                                    <input type="hidden" id="cod3" name="cod3" value ="FA03">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error3"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida3"></span></div>
                                                    
                                                </td>
                                                
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos3" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                                
                                                
                                            </tr>
                                            <tr>
                                                <td>FA04</td>
                                                <td>Copia del Reglamento de Ayuda Económica aprobado e inscripto en el INAES</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA04')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa04" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo4" style="margin-top: 6px;font-size: 14px;" name="archivo4"  required>
                                                    <input type="hidden" id="cod4" name="cod4" value ="FA04">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error4"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida4"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos4" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                                                                               
                                                
                                            </tr>
                                            <tr>
                                                <td>FA05</td>
                                                <td>Copia del Reglamento de Tarjeta de Crédito</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA05')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa05"  style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo5" style="margin-top: 6px;font-size: 14px;" name="archivo5"  required>
                                                    <input type="hidden" id="cod5" name="cod5" value ="FA05">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error5"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida5"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos5" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled  title="Seleccione un archivo .pdf para adjuntar." >Guardar</button>
                                                </td>
                                                
                                                
                                            </tr>
                                            <tr>
                                                <td>FA06</td>
                                                <td>Constancia de CUIT de la Mutual</td>
                                                <td>
                                                    @if(isset($documentos))
                                                    @foreach($documentos as $i => $valor)
                                                        @if($documentos[$i]->cod_doc == 'FA06')
                                                            <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa06" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                        @endif
                                                    @endforeach
                                                @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo6" style="margin-top: 6px;font-size: 14px;" name="archivo6"  required>
                                                    <input type="hidden" id="cod6" name="cod6" value ="FA06">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error6"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida6"></span></div>
                                                   
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos6" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                                
                                                
                                            </tr>
                                            <tr>
                                                <td>FA07</td>
                                                <td>Copia certificada por escribano de última Acta de designación de autoridades y distribución de cargos</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA07')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa07" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo7" style="margin-top: 6px;font-size: 14px;" name="archivo7"  required>
                                                    <input type="hidden" id="cod7" name="cod7" value ="FA07">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error7"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida7"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos7" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                                
                                                
                                            </tr>
                                            <tr>
                                                <td>FA08</td>
                                                <td>Copia de Poderes otorgados, que habiliten a actuar ante FAUM</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA08')
                                                                <div style="text-align: end;font: caption;font-size: unset; ">Archivo actual: <span id="fa08" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo8" style="margin-top: 6px;font-size: 14px;" name="archivo8"  required>
                                                    <input type="hidden" id="cod8" name="cod8" value ="FA08">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error8"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida8"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos8" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                                
                                                
                                            </tr>
                                            <tr>
                                                <td>FA09</td>
                                                <td colspan="3">Copia de DNI de Representantes Legales y Apoderados</td>
                                                {{-- <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA09')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa09" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo9" style="margin-top: 6px;font-size: 14px;" name="archivo9"  required>
                                                    <input type="hidden" id="cod9" name="cod9" value ="FA09">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error9"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida9"></span></div> 
                                                    
                                                </td>
                                                <td>
                                                    button type="button" class="btn btn-sm guardar-datos-adjuntos9" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>--}}
                                                
                                                
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FA09-A Copia de DNI Presidente</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA09-A')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa09-a" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo9" style="margin-top: 6px;font-size: 14px;" name="archivo9-a"  required>
                                                    <input type="hidden" id="cod9-a" name="cod9-a" value ="FA09-A">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error9-a"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida9-a"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos9-a" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FA09-B Copia de DNI Secretario</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA09-B')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa09-b" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo9-b" style="margin-top: 6px;font-size: 14px;" name="archivo9-b"  required>
                                                    <input type="hidden" id="cod9-b" name="cod9-b" value ="FA09-B">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error9-b"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida9-b"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos9-b" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FA09-C Copia de DNI Tesorero</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA09-C')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa09-c" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo9-c" style="margin-top: 6px;font-size: 14px;" name="archivo9-c"  required>
                                                    <input type="hidden" id="cod9-c" name="cod9-c" value ="FA09-C">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error9-c"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida9-c"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos9-c" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FA09-D Copia de DNI Apoderado (Opcional)</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA09-D')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa09-d" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo9-d" style="margin-top: 6px;font-size: 14px;" name="archivo9-d"  required>
                                                    <input type="hidden" id="cod9-d" name="cod9-d" value ="FA09-D">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error9-d"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida9-d"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos9-d" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>FA10</td>
                                                <td colspan="3">Formulario de DDJJ de Persona Expuesta Políticamente</td>
                                                {{--<td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA10')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa10" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo10" style="margin-top: 6px;font-size: 14px;" name="archivo10"  required>
                                                    <input type="hidden" id="cod10" name="cod10" value ="FA10">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error10"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida10"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos10" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td> --}}
                                                
                                                
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td >FA10-A DDJJ de Persona Expuesta Políticamente Presidente</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA10-A')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa10-a" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo10-a" style="margin-top: 6px;font-size: 14px;" name="archivo10-a"  required>
                                                    <input type="hidden" id="cod10-a" name="cod10-a" value ="FA10-A">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error10-a"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida10-a"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos10-a" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                                
                                                
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td >FA10-B DDJJ de Persona Expuesta Políticamente Secretario</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA10-B')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa10-b" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo10-b" style="margin-top: 6px;font-size: 14px;" name="archivo10-b"  required>
                                                    <input type="hidden" id="cod10-b" name="cod10-b" value ="FA10-B">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error10-b"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida10-b"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos10-b" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                                
                                                
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td >FA10-C DDJJ de Persona Expuesta Políticamente Tesorero</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA10-C')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa10-c" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo10-c" style="margin-top: 6px;font-size: 14px;" name="archivo10-c"  required>
                                                    <input type="hidden" id="cod10-c" name="cod10-c" value ="FA10-C">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error10-c"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida10-c"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos10-c" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                                
                                                
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td >FA10-D DDJJ de Persona Expuesta Políticamente Apoderado (Opcional)</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA10-D')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa10-d" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo10-d" style="margin-top: 6px;font-size: 14px;" name="archivo10-d"  required>
                                                    <input type="hidden" id="cod10-d" name="cod10-d" value ="FA10-D">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error10-d"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida10-d"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos10-d" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                                
                                                
                                            </tr>
                                            <tr>
                                                <td>FA11</td>
                                                <td>Memoria y Balance del último ejercicio cerrado, certificado por CPN y legalizado por CPCE</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA11')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa11" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo11" style="margin-top: 6px;font-size: 14px;" name="archivo11"  required>
                                                    <input type="hidden" id="cod11" name="cod11" value ="FA11">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error11"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida11"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos11" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                                
                                                
                                            </tr>
                                            <tr>
                                                <td>FA12</td>
                                                <td>Copia certificada por escribano del Acta donde se aprueba la Memoria y Balance citado</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA12')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa12" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo12" style="margin-top: 6px;font-size: 14px;" name="archivo12"  required>
                                                    <input type="hidden" id="cod12" name="cod12" value ="FA12">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error12"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida12"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos12" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                                
                                                
                                            </tr>

                                            <tr>
                                                <td>FA13</td>
                                                <td  colspan="3">DJ y Certificación sobre Conformación de Ahorros,certificación por CPN y legalizado por CPCE</td>
                                                {{-- <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA13')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span  id="fa13" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo13" style="margin-top: 6px;font-size: 14px;" name="archivo13"  required>
                                                    <input type="hidden" id="cod13" name="cod13" value ="FA13">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error13"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida13"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos13" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>  --}}
                                                
                                                
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FA13-A DJ sobre Conformación de Ahorros</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA13-A')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span  id="fa13-a" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo13-a" style="margin-top: 6px;font-size: 14px;" name="archivo13-a"  required>
                                                    <input type="hidden" id="cod13-a" name="cod13-a" value ="FA13-A">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error13-a"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida13-a"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos13-a" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                                
                                                
                                            </tr>

                                            <tr>
                                                <td></td>
                                                <td>FA13-B Certificación sobre Conformación de Ahorros,certificación por CPN y legalizado por CPCE</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA13-B')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span  id="fa13-b" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo13-b" style="margin-top: 6px;font-size: 14px;" name="archivo13-b"  required>
                                                    <input type="hidden" id="cod13-b" name="cod13-b" value ="FA13-B">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error13-b"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida13-b"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos13-b" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                                
                                                
                                            </tr>


                                            <tr>
                                                <td>FA14</td>
                                                <td colspan="3">DJ y Certificación sobre Recursos específicos del Servicio de Ayuda Económica con Fondos Propios, certificación por CPN y legalizado por CPCE</td>
                                                {{-- <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA14')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa14" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo14" style="margin-top: 6px;font-size: 14px;" name="archivo14"  required>
                                                    <input type="hidden" id="cod14" name="cod14" value ="FA14">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error14"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida14"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos14" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>  --}}
                                                                                               
                                            </tr>

                                            <tr>
                                                <td></td>
                                                <td>FA14-A DJ sobre Recursos específicos del Servicio de Ayuda Económica con Fondos Propios</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA14-A')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa14-a" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo14-a" style="margin-top: 6px;font-size: 14px;" name="archivo14-a"  required>
                                                    <input type="hidden" id="cod14-a" name="cod14-a" value ="FA14-A">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error14-a"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida14-a"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos14-a" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                                                                               
                                            </tr>

                                            <tr>
                                                <td></td>
                                                <td>FA14-B Certificación por CPN y legalizado por CPCE, sobre Recursos específicos del Servicio de Ayuda Económica con Fondos Propios</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA14-B')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa14-b" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo14-b" style="margin-top: 6px;font-size: 14px;" name="archivo14-b"  required>
                                                    <input type="hidden" id="cod14-b" name="cod14-b" value ="FA14-B">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error14-b"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida14-b"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos14-b" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                                                                               
                                            </tr>

                                            <tr>
                                                <td>FA15</td>
                                                <td>Copia de la Constancia de inscripción de la mutual ante la UIF como Emisora de Tarjeta de Crédito</td>
                                                <td>
                                                    @if(isset($documentos))
                                                        @foreach($documentos as $i => $valor)
                                                            @if($documentos[$i]->cod_doc == 'FA15')
                                                                <div style="text-align: end;font: caption;font-size: unset;">Archivo actual: <span id="fa15" style="text-align: end;font: caption;color: #7ed47e;">{{ $documentos[$i]->descripcion }}</span></div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <input type="file" class="form-control-file form-control-sm margen_izq" id="archivo15" style="margin-top: 6px;font-size: 14px;" name="archivo15"  required>
                                                    <input type="hidden" id="cod15" name="cod15" value ="FA15">
                                                    <div  style="text-align: end;font: caption;color: red;"><span id="salida_error15"></span></div>
                                                    <div  style="text-align: center;font: initial;color:rgb(75 191 101);"><span class="salida15"></span></div>
                                                    
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm guardar-datos-adjuntos15" style="background-color:#268fa9;float: inline-end;margin-top: 14px;color: aliceblue;display:{{ $ver }}" disabled title="Seleccione un archivo .pdf para adjuntar.">Guardar</button>
                                                </td>
                                                                                               
                                            </tr>

                                        </tbody>

                                  
                                    </table>
                                </div>
                            </div>
                        </div>
                     
                        
                    </div>

                            
                
        <!--</form> -->
       
    </div>
@endsection
<!-- Modal codigo postal-->
<div class="modal" id="myModalpre">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
        <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Buscar Localidad del Presidente</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
                     
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row sm-9" style="background-color: #e9ecef; padding-top: 15px;padding-bottom: 20px;">
                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot">Buscar por</label>
                        <input id="opcion" name="opcion" type="hidden">
                        <select class="form-control form-control-sm"  name="filtro_postal_pre" id="filtro_postal_pre">
                            <option selected value="" >Seleccione</option>
                            <option value="1">Nombre de Localidad</option>
                            <option value="2">Código Postal</p></option>
                            <option value="3">Todas</p></option>
                            
                        </select>
                    </div>

                    

                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot" id="titulo">Opción de busqueda</label>
                        <input type="text" class="form-control form-control-sm" id="buscar_postal_pre" name="buscar_postal_pre" disabled>
                      
                    </div>
        
                
                    
                    <div class="col-sm-2" style="margin-top: 15px">
                        <div><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                        <div><button type="button" class="btn btn-default buscar_codpostalpre" style="margin-bottom: -47px; margin-left: -16px;">
                            <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                        <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                    </div>
        
                </div>
                <br>
                <div class="resultado_postal"></div>
                <div class="error" style="color:crimson; text-align: center;"></div>
                
            </div>
                
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-info" data-dismiss="modal" style="color: #73c6d9;">Salir</button>
            </div>
                            
        </div>
    </div>
</div>

<!-- Modal codigo postal Secretario-->
<div class="modal" id="myModalsec">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
        <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Buscar Localidad del Secretario</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
                     
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row sm-9" style="background-color: #e9ecef; padding-top: 15px;padding-bottom: 20px;">
                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot">Buscar por</label>
                        <input id="opcion" name="opcion" type="hidden">
                        <select class="form-control form-control-sm"  name="filtro_postal_sec" id="filtro_postal_sec">
                            <option selected value="" >Seleccione</option>
                            <option value="1">Nombre de Localidad</option>
                            <option value="2">Código Postal</p></option>
                            <option value="3">Todas</p></option>
                            
                        </select>
                    </div>

                    

                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot" id="titulo">Opción de busqueda</label>
                        <input type="text" class="form-control form-control-sm" id="buscar_postal_sec" name="buscar_postal_sec" disabled>
                      
                    </div>
        
                
                    
                    <div class="col-sm-2" style="margin-top: 15px">
                        <div><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                        <div><button type="button" class="btn btn-default buscar_codpostalsec" style="margin-bottom: -47px; margin-left: -16px;">
                            <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                        <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                    </div>
        
                </div>
                <br>
                <div class="resultado_postalsec"></div>
                <div class="error" style="color:crimson; text-align: center;"></div>
                
            </div>
                
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-info" data-dismiss="modal" style="color: #73c6d9;">Salir</button>
            </div>
                            
        </div>
    </div>
</div>

<!-- Modal codigo postal Tesorero-->
<div class="modal" id="myModaltes">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
        <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Buscar Localidad del Tesorero</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
                     
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row sm-9" style="background-color: #e9ecef; padding-top: 15px;padding-bottom: 20px;">
                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot">Buscar por</label>
                        <input id="opcion" name="opcion" type="hidden">
                        <select class="form-control form-control-sm"  name="filtro_postal_tes" id="filtro_postal_tes">
                            <option selected value="" >Seleccione</option>
                            <option value="1">Nombre de Localidad</option>
                            <option value="2">Código Postal</p></option>
                            <option value="3">Todas</p></option>
                            
                        </select>
                    </div>

                    

                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot" id="titulo">Opción de busqueda</label>
                        <input type="text" class="form-control form-control-sm" id="buscar_postal_tes" name="buscar_postal_tes" disabled>
                      
                    </div>
        
                
                    
                    <div class="col-sm-2" style="margin-top: 15px">
                        <div><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                        <div><button type="button" class="btn btn-default buscar_codpostaltes" style="margin-bottom: -47px; margin-left: -16px;">
                            <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                        <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                    </div>
        
                </div>
                <br>
                <div class="resultado_postaltes"></div>
                <div class="error" style="color:crimson; text-align: center;"></div>
                
            </div>
                
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-info" data-dismiss="modal" style="color: #73c6d9;">Salir</button>
            </div>
                            
        </div>
    </div>
</div>


<!-- Modal codigo postal Apoderado-->
<div class="modal" id="myModalapo">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
        <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Buscar Localidad del Apoderado</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
                     
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row sm-9" style="background-color: #e9ecef; padding-top: 15px;padding-bottom: 20px;">
                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot">Buscar por</label>
                        <input id="opcion" name="opcion" type="hidden">
                        <select class="form-control form-control-sm"  name="filtro_postal_apo" id="filtro_postal_apo">
                            <option selected value="" >Seleccione</option>
                            <option value="1">Nombre de Localidad</option>
                            <option value="2">Código Postal</p></option>
                            <option value="3">Todas</p></option>
                            
                        </select>
                    </div>

                    

                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot" id="titulo">Opción de busqueda</label>
                        <input type="text" class="form-control form-control-sm" id="buscar_postal_apo" name="buscar_postal_apo" disabled>
                      
                    </div>
        
                
                    
                    <div class="col-sm-2" style="margin-top: 15px">
                        <div><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                        <div><button type="button" class="btn btn-default buscar_codpostalapo" style="margin-bottom: -47px; margin-left: -16px;">
                            <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                        <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                    </div>
        
                </div>
                <br>
                <div class="resultado_postalapo"></div>
                <div class="error" style="color:crimson; text-align: center;"></div>
                
            </div>
                
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-info" data-dismiss="modal" style="color: #73c6d9;">Salir</button>
            </div>
                            
        </div>
    </div>
</div>

<div class="modal fade" id="mensaje">
    <div class="modal-dialog">
      <div class="modal-content">
  
        <!-- cabecera del diálogo -->
        <div class="modal-header">
          <h4 class="modal-title">Mensaje</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- cuerpo del diálogo -->
        <div class="modal-body">
            <div class="mensaje_s">
                <strong class="small-font" id="salida" style="color:rgb(75 191 101);background-color:#f7eeee;"></strong>
            </div>
          
            <div class="mensaje_e">
                <strong class="small-font" id="error" style="color:red;background-color:#f7eeee;"></strong>
            </div>
        </div>
  
        <!-- pie del diálogo -->
        <div class="modal-footer">
          <button type="button" class="btn boton-guardar" style="background-color:#268fa9;float: inline-end;color: aliceblue;" data-dismiss="modal">Cerrar</button>
        </div>
  
      </div>
    </div>
  </div> 
@extends('layout.base')
<?php
//dd($sec['estado']);

$today = date('Y-m-d');
//dd($datos);
?>

@section('content')
    <div class="content" style="position: relative;z-index: 100;">
        <div style="">
            <div class="jumbotron jumbotron-fluid" style="display:flex; padding-top:0px; padding-bottom:0px; margin-bottom:10px;" style="background-color: #F1F2F2">
                <div class="container" style="padding-bottom: 10px;padding-top: 10px;">
                    <img src="img/jumbo.png" class="float-right d-none d-lg-block" style="margin-left: -600px; height: 100%;"> {{--radial-gradient(circle, black 0%, transparent 85%);--}}
                    <h1 class="display-6">Tarjeta Prepaga y de Crédito Mutual</h1>
                    <p class="lead">MasterCard "FAUM Con Vos", Al resguardo del bien mutuo, FAUM CON VOS, siempre a tu lado.</p>
                                 
                </div>
            </div>
        </div>
    </div>

    <div class="container" style="position: relative;z-index: -100;" id="form">
        
        @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" data-toggle="tooltip" data-placement="top" title="Salir de la Notificación">
                    <i class='far fa-times-circle' style='font-size:24px;color:red'></i>
                </button>	
                <ul>
                    @foreach ($errors->all() as $err)
                    <li>{{ $err }}</li>
                    @endforeach
                </ul>
            </div>
            
        @endif

        <h4>Actualizar Sucursal</h4><div id="estado_t" style="display: none">Estado Actual: <strong id="estado_mutual" style="color: rgb(105, 191, 105);"></strong></div>
        <div class="tab-content" style="background-color: #e9ecef;" id="usuario" style="width: 1036px" >
            <fieldset style="width:100%;" >
                <div class="row mrow" style="padding-top: 10px;">
            
                    <div class="col-md-6 col-sm-4 col-xs-2">
                        <label class="help-block text-muted small-font margen_bot">Mutual Adherente</label>
                        <select class="form-control form-control-sm margen_izq mutual" id="id_mutual" name="id_mutual" required>
                            <option value="" disabled selected>Seleccione</option>
                            @foreach ($mutuales as $p)
                                <option value="{{ $p->soc_numero_socio }}" @if(isset($mutuales->soc_numero_socio) && $mutuales->soc_numero_socio == $p->soc_numero_socio) selected @endif>({{ $p->soc_numero_socio }}) {{ $p->nombre }}</option>       
                            @endforeach
                        </select> 
                    </div>

                        
                    <div class="col-md-5 col-sm-3 col-xs-2">
                        <label class="help-block text-muted small-font margen_bot">Sucursales Adherente</label>
                        <select class="form-control form-control-sm margen_izq" id="id_sucursal" name="id_sucursal" required>
                            <option value="" selected>Seleccione</option>
                           
                        </select> 
                    </div>

                    <div class="col-md-1 col-sm-1 col-xs-1">
                        <div style="margin-bottom: 20px"><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                        <div><button type="button" class="btn btn-default buscar-suc" >
                            <i class="fas fa-search" title="Buscar Sucursal" data-toggle="tooltip" data-placement="top"></i></button></div>
                        
                    </div>

                </div>
                <div class="resultado-buscar">
                
            </fieldset>
            <div class="" style="padding-top: 0px;text-align: end;padding-bottom: 5px;"> 
                <button type="button" class="btn boton-guardar update-datos-suc" style="background-color:#268fa9;color: aliceblue;display:none">Actualizar</button>
                <!--<button type="button" class="btn boton-guardar delete-datos-usuario" style="background-color:#268fa9;float: inline-end;color: aliceblue;display:none">Eliminar</button> -->
            </div>
                    
        </div>
    </div>
    
    
    
@endsection

<!-- Modal codigo postal-->
<div class="modal" id="myModal">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
        <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Buscar Localidad de la Mutual</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
                     
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row sm-9" style="background-color: #e9ecef; padding-top: 15px;padding-bottom: 20px;">
                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot">Buscar por</label>
                        <input id="opcion" name="opcion" type="hidden">
                            <select class="form-control form-control-sm"  name="filtro_postal" id="filtro_postal">
                            <option selected value="" >Seleccione</option>
                            <option value="1">Nombre de Localidad</option>
                            <option value="2">Código Postal</p></option>
                            <option value="3">Todas</p></option>
                            
                        </select>
                    </div>

                    

                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot" id="titulo">Opción de busqueda</label>
                        <input type="text" class="form-control form-control-sm" id="buscar_postal" name="buscar_postal" disabled>
                      
                    </div>
        
                
                    
                    <div class="col-sm-2" style="margin-top: 15px">
                        <div><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                        <div><button type="button" class="btn btn-default buscar_codpostal_s" style="margin-bottom: -47px; margin-left: -16px;">
                            <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                        <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                    </div>
        
                </div>
                <br>
                <div class="resultado_postal"></div>
                <div class="error" style="color:crimson; text-align: center;"></div>
                
            </div>
                
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-info" data-dismiss="modal" style="color: #73c6d9;">Salir</button>
            </div>
                            
        </div>
    </div>
</div>

<div class="modal fade" id="mensaje_usu">
    <div class="modal-dialog">
      <div class="modal-content">
  
        <!-- cabecera del diálogo -->
        <div class="modal-header">
          <h4 class="modal-title">Mensaje</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- cuerpo del diálogo -->
        <div class="modal-body">
            <div class="mensaje_s">
                <strong class="small-font" id="salida" style="color:rgb(75 191 101);background-color:#f7eeee;"></strong>
            </div>
          
            <div class="mensaje_e">
                <strong class="small-font" id="error" style="color:red;background-color:#f7eeee;"></strong>
            </div>
        </div>
  
        <!-- pie del diálogo -->
        <div class="modal-footer">
          <button type="button" class="btn boton-guardar" style="background-color:#268fa9;float: inline-end;color: aliceblue;" data-dismiss="modal">Cerrar</button>
        </div>
  
      </div>
    </div>
  </div> 
<script>

</script>

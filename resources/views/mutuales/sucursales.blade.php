<?php
if(isset($estado_mutual) && $estado_mutual == 0){
    $ver = '';
    $color = 'rgb(105, 191, 105)';
}elseif(isset($estado_mutual) && $estado_mutual == 3){
    $ver = '';
    $color = 'red';
}else{
    $ver = 'disabled';
    $color = 'rgb(105, 191, 105)';
}
?>
<form action="" id="form" name="form" class="form" autocomplete="off">
    @if(isset($sucursales))
        
            <hr>
            <legend class="margen_izq">Sucursales Adheridas</Canvas></legend>
                       
           
            @foreach($sucursales as $i => $valor)
                <?php  ?>
                <div class="row mrow" style="padding-top: 10px;">
                             
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <div><label for="" class="help-block text-muted small-font margen_bot">N° Sucursal</label></div>
                        <div><input type="text" class="form-control form-control-sm margen_izq" id="soc_sucursal{{ $i }}" name="soc_sucursal{{ $i }}" placeholder="N° de Sucursal" value="{{$valor->soc_sucursal}}" maxlength="5" readonly></div>
                    </div>
                    <div class="col-md-6 col-sm-4 col-xs-2">
                        <div>
                            <label for="" class="help-block text-muted small-font margen_bot">Nombre</label>
                            <input type="text" class="form-control form-control-sm margen_izq" id="nombre{{ $i }}" name="nombre{{ $i }}" placeholder="Nombre de la Sucursal" maxlength="50" value="{{ $valor->nombre }}" readonly> 
                        </div>
                   
                    </div>
                </div>

                <div class="row  mrow" style="padding-top: 10px;">
                    
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <div><label for="" class="help-block text-muted small-font margen_bot">Código Postal</label></div>
                        <div><input type="text" class="form-control form-control-sm margen_izq codigo_postal_suc" id="codigo_postalsuc{{ $i }}" name="codigo_postalsuc{{ $i }}" placeholder="Código Postal" value="{{$valor->codigo_postal }}" maxlength="8"  required readonly></div>
                    </div>

                    <!--<div class="col-md-1 col-sm-1 col-xs-1">
                        <div style="margin-bottom: 20px"><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                        <div><button type="button" class="btn btn-default modal-suc"  data-id={{ $loop->index }}> 
                            <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                        <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                    </div> -->

                    <div class="col-md-3 col-sm-2 col-xs-2">
                        <div>
                            <label for="" class="help-block text-muted small-font margen_bot">Localidad</label>
                            <input type="text" class="form-control form-control-sm margen_izq localidad_nombre_localidad_suc" id="localidad_nombre_localidadsuc{{ $i }}" name="localidad_nombre_localidadsuc{{ $i }}" placeholder="" maxlength="60" value="{{$valor->localidad  }}" readonly> 
                            <input id="provincia_doc{{ $i }}" name="provincia_doc{{ $i }}" type="hidden" value="0">
                        </div>
                                          
                    </div>
                
                    <div class="col-md-3 col-sm-2 col-xs-2">
                        <div>
                            <label for="" class="help-block text-muted small-font margen_bot">Provincia</label>
                            <input type="text" class="form-control form-control-sm margen_izq provincia_nombre_provincia_suc" id="provincia_nombre_provinciasuc{{ $i }}" name="provincia_nombre_provinciasuc{{ $i }}" placeholder="" maxlength="60" value="{{$valor->provincia}}" readonly> 
                        </div>
                                                
                    </div>
                </div>
                <hr>
<!-- Modal codigo postal sucursal template -->
 <div class="modal verm" id="myModalsuc{{$loop->index}}" role="dialog" data-backdrop="false"> <!---->
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
      
            <div class="modal-header">
                <h4 class="modal-title">Buscar Localidad de la sucursal (Actualizar Cod Postal)</h4>
                <input type="hidden" id="{{ $loop->index }}">
                <!-- <input type="hidden" class="selec">-->
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
                     
         
            <div class="modal-body">
                <div class="row sm-9" style="background-color: #e9ecef; padding-top: 15px;padding-bottom: 20px;">
                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot">Buscar por</label>
                            <select class="form-control form-control-sm filtro_postalsuc" data-id="{{ $loop->index }}" >
                            <option selected value="" >Seleccione</option>
                            <option value="1">Nombre de Localidad</option>
                            <option value="2">Código Postal</option>
                            <option value="3">Todas</option>
                            
                        </select>
                    </div>

                    

                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot" id="titulo">Opción de busqueda</label>
                        <input type="text" class="form-control form-control-sm buscar_postal-suc" id="id{{ $loop->index }}">
                      
                    </div>
        
                
                    
                    <div class="col-sm-2" style="margin-top: 15px">
                        <div><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                        <div><button type="button" class="btn btn-default buscar_codpostal_suc" style="margin-bottom: -47px; margin-left: -16px;">
                            <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                        <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                    </div>
        
                </div>
                <br>
                <div class="resultado_postal_suc"></div>
                <div class="error" style="color:crimson; text-align: center;"></div>
                
            </div>
                
          
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-info salir-mt" data-dismiss="modal" style="color: #73c6d9;">Salir</button>
            </div>
                            
        </div>
    </div>
</div>
            @endforeach
         
        
    @endif
</form>

 <script>

   

    $(".modal-suc").click(function(e) {
        //console.log('nueva js'+ $(this).attr("id")); //data("fruta")
        console.log('nueva js'+ $(this).data("id"));
        console.log($(this).data("id"));
        //si le quiero pasar los valores de un campo por jquery
        $("#myModalsuc" + $(this).data("id")).modal(); // le concateno el id del boton para generar los modales dinamicos
        //$(".selec").val($(this).attr("id"));
        $($(this).data("id")).val($(this).data("id"));
        $(".filtro_postalsuc").val('');
        $(".buscar_postal-suc").val('');
       
       
    });

    $(".filtro_postalsuc").change(function(e) {
        $('.resultado_postal_suc').html('');
        console.log('seleccion filtro..........');
        console.log($(".filtro_postalsuc").val());
    });

    $(".filtro_postalsuc").click(function(e) {
        console.log('paso seleccionar el .filtro_postal');
        
        console.log('selec de sucursal');
        console.log($('#v' + $(this).data("id")).val());
        filtro = $('.filtro_postalsuc').val();
        console.log('selec filtro');
        console.log(filtro);

        if(filtro == 1){
            $(".buscar_postal-suc").val('');
            $(".buscar_postal-suc").removeAttr('disabled').attr('placeholder','Nombre de la Localidad');
                        
                
        }else if(filtro == 2){
            $(".buscar_postal-suc").val('');
            $(".buscar_postal-suc").removeAttr('disabled').attr('placeholder','Código Postal');
                
        }else if(filtro == 3){
            $(".buscar_postal-suc").val('');
            $(".buscar_postal-suc").attr('disabled','disabled').attr('placeholder','Todas');
                
                        
        }else if(filtro == 0){
            $(".buscar_postal-suc").attr('disabled','disabled').attr('placeholder',' ');
        }
    });


    $(".buscar_codpostal_suc").click(function(e) {
        console.log('entro por click buscar codpostal de alta de sucursal a una mutual 0103');
        e.preventDefault();
        var opcion = $('.filtro_postalsuc').val();
        var buscar = $('.buscar_postal-suc').val();
        var sel = 0;//$('.selec').val();
        var id = $('#cod_postal'+ $(this).attr("id")).val();
        $('.error').text('');
        //$('.resultado_postal_suc').html('');
        
        console.log('filtro_postal:', opcion);
        console.log('buscar_postal:', buscar);
          
        var error = false;


        if (!error) {
            var data = {filtro: opcion,buscar: buscar, sel:sel,id:id};
            console.log('paso :', data);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: 'POST',
                url: 'buscar-codpostal-modal-suc-up',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                   
                },
                success: function(data) {
                    if (data.success) {
                        console.log(data);
                        $('.resultado_postal_suc').html(data.datos).show();
                                             
                    }else {
                        console.log('paso succ error');
                        $('.error').html(data.error).show();
                        
                    }
                    
                }
            });
        }
    });
   
      
    

</script>


            
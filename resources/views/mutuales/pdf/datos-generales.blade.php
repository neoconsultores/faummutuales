<html>
    <title>Reporte</title>
    <h4 align="right" class="titulo">Fecha-Hora:{{ date('d-m-Y h:i') }}</h4>
 
<head>
    <?php    
              
    ?>
        <style>
                 body{
                  font-family: sans-serif;
                }
               
                
                footer {
                 
                  left: 0px;
                  bottom: -50px;
                  right: 0px;
                  height: 40px;
                 
                }
                             
                footer p {
                  text-align: center;
                }

                td {
                    text-align: center;
                    width: 60px;
                    height: 30px;
                    border-bottom: 1px solid #ddd;
                    font: 12px Helvetica, Arial, san-serif;
                    
                }

                .st {
                    text-align: left;
                    width: 60px;
                }

                th {
                    text-align: center;
                    width: 60px;
                    border-bottom: 1px solid #ddd;
                    font: 12px Helvetica, Arial, san-serif;
                }
                .th {
                    text-align: center;
                    width: 30px;
                }

                div h4 {
                  text-align: right;
                  font: 12px Helvetica, Arial, san-serif;
                }

                table {
                  border-collapse: collapse;
                  width: 100%;
                }

                .titulo {
                    font: 12px Helvetica, Arial, san-serif;
                }

                .b { border-collapse: collapse;} 
                tr, td, th{ border: 1px solid grey;}

                .td_alto {
                    height: 25px;
                }
               
               
               
            </style>
</head>
<body>
    <div>
        <img src="img/logofaum.png"  width="" height="" style="text-align: left">
       
    </div>

    <div align="center"><h3>SOLICITUD DE CONTRATACIÓN</h3>
    <h3>FEDERACIONES ALIADAS UNION MUTUAL</h3></div>
    <hr>
    <p align="right;font-size: 0.9em">{{ $localidad_mutual }}, {{ $fecha }} </p>
        <div class="row mrow"> 
            <div class="col-md-2"><label class="help-block text-muted small-font margen_bot">N° Adherente: {{ $nro_adherente }}</label></div>
            <div class="col-md-6"><label class="help-block text-muted small-font margen_bot">Sucursal: {{ $sucursal }}</label> </div>
        </div>

        
    
    <p align="" style="font-size: 0.9em">Sres Consejo Directivo de Federaciones Aliadas Unión Mutual</strong></p>
    <p align="" style="font-size: 0.9em;text-indent: 10px" >Por la presente, solicitamos nuestra incorporación, como mutual contratante, al Sistema de Tarjetas "FAUM Con Vos", 
        de acuerdo a las cláusulas del contrato entre FAUM y la Mutual, que declaramos conocer y cumplir.</p>

    <p align="center" style="font-size: 0.9em">Datos del Solicitante</p>
    <!--<p align="left" style="font-size: 0.9em">Datos Persona Juridica</p> -->

    <table class="b" style="">
        <tr>
            <th colspan="3" style="text-align: left">Nombre: {{ $nombre_mutual }}</th><th colspan="3" style="text-align: left">CUIT - N°: {{ $nro_doc }} </th>
        </tr>
        <tr>
            <td><strong> Fecha de Solicitud</strong></td>
            <td><strong>Fecha Constitución</strong></td>
            <td colspan="2"><strong>Matrícula INAES</strong></td>
            <td colspan="2"><strong>Actividad Principal</strong></td>
            
        </tr>
        <tr>
            <td>{{ date('d-m-Y') }}</td>
            <td>{{ date('d-m-Y', strtotime($fecha_constitucion)) }}</td>
            <td colspan="2">{{ $inscripcion_inaes }}</td>
            <td colspan="2">{{ $actividad_principal }}</td>
        </tr>
        <tr>
            <td><strong>Domicilio Real</strong></td>
            <td><strong>Piso / Depto</strong></td>
            <td><strong>Localidad</strong></td>
            <td><strong>Provincia</strong></td>
            <td><strong>Código Postal</strong></td>
            <td><strong>Tel Oficina</strong></td>
            
        </tr>
        <tr>
            <td>{{ $domicilio_mutual }}</td>
            <td>{{ $piso_mutual }}</td>
            <td>{{ $localidad_mutual }}</td>
            <td>{{ $provincia_mutual }}</td>
            <td>{{ $cod_postal }}</td>
            <td>{{ $telefono_ofi }}</td>
        </tr>
        <tr>
            <td><strong>Tel Celular</strong></td>
            <td colspan="2"><strong>Correo Electrónico</strong></td>
            <td><strong>Condición IVA</strong></td>
            <td><strong>Riesgo Crédito</strong></td>
            <td><strong>Inicio de Ej Económico</strong></td>
            
            
        </tr>
        <tr>
            <td>{{ $telefono_cel }}</td>
            <td colspan="2">{{ $correo }}</td>
            <td>{{ $cond_iva }}</td>
            <td>En calculo</td>
            <td>{{ $ejer_eco }}</td>
            
        </tr>
    </table>

    <table class="b" style="">
        <tr>
            <th colspan="3" style="text-align: center">Integrantes de la Comisión</th>
        </tr>
        <tr>
            <td><strong>Nombre y Apellido</strong></td>
            <td><strong>Tipo - N° Documento</strong></td>
            <td><strong>Carácter</strong></td>
        </tr>
        <tr>
            <td>{{ $nombre_pre }}</td>
            <td>{{ $tipo_doc_pre }} - {{ $nro_doc_pre }}</td>
            <td>Presidente</td>
        </tr>
        <tr>
            <td>{{ $nombre_sec }}</td>
            <td>{{ $tipo_doc_sec }} - {{ $nro_doc_sec }}</td>
            <td>Secretario</td>
        </tr>
        <tr>
            <td>{{ $nombre_tes }}</td>
            <td>{{ $tipo_doc_tes }} - {{ $nro_doc_tes }}</td>
            <td>Tesorero</td>
        </tr>
        @if(isset($nombre_apo) && $nombre_apo <> '')
            <tr>
                <td>{{ $nombre_apo }}</td>
                <td>{{ $tipo_doc_apo }} - {{ $nro_doc_apo }}</td>
                <td>Apoderado</td>
            </tr>
        @endif
    </table>

    <table class="b" style="padding-top: 5px;">
        <tr>
            <th>Firma</th><th>Aclaración y Cargo</th><th>D.N.I</th>
        </tr>
        <tr>
            <td class="td_alto"></td>
            <td class="td_alto"></td>
            <td class="td_alto"></td>
        </tr>
        <tr>
            <td class="td_alto"></td>
            <td class="td_alto"></td>
            <td class="td_alto"></td>
        </tr>
        <tr>
            <td class="td_alto"></td>
            <td class="td_alto"></td>
            <td class="td_alto"></td>
        </tr>
        <tr>
            <td class="td_alto"></td>
            <td class="td_alto"></td>
            <td class="td_alto"></td>
        </tr>
    </table>
    <p style="align=right;font-size: 0.7em">FA01</p>

</body>
</html>     

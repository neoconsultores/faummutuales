<html>
    <title>DECLARACIÓN JURADA</title>
    <h4 align="right" class="titulo">Fecha-Hora:{{ date('d-m-Y h:i') }}</h4>
 
<head>
    <?php    
              
    ?>
        <style>
                 body{
                  font-family: sans-serif;
                }
               
                
                footer {
                 
                  left: 0px;
                  bottom: -50px;
                  right: 0px;
                  height: 40px;
                 
                }
                             
                footer p {
                  text-align: center;
                }

                td {
                    text-align: center;
                    width: 60px;
                    height: 30px;
                    border-bottom: 1px solid #ddd;
                    font: 12px Helvetica, Arial, san-serif;
                    
                }

                .st {
                    text-align: left;
                    width: 60px;
                }

                th {
                    text-align: center;
                    width: 60px;
                    border-bottom: 1px solid #ddd;
                    font: 12px Helvetica, Arial, san-serif;
                }
                .th {
                    text-align: center;
                    width: 30px;
                }

                div h4 {
                  text-align: right;
                  font: 12px Helvetica, Arial, san-serif;
                }

                table {
                  border-collapse: collapse;
                  width: 100%;
                }

                .titulo {
                    font: 12px Helvetica, Arial, san-serif;
                }

                .b { border-collapse: collapse; } 
                tr, td, th{ border: 1px solid grey;}
               
               
               
              </style>
</head>
<body>
    <div><img src="img/logofaum.png"  width="" height=""></div>
    <div align="center"><h3>DECLARACIÓN JURADA</h3>
    <h3>FEDERACIONES ALIADAS UNION MUTUAL</h3></div>

    <p align="right;font-size: 0.9em">{{ $localidad_mutual }}, {{ $fecha }} </p>

    <p style="font-size: 0.9em"> <strong>{{ $nombre_pre }}</strong> y <strong>{{ $nombre_sec }}</strong> en caracter de Presidente y Secretario respectivamente de la Mutual <strong>{{ $nombre_mutual }}</strong>,
        Matrícula INAES N° <strong>{{ $nro_inaes }}</strong>, domiciliada en la calle <strong>{{ $domicilio_mutual }}</strong> de la ciudad de <strong>{{ $localidad_mutual }}</strong>,
    por medio de la presente manifestamos con carácter de DECLARACIÓN JURADA que:</p>   
    <p align="" style="font-size: 0.9em;padding-bottom: 10px">Los documentos que a continuación se describen, que se presentan digitalizados en formato PDF, a los efectos de cumplimentar con los requisitos
        exigidos para la Incorporación de la Mutual <strong>{{ $nombre_mutual }}</strong>, como Contratante, son auténticos y fieles a los originales que obran
        en nuestro poder, los cuales se encuentran a completa disposición de Federaciones Aliadas Unión Mutual (FAUM) en caso de ser requeridos: 
    @if (isset($documentos))
    <?php //dd($documentos);?>
        @foreach($documentos as $i => $res)
                <br>
                @if($documentos[$i] == 'FA01')
                    - (FA01) Formulario de Solicitud de Adhesión de Persona Jurídica.
                @endif
                    
                @if($documentos[$i] == 'FA03')
                    - (FA03) Copia del Estatuto aprobado e inscripto en el INAES.
                @endif
                @if($documentos[$i] == 'FA04')
                    - (FA04) Copia del Reglamento de Ayuda Económica aprobado e inscripto en el INAES.
                @endif
                @if($documentos[$i] == 'FA05')
                    - (FA05) Copia del Reglamento de Tarjeta de Crédito.
                @endif
                @if($documentos[$i] == 'FA06')
                    - (FA06) Constancia de CUIT de la Mutual.
                @endif
                @if($documentos[$i] == 'FA07')
                    - (FA07) Copia certificada por escribano de última Acta de designación de autoridades y cargos. <!-- distribución de -->
                @endif
                @if($documentos[$i] == 'FA08')
                    - (FA08) Copia de Poderes otorgados, que habiliten a actuar ante FAUM.
                @endif
                @if($documentos[$i] == 'FA09-A')
                    - (FA09-A) Copia de DNI del Presidente.
                @endif
                @if($documentos[$i] == 'FA09-B')
                    - (FA09-B) Copia de DNI del Secretario.
                @endif
                @if($documentos[$i] == 'FA09-C')
                    - (FA09-C) Copia de DNI del Tesorero.
                @endif
                @if($documentos[$i] == 'FA09-D')
                    - (FA09-D) Copia de DNI del Apoderado.
                @endif
                @if($documentos[$i] == 'FA10-A')
                   - (FA10-A) Formulario de DDJJ de Persona Expuesta Políticamente del Presidente.
                @endif
                @if($documentos[$i] == 'FA10-B')
                    - (FA10-B) Formulario de DDJJ de Persona Expuesta Políticamente del Secretario.
                @endif
                @if($documentos[$i] == 'FA10-C')
                    - (FA10-C) Formulario de DDJJ de Persona Expuesta Políticamente del Tesorero.
                @endif
                @if($documentos[$i] == 'FA10-D')
                    - (FA10-D) Formulario de DDJJ de Persona Expuesta Políticamente del Apoderado.
                @endif
                @if($documentos[$i] == 'FA11')
                    - (FA11) Memoria y Balance del último ejercicio cerrado, certificado por CPN y legalizado por CPCE.
                    
                @endif
                @if($documentos[$i] == 'FA12')
                    - (FA12) Copia certificada por escribano del Acta donde se aprueba la Memoria y Balance citado.
                @endif
                @if($documentos[$i] == 'FA13-A')
                    - (FA13-A) DJ sobre Conformación de Ahorros.
                @endif
                @if($documentos[$i] == 'FA13-B')
                    - (FA13-B) Certificación sobre Conformación de Ahorros,certificación por CPN y legalizado por CPCE.
                @endif

                @if($documentos[$i] == 'FA14-A')
                    - (FA14-A) DJ sobre Recursos específicos del Servicio de Ayuda Económica con Fondos Propios.
                @endif
                @if($documentos[$i] == 'FA14-B')
                    - (FA14-B) Certificación por CPN y legalizado por CPCE, sobre Recursos específicos del Servicio de Ayuda Económica con Fondos Propios.
                @endif

                @if($documentos[$i] == 'FA15')
                    - (FA15) Copia de la Constancia de inscripción de la mutual ante la UIF como Emisora de Tarjeta de Crédito.
                @endif
            
           
        @endforeach
    @else  
    
        <p style="font-size: 0.9em"> No existes docuemntos adjuntados.
    @endif
        
    

    <table class="b" style="padding-top: 0px;">
        <tr>
            <th>Firma</th><th>Aclaración y Cargo</th><th>D.N.I</th>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <p style="align=right;font-size: 0.7em">FA00</p>
</body>
</html>     

<html>
    <title>Reporte</title>
    <h4 align="right" class="titulo">Fecha-Hora:{{ date('d-m-Y h:i') }}</h4>
 
<head>
    <?php    
              
    ?>
        <style>
                 body{
                  font-family: sans-serif;
                }
               
                
                footer {
                 
                  left: 0px;
                  bottom: -50px;
                  right: 0px;
                  height: 40px;
                 
                }
                             
                footer p {
                  text-align: center;
                }

                td {
                    text-align: center;
                    width: 60px;
                    height: 30px;
                    border-bottom: 1px solid #ddd;
                    font: 12px Helvetica, Arial, san-serif;
                    
                }

                .st {
                    text-align: left;
                    width: 60px;
                }

                th {
                    text-align: center;
                    width: 60px;
                    border-bottom: 1px solid #ddd;
                    font: 12px Helvetica, Arial, san-serif;
                }
                .th {
                    text-align: center;
                    width: 30px;
                }

                div h4 {
                  text-align: right;
                  font: 12px Helvetica, Arial, san-serif;
                }

                table {
                  border-collapse: collapse;
                  width: 100%;
                }

                .titulo {
                    font: 12px Helvetica, Arial, san-serif;
                }
               
               
               
              </style>
</head>
<body>
    <div><img src="img/logofaum.png"  width="" height=""></div><br>
    <div ><h3 align="center">DECLARACIÓN JURADA SOBRE LA CONDICIÓN DE PERSONA EXPUESTA POLÍTICAMENTE</h3>
      <h3 align="center">(Resolución 11/2011)</h3>
    </div>
    

    <p style="font-size: 0.9em;text-indent: 10px">El que suscribe, <strong>{{ $nombre}} {{ $tipo_doc}} : {{$nro_doc}}</strong>  declara bajo juramento que los datos consignados en la presente
        son correctos, completos y fiel expresión de la verdad y que Si(<strong>{{ $si }}</strong>)     No(<strong>{{ $no }}</strong>)</p>  
      <p style="font-size: 0.9em">se encuentra incluido y/o alcanzado dentro
        de la "Nómina de Funciones de Personas Expuestas Políticamente" aprobada por la Unidad de Información
        Financiera, que ha leído y suscripto.</p>
      <p style="font-size: 0.9em">En caso afirmativo indicar: Cargo/Función/Jerarquía, o relación con la Persona Expuesta Políticamente:
        _____________________________________________________________________________________, 
        Además, asume el compromiso de informar cualquier modificación que se produzca a este respecto, dentro
        de los treinta (30) días de ocurrida, mediante la presentación de una nueva declaración jurada.</p>
        <p style="font-size: 0.9em">Documento: Tipo: <strong>{{ $tipo_doc}}</strong> - N°  <strong>{{$nro_doc}}</strong></p>
        <p style="font-size: 0.9em">Carácter invocado:</p>
        <p style="font-size: 0.9em">Denominación de la persona jurídica:</p>
        <p style="font-size: 0.9em">CUIL - Nº: </p>


      <p style="font-size: 0.9em">Firma:________________________   Aclaración: __________________________</p>
      <!--<p style="font-size: 0.9em">Sello del Sujeto Obligado autorizado.</p>-->
      <!--<P style="font-size: 0.9em">Certifico que la firma que antecede concuerda con la registrada en nuestros libros y fue puesta en mi presencia.</P>-->
      <p style="font-size: 0.9em">Lugar: {{ $localidad }}, {{ $provincia }} , {{ $fecha }} </p>
      
        
    
      <p style="align=right;font-size: 0.7em;padding-top:250px">{{ $doc }}</p>  
</body>
</html>     

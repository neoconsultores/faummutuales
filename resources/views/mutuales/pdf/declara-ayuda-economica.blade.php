<html>
    <title><html>
    <title>Declaración Jurada sobre recursos Específicos del Servicio de Ayuda Economica con Fondos Propios</title>
    <!--<h4 align="right" class="titulo">Fecha-Hora:{{ date('d-m-Y h:i') }}</h4>-->
 
<head>
    <?php    
              
    ?>
        <style>
                 body{
                  font-family: sans-serif;
                }
               
                
                footer {
                 
                  left: 0px;
                  bottom: -50px;
                  right: 0px;
                  height: 40px;
                 
                }
                             
                footer p {
                  text-align: center;
                }

                td {
                    text-align: center;
                    width: 60px;
                    height: 30px;
                    font: 12px Helvetica, Arial, san-serif;
                    
                }

                .st {
                    text-align: left;
                    width: 60px;
                }

                th {
                    text-align: center;
                    width: 60px;
                    border-bottom: 1px solid #ddd;
                    font: 12px Helvetica, Arial, san-serif;
                }
                .th {
                    text-align: center;
                    width: 30px;
                }

                div h4 {
                  text-align: right;
                  font: 12px Helvetica, Arial, san-serif;
                }

                table {
                  border-collapse: collapse;
                  width: 100%;
                }

                .titulo {
                    font: 12px Helvetica, Arial, san-serif;
                }

                .b { border-collapse: collapse; } 
                              
               
               
              </style>
</head>
<body>
    <div><img src="img/logofaum.png"  width="" height=""></div><br>
    <div align="center" style="padding-bottom: 30px"><h3 style="text-transform:uppercase;">{{ $nombre}}</h3></div>

    <p align="" style="font-size: 0.9em;text-indent: 15px;padding-bottom: 0px;">Los abajos firmantes, Presidente y Secretario de la Mutual ____________________declaramos que en el</p>
        
    <p style="font-size: 0.9em;text-indent: 0px;padding-bottom: 0px;">ejercicio económico N° ___ cerrado el _/_/_, hemos obtenido recursos en el servicio de ayuda económica</p>
    <p style="font-size: 0.9em;text-indent: 0px;padding-bottom: 0px;">con fondos propios por $ ________ identificados en la cuenta contable N°_______________________, </p>
    <p style="font-size: 0.9em;text-indent: 0px;padding-bottom: 150px;">operando una TNA promedio de ____________________.</p>

        <table>
          <tr>
            <td>_______________________</td><td>_______________________</td>
          
          </tr>
          <tr>
            <td><b>Presidente</b></td><td><b>Secretario</b></td>
          </tr>
    
        </table>
        <p style="align=right;font-size: 0.7em;padding-top:300px">FA14-A</p>
</body>
</html>
<html>
    <title>Reporte</title>
    <h4 align="right" class="titulo">Fecha-Hora:{{ date('d-m-Y h:i') }}</h4>
 
<head>
    <?php    
              
    ?>
        <style>
                 body{
                  font-family: sans-serif;
                }
               
                
                footer {
                 
                  left: 0px;
                  bottom: -50px;
                  right: 0px;
                  height: 40px;
                 
                }
                             
                footer p {
                  text-align: center;
                }

                td {
                    text-align: center;
                    width: 60px;
                    height: 30px;
                    border-bottom: 1px solid #ddd;
                    font: 12px Helvetica, Arial, san-serif;
                    
                }

                .st {
                    text-align: left;
                    width: 60px;
                }

                th {
                    text-align: center;
                    width: 60px;
                    border-bottom: 1px solid #ddd;
                    font: 12px Helvetica, Arial, san-serif;
                }
                .th {
                    text-align: center;
                    width: 30px;
                }

                div h4 {
                  text-align: right;
                  font: 12px Helvetica, Arial, san-serif;
                }

                table {
                  border-collapse: collapse;
                  width: 100%;
                }

                .titulo {
                    font: 12px Helvetica, Arial, san-serif;
                }
               
               
               
              </style>
</head>
<body>
    <div><img src="img/logofaum.png"  width="" height=""></div><br>
    <div ><h3 align="center">DD JJ SOBRE CONDICIÓN DE SUJETO OBLIGADO ANTE LA UNIDAD DE INFORMACIÓN FINANCIERA - PERSONA FÍSICA</h3></div>

    <p style="font-size: 0.9em">Presente</p>

    <p style="font-size: 0.9em">Por la presente, el que suscribe, <strong>{{ $nombre}} {{ $tipo_doc}} : {{$nro_doc}}</strong> declara bajo juramento que la
      empresa a la que represento Si(<strong>{{ $si }}</strong>) No(<strong>{{ $no }}</strong>)</p> es Sujeto Obligado a informar a la UIF de acuerdo a lo establecido
      en el art. 20, inc ....................................................... (1) de la Ley N° 25246 (modificada por la Ley N°
      26.683).
      Presente
      En caso afirmativo declaro bajo juramento dar estricto cumplimiento a las disposiciones vigentes para la
      Prevención del Lavado de Activos y Financiamiento del Terrorismo.</p> 

      <p style="font-size: 0.9em">Presenta Constancia de Inscripción ante la UIF (marcar con X):  Si( )     No( )</p>
      <p style="font-size: 0.9em">Fecha de Inscripción: ________________ , N° de Inscripción: _________________ </p>
      <p style="font-size: 0.9em">Además asumo el compromiso de informar cualquier modificación que se produzca a este
        respecto, dentro de los treinta 30 días de ocurrida, mediante la presentación de una nueva
        declaración jurada.</p>
      <p style="font-size: 0.9em">Firma: Aclaración: ________________________   Aclaración: __________________________</p>
      <P style="font-size: 0.9em">Certifico que la firma que antecede ha sido puesta en mi presencia. (2) __________________</P>
      <p style="font-size: 0.9em">Lugar: {{ $localidad }},  {{ $provincia }},   {{ $fecha }} </p>
      <p style="font-size: 0.9em">(1) Completar según corresponda el inciso correspondiente del artículo 20 y la
        descripción del tipo de sujeto obligado a brindar información, enumerado en Ley
        25.246 y modificatorias.</p>
      <p style="font-size: 0.9em">(2) Firma Funcionario.</p>
        
    
      <p style=" text-align=right;font-size: 0.7em">FA??</p>   
</body>
</html>     

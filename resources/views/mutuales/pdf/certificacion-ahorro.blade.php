<html>
    <title><html>
    <title>Certificación sobre conformación de Ahorros</title>
    <!--<h4 align="right" class="titulo">Fecha-Hora:{{ date('d-m-Y h:i') }}</h4>-->
 
<head>
    <?php    
              
    ?>
        <style>
                 body{
                  font-family: sans-serif;
                }
               
                
                footer {
                 
                  left: 0px;
                  bottom: -50px;
                  right: 0px;
                  height: 40px;
                 
                }
                             
                footer p {
                  text-align: center;
                }

                td {
                    text-align: center;
                    width: 60px;
                    height: 30px;
                    border-bottom: 1px solid #ddd;
                    font: 12px Helvetica, Arial, san-serif;
                    
                }

                .st {
                    text-align: left;
                    width: 60px;
                }

                th {
                    text-align: center;
                    width: 60px;
                    border-bottom: 1px solid #ddd;
                    font: 12px Helvetica, Arial, san-serif;
                }
                .th {
                    text-align: center;
                    width: 30px;
                }

                div h4 {
                  text-align: right;
                  font: 12px Helvetica, Arial, san-serif;
                }

                table {
                  border-collapse: collapse;
                  width: 100%;
                }

                .titulo {
                    font: 12px Helvetica, Arial, san-serif;
                }

                .b { border-collapse: collapse; } 
                tr, td, th{ border: 1px solid grey;}
               
               
               
              </style>
</head>
<body>
    <div><img src="img/logofaum.png"  width="" height=""></div><br>
    <div align="center"><h3 style="text-transform:uppercase;">{{ $nombre}}</h3></div>

    <p style="font-size: 0.9em">Destinatario ____________________
    <p style="font-size: 0.9em">Domicilio legal __________________
    <p style="padding-bottom: 20px;font-size: 0.9em">C.U.I.T. N° _____________________
       
    <p><b>1.1 Explicación del alcance de la certificación</b>

    <p style="font-size: 0.9em;padding-bottom: 20px;text-align: justify">En mi carácter de contador público independiente, a su pedido, y para su presentación ante
      Federaciones Aliadas Unión Mutual emito la presente certificación conforme con lo dispuesto por las normas incluidas en la 
      sección VI de la Resolución Técnica N° 37 de la Federación Argentina de Consejos Profesinales de Ciencias Económicas. Dichas
      normas exigen que cumpla los requerimientos de ética, así como que planifique mi tarea. La certificación se aplica a ciertas 
      situaciones de hecho o comprobaciones especiales, a través de la constatación con registros contables y otra documentación de
      respaldo. Este trabajo profesional no constituye una auditoría ni una revisión y, por lo tanto, las manifestaciones del contador público
      no representan la emisión de un juicio técnico respecto de la información objeto de la certificación.

    <p><b>1.2 Detalle de lo que se certifica</b>
    
    <p style="font-size: 0.9em;padding-bottom: 20px;text-align: justify">Declaración preparada por _______________________, C.U.I.T. N°  _______________________ , Domicilio
      legal  _______________________ bajo su exclusiva responsabilidad, la que se adjunta a la presente, sobre Conformación de Ahorros.

    <p><b>1.3 Alcance específico de la tarea realizada</b>

    <p style="font-size: 0.9em;padding-bottom: 5px;text-align: justify">Mi tarea profesional se limitó únicamente a cotejar la información incluida en la Declaración
      sobre Conformación de Ahorros detallado en el párrafo anterior con la siguiente documentación:

    <p style="font-size: 0.9em;text-indent: 15px">- Balance de Sumas y Saldos
    <p style="font-size: 0.9em;text-indent: 15px">- Libro Diario
    <p style="font-size: 0.9em;text-indent: 15px">- Subdiario de Liquidación de Ahorro

    <p><b>1.4 Manifestación o Aseveración del Contador Público</b>

    <p style="font-size: 0.9em;padding-bottom: 20px;text-align: justify">Sobre la base de la tarea descripta, certifico que la información incluida en la declaración a la
      que se refiere el punto 1.2 de esta certificación concuerda con la documentación y registraciones contables indicadas en el punto 1.3.

    <p>___________________, _____ de _____ de 20__
    
    <p style="align=right;font-size: 0.7em;">FA13-B</p>

    
</body>
</html>
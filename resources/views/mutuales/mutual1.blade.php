@extends('layout.base')
<?php
//dd(date('Y-m-d', strtotime($presidente->soc_fecha_nacimiento)));
$today = date('Y-m-d');
use Carbon\Carbon;
?>

@section('content')
    <div class="content" style="position: relative;z-index: 100;">
        <div style="">
            <div class="jumbotron jumbotron-fluid" style="display:flex; padding-top:0px; padding-bottom:0px; margin-bottom:10px;" style="background-color: #F1F2F2">
                <div class="container" style="padding-bottom: 10px;padding-top: 10px;">
                    <img src="img/jumbo.png" class="float-right d-none d-lg-block" style="margin-left: -600px; height: 100%;"> {{--radial-gradient(circle, black 0%, transparent 85%);--}}
                    <h1 class="display-6">Tarjeta Prepaga y de Crédito Mutual</h1>
                    <p class="lead">MasterCard "FAUM Con Vos", Al resguardo del bien mutuo, FAUM CON VOS, siempre a tu lado.</p>
                                 
                </div>
            </div>
        </div>
    </div>

    <div class="container" style="position: relative;z-index: -100;">
        
        @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" data-toggle="tooltip" data-placement="top" title="Salir de la Notificación">
                    <i class='far fa-times-circle' style='font-size:24px;color:red'></i>
                </button>	
                <ul>
                    @foreach ($errors->all() as $err)
                    <li>{{ $err }}</li>
                    @endforeach
                </ul>
            </div>
            
        @endif
    
   
        <div class="mensaje_s">
            <strong class="small-font" id="salida" style="color:rgb(75 191 101);background-color:#f7eeee;"></strong>
        </div>
        <div class="response"></div>
        

        <div class="mensaje_e">
            <strong class="small-font margen_bot" id="error" style="color:red;background-color:#f7eeee;"></strong>
        </div>
            
        <br>
            
        <h4>Alta de Mutual Adherente</h4><br>
        <form action="#" method="POST" autocomplete="off">
                
            @csrf
           <div class="container" >
                <ul class="nav nav-tabs" id="myTab" role="tablist" >
                    <li class="nav-item">
                        <a href="#personal" data-toggle="tab" class="nav-link active link-act" role="tab" aria-controls="personal" aria-selected="true" id="personal-tab" style="">Datos Generales</a>
                    </li>
                        <li class="nav-item">
                            <a href="#sucursales" data-toggle="tab" class="nav-link" role="tab" aria-controls="sucursales" aria-selected="false" id="sucursales-tab" style="">Sucursales</a>
                        </li>
                        <li class="nav-item">
                            <a href="#presidente" data-toggle="tab" class="nav-link" role="tab" aria-controls="presidente" aria-selected="false" id="presidente-tab">Presidente</a>
                        </li>
                        <li class="nav-item">
                            <a href="#secretario" data-toggle="tab" class="nav-link" role="tab" aria-controls="secretario" aria-selected="false" id="secretario-tab">Secretario</a>
                        </li>
                        <li class="nav-item">
                            <a href="#tesorero" data-toggle="tab" class="nav-link" role="tab" aria-controls="tesorero" aria-selected="false" id="tesorero-tab">Tesorero</a>
                        </li>
                        <li class="nav-item">
                            <a href="#apoderado"data-toggle="tab" class="nav-link" role="tab" aria-controls="apoderado" aria-selected="false" id="apoderado-tab">Apoderado</a>
                        </li>
                        <li class="nav-item">
                            <a href="#documentos"data-toggle="tab" class="nav-link" role="tab" aria-controls="documentos" aria-selected="false" id="documentos-tab">Adjuntar Documentos</a>
                        </li>
                        
                    </ul>
                        
                </div>
                            
                <div class="tab-content" id="myTabContent" style="width: 1036px"> <!--93% -->

                    <div class="tab-pane fade show active" style="background-color: #e9ecef;" id="personal" role="tabpanel" aria-labelledby="personal-tab">
                        
                    </div>
                    <div class="tab-pane fade suc" style="background-color: #e9ecef;" id="sucursales" role="tabpanel" aria-labelledby="sucursales-tab">
              
                    </div> 
                    
                    <div class="tab-pane fade" style="background-color: #e9ecef;" id="presidente" role="tabpanel" aria-labelledby="presidente-tab"> <!--presidente -->
                        
                    </div> 
                    
                    
                    <div class="tab-pane fade" style="background-color: #e9ecef;" id="secretario" role="tabpanel" aria-labelledby="Secretario-tab">
                        <fieldset style="width:100%;">
                            
                                        

                        </fieldset>
                    </div>

                    <div class="tab-pane fade" style="background-color: #e9ecef;" id="tesorero" role="tabpanel" aria-labelledby="tesorero-tab">
                        <fieldset style="width:100%;">
                            <div class="margen_izq"><h5></h5></div><br>
                            
                        </fieldset>
                    </div>


                    <div class="tab-pane fade" style="background-color: #e9ecef;" id="apoderado" role="tabpanel" aria-labelledby="apoderado-tab">
                        <fieldset style="width:100%;">
                            <div class="margen_izq"><h5></h5></div><br>
                            

                        </fieldset>
                    </div>

                    <div class="tab-pane fade" style="background-color: #e9ecef;" id="documentos" role="tabpanel" aria-labelledby="documentos-tab">
                        <fieldset style="width:100%;">
                            <div class="margen_izq"><h5></h5></div><br>
                            
                        </fieldset>
                    </div>

                    

                            
                <div class="form-group row sm-12" style="padding-top: 15px;">
                    <div class="col-sm">
                        <a class=""  style="color: #73c6d9;" href="#"><i class="fal fa-undo"></i>Volver a Pagina Principal</a>
                    </div>


            
                
                    <div class="col-md-3 col-sm-4 col-xs-3" style="padding-top: 15px;text-align: end;"> 
                        <button type="button" class="btn boton-guardar guardar-datos" style="background-color:#268fa9;float: inline-end;color: aliceblue;">Guardar</button>
                    </div>
                </div>
        </form>
       
    </div>
@endsection
<!-- Modal codigo postal-->
<div class="modal" id="myModal">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
        <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Buscar Localidad</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
                     
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row sm-9" style="background-color: #e9ecef; padding-top: 15px;padding-bottom: 20px;">
                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot">Buscar por</label>
                        <select class="form-control form-control-sm"  name="filtro_postal" id="filtro_postal">
                            <option selected value="" >Seleccione</option>
                            <option value="1">Nombre de Localidad</option>
                            <option value="2">Código Postal</p></option>
                            <option value="3">Todas</p></option>
                            
                        </select>
                    </div>

                    

                    <div class="col-sm-4">
                        <label for="" class="help-block text-muted small-font margen_bot" id="titulo">Opción de busqueda</label>
                        <input type="text" class="form-control form-control-sm" id="buscar_postal" name="buscar_postal" disabled>
                      
                    </div>
        
                
                    
                    <div class="col-sm-2" style="margin-top: 15px">
                        <div><label for="" class="help-block text-muted small-font margen_bot"></label></div>
                        <div><button type="button" class="btn btn-default buscar_codpostal" style="margin-bottom: -47px; margin-left: -16px;">
                            <i class="fas fa-search" title="Buscar Cod Postal" data-toggle="tooltip" data-placement="top"></i></button></div>
                        <div id="resp" style="margin-right: -252px; margin-left: 63px;margin-top: -30px;"></div>
                    </div>
        
                </div>
                <br>
                <div class="resultado_postal"></div>
                <div class="error" style="color:crimson; text-align: center;"></div>
                
            </div>
                
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-info" data-dismiss="modal" style="color: #73c6d9;">Salir</button>
            </div>
                            
        </div>
    </div>
</div>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
    </head>
    <body lang=ES-AR>
        <div style="text-align: center">
            <table width="600" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border:none">
                <tr>
                    <td style="text-align:left"><img src="https://gruponeosistemas.com/img/logo_faum.png"></td>
                    <td style="text-align: right"><img src="https://gruponeosistemas.com/img/logofaum.png" style="width: 150%;"></td> <!--style="height: 50%;" -->
                </tr>

                <tr><td><h3 align="center">SOLICITUD DE CONTRATACIÓN</h3></td></tr>
                <tr>
                    <td style="padding:0cm 5.4pt 0cm 5.4pt">
                        <p>&nbsp;</p>
                        <p><b><span style="font-size:16.0pt"> Bienvenido al registro del Sistema Faum Mutuales </span></b></p>
                    </td>
                </tr>
                <tr style="text-align: justify">
                    <td style="padding:0cm 5.4pt 0cm 5.4pt">
                        <p>&nbsp;</p>
                        <p>Estimado/a <span style="font-weight: bold;">{{ $nombre }} </span></p>
                        <p>Este es un servicio de respuesta automática del Sistema Faum Mutuales.
                        Le informamos que a vuestra solicitud, se han actualizados sus datos del Usuario.</p>
                    </td>
                </tr>
                <tr style="text-align: justify">
                    <td style="">
                        <p>&nbsp;</p>
                        Mutual de Carga: <span style="font-weight: bold;background: #cccccc;">{{ $mutual }}</span>
                    </td>
                </tr>
                <tr style="text-align: justify">
                    <td style="">
                        <p>&nbsp;</p>
                        Su usuario es: <span style="font-weight: bold;background: #cccccc;">{{ $usuario }}</span>
                    </td>
                </tr>
                <tr style="text-align: justify">
                    <td style="">
                        Nombre del Usuario: <span style="font-weight: bold;background: #cccccc;">{{ $nombre }}</span>
                        <p>&nbsp;</p>
                    </td>
                </tr>

                <tr style="text-align: justify">
                    <td style="">
                        Estado del Usuario: <span style="font-weight: bold;background: #cccccc;">@if(isset($habilitado) && $habilitado == 1) Habilitado @else Deshabilitado @endif</span>
                        <p>&nbsp;</p>
                    </td>
                </tr>
               
                <tr style="text-align: justify">
                    <td style="">
                        <p>&nbsp;</p>
                        <p>Puede acceder al sistema Faum Con Vos, ingresando <a href="https://faum.com.ar" style=""> Aquí </a></p>
                        <p>&nbsp;</p>
                    </td>
                </tr>
                
                <tr>
                    <td style="padding:0cm 5.4pt 0cm 5.4pt">
                        <p><span style="font-size:7.5pt;font-family:Arial,sans-serif;color:#555555">&nbsp;</span></p>
                        <p style="font-size:7.0pt;font-family:Arial,sans-serif;color:#58595B">
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="background:#e1ecf4;padding:0cm 5.4pt 0cm 5.4pt">
                    <p><span style="font-size:6.5pt;font-family:Arial,sans-serif;color:#555555">&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td style="padding:0cm 5.4pt 0cm 5.4pt">
                    <p><span style="font-size:6.0pt;font-family:Arial,sans-serif;color:#58595B">&nbsp;</span> </p>
                    <p>
                    <span style="font-size:6.0pt;font-family:Arial,sans-serif;color:#58595B">
                    TÉRMINOS Y CONDICIONES: ESTE MENSAJE FUE ORIGINADO AUTOMÁTICAMENTE. POR FAVOR, NO RESPONDER AL MISMO. EL CONTENIDO DEL PRESENTE MENSAJE ES PRIVADO, CONFIDENCIAL Y EXCLUSIVO PARA SUS DESTINATARIOS, PUDIENDO CONTENER INFORMACIÓN PROTEGIDA POR NORMAS LEGALES Y DE SECRETO PROFESIONAL. BAJO NINGUNA CIRCUNSTANCIA, SU CONTENIDO PUEDE SER TRANSMITIDO O RELEVADO A TERCEROS NI DIVULGADO EN FORMA ALGUNA. LA MUTUAL DE RESIDENTES DEL BARRIO TAIS NO SE RESPONSABILIZARÁ POR LOS DAÑOS O PERJUICIOS DERIVADOS DEL INCUMPLIMIENTO DE LO AQUÍ ESTABLECIDO.
                    </span>
                    </p>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
    </head>
    <body lang=ES-AR>
        <div style="text-align: center">
            <table width="600" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border:none">
                <tr>
                    <td style="text-align:left"><img src="https://gruponeosistemas.com/img/logo_faum.png"></td>
                    <td style="text-align: right"><img src="https://gruponeosistemas.com/img/logofaum.png" style="width: 150%;"></td> <!--style="height: 50%;" -->
                </tr>

                <tr><td><h3 align="center">SOLICITUD DE CONTRATACIÓN</h3></td></tr>
                <tr>
                    <td style="padding:0cm 5.4pt 0cm 5.4pt">
                        <p>&nbsp;</p>
                        <p><b><span style="font-size:16.0pt"> Bienvenido al registro del Sistema Faum Mutuales </span></b></p>
                    </td>
                </tr>
                <tr style="text-align: justify">
                    <td style=""> <!--padding:0cm 5.4pt 0cm 5.4pt-->
                        <p>&nbsp;</p>
                        <p>Estimado/a <span style="font-weight: bold;">{{ $nombre }} </span></p>
                        <p>Este es un servicio de respuesta automática del Sistema Faum Mutuales.
                        Le informamos que a vuestra solicitud, se ha generado una contraseña provisoria de ingreso al servicio.</p>
                    </td>
                </tr>
                <tr style="text-align: justify">
                    <td style="">
                        <p>&nbsp;</p>
                        Mutual de Carga: <span style="font-weight: bold;background: #cccccc;">{{ $mutual }}</span>
                    </td>
                </tr>
                <tr style="text-align: justify">
                    <td style="">
                        Su usuario es: <span style="font-weight: bold;background: #cccccc;">{{ $usuario }}</span>
                    </td>
                </tr>
                <tr style="text-align: justify">
                    <td style="">
                        Su contraseña es: <span style="font-weight: bold;background: #cccccc;">{{ $clave }}</span>
                        <p>&nbsp;</p>
                    </td>
                </tr>
               
                <tr style="text-align: justify">
                    <td>
                        <p><b>Solicitud de Información y/o documentación</b></p>
                        <p style ="text-indent: 10px">A los efectos de cumplimentar normativa vigente de la Unidad de Información Financiera solicitamos, en el plazo de 30 días, el aporte de la documentación que 
                            se detalla a continuación relacionada con la incorporación de su entidad a la <b>Tarjeta FAUM Con Vos.</b></p>
                        </p>
                        <p style="text-indent: 10px">Dicha documentación deberá ser enviada escaneada en archivos PDF, a través de la página web <u>www.faum.com.ar</u>, ingresando con usuario y contraseña. 
                        </p>
                        <p>1) <b>Formulario de Solicitud de Adhesión de Persona Jurídica</b> (en la página web). El mismo, una vez completado en todos sus ítems, deberá imprimirse, firmarse por representantes legales 
                            y/o apoderados, con firmas certificadas por Presidente y Secretario del Consejo Directivo; y finalmente cargarse escaneado en la página web.</p>
                        <p>2) <b>Planilla de Datos de Representantes Legales y Apoderados</b> (Solo cargar en la página web).</p>
                        <p>3) Copia del <b>Estatuto</b> aprobado e inscripto en el INAES, certificada por Presidente y Secretario del Consejo Directivo.</p>
                        <p>4) Copia del <b>Reglamento de Ayuda Económica</b> aprobado e inscripto en el INAES, certificada por Presidente y Secretario del Consejo Directivo.</p>
                        <p>5) Copia del <b>Reglamento de Tarjeta de Crédito</b>, si la entidad lo posee aprobado, certificada por Presidente y Secretario del Consejo Directivo.</p>
                        <p>6) <b>Constancia de CUIT</b> de la Mutual.</p>
                        <p>7) Copia certificada por escribano de última <b>Acta de designación de autoridades</b> y distribución de cargos.</p>
                        <p>8) Copia de <b>Poderes</b> otorgados, que habiliten a actuar ante FAUM.</p>
                        <p>9) Copia de <b>DNI de Representantes Legales y Apoderados</b>.</p>
                        <p>10) <b>Formulario de DDJJ de Persona Expuesta Políticamente</b> (en la página web). Deberá completarse una declaración jurada por cada representante legal y apoderado. Mismo procedimiento del punto 1.</p>
                        <p>11) <b>Memoria y Balance</b> del último ejercicio cerrado, certificado por CPN y legalizado por CPCE.</p>
                        <p>12) Copia certificada por escribano del <b>Acta donde se aprueba la Memoria y Balance</b> citado.</p>
                        <!--<p>13) <b>Informe Especial de Conformación de Ahorros</b> (en la página web), certificado por CPN y legalizado por CPCE, con información de una antigüedad no mayor a 90 días de la fecha del informe.</p>-->
                        <p>13) <b>DJ y Certificación sobre Conformación de Ahorros</b> (modelos a disposición en la página web), certificación por CPN y legalizado por CPCE.</p>
                        <p>14) <b>DJ y Certificación sobre Recursos específicos del Servicio de Ayuda Económica con Fondos Propios</b> (modelos a disposición en la página web), certificación por CPN y legalizado por CPCE.</p>
                        <p>15) Copia de la <b>Constancia de inscripción de la mutual ante la UIF</b> como Emisora de Tarjeta de Crédito.</p>
                        <p style ="text-indent: 10px">En caso de necesitar aclaraciones adicionales sobre lo solicitado, favor de contactarse a la siguiente dirección de mail: <u>info@faum.com.ar</u>.</p>

                        <p>Puede acceder al sistema Faum Con Vos, ingresando <a href="https://faum.com.ar" style=""> Aquí </a></p> <!--http://azurelocal:2021/admin/login    -->
                    </td>
                </tr>
                
                <tr>
                    <td style="padding:0cm 5.4pt 0cm 5.4pt">
                        <p><span style="font-size:7.5pt;font-family:Arial,sans-serif;color:#555555">&nbsp;</span></p>
                        <p style="font-size:7.0pt;font-family:Arial,sans-serif;color:#58595B">
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="background:#e1ecf4;padding:0cm 5.4pt 0cm 5.4pt">
                    <p><span style="font-size:6.5pt;font-family:Arial,sans-serif;color:#555555">&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td style="padding:0cm 5.4pt 0cm 5.4pt">
                    <p><span style="font-size:6.0pt;font-family:Arial,sans-serif;color:#58595B">&nbsp;</span> </p>
                    <p>
                    <span style="font-size:6.0pt;font-family:Arial,sans-serif;color:#58595B">
                    TÉRMINOS Y CONDICIONES: ESTE MENSAJE FUE ORIGINADO AUTOMÁTICAMENTE. POR FAVOR, NO RESPONDER AL MISMO. EL CONTENIDO DEL PRESENTE MENSAJE ES PRIVADO, CONFIDENCIAL Y EXCLUSIVO PARA SUS DESTINATARIOS, PUDIENDO CONTENER INFORMACIÓN PROTEGIDA POR NORMAS LEGALES Y DE SECRETO PROFESIONAL. BAJO NINGUNA CIRCUNSTANCIA, SU CONTENIDO PUEDE SER TRANSMITIDO O RELEVADO A TERCEROS NI DIVULGADO EN FORMA ALGUNA. LA MUTUAL DE RESIDENTES DEL BARRIO TAIS NO SE RESPONSABILIZARÁ POR LOS DAÑOS O PERJUICIOS DERIVADOS DEL INCUMPLIMIENTO DE LO AQUÍ ESTABLECIDO.
                    </span>
                    </p>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
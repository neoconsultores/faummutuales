<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" href="/img/favicon.png">
    <link rel="shortcut icon" type="image/png" href="/img/favicon.png"/>

    <title>Faum</title>

    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="/fonts/ionicons.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="/css/Login-Form-Dark.css">

</head>

<body style="width: 100vw;height: 100vh;">
    <div class="login-dark" style="height: 100vh;background-image: url(&quot;/img/fondo.png&quot;);background-size: contain;background-repeat: no-repeat;background-color: rgb(255,255,255);">
        <form method="post" action="/admin/index">
        {{csrf_field()}}
            <h2 class="sr-only"></h2><img class="rounded" src="/img/logofaum.png" width="250" style="padding: 5px;">
            <div class="d-xl-flex justify-content-xl-center align-items-xl-start illustration" style="height: 104px;"><i class="icon ion-ios-locked-outline d-flex d-sm-flex d-md-flex d-lg-flex d-xl-flex justify-content-center justify-content-sm-center justify-content-md-center justify-content-lg-center align-items-xl-start shake animated" style="color: rgb(255,255,255);font-size: 82px;"></i></div>
            <div class="form-group"><input class="form-control" type="text" name="user" placeholder="Usuario"></div>
            <div class="form-group"><input class="form-control" type="password" name="pass" placeholder="Contraseña"></div>
            <div class="form-group"><button class="btn btn-primary btn-block" type="submit">Iniciar Sesión</button></div>
            @if(isset($error))
                <p class="text-center" style="color:red;">{{$error}}</p>
            @endif
        </form>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>
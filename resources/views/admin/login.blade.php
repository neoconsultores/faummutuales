<!DOCTYPE html>
<html>
<head>
	<title>Faum</title>
	<meta charset="UTF-8">
	<meta name="csrf-token" content="{{ csrf_token()}}">
	
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->


<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>

<body style="background-color: #666666;">
	
	<div class="limiter">
		<div class="container-login100" >
			<div class="wrap-login100" id="form">
                <form class="login100-form" action="/admin/index" class="" autocomplete="off" method="POST" style="background-image: linear-gradient(to right, #585e67, #80dee4);">   <!--style="background-image: linear-gradient(to right, #1da1f2, #98dfe3);"-->
                    {{csrf_field()}}
					<div class="login100-form-title p-b-43" style="margin-top: -140px;">
						<img src="/admin/img/logofaum.png">
					</div>
					@if(isset($error))
						<div class="" id="CampoError">
						<h5 class="text-center text-danger" style="color:#da3547">{{$error}}</h5>
						</div>
					@endif	
					<span class="login100-form-title p-b-43">
						FAUM
					</span>
									
					<div class="wrap-input100 validate-input" data-validate ="Usuario requerido">
						<input class="input100" type="text" name="login" id="login" required>
						<span class="focus-input100"></span>
						<span class="label-input100">Usuario</span>
					</div>
									
					<div class="wrap-input100 validate-input" data-validate="Password requerido">
						<input class="input100" type="password" name="password" id="password" required>
						<span class="focus-input100"></span>
						<span class="label-input100">Password</span>
						<button id="show_password" class="boton-ver" type="button" title="Ver Password" onclick="mostrarPassword()"> 
							<span class="fa fa-eye-slash icon" style="color: #17a2b8; font-size: 20px;"></span> 
						</button>
						
					</div>

					
					
					<div class="flex-sb-m w-full p-t-3 p-b-32">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								Recordar
							</label>

						</div>

						<div>
							<a href="#" class="txt1">
								Olvido Password?
							</a>
						</div>
					</div>
			

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" id="">
							Aceptar
                        </button>
                       
					</div>
					
					<div class="text-center p-t-46 p-b-20">
						<span class="txt2">
							Enlaces
						</span>
					</div>

					<div class="login100-form-social flex-c-m">
						<a href="" class="login100-form-social-item flex-c-m bg1 m-r-5">
							<i class="fa fa-facebook-f" aria-hidden="false"></i>
						</a>

						<a href="" class="login100-form-social-item flex-c-m bg2 m-r-5">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a>
					</div>
				</form>

				<div class="login100-more" style="background-image: url('img/faum_mutuales.png');">
				</div>
			</div>
		</div>
	</div>
	
	

	
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
<script>
	function mostrarPassword(){
		var cambio = document.getElementById("password"); 
		console.log(cambio);
		if(cambio.type == "password"){
			cambio.type = "text";
			$('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	}

</script>
</html>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})

$('.editormenu').click(function(e) {
    e.preventDefault()
    var bit1 = $(this).parent().find('input[name=bit1]').val()
    var descripcion_menu = $(this).parent().find('input[name=descripcion_menu]').val();
    var codigo_nivel = $(this).parent().find('input[name=codigo_nivel]').val();
    var nro_modulo = $(this).parent().find('input[name=nro_modulo]').val();
    console.log(descripcion_menu, codigo_nivel, bit1, nro_modulo);
    $.ajax({
        type: 'POST',
        url: '/ajaxarmamenu',
        data: {
            bit1: bit1,
            descripcion_menu: descripcion_menu,
            codigo_nivel: codigo_nivel,
            nro_modulo: nro_modulo
        },

        beforeSend: function() {
            $('#editor').html('<div class="loader text-center"><span>Procesando... Aguarde un momento.<br></span><img src="/img/ajax-loader.gif"></div></br>')
        },
        success: function(data) {
            console.log(data)
            $('#editor').html(data.html).fadeIn()
        }
    })
})

$('.nivel1').click(function(e) {
    e.preventDefault()

    var bit1 = $(this).parent().find('input[name=bit1]').val()
    var bit2 = $(this).parent().find('input[name=bit2]').val()
    var bit3 = $(this).parent().find('input[name=bit3]').val()
    var bit4 = $(this).parent().find('input[name=bit4]').val()
    var bit5 = $(this).parent().find('input[name=bit5]').val()
    var bit6 = $(this).parent().find('input[name=bit6]').val()
    var bit7 = $(this).parent().find('input[name=bit7]').val()
    var bit8 = $(this).parent().find('input[name=bit8]').val()
    var codigo_nivel = $(this).parent().find('input[name=codigo_nivel]').val()
    var nro_modulo = $(this).parent().find('input[name=nro_modulo]').val()
    var item = $(this).parent().find('input[name=item]').val()

    $.ajax({
        type: 'POST',
        url: '/additemform',
        data: {
            bit1: bit1,
            bit2: bit2,
            bit3: bit3,
            bit4: bit4,
            bit5: bit5,
            bit6: bit6,
            bit7: bit7,
            bit8: bit8,
            codigo_nivel: codigo_nivel,
            nro_modulo: nro_modulo,
            item: item
        },

        beforeSend: function() {
            $('#additemres').html('<div class="loader text-center"><span>Procesando... Aguarde un momento.<br></span><img src="/img/ajax-loader.gif"></div></br>')
        },

        success: function(data) {
            console.log(data)
            $('#additemres').html(data.additemform).fadeIn()
        }
    })
})

$('.nivel1add').click(function(e) {
    e.preventDefault()

    var bit1 = $(this).parent().find('input[name=bit1]').val()
    var bit2 = $(this).parent().find('input[name=bit2]').val()
    var bit3 = $(this).parent().find('input[name=bit3]').val()
    var bit4 = $(this).parent().find('input[name=bit4]').val()
    var bit5 = $(this).parent().find('input[name=bit5]').val()
    var bit6 = $(this).parent().find('input[name=bit6]').val()
    var bit7 = $(this).parent().find('input[name=bit7]').val()
    var bit8 = $(this).parent().find('input[name=bit8]').val()
    var codigo_nivel = $(this).parent().find('input[name=codigo_nivel]').val()
    var nro_modulo = $(this).parent().find('input[name=nro_modulo]').val()
    var item = $(this).parent().find('input[name=item]').val()
    var descripcion_menu = $(this).parent().find('input[name=descripcion_menu]').val()
    var help_menu = $(this).parent().find('input[name=help_menu]').val()
    var ruta = $(this).parent().find('input[name=ruta]').val()
    var soloadministrador = $(this).parent().find('input[name=soloadministrador]').val()
    console.log($descripcion_menu)
    $.ajax({
        type: 'POST',
        url: '/itemagregar',
        data: {
            bit1: bit1,
            bit2: bit2,
            bit3: bit3,
            bit4: bit4,
            bit5: bit5,
            bit6: bit6,
            bit7: bit7,
            bit8: bit8,
            codigo_nivel: codigo_nivel,
            nro_modulo: nro_modulo,
            item: item,
            descripcion_menu: descripcion_menu,
            help_menu: help_menu,
            ruta: ruta,
            solo_administrador: solo_administrador
        },

        beforeSend: function() {
            $('#additemres').html('<div class="loader text-center"><span>Procesando... Aguarde un momento.<br></span><img src="/img/ajax-loader.gif"></div></br>')
        },

        success: function(data) {
            console.log(data)
            $('#additemres').html(data.itemadd).fadeIn()
        }
    })
})
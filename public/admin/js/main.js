
(function ($) {
    "use strict";
     var b = $("body");

     $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ajaxSuccess(function(event, request, settings) {
        var json = request.responseJSON;
        //console.log('paso json');
        //console.log(json);
        if (typeof json.reload != 'undefined' && json.reload) {
            reLogin();
        }
    });


    /*==================================================================
    [ Focus Contact2 ]*/
    $('.input100').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })
  
  
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });

    $('.aceptar').on('click',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }
        return check;
    });

    $('.aceptar').on('click',function(){
        var cambio = $('#password').val().length; 
        console.log(cambio);
        if(cambio > 0){
            $('.icon').addClass('fa fa-eye');
            $('.boton-ver').show();

        }else{
            $('.icon').removeClass('fa fa-eye');
            $('.boton-ver').hide();
        }
    });

    $('#login').on('click', function(){
        var v = $('#login').val().length;
        console.log(v);
        if(v > 0){
            $('.icon').removeClass('fa fa-eye');
            $('.boton-ver').hide();
        }
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
           $('.icon').addClass('fa fa-eye');
           $('.boton-ver').show();
        });
    });

    b.on('click', '.aceptar', function(e) {
        console.log('entro por click');
        e.preventDefault();
        var form = $("#form form");
        //console.log('input:',form);
        var error = false;
                
    
        if (!error) {
            var data = form.serialize();
            console.log('paso :',data);
            $.ajax({
                type: 'POST',
                url: '/index',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $("#CampoError h5").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
                    $("#aceptar").attr('disabled', true);
                },
                success: function(data) {
                    console.log('paso succ');
                    $("#aceptar").attr('disabled', false);
                    if (data.success) {
                        $(".tarjeta").html(data.detalle);
                        
                        window.location = '/home';
                    } else {
                        $("#CampoError h5").html('<div class="">' + data.error + '</div>');
                    }.0
                }
                });
            }
    });

    
    function reLogin() {
        $('.alert-error').append(' Serás redireccionado en <span>5</span> segundos.');
        var cont = 0;
        var seconds = 5;
        setInterval(function() {
            cont++;
            var r = seconds - cont;
            if (r < 0) r = 0;
            $('.alert-error span').html(r);
            if (cont == 5) {
                location.reload();
            }
        }, 1000);
    }

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).addClass('alert-validate');
        $('.icon').removeClass('fa fa-eye');
        $('.boton-ver').hide();
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).removeClass('alert-validate');
        $('.icon').addClass('fa fa-eye');
        $('.boton-ver').show();
        
    }
    

});
(function ($) {
    "use strict";
     var b = $("body");

     $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ajaxSuccess(function(event, request, settings) {
        var json = request.responseJSON;
        //console.log('paso json');
        //console.log(json);
        if (typeof json.reload != 'undefined' && json.reload) {
            reLogin();
        }
    });

    


});

// JavaScript Document
function login(){
	window.location.href = "index.html";	
}

function changeContentPage(id){
	var frameNode = document.getElementById(id);
	frameNode.src = "appeon_code_examples.html";
}

/**用于右边Iframe里面的导航以及导航页面调用**/
function exNodeClick(nodeid,describe)
{        
    //高亮显示节点       
    if( parent.curNodeid != "")
    {        
        parent.document.getElementById(parent.curNodeid).className = "";                    
    }               
    parent.document.getElementById(nodeid).className  = "node-highline";        
    
    //设置parent的当前显示节点信息
    parent.curNodeid = nodeid;    
    //显示详细信息
    parent.document.getElementById("describeDiv").innerHTML = describe;   
    
    //显示当前节点所对应的父节点
    displayNode(nodeid);   
    parent.document.getElementById(nodeid).focus();                         
}


function displayNode(nodeid)
{

    var parentNodeid = parent.document.getElementById(nodeid).parentNode.parentNode.id;    
    if(parentNodeid != "" && parentNodeid !="divTreeList")
    {
        var treeImg = "img" +  parentNodeid;
        if(parent.document.getElementById(nodeid).parentNode.parentNode.style.display == "none")
        {   
            parent.document.getElementById(treeImg).onclick();
            /*
            var imggg = parent.document.getElementById(treeImg);   
            if(document.all)//IE
            {                    
                parent.document.getElementById(treeImg).click();                                  
            }
            else//Chrome，Others
            {
                var evt = parent.document.createEvent("MouseEvents");
                evt.initEvent("click",true,true);
                parent.document.getElementById(treeImg).dispatchEvent(evt);
            }*/
        }
        displayNode(treeImg);
    }
}

function deviceIDStatus(obj, id, isTitle) {

    obj = typeof (obj) == "string" ? document.getElementById(obj) : obj;
    if (obj == null)
        return;

    var node = document.getElementById(id);
    var nodeDisplay = node.style.display;
    if (nodeDisplay == 'none') {
        treeOpen(obj, node, isTitle);
    } else {
        treeClose(obj, node, isTitle);
    }
}

/*************************treeview start********************************/

function treeStatus(obj,id,isTitle){

    obj = typeof (obj) == "string" ? document.getElementById(obj) : obj;
    if (obj == null)
        return;
  
    var node = document.getElementById(id);
	var nodeDisplay = node.style.display;
	if(nodeDisplay=='none'){		
		treeOpen(obj,node,isTitle);
	}else{
		treeClose(obj,node,isTitle);
	}
}

function treeOpen(obj,node,isTitle){
	
	node.style.display = "";
	if(isTitle){		
		obj.src = "../images/treetitleopen.gif";
	}else{		
		obj.src = "images/treeopen.png";
	}
	
}

function treeClose(obj,node,isTitle){		
	node.style.display = "none";
	if(isTitle){
		obj.src = "../images/treetitleclose.gif";
	}else{
		obj.src = "images/treeclose.png";
	}
}

function treeClick(id,url){	
	document.getElementById("describeDiv").innerHTML = "You can review all configurations of each application here." ;	
	parent.document.getElementById("detailContent").src = "content.html";
}

function treeDisplay(){
    var node = document.getElementById("tdNavigation");
    var w = document.documentElement.clientWidth;                                 
	if(node.style.display == "none"){
		node.style.display = "";
		document.getElementById("imgNav").src = "images/treeview_hidden.gif";
		var iframeW = w * 0.75 - 15;
	}else{
		node.style.display = "none";
		document.getElementById("imgNav").src = "images/treeview_display.gif";
		var iframeW = w * 0.99 - 15;
   }
   document.getElementById("detailContent").style.width = iframeW + "px"; 
}

function cambiar_pin(){
    var pin_act = $('#pin_actual').val();
    var pin_nue = $('#pin_nuevo').val();
    data = {pin_nuevo:pin_nue, pin_actual:pin_act};
    $.ajax({
        type: 'POST',
        url: '/cambiarpin',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $(".campo5rror").html('<div class="loader"><span>Procesando... Aguarde un momento.</span><img src="/images/ajax-loader.gif"></div>');
            //$('.cambiar_pin').attr("disabled","disabled");//lo desabilito al procesar
            //$('.modal-body').css('opacity', '.5');//coloco opaco el cuerpo, datos de cambio

           // $("#aceptar").attr('disabled', true);//desabilito el boton
        },
        success: function(data) {
            //$("#aceptar").attr('disabled', false);
            if (data.success) {
                console.log('paso succ');
                $('#pin_actual').val('');
                $('#pin_nuevo').val('');
                
                $(".mensaje").html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button>' + data.mensaje + ' en la tarjeta: ' + data.tarjeta + '</div>');
                $(".mensaje").fadeTo(3000);//fadeToggle(4000);
                
               
            } else {
                console.log('paso no succ');
                $('.mensaje').html('<div class="alert alert-danger alert-dismissible" style=""><button type="button" class="close" data-dismiss="alert">&times;</button>' + data.mensaje + '</div>');
                $('.mensaje').fadeTo(3000); //fadeToggle(4000);
             
                $('.cambiar_pin').removeAttr("disabled");
                $('.modal-body').css('opacity', '');
            }
        }
        });
   
}

function cambiar(){
    console.log('num:---');
    var data_tarjeta = $("#num_tarj").text();
    console.log('num:',data_tarjeta);
    $('.numero').text(data_tarjeta);
         
}

function refrescar(){//no hace lo que se pide
    console.log('cerro modal');
    $('#pin_actual').val('');
    $('#pin_nuevo').val('');
    $(".mensaje").html('');
        
}




<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('admin/login', 'MutualController@index');
//Route::post('admin/login', 'MutualController@index');

Route::get('/', 'MutualController@index');
Route::post('/', 'MutualController@index');
Route::get('admin/index', 'MutualController@login');
Route::post('admin/index', 'MutualController@login');


Route::group(['middleware' => ['rol']], function () {
    Route::get('admin/alta-mutual', 'MutualController@altaMutual');
    Route::post('admin/alta-mutual', 'MutualController@altaMutual');
    Route::post('admin/update-mutual', 'MutualController@updateMutual');
    Route::post('admin/delete-mutual', 'MutualController@deleteMutual');
    Route::post('admin/buscar-mutual', 'MutualController@buscarMutual');
    Route::get('admin/alta-usuario', 'MutualController@altaUsuario');
    Route::post('admin/alta-usuario', 'MutualController@altaUsuario');
    Route::post('admin/buscar-usuario', 'MutualController@buscarUsuario');
    Route::post('admin/update-usuario', 'MutualController@updateUsuario');
    Route::post('admin/delete-usuario', 'MutualController@deleteUsuario');
    
    Route::post('admin/agregar-sucursal', 'MutualController@altaSucursal');
    Route::get('admin/up-sucursal', 'MutualController@updateSucursal');
    Route::post('admin/up-sucursal', 'MutualController@updateSucursal');
    Route::post('admin/buscar-suc-mutual', 'MutualController@buscarSucMutual');
    Route::post('admin/buscar-suc', 'MutualController@buscarSucursal');

});


Route::group(['middleware' => ['sesion']], function () {

    Route::get('admin/cargar', 'MutualController@cargarMutual');
    Route::post('admin/cargar', 'MutualController@cargarMutual');
    Route::get('admin/aprobar', 'MutualController@aprobar');
    Route::post('admin/guardar-datos-generales', 'MutualController@updateMutualGenerales');
    Route::post('admin/guardar-datos-sucursales', 'MutualController@updateMutualSucursales');
    Route::post('admin/guardar-datos-presidente', 'MutualController@updateMutualPresidente');
    Route::post('admin/guardar-datos-secretario', 'MutualController@updateMutualSecretario');
    Route::post('admin/guardar-datos-tesorero', 'MutualController@updateMutualTesorero');
    Route::post('admin/guardar-datos-apoderado', 'MutualController@updateMutualApoderado');

    Route::post('admin/imprimir-so', 'MutualController@imprimirSo');
    Route::post('admin/imprimir-pep', 'MutualController@imprimirPep');
    Route::post('admin/imprimir-jur', 'MutualController@imprimirJur');
    Route::post('admin/imprimir-datos-generales', 'MutualController@imprimirDatosGenerales');
    Route::post('admin/guardar-documento', 'MutualController@guardarDocumento');
    Route::post('admin/confirmar-datos', 'MutualController@confirmarDatos');
    Route::get('admin/cambio-password', 'MutualController@cambioPassword');
    Route::post('admin/cambio-password', 'MutualController@cambioPassword');


    Route::post('admin/buscar-codpostal-modal', 'MutualController@buscarCodPostalModal');
    Route::post('admin/buscar-codpostal-modal-suc', 'MutualController@buscarCodPostalModalSuc');//para la nueva modificacion de sucursal
    Route::post('admin/buscar-codpostal-modal-suc-up', 'MutualController@buscarCodPostalModalSucUp');
    Route::get('admin/logout', 'MutualController@logout');

    Route::get('admin/imprimir/declara-ahorro', 'MutualController@imprimirDeclaraAhorro');
    Route::get('admin/imprimir/certificacion-ahorro', 'MutualController@imprimirCertificacionAhorro');
    Route::get('admin/imprimir/declara-ayuda-economica', 'MutualController@imprimirAyudaEconomica');
    Route::get('admin/imprimir/certificacion-ayuda-economica', 'MutualController@imprimirCertificacionAyudaEconomica');
    

});


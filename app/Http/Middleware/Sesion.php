<?php

namespace App\Http\Middleware;

use Auth;

use Closure;
use Redirect;

class sesion
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(session()->get('user'));
        if (!session()->get('user')){ //&& session()->get('rol') <> 'Usuario'
			return Redirect::to('/');
        } 
        
        return $next($request);
    }
}

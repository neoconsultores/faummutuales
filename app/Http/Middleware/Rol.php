<?php

namespace App\Http\Middleware;

use Auth;

use Closure;
use Redirect;

class rol
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(session()->get('user'));
        if (session()->get('rol') <> 'Administrador'){
			return Redirect::to('/');
        } 
        
        return $next($request);
    }
}

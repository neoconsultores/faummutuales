<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Log;
use PDF;
use Validator;
use Mail;

class MutualController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        //dd('paso por mutual');
        return view('admin.login');
    }

    public function comprobar_password($valor){
		//compruebo que el tamaño del string sea válido.
		if (strlen($valor)<6 || strlen($valor)>15){
		   return false;
		}
	 
		//compruebo que los caracteres sean los permitidos
		$permitidos = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz@.,:;#+-";
		for ($i=0; $i<strlen($valor); $i++){
		   if (strpos($permitidos, substr($valor,$i,1))===false){
			  return false;
		   }
		}
		return true;
	 } 

    public function cambioPassword(Request $request)
    {
        //dd('paso por mutual');
        $id_mutual = session()->get('id_mutual');
        $codigo_user =session()->get('codigo_user');
        $nombre =session()->get('user');
        $mail =session()->get('mail');

        if ($request->isMethod('post')) {
            $input = $request->all();
            Log::info($input);
            if($input['password1'] != $input['password2']){

                return response()->json(['success' => false, 'error' => 'Los 2 Password deben ser iguales.']);

            }elseif(empty($input['password1']) || empty($input['password2'])){
                return response()->json(['success' => false, 'error' => 'Ninguno de los Password pueden estar vacio.']);
            }

            $rules = [
                'usuario'	=> 	'required',
                'nombre'	=> 	'required',
                //'usuario-registro'		=> 	'required|max:30|alpha_num',
                'password1' 		=> 	[
                                            'required',
                                            //'confirmed',
                                            'min:6',
                                            'max:15',
                                            'regex:/(?=[^A-Za-z]*[A-Za-z])(?:([\w\d@.,:;#+-])\\1?(?!\\1))+$/',
                                            'regex:/(?=[0-9]*[0-9])(?:([\w\d@.,:;#+-])\\1?(?!\\1))+$/',
                                            'regex:/[@.,:;#+-]/', //@.,:;#+-
                                            ], 
            ];
            
            $messages = [
                'usuario.required' => 'Error al procesar la operación, perdio la sesión.',
                'nombre.required' => 'Error al procesar la operación.',
                
                'password1.required' => 'Ingrese una contraseña nueva.',
                //'password1.confirmed'=> 'Las contraseñas no coinciden.',
                'password1.max' => 'La contraseña supera los 15 caracteres permitidos.',
                'password1.min' => 'La contraseña requiere un minimo de 6 caracteres.',
                'password1.regex' => 'La contraseña debe tener al menos una mayúscula, una minúscula, un número y un símbolo de estos (@.,:;#+-), no acepta 3 numeros consecutivos ni las ñ/Ñ.',
            ];
    
            $v = Validator::make($input, $rules, $messages);
            
            if($v->fails()){
                $msg = $v->errors()->all()[0];
                return response()->json(['success' => false, 'error' => $msg]);
            }

            $cv = $this->comprobar_password($input['password1']);
			if($cv){
				$clave = $input['password1'];
			}else{
				$mensaje_cad = 'La contraseña no puede contener (Ñ,ñ) y solo acepta estos caracteres especiales: @.,:;#+-';
				return response()->json(['success' => false, 'error' => $mensaje_cad]);
			}

            $datos = [
                'password' => $clave,
            ];

            try {
                DB::beginTransaction();

                DB::table('usuarios')
                            ->where('codigo_user', $codigo_user)
                            ->where('id_mutual', $id_mutual)
                            ->where('user', $mail)
                            ->update($datos);

                DB::commit();

                return response()->json(['success' => true, 'salida' => 'Password cambiado con exito.']);

            } catch (\Illuminate\Database\QueryException $ex) {
                DB::rollback();
                Log::info('Error: '.$ex->getMessage());  

                return response()->json(['success' => false, 'error' => 'No se actualizo el Password, intente de nuevo.']);
            }

        }

        return view('usuarios.cambio-password')->with('user',$mail)->with('nombre',$nombre);
    }

    public function login(Request $request)
    {
        //dd($input);
        if ($request->isMethod('post')) {
            $input = $request->all();
            //dd($input);

            $user = DB::table('usuarios')->where('user', $input['login'])->where('password', $input['password'])->first();
            //dd($user);
            if ($user) {
                if($user->habilitado == 0){
                    return view('admin.login')->with('error', 'Usuario No habilitado.');
                }
                session()->flush();
                if ($user->rol == 1) {
                    session()->put('rol', 'Administrador');
                } else {
                    session()->put('rol', 'Usuario');
                }

                session()->put('id_mutual', $user->id_mutual);
                session()->put('id_sucursal', $user->id_sucursal);
                session()->put('user', $user->nombre);
                session()->put('codigo_user', $user->codigo_user);
                session()->put('mail', $user->user);

                //session()->put('apellido',$user->apellido);

                //dd($mutual);
               
                return redirect('/admin/cargar');
            } else {
                return view('admin.login')->with('error', 'Usuario ó Password inválido.');
            }
        }

        return view('admin.login');
    }

    public function aprobar()
    {
        //$config = Config::all();
        $id_mutual = session()->get('id_mutual');
        $id_sucursal = session()->get('id_sucursal');
        if (session()->get('rol') == 'Usuario') {
            $mutual = DB::table('personas')->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', $id_sucursal)->first();

            if($mutual->nro_cuit <> '' && $mutual->domicilio_calle <> '' && $mutual->domicilio_nro <> '' && $mutual->cond_iva <> ''
                && $mutual->telefono1 <> '' && $mutual->telefono_celular <> '' && $mutual->email <> '' && $mutual->soc_ejercicio_economico <> '' 
                && $mutual->soc_jud_fecha_constitucion <> '' && $mutual->soc_jud_nro_inscripcion <> ''){
                            
                $datos_completos = 1; //tiene todos los datos requeridos

            }else{
                $datos_completos = 0;
            }
            
            $mutual->datos_completos = $datos_completos;
                

            switch ($mutual->confirmada){
                case 0:
                    $estado_actual = 'En carga de datos.';
                break;
                case 1:
                    $estado_actual = 'Datos Confirmados, en revisión por FAUM.';
                break;
                case 2:
                    $estado_actual = 'Datos Aprobados por FAUM.';
                break;  
                case 3:
                    $estado_actual = 'Rechazada por FAUM, actualizar datos.';
                break;  
                
                
            }

            $mutual->estado_actual = $estado_actual;

            $sucursales = DB::table('personas')->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', '>', 1)->get();

            $presidente = DB::table('personas')
                                    ->join('personas_sociedad', 'personas_sociedad.nro_persona', 'personas.nro_persona')
                                    ->where('personas.soc_numero_socio', $id_mutual)->where('personas.soc_sucursal', 0)->where('personas_sociedad.socio_caracter', 8)->first();

            $secretario = DB::table('personas')
                                    ->join('personas_sociedad', 'personas_sociedad.nro_persona', 'personas.nro_persona')
                                    ->where('personas.soc_numero_socio', $id_mutual)->where('personas.soc_sucursal',0)->where('personas_sociedad.socio_caracter', 6)->first();
                        
            $tesorero = DB::table('personas')
                                    ->join('personas_sociedad', 'personas_sociedad.nro_persona', 'personas.nro_persona')
                                    ->where('personas.soc_numero_socio', $id_mutual)->where('personas.soc_sucursal', 0)->where('personas_sociedad.socio_caracter', 7)->first();

            $apoderado = DB::table('personas')
                                    ->join('personas_sociedad', 'personas_sociedad.nro_persona', 'personas.nro_persona')
                                    ->where('personas.soc_numero_socio', $id_mutual)->where('personas.soc_sucursal',0)->where('personas_sociedad.socio_caracter', 1)->first();

            $documentos = DB::table('documentacion_mutuales')->where('id_mutual',$id_mutual)->whereNotIn('cod_doc',['FA00','FA02','FA09-D','FA10-D'])->select('id_mutual','cod_doc','descripcion')->get();
            //dd(count($documentos));

            
            if($mutual->datos_completos == 1){
                $datos_mut = [
                    'estado' => 1,
                    'mensaje' => '',
                ];
            }else{
                $datos_mut = [
                    'estado' => 0,
                    'mensaje' => '-Falta cargar información de la Mutual en datos generales.',
                ];
                
            }
            
            if(count($documentos) == 20){
                $datos_doc = [
                    'estado' => 1,
                    'mensaje' => '',
                ];
            
            }else{
                $datos_doc = [
                    'estado' => 0,
                    'mensaje' => '-Falta adjuntar Documentos.',
                ];
            }

            if($presidente){
                $datos_pre = [
                    'estado' => 1,
                    'mensaje' => '',
                ];
            }else{
                $datos_pre = [
                    'estado' => 0,
                    'mensaje' => '-Falta cargar la información del Presidente.',
                ];
                
            }

            if($secretario){
                $datos_sec = [
                    'estado' => 1,
                    'mensaje' => '',
                ];
            }else{
                $datos_sec = [
                    'estado' => 0,
                    'mensaje' => '-Falta cargar la información del Secretario.',
                ];
            
            }

            if($tesorero){
                $datos_tes = [
                    'estado' => 1,
                    'mensaje' => '',
                ];
            }else{
                $datos_tes = [
                    'estado' => 0,
                    'mensaje' => '-Falta cargar la información del Tesorero.',
                ];
                
            }

            if($apoderado){
                $datos_apo = [
                    'estado' => 1,
                    'mensaje' => '',
                ];
            }else{
                $datos_apo = [
                    'estado' => 0,
                    'mensaje' => '-Falta cargar la información del Apoderado (Opcional).',
                ];
                
            }

            Log::info($datos_doc);
            Log::info($datos_pre);
            Log::info($datos_sec);
            Log::info($datos_tes);
            Log::info($datos_apo);
            
            
            return view('mutuales.confirmar')->with('datos', $mutual)->with('datos_doc',$datos_doc)->with('pre',$datos_pre)->with('sec',$datos_sec)->with('tes',$datos_tes)->with('apo',$datos_apo)->with('mut',$datos_mut);
        }else{
            return view('admin.login');
        }
    }

    public function confirmarDatos(Request $request)
    {
        $input = $request->all();
        $id_mutual = session()->get('id_mutual');
        $id_sucursal = session()->get('id_sucursal');
        $nro_persona = session()->get('nro_persona');

        
        if ($request->isMethod('post')) {
            $input = $request->all();
  
            $id_mutual = session()->get('id_mutual');
            $id_sucursal = session()->get('id_sucursal');
            $nro_persona = session()->get('nro_persona');


            Log::info('entro a confirmar datos');
            Log::info($input);
            try {
                DB::beginTransaction();

                $datos = [
                    'confirmada' => 1,
                ];

                DB::table('personas')
                        ->where('soc_numero_socio', $id_mutual)
                        ->where('soc_sucursal', $id_sucursal)
                        ->where('nro_persona', $nro_persona)
                        ->update($datos);

                DB::commit();
                $mutual = DB::table('personas')->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', $id_sucursal)->first();
                switch ($mutual->confirmada){
                    case 0:
                        $estado_actual = 'En carga de datos.';
                    break;
                    case 1:
                        $estado_actual = 'Datos Confirmados, en revisión por FAUM.';
                    break;
                    case 2:
                        $estado_actual = 'Datos Aprobados por FAUM.';
                    break;  
                    case 3:
                        $estado_actual = 'Rechazada por FAUM, actualizar datos.';
                    break;  
                    
                    
                }
                $mutual->estado_actual = $estado_actual;
                Log::info([$mutual]);
                
                return response()->json(['success' => true, 'salida' => 'Datos confirmados con exito.', 'estado' => $mutual->estado_actual]);

            } catch (\Illuminate\Database\QueryException $ex) {
                DB::rollback();
                Log::info('Error: '.$ex->getMessage());   
                return response()->json(['success' => false, 'salida' => 'No se confirmados, intente de nuevo.']);
            }

        }
    }

    public function altaMutual(Request $request)
    {
        Log::info('alta mutual');
        if ($request->isMethod('post')) {
            $input = $request->all();
            Log::info($input);
            $rules = [
                'soc_numero_socio' => 'required|numeric',
                'nombre' => 'required',
                'codigo_postal' => 'required',
               
            ];

            $messages = [
                'soc_numero_socio.required' => 'Ingrese el número de Adherente de la Mutual.',
                'soc_numero_socio.numeric' => 'El número de Adherente de la Mutual es solo números.',

                'nombre.required' => 'Ingrese el nombre de la Mutual.',
                'codigo_postal.required' => 'Seleccione el código postal de la Mutual.',
            
            ];

            $v = Validator::make($input, $rules, $messages);

            if ($v->fails()) {
                $msg = $v->errors()->all()[0];

                return response()->json(['success' => false, 'error' => $msg]);
            }
            
            //dd($input);
            $existe_mut = DB::table('personas')->where('soc_numero_socio',$input['soc_numero_socio'])->first();
            if($existe_mut){
                return response()->json(['success' => false, 'error' => 'N° Adherente: '.$input['soc_numero_socio'].' ya existe, coloque uno diferente.']);
            }else{

                try {
                    DB::beginTransaction();
                    $ult_persona = DB::table('personas')->select('nro_persona')->orderby('nro_persona', 'DESC')->first();
                    if($ult_persona){
                        $prox = $ult_persona->nro_persona + 1;
                    }else{
                        $prox = 1;
                    }
                    $datos = [
                        'nro_persona' => $prox,
                        'soc_numero_socio' => $input['soc_numero_socio'],
                        'es_usuario' => 1,
                        'es_socio' => 1,
                        'soc_estado' => 'A',
                        'soc_sucursal' => 1,
                        'nombre' => $input['nombre'],
                        'codigo_postal' => $input['codigo_postal'],
                        'confirmada' => 0,
                        'soc_fisica_juridica' =>2,
                        'soc_codigo_baja' => 0,
                    ];
                    DB::table('personas')->insert($datos);
                    $accion = 'Mutual creada con exito.';
                    DB::commit();
                    return response()->json(['success' => true, 'salida' => $accion]);

                } catch (\Illuminate\Database\QueryException $ex) {
                    DB::rollback();
                    Log::info('Error: '.$ex->getMessage());
    
                    $mutual = DB::table('personas')->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', $id_sucursal)->first();
                    $sql = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                            FROM localidad, provincia
                            where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal  = '.$mutual->codigo_postal;
    
                    $buscar = DB::connection('mysql')->select($sql);
                    //dd($buscar);
                    $mutual->localidad = $buscar[0]->localidad;
                    $mutual->provincia = $buscar[0]->provincia;
    
                    return response()->json(['success' => false, 'error' => 'error en server.']);
                }


            }
         
            
        }

        
        return view('mutuales.alta-mutual');
        
    }

    public function updateMutual(Request $request)
    {
        Log::info('update mutual');
        if ($request->isMethod('post')) {
            $input = $request->all();
            Log::info($input);
            $rules = [
                'soc_numero_socio' => 'required|numeric',
                'nombre' => 'required|max:35',
                'codigo_postal' => 'required',
               
            ];

            $messages = [
                'soc_numero_socio.required' => 'Ingrese el número de Adherente de la Mutual.',
                'soc_numero_socio.numeric' => 'El número de Adherente de la Mutual es solo números.',

                'nombre.required' => 'Ingrese el nombre de la Mutual.',
                'nombre.max' => 'El nombre de la Mutual no puede exceder los 35 caracteres.',
                'codigo_postal.required' => 'Seleccione el código postal de la Mutual.',
            
            ];

            $v = Validator::make($input, $rules, $messages);

            if ($v->fails()) {
                $msg = $v->errors()->all()[0];

                return response()->json(['success' => false, 'error' => $msg]);
            }
            
            //dd($input);
            $existe_mut = DB::table('personas')->where('soc_numero_socio',$input['soc_numero_socio'])->first();
            if($existe_mut){
               
                try {
                    DB::beginTransaction();
                    $total_suc = DB::table('personas')->where('soc_numero_socio', $input['soc_numero_socio'])->where('soc_sucursal','>',1)->get()->count();
                    $datos = [
                        'soc_numero_socio' => $input['soc_numero_socio'],
                        'nombre' => $input['nombre'],
                        'codigo_postal' => $input['codigo_postal'],
                        
                    ];
                    DB::table('personas')->where('soc_numero_socio',$input['soc_numero_socio'])->where('soc_sucursal', $existe_mut->soc_sucursal)->update($datos);
                    $accion = 'Mutual actualizada con exito.';

                    $e =0;
                ////dd(count($input['Movimientos']));
                for ($i=0;$i<$total_suc;$i++) {
                    Log::info('for');
                    //dd($mov[$i]['fechaMovimiento']);

                    $rules = [
                        'nombre'.$e.'' => 'required|max:35',
                        'codigo_postalsuc'.$e.'' => 'required',
                       
                    ];

                    $messages = [
                        'nombre'.$e.'.required' => 'Ingrese el Nombre de la Sucursal Adherente N° ('.$input['soc_sucursal'.$e].').',
                        'nombre'.$e.'.max' => 'El Nombre de la Sucursal Adherente N° ('.$input['soc_sucursal'.$e].') no puede exceder los 35 caracteres.',
                        'codigo_postalsuc'.$e.'.required' => 'Seleccione el codigo postal de la Sucursal Adherente N° ('.$input['soc_sucursal'.$e].').',
                      
                    
                    ];

                    $v = Validator::make($input, $rules, $messages);

                    if ($v->fails()) {
                        $msg = $v->errors()->all()[0];

                        return response()->json(['success' => false, 'error' => $msg]);
                    }

                    //dd($input);
                    /*foreach ($input as $k => $valor) {
                        if (!is_array($valor)) {
                            $input[$k] = strip_tags($valor);
                        }
                    }*/
                    
                    $datos = [
                        'nombre' => $input['nombre'.$e.''],
                        'codigo_postal' => $input['codigo_postalsuc'.$e.''],
                           
                    ];
                    Log::info($datos);
                    $e = $e +1;

                    DB::table('personas') //aqui voy 
                            ->where('soc_numero_socio', $input['soc_numero_socio'])
                            ->where('soc_sucursal', $input['soc_sucursal'.$i])
                            //->where('nro_persona', $nro_persona)
                            ->update($datos);
                 
                   
                }
                

            //DB::commit();
            //$sucursales = DB::table('personas')->where('soc_numero_socio', $i
                    DB::commit();
                    return response()->json(['success' => true, 'salida' => $accion]);

                } catch (\Illuminate\Database\QueryException $ex) {
                    DB::rollback();
                    Log::info('Error: '.$ex->getMessage());
    
                    $mutual = DB::table('personas')->where('soc_numero_socio', $input['soc_numero_socio'])->where('soc_sucursal', $existe_mut->soc_sucursal)->first();
                    $sql = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                            FROM localidad, provincia
                            where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal  = '.$existe_mut->codigo_postal;
    
                    $buscar = DB::connection('mysql')->select($sql);
                    //dd($buscar);
                    $mutual->localidad = $buscar[0]->localidad;
                    $mutual->provincia = $buscar[0]->provincia;
    
                    return response()->json(['success' => false, 'error' => 'error en server.']);

                }

            }else{

                    return response()->json(['success' => false, 'error' => 'N° Adherente: '.$input['soc_numero_socio'].' no existe, coloque uno deferente.']); //prueba 
                }


        }
         
            
    }

    public function deleteMutual(Request $request)
    {
        Log::info('delete mutual');
        if ($request->isMethod('post')) {
            $input = $request->all();
            Log::info($input);
            $rules = [
                'soc_numero_socio' => 'required|numeric',
                'nombre' => 'required',
                'codigo_postal' => 'required',
               
            ];

            $messages = [
                'soc_numero_socio.required' => 'Ingrese el número de Adherente de la Mutual.',
                'soc_numero_socio.numeric' => 'El número de Adherente de la Mutual es solo números.',

                'nombre.required' => 'Ingrese el nombre de la Mutual.',
                'codigo_postal.required' => 'Seleccione el código postal de la Mutual.',
            
            ];

            $v = Validator::make($input, $rules, $messages);

            if ($v->fails()) {
                $msg = $v->errors()->all()[0];

                return response()->json(['success' => false, 'error' => $msg]);
            }
            
            //dd($input);
            $existe_mut = DB::table('personas')->where('soc_numero_socio',$input['soc_numero_socio'])->first();
            if($existe_mut){
               
                try {
                    DB::beginTransaction();
                    
                    $datos = [
                        'soc_numero_socio' => $input['soc_numero_socio'],
                        'nombre' => $input['nombre'],
                        'codigo_postal' => $input['codigo_postal'],
                        
                    ];
                    DB::table('personas')->where('soc_numero_socio',$input['soc_numero_socio'])->delete();
                    $accion = 'Mutual Eliminada con exito.';
                    DB::commit();
                    return response()->json(['success' => true, 'salida' => $accion]);

                } catch (\Illuminate\Database\QueryException $ex) {
                    DB::rollback();
                    Log::info('Error: '.$ex->getMessage());
    
                    $mutual = DB::table('personas')->where('soc_numero_socio', $input['soc_numero_socio'])->where('soc_sucursal', $existe_mut->soc_sucursal)->first();
                    $sql = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                    FROM localidad, provincia
                    where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal  = '.$existe_mut->codigo_postal;
    
                    $buscar = DB::connection('mysql')->select($sql);
                    //dd($buscar);
                    $mutual->localidad = $buscar[0]->localidad;
                    $mutual->provincia = $buscar[0]->provincia;
    
                    return response()->json(['success' => false, 'error' => 'error en server.']);

                }

            }else{

                    return response()->json(['success' => false, 'error' => 'N° Adherente: '.$input['soc_numero_socio'].' no existe, no se puede eliminar.']); //prueba 
                }


        }
         
            
    }

    public function buscarMutual(Request $request)
    {
        Log::info('buscar mutual');
        if ($request->isMethod('post')) {
            $input = $request->all();
            Log::info($input);
            $rules = [
                'soc_numero_socio' => 'required|numeric',
                             
            ];

            $messages = [
                'soc_numero_socio.required' => 'Ingrese el número de Adherente de la Mutual.',
                'soc_numero_socio.numeric' => 'El número de Adherente de la Mutual es solo números.',
           
            ];

            $v = Validator::make($input, $rules, $messages);

            if ($v->fails()) {
                $msg = $v->errors()->all()[0];

                return response()->json(['success' => false, 'error' => $msg]);
            }
            
            //dd($input);
            $mutual = DB::table('personas')->where('soc_numero_socio',$input['soc_numero_socio'])->where('soc_sucursal',1)->first();
            if($mutual){
                
                $sql = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal  = '.$mutual->codigo_postal;
    
                $buscar = DB::connection('mysql')->select($sql);
                //dd($buscar);
                $mutual->localidad = $buscar[0]->localidad;
                $mutual->provincia = $buscar[0]->provincia;

                switch ($mutual->confirmada){
                    case 0:
                        $estado_actual = 'En carga de datos.';
                    break;
                    case 1:
                        $estado_actual = 'Datos Confirmados, en revisión por FAUM.';
                    break;
                    case 2:
                        $estado_actual = 'Datos Aprobados por FAUM.';
                    break;  
                    case 3:
                        $estado_actual = 'Rechazada por FAUM, actualizar datos.';
                    break;  
                    
                    
                }

                $mutual->estado_actual = $estado_actual;

                $sucursales = DB::table('personas')->where('soc_numero_socio', $input['soc_numero_socio'])->where('soc_sucursal', '>', 1)->orderby('soc_sucursal', 'ASC')->get();
                Log::info([$sucursales]);
                if(count($sucursales) > 0){
                    foreach ($sucursales as $i => $res) {
                        $sql_s = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                                  FROM localidad, provincia
                                  where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal  = '.$res->codigo_postal;
    
                        $buscar_s = DB::connection('mysql')->select($sql_s);
    
                        $sucursales[$i]->localidad = $buscar_s[0]->localidad;
                        $sucursales[$i]->provincia = $buscar_s[0]->provincia;
                    }
                    $html_contenido = view('mutuales.sucursales')->with('sucursales', $sucursales)->with('estado_mutual',$mutual->confirmada)->render();

                    
                    //return response()->json(['success' => true, 'datos_html' => $html_contenido, 'error' => '']);

                }else{
                    $html_contenido = view('mutuales.sucursales')->render();
                }
               
                
    
                return response()->json(['success' => true, 'salida' => $mutual, 'datos_html' => $html_contenido, 'error' => '']);

            }else{
                $html_contenido = view('mutuales.sucursales')->render();

                return response()->json(['success' => false, 'error' => 'mutual nueva', 'datos_html' => '']);
                
                


            }
         
            
        }

              
    }

    public function altaUsuario(Request $request)
    {
        Log::info('alta de usuario');
        $mutuales = DB::table('personas')->where('soc_sucursal',1)->where('es_socio',1)->orderby('soc_numero_socio','asc')->get();
        if ($request->isMethod('post')) {
            $input = $request->all();
            Log::info($input);
            $rules = [
                'id_mutual' => 'required',
                'usuario_correo' => [
                    'required',
                    'regex:/[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/', 
                    ],
                'nombre_usuario' => 'required',
                'rol' => 'required',
                'habilitado' => 'required',
               
            ];

            $messages = [
                'id_mutual.required' => 'Ingrese la Mutual Adherente de la Mutual.',
                'usuario_correo.required' => 'Ingrese el correo del Usuario.',
                'usuario_correo.regex' => 'Ingrese un mail valido para el usuario.',
                'nombre_usuario.required' => 'Ingrese el nombre del Usuario.',
                'rol.required' => 'Seleccione el rol del Usuario.',
                'habilitado.required' => 'Seleccione el un estado para el nuevo Usuario.',
            
            ];

            $v = Validator::make($input, $rules, $messages);

            if ($v->fails()) {
                $msg = $v->errors()->all()[0];

                return response()->json(['success' => false, 'error' => $msg]);
            }

            
            $existe_usu = DB::table('usuarios')->where('user',$input['usuario_correo'])->where('id_mutual',$input['id_mutual'])->first();
            if($existe_usu){
                return response()->json(['success' => false, 'error' => 'El Usuario: '.$input['usuario_correo'].' ya existe, coloque uno diferente.']);
            }else{

                try {
                    DB::beginTransaction();
                    $pass = rand(11223344, 99887766);
                    $ult_persona = DB::table('usuarios')->select('codigo_user')->orderby('codigo_user', 'DESC')->first();
                    if($ult_persona){
                        $prox = $ult_persona->codigo_user + 1;
                    }else{
                        $prox = 1;
                    }
                   
                    $datos = [
                        'codigo_user' => $prox,
                        'user' => $input['usuario_correo'],
                        'id_mutual' => $input['id_mutual'],
                        'id_sucursal' => 1,
                        'nombre' => $input['nombre_usuario'],
                        'rol' => $input['rol'],
                        'habilitado' => $input['habilitado'],
                        'password' =>$pass,
                       
                    ];
                    DB::table('usuarios')->insert($datos);
                    $accion = 'Usuario creado con exito.';
                    $mutual = DB::table('personas')->where('soc_numero_socio',$input['id_mutual'])->where('soc_sucursal',1)->first();
                    try {
                        session()->put('email', $input['usuario_correo']);
                        Mail::send('admin.correo-password', [
                        'nombre' => $input['nombre_usuario'],
                        'usuario' => $input['usuario_correo'],
                        'clave' => $pass,
                        'mutual' => $mutual->nombre,
                        'link' => 'http://mutuales.faum.com.ar',//local en azureLocal: http://azurelocal:2021 , en produccion http://mutuales.faum.com.ar
                    ], function ($message) {
                        $message->from('info@faum.com.ar', 'Faum Mutuales'); //faum.tarjeta@gmail.com
                        $message->to(session()->get('email'))
                                ->subject('Usuario y Contraseña Sistema Faum Mutuales');
                    });
                    } catch (\Exception $e) {
                        Log::info($e->getMessage());
        
                        return response()->json(['success' => false, 'token' => '', 'data' => '1', 'error' => $e->getMessage()]);
                    }
                    DB::commit();
                    
        
                    session()->forget('email');
                    return response()->json(['success' => true, 'salida' => $accion]);

                } catch (\Illuminate\Database\QueryException $ex) {
                    DB::rollback();
                    Log::info('Error: '.$ex->getMessage());
                           
                    return response()->json(['success' => false, 'error' => 'error al crear el Usuario.']);
                }


            }
         
        }
        return view('usuarios.alta-usuario')->with('mutuales', $mutuales);
    }

    public function buscarUsuario(Request $request)
    {
        Log::info('buscar usuario');
        if ($request->isMethod('post')) {
            $input = $request->all();
            Log::info($input);
            $rules = [
                'mutual' => 'required',
                'usuario' => [
                    'required',
                    'regex:/[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/', 
                    ],
                             
            ];

            $messages = [
                'mutual.required' => 'Seleccione el número de Adherente de la Mutual.',
                'usuario.required' => 'Ingrese el correo del Usuario.',
                'usuario.regex' => 'Ingrese un mail valido para el usuario.',
           
            ];

            $v = Validator::make($input, $rules, $messages);

            if ($v->fails()) {
                $msg = $v->errors()->all()[0];

                return response()->json(['success' => false, 'error' => $msg]);
            }
            
            //dd($input);
            $usuario = DB::table('usuarios')->where('user',$input['usuario'])->where('id_mutual',$input['mutual'])->first();
            Log::info([$usuario]);
            if($usuario){
                
                return response()->json(['success' => true, 'salida' => $usuario, 'error' => '']);
     

            }else{
               
                return response()->json(['success' => false, 'error' => 'usuario nuevo', 'datos_html' => '']);
                
                


            }
         
            
        }

              
    }

    public function updateUsuario(Request $request)
    {
        Log::info('update usuario');
        if ($request->isMethod('post')) {
            $input = $request->all();
            Log::info($input);
            //dd($input);
            $rules = [
                'id_mutual' => 'required',
                'usuario_correo' => [
                    'required',
                    'regex:/[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/', 
                    ],
                'nombre_usuario' => 'required',
                'rol' => 'required',
                'habilitado' => 'required',
               
            ];

            $messages = [
                'id_mutual.required' => 'Ingrese la Mutual Adherente de la Mutual.',
                'usuario_correo.required' => 'Ingrese el correo del Usuario.',
                'usuario_correo.regex' => 'Ingrese un mail valido para el usuario.',
                'nombre_usuario.required' => 'Ingrese el nombre del Usuario.',
                'rol.required' => 'Seleccione el rol del Usuario.',
                'habilitado.required' => 'Seleccione el un estado para el nuevo Usuario.',
            
            ];

            $v = Validator::make($input, $rules, $messages);

            if ($v->fails()) {
                $msg = $v->errors()->all()[0];

                return response()->json(['success' => false, 'error' => $msg]);
            }
            
            //dd($input);
            $existe_usu = DB::table('usuarios')->where('user',$input['usuario_correo'])->where('id_mutual', $input['id_mutual'])->first();
            if($existe_usu){
               
                try {
                    DB::beginTransaction();
                    
                    $datos = [
                        'user' => $input['usuario_correo'],
                        'id_mutual' => $input['id_mutual'],
                        'id_sucursal' => 1,
                        'nombre' => $input['nombre_usuario'],
                        'rol' => $input['rol'],
                        'habilitado' => $input['habilitado'],
                        //'password' =>$pass,
                       
                    ];
                    DB::table('usuarios')->where('codigo_user',$existe_usu->codigo_user)->where('id_mutual', $input['id_mutual'])->update($datos);
                    $accion = 'Usuario actualizado con exito.';
                    $mutual = DB::table('personas')->where('soc_numero_socio',$input['id_mutual'])->where('soc_sucursal',1)->first();
                    try {
                        session()->put('email', $input['usuario_correo']);
                        Mail::send('admin.update-usuario', [
                        'nombre' => $input['nombre_usuario'],
                        'usuario' => $input['usuario_correo'],
                        'habilitado' => $input['habilitado'],
                        'mutual' => $mutual->nombre,
                        'link' => 'http://mutuales.faum.com.ar',//local en azureLocal: http://azurelocal:2021 , en produccion http://mutuales.faum.com.ar
                    ], function ($message) {
                        $message->from('info@faum.com.ar', 'Faum Mutuales');
                        $message->to(session()->get('email'))
                                ->subject('Usuario Actualizado - Sistema Faum Mutuales');
                    });
                    } catch (\Exception $e) {
                        Log::info($e->getMessage());
        
                        return response()->json(['success' => false, 'token' => '', 'data' => '1', 'error' => $e->getMessage()]);
                    }
                    DB::commit();
                    session()->forget('email');
                    return response()->json(['success' => true, 'salida' => $accion]);

                } catch (\Illuminate\Database\QueryException $ex) {
                    DB::rollback();
                    Log::info('Error: '.$ex->getMessage());
                          
                    return response()->json(['success' => false, 'error' => 'error en server.']);

                }

            }else{

                    return response()->json(['success' => false, 'error' => 'El Usuario '.$input['usaurio_correo'].' no existe, coloque uno deferente.']);
                }


        }
         
            
    }

    public function altaSucursal(Request $request)
    {
        Log::info('alta sucursal');
        if ($request->isMethod('post')) {
            $input = $request->all();
            Log::info($input);
            $rules = [
                'mutual' => 'required|numeric',
                'nro_sucursal' => 'required|numeric',
                'nombre_sucursal' => 'required',
                'cod_postal' => 'required',
               
            ];

            $messages = [
                'mutual.required' => 'Ingrese el número de Adherente de la Mutual.',
                'mutual.numeric' => 'El número de Adherente de la Mutual es solo números.',

                'nro_sucursal.required' => 'Ingrese el número de la Sucursal.',
                'nro_sucursal.numeric' => 'El número de la Sucursal es solo números.',

                'nombre_sucursal.required' => 'Ingrese el nombre de la Sucursal.',
                'cod_postal.required' => 'Seleccione el código postal de la Sucursal.',
            
            ];

            $v = Validator::make($input, $rules, $messages);

            if ($v->fails()) {
                $msg = $v->errors()->all()[0];

                return response()->json(['success' => false, 'error' => $msg]);
            }
            
            //dd($input);
            $existe = DB::table('personas')->where('soc_numero_socio',$input['mutual'])->where('soc_sucursal',$input['nro_sucursal'])->first();
            if($existe){
                return response()->json(['success' => false, 'error' => 'N° Sucursal: '.$input['nro_sucursal'].' ya existe, coloque un Nro diferente de Sucursal.']);
            }else{

                try {
                    DB::beginTransaction();
                    $ult_persona = DB::table('personas')->select('nro_persona')->orderby('nro_persona', 'DESC')->first();
                    if($ult_persona){
                        $prox = $ult_persona->nro_persona + 1;
                    }else{
                        $prox = 1;
                    }
                    $datos = [
                        'nro_persona' => $prox,
                        'soc_numero_socio' => $input['mutual'],
                        'es_usuario' => 1,
                        'es_socio' => 1,
                        'soc_estado' => 'A',
                        'soc_sucursal' => $input['nro_sucursal'],
                        'nombre' => $input['nombre_sucursal'],
                        'codigo_postal' => $input['cod_postal'],
                        'confirmada' => 0,
                        'soc_fisica_juridica' =>2,
                        'soc_codigo_baja' => 0,
                    ];
                    DB::table('personas')->insert($datos);
                    $accion = 'Sucursal creada con exito.';
                    DB::commit();
                    return response()->json(['success' => true, 'salida' => $accion]);

                } catch (\Illuminate\Database\QueryException $ex) {
                    DB::rollback();
                    Log::info('Error: '.$ex->getMessage());
                        
                    return response()->json(['success' => false, 'error' => 'error en server.']);
                }


            }
         
            
        }

        
        return view('mutuales.alta-mutual');
        
    }

    public function cargarMutual(Request $request)
    {
        Log::info('Nombre del Asociado: ');
        Log::info(session()->get('user'));
        if (session()->get('user') != null) {
            $categoria = DB::table('categorias')->get();
            $ente_liq = DB::table('ente_liquidador')->get();
            $uif_act = DB::table('uif_actividad')->get();
            $rubros = DB::table('rubros')->get();
            $origen = DB::table('origen_socio')->get();

            if (session()->get('rol') == 'Usuario') {
                $id_mutual = session()->get('id_mutual');
                $id_sucursal = session()->get('id_sucursal');

                $mutual = DB::table('personas')->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', $id_sucursal)->first();
               // Log::info($mutual);
                
                if($mutual->nro_cuit <> '' && $mutual->domicilio_calle <> '' && $mutual->domicilio_nro <> '' && $mutual->cond_iva <> ''
                        && $mutual->telefono1 <> '' && $mutual->telefono_celular <> '' && $mutual->email <> '' && $mutual->soc_ejercicio_economico <> '' 
                        && $mutual->soc_jud_fecha_constitucion <> '' && $mutual->soc_jud_nro_inscripcion <> ''){
                            
                    $datos_completos = 1; //tiene todos los datos requeridos

                }else{
                    $datos_completos = 0;
                }
                $mutual->datos_completos = $datos_completos;
                //dd($mutual->confirmada);
                session()->put('nro_persona', $mutual->nro_persona);
                $sucursales = DB::table('personas')->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', '>', 1)->orderby('soc_sucursal','asc')->get();

                //dd($sucursales);
                foreach ($sucursales as $i => $res) {
                    $sql_s = 'SELECT localidad.codigo_postal as codigo_postal, localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad ,
                              provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp
                              from localidad, provincia
                              where provincia.codigo_provincia=localidad.codigo_provincia and localidad.codigo_postal= '.$res->codigo_postal;
                    //dd($sql_s);
                    $buscar_s = DB::connection('mysql')->select($sql_s);

                    $sucursales[$i]->localidad = $buscar_s[0]->localidad;
                    $sucursales[$i]->provincia = $buscar_s[0]->provincia;
                }

                //dd($sucursales);
               
                $sql = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal = '.$mutual->codigo_postal;

                $buscar = DB::connection('mysql')->select($sql);
                //dd($buscar);
                $mutual->localidad = $buscar[0]->localidad;
                $mutual->provincia = $buscar[0]->provincia;

                $presidente = DB::table('personas')
                                    ->join('personas_sociedad', 'personas_sociedad.nro_persona', 'personas.nro_persona')
                                    ->where('personas.soc_numero_socio', $id_mutual)->where('personas.soc_sucursal', 0)->where('personas_sociedad.socio_caracter', 8)->first();
                Log::info('presi');
                Log::info([$presidente]);
                if ($presidente) {
                    $postal_pre = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal = '.$presidente->codigo_postal;

                    $buscar_pre = DB::connection('mysql')->select($postal_pre);
                    //dd($buscar);
                    $presidente->localidad = $buscar_pre[0]->localidad;
                    $presidente->provincia = $buscar_pre[0]->provincia;
                    $presidente->soc_fecha_nacimiento = trim(date('Y-m-d', strtotime($presidente->soc_fecha_nacimiento)));
                } else {
                    $presidente = '';
                }

                $secretario = DB::table('personas')
                                    ->join('personas_sociedad', 'personas_sociedad.nro_persona', 'personas.nro_persona')
                                    ->where('personas.soc_numero_socio', $id_mutual)->where('personas.soc_sucursal', 0)->where('personas_sociedad.socio_caracter', 6)->first();
                Log::info('secretario');
                Log::info([$secretario]);

                if ($secretario) {
                    $postal_sec = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal = '.$secretario->codigo_postal;

                    $buscar_sec = DB::connection('mysql')->select($postal_sec);
                    //dd($buscar);
                    $secretario->localidad = $buscar_sec[0]->localidad;
                    $secretario->provincia = $buscar_sec[0]->provincia;
                    $secretario->soc_fecha_nacimiento = trim(date('Y-m-d', strtotime($secretario->soc_fecha_nacimiento)));
                } else {
                    $secretario = '';
                }

                $tesorero = DB::table('personas')
                                    ->join('personas_sociedad', 'personas_sociedad.nro_persona', 'personas.nro_persona')
                                    ->where('personas.soc_numero_socio', $id_mutual)->where('personas.soc_sucursal', 0)->where('personas_sociedad.socio_caracter', 7)->first();
                Log::info('tesorero');
                Log::info([$tesorero]);
                if ($tesorero) {
                    $postal_tes = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal = '.$tesorero->codigo_postal;

                    $buscar_tes = DB::connection('mysql')->select($postal_tes);
                    //dd($buscar);
                    $tesorero->localidad = $buscar_tes[0]->localidad;
                    $tesorero->provincia = $buscar_tes[0]->provincia;
                    $tesorero->soc_fecha_nacimiento = trim(date('Y-m-d', strtotime($tesorero->soc_fecha_nacimiento)));
                } else {
                    $tesorero = '';
                }

                $apoderado = DB::table('personas')
                                    ->join('personas_sociedad', 'personas_sociedad.nro_persona', 'personas.nro_persona')
                                    ->where('personas.soc_numero_socio', $id_mutual)->where('personas.soc_sucursal', 0)->where('personas_sociedad.socio_caracter', 1)->first();
                Log::info('apoderado');
                Log::info([$apoderado]);
                if ($apoderado) {
                    $postal_apo = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal = '.$apoderado->codigo_postal;

                    $buscar_apo = DB::connection('mysql')->select($postal_apo);
                    //dd($buscar);
                    $apoderado->localidad = $buscar_apo[0]->localidad;
                    $apoderado->provincia = $buscar_apo[0]->provincia;
                    $apoderado->soc_fecha_nacimiento = trim(date('Y-m-d', strtotime($apoderado->soc_fecha_nacimiento)));
                } else {
                    $apoderado = '';
                }

                $documentos = DB::table('documentacion_mutuales')->where('id_mutual',$id_mutual)->select('id_mutual','cod_doc','descripcion')->get();

                /*if($documentos){
                    $documentos = $documentos->toJson();
                    $documentos = json_decode($documentos, true);
                }else{
                    $documentos = '';
                }*/

                //$pais =DB::table('pais')->get();
                switch ($mutual->confirmada){
                    case 0:
                        $estado_actual = 'En carga de datos.';
                    break;
                    case 1:
                        $estado_actual = 'Datos Confirmados, en revisión por FAUM.';
                    break;
                    case 2:
                        $estado_actual = 'Datos Aprobados por FAUM.';
                    break;  
                    case 3:
                        $estado_actual = 'Rechazada por FAUM, actualizar datos.';
                    break;  
                    
                    
                }
                $mutual->estado_actual = $estado_actual;
                return view('mutuales.mutual')->with('categoria', $categoria)->with('ente_liq', $ente_liq)->with('uif_act', $uif_act)->with('origen', $origen)->with('rubros', $rubros)->with('datos', $mutual)->with('sucursales', $sucursales)
                                            ->with('presidente', $presidente)->with('secretario', $secretario)->with('tesorero', $tesorero)->with('apoderado', $apoderado)->with('documentos', $documentos);
            } else {
                return view('mutuales.alta-mutual')->with('categoria', $categoria)->with('ente_liq', $ente_liq)->with('uif_act', $uif_act)->with('origen', $origen)->with('rubros', $rubros);
            }

        }else{
            return view('admin.login');
        }
    }

    public function updateMutualGenerales(Request $request)
    {
        $input = $request->all();
        $id_mutual = session()->get('id_mutual');
        $id_sucursal = session()->get('id_sucursal');
        $nro_persona = session()->get('nro_persona');

        $categoria = DB::table('categorias')->get();
        $ente_liq = DB::table('ente_liquidador')->get();
        $uif_act = DB::table('uif_actividad')->get();
        $origen = DB::table('origen_socio')->get();
        $rubros = DB::table('rubros')->get();

        if ($request->isMethod('post')) {
            $input = $request->all();
            Log::info('entro a modificar datos generales');
            Log::info($input);
            //dd($input);

            $rules = [
                'nro_cuit' => 'required|numeric',
                'domicilio_calle' => 'required',
                'domicilio_nro' => 'required',
                'telefono1' => 'required|numeric',
                'telefono_celular' => 'required|numeric',
                'email' => [
                    'required',
                    'regex:/[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/', 
                    ], 
                'cond_iva' => 'required',
                'soc_ejercicio_economico' => 'required',
                'soc_jud_fecha_constitucion' => 'required',
                'soc_jud_inscripcion_inaes' => 'required',
            ];

            

            $messages = [
                'nro_cuit.required' => 'Ingrese el numero de CUIL/CUIT de la Mutual.',
                //'nro_cuit_pre.integer' => 'El numero de CUIL/CUIT es solo números (Mutual).',
                'nro_cuit.numeric' => 'El número de CUIL/CUIT es solo números (Mutual).',

                'domicilio_calle.required' => 'Ingrese el domicilio de la Mutual.',
                'domicilio_nro.required' => 'Ingrese el numero del domicilio de la Mutual.',

                'telefono_celular.required' => 'Ingrese el número de teléfono celular de la Mutual.',
                //'telefono_celular.integer' => 'El numero de teléfono celular es solo números (mutual).',
                'telefono_celular.numeric' => 'El número de teléfono celular es solo números (Mutual).',

                'telefono1.required' => 'Ingrese el número de teléfono de la Mutual.',
                //'telefono1.integer' => 'El numero de teléfono es solo números (mutual).',
                'telefono1.numeric' => 'El número de teléfono es solo números (Mutual).',

                'email.required' => 'Ingrese el mail de la Mutual.',
                'email.regex' => 'Ingrese un mail valido para la Mutual.',
                'cond_iva.required' => 'Seleccione la condición IVA de la Mutual.',
                'soc_ejercicio_economico.required' => 'Seleccione el mes del Inicio Economico de la Mutual.',
                'soc_jud_fecha_constitucion.required' => 'Ingrese la fecha de Constitución de la Mutual.',
                'soc_jud_inscripcion_inaes.required' => 'Ingrese el N° del INAES de la Mutual.',
            ];

            $v = Validator::make($input, $rules, $messages);

            if ($v->fails()) {
                $msg = $v->errors()->all()[0];

                return response()->json(['success' => false, 'error' => $msg]);
            }

            //dd($input);
            foreach ($input as $k => $valor) {
                if (!is_array($valor)) {
                    $input[$k] = strip_tags($valor);
                }
            }
            Log::info('despues de arreglo');
            ///Log::info($input['soc_numero_socio']);
            //dd($input);
            try {
                DB::beginTransaction();

                $id_mutual = session()->get('id_mutual');
                $id_sucursal = session()->get('id_sucursal');
                $nro_persona = session()->get('nro_persona');

                $categoria = DB::table('categorias')->get();
                $ente_liq = DB::table('ente_liquidador')->get();
                $uif_act = DB::table('uif_actividad')->get();
                $origen = DB::table('origen_socio')->get();
                $rubros = DB::table('rubros')->get();

                if (isset($input['cond_iva']) && $input['cond_iva'] > 0) {
                    $input['cond_iva'] = $input['cond_iva'];
                } else {
                    $input['cond_iva'] = null;
                }

                if (isset($input['soc_jud_fecha_constitucion']) && $input['soc_jud_fecha_constitucion'] != '') {
                    $input['soc_jud_fecha_constitucion'] = $input['soc_jud_fecha_constitucion'];
                } else {
                    $input['soc_jud_fecha_constitucion'] = null;
                }

                $datos = [
                    'tipo_cuit' => $input['tipo_cuit'],
                    'nro_cuit' => $input['nro_cuit'],
                    'domicilio_calle' => $input['domicilio_calle'],
                    'domicilio_nro' => $input['domicilio_nro'],
                    'domicilio_piso' => $input['domicilio_piso'],
                    'domicilio_depto' => $input['domicilio_depto'],
                    'domicilio_adicional' => $input['domicilio_adicional'],
                    'telefono1' => $input['telefono1'],

                    'telefono_celular' => $input['telefono_celular'],
                    'telefono2' => $input['telefono2'],
                    'telefono3' => $input['telefono3'],
                    'email' => $input['email'],
                    'cond_iva' => $input['cond_iva'],
                    'soc_ejercicio_economico' => $input['soc_ejercicio_economico'],
                    'soc_jud_fecha_constitucion' => $input['soc_jud_fecha_constitucion'],
                    'soc_jud_nro_inscripcion' => $input['soc_jud_inscripcion_inaes'],
                ];



                DB::table('personas')
                        ->where('soc_numero_socio', $id_mutual)
                        ->where('soc_sucursal', $id_sucursal)
                        ->where('nro_persona', $nro_persona)
                        ->update($datos);

                DB::commit();
                $mutual = DB::table('personas')->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', $id_sucursal)->first();
                
                $sql = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal = '.$mutual->codigo_postal;

                $buscar = DB::connection('mysql')->select($sql);
                //dd($buscar);
                $mutual->localidad = $buscar[0]->localidad;
                $mutual->provincia = $buscar[0]->provincia;

                //$pais =DB::table('pais')->get();
                //return view('mutuales.mutual_1')->with('categoria',$categoria)->with('ente_liq',$ente_liq)->with('uif_act',$uif_act)->with('origen',$origen)->with('datos',$mutual);
                Log::info('datos a la vista');
                Log::info([$mutual]);

                return response()->json(['success' => true, 'categoria' => $categoria, 'ente_liq' => $ente_liq,
                                         'uif_act' => $uif_act, 'origen' => $origen, 'datos' => $mutual, 'salida' => 'Datos actualizados con exito.', ]);
            } catch (\Illuminate\Database\QueryException $ex) {
                DB::rollback();
                Log::info('Error: '.$ex->getMessage());

                $mutual = DB::table('personas')->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', $id_sucursal)->first();
                $sql = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal  = '.$mutual->codigo_postal;

                $buscar = DB::connection('mysql')->select($sql);
                //dd($buscar);
                $mutual->localidad = $buscar[0]->localidad;
                $mutual->provincia = $buscar[0]->provincia;

                return view('mutuales.mutual')->with('success', false)->with('categoria', $categoria)->with('ente_liq', $ente_liq)->with('uif_act', $uif_act)->with('rubros', $rubros)->with('origen', $origen)
                                                                       ->with('datos', $mutual)->with('error', $ex->getMessage());
            }
            $mutual = DB::table('personas')->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', $id_sucursal)->first();
            $sql = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                    FROM localidad, provincia
                    where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal  = '.$mutual->codigo_postal;

            $buscar = DB::connection('mysql')->select($sql);
            //dd($buscar);
            $mutual->localidad = $buscar[0]->localidad;
            $mutual->provincia = $buscar[0]->provincia;

            //$pais =DB::table('pais')->get();
            //return view('mutuales.mutual_1')->with('categoria',$categoria)->with('ente_liq',$ente_liq)->with('uif_act',$uif_act)->with('origen',$origen)->with('datos',$mutual);
            return response()->json(['success' => true, 'categoria' => $categoria, 'ente_liq' => $ente_liq, 'uif_act' => $uif_act, 'origen' => $origen, 'datos' => $datos]);
            //dd($input);
        }
    }

    public function updateMutualSucursales(Request $request)
    {
        $input = $request->all();
        $id_mutual = session()->get('id_mutual');
        $id_sucursal = session()->get('id_sucursal');
        $nro_persona = session()->get('nro_persona');

        $categoria = DB::table('categorias')->get();
        $ente_liq = DB::table('ente_liquidador')->get();
        $uif_act = DB::table('uif_actividad')->get();
        $origen = DB::table('origen_socio')->get();

        if ($request->isMethod('post')) {
            $input = $request->all();
            Log::info('entro a sucursales');
            Log::info($input);
            $total_suc = DB::table('personas')->where('soc_numero_socio', $id_mutual)->where('soc_sucursal','>',1)->get()->count();
            //$suc_input = [];
           // $i =0;
            //dd($input['domicilio_calle'.$i.'']);
            DB::beginTransaction();
            try {
         
                $e =0;
                ////dd(count($input['Movimientos']));
                for ($i=0;$i<$total_suc;$i++) {
                    Log::info('for');
                    //dd($mov[$i]['fechaMovimiento']);

                    $rules = [
                        'domicilio_calle'.$e.'' => 'required',
                        'domicilio_nro'.$e.'' => 'required',
                        'telefono1'.$e.'' => 'required',
                        'telefono_celular'.$e.'' => 'required',
                        'email'.$e.'' => 'required',
                        
                    ];

                    $messages = [
                        'domicilio_calle'.$e.'.required' => 'Ingrese el domicilio de la Sucursal Adherente N° ('.$input['soc_sucursal'.$e].').',
                        'domicilio_nro'.$e.'.required' => 'Ingrese el numero del domicilio de la Sucursal Adherente N° ('.$input['soc_sucursal'.$e].').',
                        'telefono1'.$e.'.required' => 'Ingrese el numero de teléfono de la Sucursal Adherente N° ('.$input['soc_sucursal'.$e].').',
                        'telefono_celula'.$e.'r.required' => 'Ingrese un numero de teléfono celular de la Sucursal Adherente N° ('.$input['soc_sucursal'.$e].').',
                        'email'.$e.'.required' => 'Ingrese un mail de laSucursal Adherente N° ('.$input['soc_sucursal'.$e].').',
                    
                    ];

                    $v = Validator::make($input, $rules, $messages);

                    if ($v->fails()) {
                        $msg = $v->errors()->all()[0];

                        return response()->json(['success' => false, 'error' => $msg]);
                    }

                    //dd($input);
                    /*foreach ($input as $k => $valor) {
                        if (!is_array($valor)) {
                            $input[$k] = strip_tags($valor);
                        }
                    }*/
                    
                    $datos = [
                        'domicilio_calle' => $input['domicilio_calle'.$e.''],
                        'domicilio_nro' => $input['domicilio_nro'.$e.''],
                        'domicilio_piso' => $input['domicilio_piso'.$e.''],
                        'domicilio_depto' => $input['domicilio_depto'.$e.''],
                        'domicilio_adicional' => $input['domicilio_adicional'.$e.''],
                        'telefono1' => $input['telefono1'.$e.''],
                        'telefono_celular' => $input['telefono_celular'.$e.''],
                        'email' => $input['email'.$e.''],
    
                    ];
                    Log::info($datos);
                    $e = $e +1;

                    DB::table('personas') //aqui voy 
                            ->where('soc_numero_socio', $id_mutual)
                            ->where('soc_sucursal', $input['soc_sucursal'.$i])
                            //->where('nro_persona', $nro_persona)
                            ->update($datos);
                 
                   
                }
                

            DB::commit();
            $sucursales = DB::table('personas')->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', '>', 1)->get();

            //dd($presidente);
            foreach ($sucursales as $i => $res) {
                $sql_s = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal = '.$res->codigo_postal;

                $buscar_s = DB::connection('mysql')->select($sql_s);

                $sucursales[$i]->localidad = $buscar_s[0]->localidad;
                $sucursales[$i]->provincia = $buscar_s[0]->provincia;
            }

                //$pais =DB::table('pais')->get();
                //return view('mutuales.mutual_1')->with('categoria',$categoria)->with('ente_liq',$ente_liq)->with('uif_act',$uif_act)->with('origen',$origen)->with('datos',$mutual);
                Log::info('datos a la vista');
              

                return response()->json(['success' => true, 'categoria' => $categoria, 'ente_liq' => $ente_liq,
                                         'uif_act' => $uif_act, 'origen' => $origen, 'sucrusales' => $sucursales, 'salida' => 'Datos actualizados con exito.', ]);
            } catch (\Illuminate\Database\QueryException $ex) {
                DB::rollback();
                return response()->json(['success'=>'Nok','error'=> $ex->getMessage()]);
            }
            
        }else{
            $mutual = DB::table('personas')->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', $id_sucursal)->first();
                $sql = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal  = '.$mutual->codigo_postal;

            $buscar = DB::connection('mysql')->select($sql);
            //dd($buscar);
            $mutual->localidad = $buscar[0]->localidad;
            $mutual->provincia = $buscar[0]->provincia;

            //$pais =DB::table('pais')->get();
            //return view('mutuales.mutual_1')->with('categoria',$categoria)->with('ente_liq',$ente_liq)->with('uif_act',$uif_act)->with('origen',$origen)->with('datos',$mutual);
            return response()->json(['success' => true, 'categoria' => $categoria, 'ente_liq' => $ente_liq, 'uif_act' => $uif_act, 'origen' => $origen, 'datos' => $datos]);
        }   
        
    
    }

    public function updateMutualPresidente(Request $request)
    {
        $input = $request->all();
        $id_mutual = session()->get('id_mutual');
        $id_sucursal = session()->get('id_sucursal');
        $nro_persona = session()->get('nro_persona');
        $rubros = DB::table('rubros')->get();
        $uif_act = DB::table('uif_actividad')->get();

        if ($request->isMethod('post')) {
            $input = $request->all();
            Log::info('entro a insert / Up presidente');
            Log::info($input);

            $rules = [
                'nombre_pre' => 'required',
                'tipo_cuit_pre' => 'required',
                'nro_cuit_pre' => 'required|numeric',
                'tipo_doc_pre' => 'required',
                'nro_doc_pre' => 'required|numeric',
                'sexo_pre' => 'required',
                'fecha_nac_pre' => 'required',
                'lugar_nac_pre' => 'required',
                'nacionalidad_pre' => 'required',
                'codigo_postal_pre' => 'required',
                'telefono_celular_pre' => 'required|numeric',
                'email_pre' => [
                    'required',
                    'regex:/[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/', 
                    ],
                'empresa_pre' => 'required',
                'cargo_pre' => 'required',
                'uif_actividades_pre' => 'required',
                'edo_civil_pre' => 'required',
                //'lavado_pre' => 'required|numeric',
                'pep_pre' => 'required|numeric',
            ];

            $messages = [
                'nombre_pre.required' => 'Ingrese el nombre del presidente.',
                'tipo_cuit_pre.required' => 'Seleccione el tipo de CUIL/CUIT del presidente.',
                'nro_cuit_pre.required' => 'Ingrese el numero de CUIL/CUIT del presidente.',
                //'nro_cuit_pre.integer' => 'El numero de CUIL/CUIT es solo números (presidente).',
                'nro_cuit_pre.numeric' => 'El numero de CUIL/CUIT es solo números (presidente).',
                'tipo_doc_pre.required' => 'Seleccione el tipo de documento del presidente.',
                'nro_doc_pre.required' => 'Ingrese el numero de documento del presidente.',
                //'nro_doc_pre.integer' => 'El numero de documento es solo números (presidente).',
                'nro_doc_pre.numeric' => 'El numero documento solo números (presidente).',
                'sexo_pre.required' => 'Seleccione el sexo del presidente.',
                'fecha_nac_pre.required' => 'Ingrese la fecha de nacimiento del presidente.',
                'lugar_nac_pre.required' => 'Ingrese el lugar de nacimiento del presidente.',
                'nacionalidad_pre.required' => 'Seleccione la nacionalidad del presidente.',
                'codigo_postal_pre.required' => 'Seleccione el código postal del presidente.',

                'telefono_celular_pre.required' => 'Ingrese el numero de telefono celular del presidente.',
                //'telefono_celular_pre.integer' => 'El numero de telefono celular es solo números (presidente).',
                'telefono_celular_pre.numeric' => 'El numero de telefono celular es solo números (presidente).',
                'email_pre.required' => 'Ingrese el mail del presidente.',
                'email_pre.regex' => 'Ingrese un mail valido para el presidente.',
                'cargo_pre.required' => 'Ingrese el cargo de donde trabaja del presidente.',
                'empresa_pre.required' => 'Ingrese la empresa donde trabaja del presidente.',
                'uif_actividades_pre.required' => 'Seleccione la actividad UIF del presidente.',
                //'lavado_pre.required' => 'Seleccione si es Sujeto Obligado Prevención Lavado del presidente.',
                'pep_pre.required' => 'Seleccione si Reviste Calidad de Pep´s del presidente.',
            ];

            $v = Validator::make($input, $rules, $messages);

            if ($v->fails()) {
                $msg = $v->errors()->all()[0];

                return response()->json(['success' => false, 'error' => $msg]);
            }

            //dd($input);
            foreach ($input as $k => $valor) {
                if (!is_array($valor)) {
                    $input[$k] = strip_tags($valor);
                }
            }
            Log::info('despues de arrglo');
            ///Log::info($input['soc_numero_socio']);
            //dd($input);
            try {
                DB::beginTransaction();

                $uif_act = DB::table('uif_actividad')->get();

                if (isset($input['nro_doc_conyugue_pre']) && $input['nro_doc_conyugue_pre'] != '') {
                    $input['nro_doc_conyugue_pre'] = $input['nro_doc_conyugue_pre'];
                } else {
                    $input['nro_doc_conyugue_pre'] = null;
                }

                if (isset($input['soc_conyuge_tipo_doc_pre']) && $input['soc_conyuge_tipo_doc_pre'] != '') {
                    $input['soc_conyuge_tipo_doc_pre'] = $input['soc_conyuge_tipo_doc_pre'];
                } else {
                    $input['soc_conyuge_tipo_doc_pre'] = null;
                }

                if (isset($input['rubro_pre']) && $input['rubro_pre'] > 0) {
                    $input['rubro_pre'] = $input['rubro_pre'];
                } elseif (isset($input['rubro_pre']) && $input['rubro_pre'] == 0) {
                    $nuevo_rubro = $input['nuevo_rubro_pre'];
                    $ult_rubro = DB::table('rubros')->select('id_rubro')->orderby('id_rubro', 'DESC')->first();

                    if ($ult_rubro) {
                        $id = $ult_rubro->id_rubro + 1;
                    } else {
                        $id = 1;
                    }
                    $rubro_insert = [
                        'id_rubro' => $id,
                        'descripcion' => $nuevo_rubro,
                    ];

                    DB::table('rubros')->insert($rubro_insert);
                    //$rubro_table = DB::table('personas')->where('nro_doc',$input['nro_doc_pre'])->where('tipo_doc',$input['tipo_doc_pre'])->where('soc_numero_socio',$id_mutual)->where('soc_sucursal',$id_sucursal)->first();

                    $input['rubro_pre'] = $id;
                } else {
                    $input['rubro_pre'] = null;
                }

                $datos_pre = [
                    'nombre' => $input['nombre_pre'],
                    'tipo_cuit' => $input['tipo_cuit_pre'],
                    'nro_cuit' => $input['nro_cuit_pre'],
                    'tipo_doc' => $input['tipo_doc_pre'],
                    'nro_doc' => $input['nro_doc_pre'],
                    'codigo_postal' => $input['codigo_postal_pre'],

                    'sexo' => $input['sexo_pre'],
                    'soc_fecha_nacimiento' => $input['fecha_nac_pre'],
                    'soc_lugar_nacimiento' => $input['lugar_nac_pre'],
                    'cod_nacionalidad' => $input['nacionalidad_pre'],
                    'telefono1' => $input['telefono1_pre'],
                    'telefono_celular' => $input['telefono_celular_pre'],
                    'email' => $input['email_pre'],
                    'soc_actividad_uif' => $input['uif_actividades_pre'],
                    'com_rubro' => $input['rubro_pre'],
                    'soc_lugar_trabajo' => $input['empresa_pre'],
                    'soc_jud_actividad_principal' => $input['cargo_pre'],
                    'estado_civil' => $input['edo_civil_pre'],

                    'soc_conyuge_nombre' => $input['conyugue_pre'],
                    'soc_conyuge_tipo_doc' => $input['tipo_doc_conyugue_pre'],
                    'soc_conyuge_nro_doc' => $input['nro_doc_conyugue_pre'],

                    //'sujeto_obligado' => $input['lavado_pre'],
                    'persona_pep' => $input['pep_pre'],
                ];

                $presidente = DB::table('personas')->where('nro_doc', $input['nro_doc_pre'])->where('tipo_doc', $input['tipo_doc_pre'])->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', 0)->first();

                if ($presidente) {
                    DB::table('personas')
                        ->where('nro_doc', $input['nro_doc_pre'])->where('tipo_doc', $input['tipo_doc_pre'])
                        ->where('soc_sucursal', 0)
                        ->where('soc_numero_socio', $id_mutual)
                        ->update($datos_pre);

                    $accion = 'Datos del Presidente actualizados con exito.';
                } else {
                    $ult_persona = DB::table('personas')->select('nro_persona')->orderby('nro_persona', 'DESC')->first();
                    Log::info([$ult_persona]);
                    $datos_pre['nro_persona'] = $ult_persona->nro_persona + 1;
                    $datos_pre['es_nosocio'] = 1;
                    $datos_pre['es_socio'] = 0;
                    $datos_pre['soc_numero_socio'] = $id_mutual;
                    $datos_pre['soc_sucursal'] = 0; 
                    $datos_pre['soc_fisica_juridica'] = 1;
                    DB::table('personas')
                        ->where('nro_doc', $input['nro_doc_pre'])->where('tipo_doc', $input['tipo_doc_pre'])
                        ->where('soc_sucursal',0)
                        ->where('soc_numero_socio', $id_mutual)
                        ->insert($datos_pre);

                    $presidente = DB::table('personas')->where('nro_doc', $input['nro_doc_pre'])->where('tipo_doc', $input['tipo_doc_pre'])->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', 0)->first();
                    //Log::info();
                    $cargo_pres = [
                    'nro_persona' => $presidente->nro_persona,
                    'socio_caracter' => 8,
                    ];

                    DB::table('personas_sociedad')->insert($cargo_pres);

                    $accion = 'Datos del Presidente guardados con exito.';
                }

                /*
                'rubro_pre' => NULL,
               */

                DB::commit();
                $datos_presidente = DB::table('personas')->where('nro_doc', $input['nro_doc_pre'])->where('tipo_doc', $input['tipo_doc_pre'])->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', 0)->first();

                return response()->json(['success' => true, 'presidente' => $datos_presidente, 'salida' => $accion]);
            } catch (\Illuminate\Database\QueryException $ex) {
                DB::rollback();
                Log::info('Error: '.$ex->getMessage());

                return view('mutuales.mutual')->with('success', false)->with('error', $ex->getMessage());
            }

            return response()->json(['success' => true]);
            //dd($input);
        }
    }

    public function updateMutualSecretario(Request $request)
    {
        $input = $request->all();
        $id_mutual = session()->get('id_mutual');
        $id_sucursal = session()->get('id_sucursal');
        $nro_persona = session()->get('nro_persona');

        $uif_act = DB::table('uif_actividad')->get();

        if ($request->isMethod('post')) {
            $input = $request->all();
            Log::info('entro a insert / Up secretario');
            Log::info($input);

            $rules = [
                'nombre_sec' => 'required',
                'tipo_cuit_sec' => 'required',
                'nro_cuit_sec' => 'required|numeric',
                'tipo_doc_sec' => 'required',
                'nro_doc_sec' => 'required|numeric',
                'sexo_sec' => 'required',
                'fecha_nac_sec' => 'required',
                'lugar_nac_sec' => 'required',
                'nacionalidad_sec' => 'required',
                'codigo_postal_sec' => 'required',
                'telefono_celular_sec' => 'required|numeric',
                'email_sec' => [
                    'required',
                    'regex:/[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/', 
                    ],
                'empresa_sec' => 'required',
                'cargo_sec' => 'required',
                'uif_actividades_sec' => 'required',
                'edo_civil_sec' => 'required',
                //'lavado_sec' => 'required|numeric',
                'pep_sec' => 'required|numeric',
            ];

            $messages = [
                'nombre_sec.required' => 'Ingrese el nombre del secretario.',
                'tipo_cuit_sec.required' => 'Seleccione el tipo de CUIL/CUIT del secretario.',
                'nro_cuit_sec.required' => 'Ingrese el numero de CUIL/CUIT del secretario.',
                //'nro_cuit_sec.integer' => 'El numero de CUIL/CUIT es solo números (secretario).',
                'nro_cuit_sec.numeric' => 'El numero de CUIL/CUIT es solo números (secretario).',
                'tipo_doc_sec.required' => 'Seleccione el tipo de documento del secretario.',
                'nro_doc_sec.required' => 'Ingrese el numero de documento del secretario.',
                //'nro_doc_sec.integer' => 'El numero de documento es solo números (secretario).',
                'nro_doc_sec.numeric' => 'El numero documento solo números (secretario).',
                'sexo_sec.required' => 'Seleccione el sexo del secretario.',
                'fecha_nac_sec.required' => 'Ingrese la fecha de nacimiento del secretario.',
                'lugar_nac_sec.required' => 'Ingrese el lugar de nacimiento del secretario.',
                'nacionalidad_sec.required' => 'Seleccione la nacionalidad del secretario.',
                'codigo_postal_sec.required' => 'Seleccione el código postal del secretario.',

                'telefono_celular_sec.required' => 'Ingrese el numero de telefono celular del secretario.',
                //'telefono_celular_sec.integer' => 'El numero de telefono celular es solo números (secretario).',
                'telefono_celular_sec.numeric' => 'El numero de telefono celular es solo números (secretario).',
                'email_sec.required' => 'Ingrese el mail del secretario.',
                'email_sec.regex' => 'Ingrese un mail valido para el secretario.',
                'cargo_sec.required' => 'Ingrese el cargo de donde trabaja del secretario.',
                'empresa_sec.required' => 'Ingrese la empresa donde trabaja del secretario.',
                'uif_actividades_sec.required' => 'Seleccione la actividad UIF del secretario.',
                //'lavado_sec.required' => 'Seleccione si es Sujeto Obligado Prevención Lavado del secretario.',
                'pep_sec.required' => 'Seleccione si Reviste Calidad de Pep´s del secretario.',
            ];

            $v = Validator::make($input, $rules, $messages);

            if ($v->fails()) {
                $msg = $v->errors()->all()[0];

                return response()->json(['success' => false, 'error' => $msg]);
            }

            //dd($input);
            foreach ($input as $k => $valor) {
                if (!is_array($valor)) {
                    $input[$k] = strip_tags($valor);
                }
            }
            Log::info('despues de arrglo');
            ///Log::info($input['soc_numero_socio']);
            //dd($input);
            try {
                DB::beginTransaction();

                $uif_act = DB::table('uif_actividad')->get();

                if (isset($input['nro_doc_conyugue_sec']) && $input['nro_doc_conyugue_sec'] != '') {
                    $input['nro_doc_conyugue_sec'] = $input['nro_doc_conyugue_sec'];
                } else {
                    $input['nro_doc_conyugue_sec'] = null;
                }

                if (isset($input['tipo_doc_conyugue_sec']) && $input['tipo_doc_conyugue_sec'] != '') {
                    $input['tipo_doc_conyugue_sec'] = $input['tipo_doc_conyugue_sec'];
                } else {
                    $input['tipo_doc_conyugue_sec'] = null;
                }

                if (isset($input['rubro_sec']) && $input['rubro_sec'] > 0) {
                    $input['rubro_sec'] = $input['rubro_sec'];
                } elseif (isset($input['rubro_sec']) && $input['rubro_sec'] == 0) {
                    $nuevo_rubro = $input['nuevo_rubro_sec'];
                    $ult_rubro = DB::table('rubros')->select('id_rubro')->orderby('id_rubro', 'DESC')->first();

                    if ($ult_rubro) {
                        $id = $ult_rubro->id_rubro + 1;
                    } else {
                        $id = 1;
                    }
                    $rubro_insert = [
                        'id_rubro' => $id,
                        'descripcion' => $nuevo_rubro,
                    ];

                    DB::table('rubros')->insert($rubro_insert);
                    //$rubro_table = DB::table('personas')->where('nro_doc',$input['nro_doc_pre'])->where('tipo_doc',$input['tipo_doc_pre'])->where('soc_numero_socio',$id_mutual)->where('soc_sucursal',$id_sucursal)->first();

                    $input['rubro_sec'] = $id;
                } else {
                    $input['rubro_sec'] = null;
                }

                $datos_sec = [
                    'nombre' => $input['nombre_sec'],
                    'tipo_cuit' => $input['tipo_cuit_sec'],
                    'nro_cuit' => $input['nro_cuit_sec'],
                    'tipo_doc' => $input['tipo_doc_sec'],
                    'nro_doc' => $input['nro_doc_sec'],
                    'codigo_postal' => $input['codigo_postal_sec'],

                    'sexo' => $input['sexo_sec'],
                    'soc_fecha_nacimiento' => $input['fecha_nac_sec'],
                    'soc_lugar_nacimiento' => $input['lugar_nac_sec'],
                    'cod_nacionalidad' => $input['nacionalidad_sec'],
                    'telefono1' => $input['telefono1_sec'],
                    'telefono_celular' => $input['telefono_celular_sec'],
                    'email' => $input['email_sec'],
                    'soc_actividad_uif' => $input['uif_actividades_sec'],
                    'com_rubro' => $input['rubro_sec'],
                    'soc_lugar_trabajo' => $input['empresa_sec'],
                    'soc_jud_actividad_principal' => $input['cargo_sec'],
                    'estado_civil' => $input['edo_civil_sec'],

                    'soc_conyuge_nombre' => $input['conyugue_sec'],
                    'soc_conyuge_tipo_doc' => $input['tipo_doc_conyugue_sec'],
                    'soc_conyuge_nro_doc' => $input['nro_doc_conyugue_sec'],

                   //'sujeto_obligado' => $input['lavado_sec'],
                    'persona_pep' => $input['pep_sec'],
                ];

                $secretaria = DB::table('personas')->where('nro_doc', $input['nro_doc_sec'])->where('tipo_doc', $input['tipo_doc_sec'])->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', 0)->first();

                if ($secretaria) {
                    DB::table('personas')
                        ->where('nro_doc', $input['nro_doc_pre'])->where('tipo_doc', $input['tipo_doc_sec'])
                        ->where('soc_sucursal', 0)
                        ->where('soc_numero_socio', $id_mutual)
                        ->update($datos_sec);

                    $accion = 'Datos del  Secretario actualizados con exito.';
                } else {
                    $ult_persona = DB::table('personas')->select('nro_persona')->orderby('nro_persona', 'DESC')->first();
                    Log::info([$ult_persona]);
                    $datos_sec['nro_persona'] = $ult_persona->nro_persona + 1;
                    $datos_sec['es_nosocio'] = 1;
                    $datos_sec['es_socio'] = 0;
                    $datos_sec['soc_numero_socio'] = $id_mutual;
                    $datos_sec['soc_sucursal'] = 0;
                    $datos_sec['soc_fisica_juridica'] = 1;
                    DB::table('personas')
                        ->where('nro_doc', $input['nro_doc_sec'])->where('tipo_doc', $input['tipo_doc_sec'])
                        ->where('soc_sucursal', 0)
                        ->where('soc_numero_socio', $id_mutual)
                        ->insert($datos_sec);

                    $secretaria = DB::table('personas')->where('nro_doc', $input['nro_doc_sec'])->where('tipo_doc', $input['tipo_doc_sec'])->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', 0)->first();
                    //Log::info();
                    $cargo_sec = [
                    'nro_persona' => $secretaria->nro_persona,
                    'socio_caracter' => 6,
                    ];

                    DB::table('personas_sociedad')->insert($cargo_sec);

                    $accion = 'Datos del Secretario guardados con exito.';
                }

                /*
                'rubro_pre' => NULL,
               */

                DB::commit();
                $datos_secretaria = DB::table('personas')->where('nro_doc', $input['nro_doc_sec'])->where('tipo_doc', $input['tipo_doc_sec'])->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', 0)->first();

                return response()->json(['success' => true, 'secretario' => $datos_secretaria, 'salida' => $accion]);
            } catch (\Illuminate\Database\QueryException $ex) {
                DB::rollback();
                Log::info('Error: '.$ex->getMessage());

                return view('mutuales.mutual')->with('success', false)->with('error', $ex->getMessage());
            }

            return response()->json(['success' => true]);
            //dd($input);
        }
    }

    public function updateMutualTesorero(Request $request)
    {
        $input = $request->all();
        $id_mutual = session()->get('id_mutual');
        $id_sucursal = session()->get('id_sucursal');
        $nro_persona = session()->get('nro_persona');

        $uif_act = DB::table('uif_actividad')->get();

        if ($request->isMethod('post')) {
            $input = $request->all();
            Log::info('entro a insert / Up tesorero');
            Log::info($input);

            $rules = [
                'nombre_tes' => 'required',
                'tipo_cuit_tes' => 'required',
                'nro_cuit_tes' => 'required|numeric',
                'tipo_doc_tes' => 'required',
                'nro_doc_tes' => 'required|numeric',
                'sexo_tes' => 'required',
                'fecha_nac_tes' => 'required',
                'lugar_nac_tes' => 'required',
                'nacionalidad_tes' => 'required',
                'codigo_postal_tes' => 'required',
                'telefono_celular_tes' => 'required|numeric',
                'email_tes' => [
                    'required',
                    'regex:/[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/', 
                    ],
                'empresa_tes' => 'required',
                'cargo_tes' => 'required',
                'uif_actividades_tes' => 'required',
                'edo_civil_tes' => 'required',
                //'lavado_tes' => 'required|numeric',
                'pep_tes' => 'required|numeric',
            ];

            $messages = [
                'nombre_tes.required' => 'Ingrese el nombre del tesorero.',
                'tipo_cuit_tes.required' => 'Seleccione el tipo de CUIL/CUIT del tesorero.',
                'nro_cuit_tes.required' => 'Ingrese el numero de CUIL/CUIT del tesorero.',
                //'nro_cuit_tes.integer' => 'El numero de CUIL/CUIT es solo números (tesorero).',
                'nro_cuit_tes.numeric' => 'El numero de CUIL/CUIT es solo números (tesorero).',
                'tipo_doc_tes.required' => 'Seleccione el tipo de documento del tesorero.',
                'nro_doc_tes.required' => 'Ingrese el numero de documento del tesorero.',
                //'nro_doc_tes.integer' => 'El numero de documento es solo números (tesorero).',
                'nro_doc_tes.numeric' => 'El numero documento solo números (tesorero).',
                'sexo_tes.required' => 'Seleccione el sexo del tesorero.',
                'fecha_nac_tes.required' => 'Ingrese la fecha de nacimiento del tesorero.',
                'lugar_nac_tes.required' => 'Ingrese el lugar de nacimiento del tesorero.',
                'nacionalidad_tes.required' => 'Seleccione la nacionalidad del tesorero.',
                'codigo_postal_tes.required' => 'Seleccione el código postal del tesorero.',

                'telefono_celular_tes.required' => 'Ingrese el numero de telefono celular del tesorero.',
                //'telefono_celular_tes.integer' => 'El numero de telefono celular es solo números (tesorero).',
                'telefono_celular_tes.numeric' => 'El numero de telefono celular es solo números (tesorero).',
                'email_tes.required' => 'Ingrese el mail del tesorero.',
                'email_tes.regex' => 'Ingrese un mail valido para el tesorero.',
                'cargo_tes.required' => 'Ingrese el cargo de donde trabaja del tesorero.',
                'empresa_tes.required' => 'Ingrese la empresa donde trabaja del tesorero.',
                'uif_actividades_tes.required' => 'Seleccione la actividad UIF del tesorero.',
                //'lavado_tes.required' => 'Seleccione si es Sujeto Obligado Prevención Lavado del tesorero.',
                'pep_tes.required' => 'Seleccione si Reviste Calidad de Pep´s del tesorero.',
            ];

            $v = Validator::make($input, $rules, $messages);

            if ($v->fails()) {
                $msg = $v->errors()->all()[0];

                return response()->json(['success' => false, 'error' => $msg]);
            }

            //dd($input);
            foreach ($input as $k => $valor) {
                if (!is_array($valor)) {
                    $input[$k] = strip_tags($valor);
                }
            }
            Log::info('despues de arrglo');
            ///Log::info($input['soc_numero_socio']);
            //dd($input);
            try {
                DB::beginTransaction();

                $uif_act = DB::table('uif_actividad')->get();

                if (isset($input['nro_doc_conyugue_tes']) && $input['nro_doc_conyugue_tes'] != '') {
                    $input['nro_doc_conyugue_tes'] = $input['nro_doc_conyugue_tes'];
                } else {
                    $input['nro_doc_conyugue_tes'] = null;
                }

                if (isset($input['tipo_doc_conyugue_tes']) && $input['tipo_doc_conyugue_tes'] != '') {
                    $input['tipo_doc_conyugue_tes'] = $input['tipo_doc_conyugue_tes'];
                } else {
                    $input['tipo_doc_conyugue_tes'] = null;
                }

                if (isset($input['rubro_tes']) && $input['rubro_tes'] > 0) {
                    $input['rubro_tes'] = $input['rubro_tes'];
                } elseif (isset($input['rubro_tes']) && $input['rubro_tes'] == 0) {
                    $nuevo_rubro = $input['nuevo_rubro_tes'];
                    $ult_rubro = DB::table('rubros')->select('id_rubro')->orderby('id_rubro', 'DESC')->first();

                    if ($ult_rubro) {
                        $id = $ult_rubro->id_rubro + 1;
                    } else {
                        $id = 1;
                    }
                    $rubro_insert = [
                        'id_rubro' => $id,
                        'descripcion' => $nuevo_rubro,
                    ];

                    DB::table('rubros')->insert($rubro_insert);
                    //$rubro_table = DB::table('personas')->where('nro_doc',$input['nro_doc_pre'])->where('tipo_doc',$input['tipo_doc_pre'])->where('soc_numero_socio',$id_mutual)->where('soc_sucursal',$id_sucursal)->first();

                    $input['rubro_tes'] = $id;
                } else {
                    $input['rubro_tes'] = null;
                }

                $datos_tes = [
                    'nombre' => $input['nombre_tes'],
                    'tipo_cuit' => $input['tipo_cuit_tes'],
                    'nro_cuit' => $input['nro_cuit_tes'],
                    'tipo_doc' => $input['tipo_doc_tes'],
                    'nro_doc' => $input['nro_doc_tes'],
                    'codigo_postal' => $input['codigo_postal_tes'],

                    'sexo' => $input['sexo_tes'],
                    'soc_fecha_nacimiento' => $input['fecha_nac_tes'],
                    'soc_lugar_nacimiento' => $input['lugar_nac_tes'],
                    'cod_nacionalidad' => $input['nacionalidad_tes'],
                    'telefono1' => $input['telefono1_tes'],
                    'telefono_celular' => $input['telefono_celular_tes'],
                    'email' => $input['email_tes'],
                    'soc_actividad_uif' => $input['uif_actividades_tes'],
                    'com_rubro' => $input['rubro_tes'],
                    'soc_lugar_trabajo' => $input['empresa_tes'],
                    'soc_jud_actividad_principal' => $input['cargo_tes'],
                    'estado_civil' => $input['edo_civil_tes'],

                    'soc_conyuge_nombre' => $input['conyugue_tes'],
                    'soc_conyuge_tipo_doc' => $input['tipo_doc_conyugue_tes'],
                    'soc_conyuge_nro_doc' => $input['nro_doc_conyugue_tes'],

                    //'sujeto_obligado' => $input['lavado_tes'],
                    'persona_pep' => $input['pep_tes'],
                ];

                $tesorero = DB::table('personas')->where('nro_doc', $input['nro_doc_tes'])->where('tipo_doc', $input['tipo_doc_tes'])->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', 0)->first();

                if ($tesorero) {
                    DB::table('personas')
                        ->where('nro_doc', $input['nro_doc_tes'])->where('tipo_doc', $input['tipo_doc_tes'])
                        ->where('soc_sucursal', 0)
                        ->where('soc_numero_socio', $id_mutual)
                        ->update($datos_tes);

                    $accion = 'Datos del  Tesorero actualizados con exito.';
                } else {
                    $ult_persona = DB::table('personas')->select('nro_persona')->orderby('nro_persona', 'DESC')->first();
                    Log::info([$ult_persona]);
                    $datos_tes['nro_persona'] = $ult_persona->nro_persona + 1;
                    $datos_tes['es_nosocio'] = 1;
                    $datos_tes['es_socio'] = 0;
                    $datos_tes['soc_numero_socio'] = $id_mutual;
                    $datos_tes['soc_sucursal'] = 0;
                    $datos_tes['soc_fisica_juridica'] = 1;
                    DB::table('personas')
                        ->where('nro_doc', $input['nro_doc_tes'])->where('tipo_doc', $input['tipo_doc_tes'])
                        ->where('soc_sucursal', 0)
                        ->where('soc_numero_socio', $id_mutual)
                        ->insert($datos_tes);

                    $tesorero = DB::table('personas')->where('nro_doc', $input['nro_doc_tes'])->where('tipo_doc', $input['tipo_doc_tes'])->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', 0)->first();
                    //Log::info();
                    $cargo_tes = [
                    'nro_persona' => $tesorero->nro_persona,
                    'socio_caracter' => 7,
                    ];

                    DB::table('personas_sociedad')->insert($cargo_tes);

                    $accion = 'Datos del Tesorero guardados con exito.';
                }

                /*
                'rubro_pre' => NULL,
               */

                DB::commit();
                $datos_tesorero = DB::table('personas')->where('nro_doc', $input['nro_doc_tes'])->where('tipo_doc', $input['tipo_doc_tes'])->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', 0)->first();

                return response()->json(['success' => true, 'tesorero' => $datos_tesorero, 'salida' => $accion]);
            } catch (\Illuminate\Database\QueryException $ex) {
                DB::rollback();
                Log::info('Error: '.$ex->getMessage());

                return view('mutuales.mutual')->with('success', false)->with('error', $ex->getMessage());
            }

            return response()->json(['success' => true]);
            //dd($input);
        }
    }

    public function updateMutualApoderado(Request $request)
    {
        $input = $request->all();
        $id_mutual = session()->get('id_mutual');
        $id_sucursal = session()->get('id_sucursal');
        $nro_persona = session()->get('nro_persona');

        $uif_act = DB::table('uif_actividad')->get();

        if ($request->isMethod('post')) {
            $input = $request->all();
            Log::info('entro a insert / Up apoderado');
            Log::info($input);
            //dd($input);

            $rules = [
                'nombre_apo' => 'required',
                'tipo_cuit_apo' => 'required',
                'nro_cuit_apo' => 'required|numeric',
                'tipo_doc_apo' => 'required',
                'nro_doc_apo' => 'required|numeric',
                'sexo_apo' => 'required',
                'fecha_nac_apo' => 'required',
                'lugar_nac_apo' => 'required',
                'nacionalidad_apo' => 'required',
                'codigo_postal_apo' => 'required',
                'telefono_celular_apo' => 'required|numeric',
                'email_apo' => [
                    'required',
                    'regex:/[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/', 
                    ],
                'empresa_apo' => 'required',
                'cargo_apo' => 'required',
                'uif_actividades_apo' => 'required',
                'edo_civil_apo' => 'required',
                //'lavado_apo' => 'required|numeric',
                'pep_apo' => 'required|numeric',
            ];

            $messages = [
                'nombre_apo.required' => 'Ingrese el nombre del apoderado.',
                'tipo_cuit_apo.required' => 'Seleccione el tipo de CUIL/CUIT del apoderado.',
                'nro_cuit_apo.required' => 'Ingrese el numero de CUIL/CUIT del apoderado.',
                //'nro_cuit_apo.integer' => 'El numero de CUIL/CUIT es solo números (apoderado).',
                'nro_cuit_apo.numeric' => 'El numero de CUIL/CUIT es solo números (apoderado).',
                'tipo_doc_apo.required' => 'Seleccione el tipo de documento del apoderado.',
                'nro_doc_apo.required' => 'Ingrese el numero de documento del apoderado.',
                //'nro_doc_apo.integer' => 'El numero de documento es solo números (apoderado).',
                'nro_doc_apo.numeric' => 'El numero documento solo números (apoderado).',
                'sexo_apo.required' => 'Seleccione el sexo del apoderado.',
                'fecha_nac_apo.required' => 'Ingrese la fecha de nacimiento del apoderado.',
                'lugar_nac_apo.required' => 'Ingrese el lugar de nacimiento del apoderado.',
                'nacionalidad_apo.required' => 'Seleccione la nacionalidad del apoderado.',
                'codigo_postal_apo.required' => 'Seleccione el código postal del apoderado.',

                'telefono_celular_apo.required' => 'Ingrese el numero de telefono celular del apoderado.',
                //'telefono_celular_apo.integer' => 'El numero de telefono celular es solo números (apoderado).',
                'telefono_celular_apo.numeric' => 'El numero de telefono celular es solo números (apoderado).',
                'email_apo.required' => 'Ingrese el mail del apoderado.',
                'email_apo.regex' => 'Ingrese un mail valido para el apoderado.',
                'cargo_apo.required' => 'Ingrese el cargo de donde trabaja del apoderado.',
                'empresa_apo.required' => 'Ingrese la empresa donde trabaja del apoderado.',
                'uif_actividades_apo.required' => 'Seleccione la actividad UIF del apoderado.',
                //'lavado_apo.required' => 'Seleccione si es Sujeto Obligado Prevención Lavado del apoderado.',
                'pep_apo.required' => 'Seleccione si Reviste Calidad de Pep´s del apoderado.',
            ];

            $v = Validator::make($input, $rules, $messages);

            if ($v->fails()) {
                $msg = $v->errors()->all()[0];
                //dd($v->errors()->all()[0]);
                return response()->json(['success' => false, 'error' => $msg]);
            }

            //dd($input);
            foreach ($input as $k => $valor) {
                if (!is_array($valor)) {
                    $input[$k] = strip_tags($valor);
                }
            }
            Log::info('despues de arrglo');
            ///Log::info($input['soc_numero_socio']);
            //dd($input);
            try {
                DB::beginTransaction();

                $uif_act = DB::table('uif_actividad')->get();

                if (isset($input['nro_doc_conyugue_apo']) && $input['nro_doc_conyugue_apo'] != '') {
                    $input['nro_doc_conyugue_apo'] = $input['nro_doc_conyugue_apo'];
                } else {
                    $input['nro_doc_conyugue_apo'] = null;
                }

                if (isset($input['tipo_doc_conyugue_apo']) && $input['tipo_doc_conyugue_apo'] != '') {
                    $input['tipo_doc_conyugue_apo'] = $input['tipo_doc_conyugue_apo'];
                } else {
                    $input['tipo_doc_conyugue_apo'] = null;
                }

                if (isset($input['rubro_apo']) && $input['rubro_apo'] > 0) {
                    $input['rubro_apo'] = $input['rubro_apo'];
                } elseif (isset($input['rubro_apo']) && $input['rubro_apo'] == 0) {
                    $nuevo_rubro = $input['nuevo_rubro_apo'];
                    $ult_rubro = DB::table('rubros')->select('id_rubro')->orderby('id_rubro', 'DESC')->first();

                    if ($ult_rubro) {
                        $id = $ult_rubro->id_rubro + 1;
                    } else {
                        $id = 1;
                    }
                    $rubro_insert = [
                        'id_rubro' => $id,
                        'descripcion' => $nuevo_rubro,
                    ];

                    DB::table('rubros')->insert($rubro_insert);
                    //$rubro_table = DB::table('personas')->where('nro_doc',$input['nro_doc_pre'])->where('tipo_doc',$input['tipo_doc_pre'])->where('soc_numero_socio',$id_mutual)->where('soc_sucursal',$id_sucursal)->first();

                    $input['rubro_apo'] = $id;
                } else {
                    $input['rubro_apo'] = null;
                }

                $datos_apo = [
                    'nombre' => $input['nombre_apo'],
                    'tipo_cuit' => $input['tipo_cuit_apo'],
                    'nro_cuit' => $input['nro_cuit_apo'],
                    'tipo_doc' => $input['tipo_doc_apo'],
                    'nro_doc' => $input['nro_doc_apo'],
                    'codigo_postal' => $input['codigo_postal_apo'],

                    'sexo' => $input['sexo_apo'],
                    'soc_fecha_nacimiento' => $input['fecha_nac_apo'],
                    'soc_lugar_nacimiento' => $input['lugar_nac_apo'],
                    'cod_nacionalidad' => $input['nacionalidad_apo'],
                    'telefono1' => $input['telefono1_apo'],
                    'telefono_celular' => $input['telefono_celular_apo'],
                    'email' => $input['email_apo'],
                    'soc_actividad_uif' => $input['uif_actividades_apo'],
                    'com_rubro' => $input['rubro_apo'],
                    'soc_lugar_trabajo' => $input['empresa_apo'],
                    'soc_jud_actividad_principal' => $input['cargo_apo'],
                    'estado_civil' => $input['edo_civil_apo'],

                    'soc_conyuge_nombre' => $input['conyugue_apo'],
                    'soc_conyuge_tipo_doc' => $input['tipo_doc_conyugue_apo'],
                    'soc_conyuge_nro_doc' => $input['nro_doc_conyugue_apo'],

                    //'sujeto_obligado' => $input['lavado_apo'],
                    'persona_pep' => $input['pep_apo'],
                ];

                $apoderado = DB::table('personas')->where('nro_doc', $input['nro_doc_apo'])->where('tipo_doc', $input['tipo_doc_apo'])->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', 0)->first();

                if ($apoderado) {
                    DB::table('personas')
                        ->where('nro_doc', $input['nro_doc_apo'])->where('tipo_doc', $input['tipo_doc_apo'])
                        ->where('soc_sucursal', 0)
                        ->where('soc_numero_socio', $id_mutual)
                        ->update($datos_apo);

                    $accion = 'Datos del Apoderado actualizados con exito.';
                } else {
                    $ult_persona = DB::table('personas')->select('nro_persona')->orderby('nro_persona', 'DESC')->first();
                    Log::info([$ult_persona]);
                    $datos_apo['nro_persona'] = $ult_persona->nro_persona + 1;
                    $datos_apo['es_nosocio'] = 1;
                    $datos_apo['es_socio'] = 0;
                    $datos_apo['soc_numero_socio'] = $id_mutual;
                    $datos_apo['soc_sucursal'] = 0;
                    $datos_apo['soc_fisica_juridica'] = 1;
                    DB::table('personas')
                        ->where('nro_doc', $input['nro_doc_apo'])->where('tipo_doc', $input['tipo_doc_apo'])
                        ->where('soc_sucursal', 0)
                        ->where('soc_numero_socio', $id_mutual)
                        ->insert($datos_apo);

                    $apoderado = DB::table('personas')->where('nro_doc', $input['nro_doc_apo'])->where('tipo_doc', $input['tipo_doc_apo'])->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', 0)->first();
                    //Log::info();
                    $cargo_apo = [
                    'nro_persona' => $apoderado->nro_persona,
                    'socio_caracter' => 1,
                    ];

                    DB::table('personas_sociedad')->insert($cargo_apo);

                    $accion = 'Datos del Apoderado guardados con exito.';
                }

                /*
                'rubro_pre' => NULL,
               */

                DB::commit();
                $datos_apoderado = DB::table('personas')->where('nro_doc', $input['nro_doc_apo'])->where('tipo_doc', $input['tipo_doc_apo'])->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', 0)->first();

                return response()->json(['success' => true, 'apoderado' => $datos_apoderado, 'salida' => $accion]);
            } catch (\Illuminate\Database\QueryException $ex) {
                DB::rollback();
                Log::info('Error: '.$ex->getMessage());

                return view('mutuales.mutual')->with('success', false)->with('error', $ex->getMessage());
            }

            return response()->json(['success' => true]);
            //dd($input);
        }
    }

    public function logout(Request $request)
    {
        session()->flush();

        return redirect('/');//admin/login
    }

    public function buscarCodPostalModal(Request $request)
    {
        Log::info('paso a buscar cod postal----');

        $input = request()->all();
        Log::info($input);
        //dd($input);

        if (isset($input['filtro'])) {
            if ($input['filtro'] == 1) {
                $sql = "SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia and localidad.nombre_localidad like '%".$input['buscar']."%'";

                $buscar = DB::connection('mysql')->select($sql);
            //dd($buscar);
            } elseif ($input['filtro'] == 2) {
                $sql = "SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal like '%".$input['buscar']."%'";

                $buscar = DB::connection('mysql')->select($sql);
            //dd('cod',$buscar);
            } elseif ($input['filtro'] == 3) {
                $sql = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia';

                $buscar = DB::connection('mysql')->select($sql);
                //dd('cod',$buscar);

                //dd('to',$buscar);
            }

            //dd(count($buscar));

            if (count($buscar) == 0) {
                $html_contenido = view('mutuales.localidad')->with('error', 'No se encuentra Ninguna Localidad con ese Nombre.')->render();

                //dd($html_contenido);
                return response()->json(['success' => false, 'datos' => $html_contenido, 'error' => 'No se encuentra Ninguna Localidad con ese Nombre.']);
            }

            if(isset($input['tipo_cargo']) && $input['tipo_cargo'] <> 99){
                $html_contenido = view('mutuales.localidad')->with('datos', $buscar)->with('tipo_cargo', $input['tipo_cargo'])->render();

            }elseif(isset($input['tipo_cargo']) && $input['tipo_cargo'] == 99){
                $html_contenido = view('mutuales.localidad')->with('datos', $buscar)->with('tipo_cargo', $input['tipo_cargo'])->render();
            }else{
                $html_contenido = view('mutuales.localidad')->with('datos', $buscar)->render();
            }

            

            //dd($html_contenido);
            return response()->json(['success' => true, 'datos' => $html_contenido, 'error' => '']);

            //return response()->json(['success' => true, 'datos' => $busqueda,'error' => '']);
        }
        $html_contenido = view('mutuales.localidad')->with('error', 'Seleccione una opción para buscar.')->render();

        //dd($html_contenido);
        return response()->json(['success' => false, 'datos' => $html_contenido, 'error' => 'Seleccione una opción para buscar.']);
    }

    public function buscarCodPostalModalSuc(Request $request) //nueva modificacion de sucursal
    {
        Log::info('paso a buscar cod postal para sucursales----');

        $input = request()->all();
        Log::info($input);
        //dd($input);

        if (isset($input['filtro'])) {
            if ($input['filtro'] == 1) {
                $sql = "SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia and localidad.nombre_localidad like '%".$input['buscar']."%'";

                $buscar = DB::connection('mysql')->select($sql);
            //dd($buscar);
            } elseif ($input['filtro'] == 2) {
                $sql = "SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal like '%".$input['buscar']."%'";

                $buscar = DB::connection('mysql')->select($sql);
            //dd('cod',$buscar);
            } elseif ($input['filtro'] == 3) {
                $sql = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia';

                $buscar = DB::connection('mysql')->select($sql);
                //dd('cod',$buscar);

                //dd('to',$buscar);
            }

            //dd(count($buscar));

            if (count($buscar) == 0) {
                $html_contenido = view('mutuales.localidad-sucursal')->with('error', 'No se encuentra Ninguna Localidad con ese Nombre.')->render();

                //dd($html_contenido);
                return response()->json(['success' => false, 'datos' => $html_contenido, 'error' => 'No se encuentra Ninguna Localidad con ese Nombre.']);
            }

            
            $html_contenido = view('mutuales.localidad-sucursal')->with('datos', $buscar)->render();
            

            return response()->json(['success' => true, 'datos' => $html_contenido, 'error' => '']);

            //return response()->json(['success' => true, 'datos' => $busqueda,'error' => '']);
        }

        $html_contenido = view('mutuales.localidad-sucursal')->with('error', 'Seleccione una opción para buscar.')->render();

        //dd($html_contenido);
        return response()->json(['success' => false, 'datos' => $html_contenido, 'error' => 'Seleccione una opción para buscar.']);
    }

    public function buscarCodPostalModalSucUp(Request $request)
    {
        Log::info('paso a buscar cod postal para sucursales----up');

        $input = request()->all();
        Log::info($input);
        //dd($input);

        if (isset($input['filtro'])) {
            if ($input['filtro'] == 1) {
                $sql = "SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia and localidad.nombre_localidad like '%".$input['buscar']."%'";

                $buscar = DB::connection('mysql')->select($sql);
            //dd($buscar);
            } elseif ($input['filtro'] == 2) {
                $sql = "SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal like '%".$input['buscar']."%'";

                $buscar = DB::connection('mysql')->select($sql);
            //dd('cod',$buscar);
            } elseif ($input['filtro'] == 3) {
                $sql = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                        FROM localidad, provincia
                        where provincia.codigo_provincia=localidad.codigo_provincia';

                $buscar = DB::connection('mysql')->select($sql);
                //dd('cod',$buscar);

                //dd('to',$buscar);
            }

            //dd(count($buscar));

            if (count($buscar) == 0) {
                $html_contenido = view('mutuales.sucursal-up')->with('error', 'No se encuentra Ninguna Localidad con ese Nombre.')->render();

                //dd($html_contenido);
                return response()->json(['success' => false, 'datos' => $html_contenido, 'error' => 'No se encuentra Ninguna Localidad con ese Nombre.']);
            }

            
            $html_contenido = view('mutuales.sucursal-up')->with('datos', $buscar)->with('sel',$input['sel'])->render();
            

            return response()->json(['success' => true, 'datos' => $html_contenido, 'error' => '']);

            //return response()->json(['success' => true, 'datos' => $busqueda,'error' => '']);
        }

        $html_contenido = view('mutuales.sucursal-up')->with('error', 'Seleccione una opción para buscar.')->render();

        //dd($html_contenido);
        return response()->json(['success' => false, 'datos' => $html_contenido, 'error' => 'Seleccione una opción para buscar.']);
    }

    public function imprimirSo()//esta opción se saco del formulario de craga de datos de la junta directiva
    {
        $input = request()->all();
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        //dd($input);
        Log::info($input);
        $id_mutual = session()->get('id_mutual');
        $id_sucursal = session()->get('id_sucursal');
        $fecha = date('d-m-Y H:i:s'); 

        //dd($fdesde,date('Y-m-d', strtotime($fdesde)),$fd,$fhasta,date('Y-m-d', strtotime($fhasta)),$fh);
        $persona = DB::table('personas')->where('nro_persona', $input['nro_personaso'])->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', 0)->first();
        $mutual  = DB::table('personas')->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', $id_sucursal)->first(); 
        
        $sql = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                FROM localidad, provincia
                where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal = '.$mutual->codigo_postal;

        $buscar = DB::connection('mysql')->select($sql);
        $localidad = $buscar[0]->localidad;
        $provincia = $buscar[0]->provincia;

        $nombre = $persona->nombre;
        $tipo_doc = $persona->tipo_doc;
        switch ($tipo_doc){
            case 0:
                $tipo = 'No Informado';
            break;
            case 1:
                $tipo = 'DNI';
            break;
            case 2:
                $tipo = 'CI';
            break;  
            case 4:
                $tipo = 'Pasaporte';
            break;  
            case 6:
                $tipo = 'RUT';
            break;
            case 7:
                $tipo = 'CUIL';
            break;
            case 8:
                $tipo = 'CUIT';
            break;  
            case 9:
                $tipo = 'Otro';
            break;    
            
        }

        $opcion = $persona->sujeto_obligado;
        switch($opcion){
            case 0:
                $si = ' ';
                $no = 'X';
            break;
            case 1: 
                $si = 'X';
                $no = ' ';
        }
        $tipo_doc = $tipo;
        $nro_doc = $persona->nro_doc;
        $fecha = $this->fechaCastellano($fecha);
       
        //dd($nombre);
        $view= \View::make('mutuales/pdf/sujeto', compact('nombre','tipo_doc','nro_doc','si','no','fecha','localidad','provincia'))->render();
        $pdf= \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        //return $pdf->stream('reporte_pdf'.'.pdf');
        return $pdf->download('sujeto-'.$nro_doc.'.pdf');
    }

    public function imprimirPep()
    {
        $input = request()->all();
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        //dd($input);
        if(isset($input['p'])){
            $input['nro_personapep'] = $input['nro_personapepp'];
            $doc = 'FA10-A';
        }elseif(isset($input['s'])){
            $input['nro_personapep'] = $input['nro_personapeps'];
            $doc = 'FA10-B';
        }elseif(isset($input['t'])){
            $input['nro_personapep'] = $input['nro_personapept'];
            $doc = 'FA10-C';
        }elseif(isset($input['a'])){
            $input['nro_personapep'] = $input['nro_personapepa'];
            $doc = 'FA10-D';

        }

        Log::info($input);
        $id_mutual = session()->get('id_mutual');
        $id_sucursal = session()->get('id_sucursal');
        $fecha = date('d-m-Y H:i:s'); 

        //dd($fdesde,date('Y-m-d', strtotime($fdesde)),$fd,$fhasta,date('Y-m-d', strtotime($fhasta)),$fh);
        $persona = DB::table('personas')->where('nro_persona', $input['nro_personapep'])->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', 0)->first();
        $mutual  = DB::table('personas')->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', $id_sucursal)->first(); 
        
        $sql = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                FROM localidad, provincia
                where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal = '.$mutual->codigo_postal;

        $buscar = DB::connection('mysql')->select($sql);
        $localidad = $buscar[0]->localidad;
        $provincia = $buscar[0]->provincia;

        $nombre = $persona->nombre;
        $tipo_doc = $persona->tipo_doc;
        switch ($tipo_doc){
            case 0:
                $tipo = 'No Informado';
            break;
            case 1:
                $tipo = 'DNI';
            break;
            case 2:
                $tipo = 'CI';
            break;  
            case 4:
                $tipo = 'Pasaporte';
            break;  
            case 6:
                $tipo = 'RUT';
            break;
            case 7:
                $tipo = 'CUIL';
            break;
            case 8:
                $tipo = 'CUIT';
            break;  
            case 9:
                $tipo = 'Otro';
            break;    
            
        }

        $opcion = $persona->persona_pep;
        switch($opcion){
            case 0:
                $si = ' ';
                $no = 'X';
            break;
            case 1: 
                $si = 'X';
                $no = ' ';
        }
        $tipo_doc = $tipo;
        $nro_doc = $persona->nro_doc;
        $fecha = $this->fechaCastellano($fecha);
       
        //dd($nombre);
        $view= \View::make('mutuales/pdf/pep', compact('nombre','tipo_doc','nro_doc','si','no','fecha','localidad','provincia','doc'))->render();
        $pdf= \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        //return $pdf->stream('reporte_pdf'.'.pdf');
        return $pdf->download('pep-'.$nro_doc.'.pdf');
    }


    public function imprimirJur()
    {
        $input = request()->all();
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        //dd($input);
        Log::info($input);
        $id_mutual = session()->get('id_mutual');
        $id_sucursal = session()->get('id_sucursal');
        $fecha = date('d-m-Y H:i:s'); 

        $doc_cargados = DB::table('documentacion_mutuales')->where('id_mutual', $id_mutual)->where('cod_doc','<>','FA00')->orderby('cod_doc','asc')->select('cod_doc')->get();
        //dd([$doc_cargados]);
        $documentos = null;
        foreach ($doc_cargados as $i => $res) {
            
            //dd($res[$i]->cod_doc);
            $documentos[$i] = $res->cod_doc;
    
        }
        //dd($documentos);

        //dd($fdesde,date('Y-m-d', strtotime($fdesde)),$fd,$fhasta,date('Y-m-d', strtotime($fhasta)),$fh);
        $persona = DB::table('personas')->where('nro_persona', $input['nro_persona'])->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', $id_sucursal)->first();

        $sql = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                FROM localidad, provincia
                where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal  = '.$persona->codigo_postal;

        $buscar = DB::connection('mysql')->select($sql);
        //dd($buscar);
           

        $presidente = DB::table('personas')
                                ->join('personas_sociedad', 'personas_sociedad.nro_persona', 'personas.nro_persona')
                                ->where('personas.soc_numero_socio', $id_mutual)->where('personas.soc_sucursal', 0)->where('personas_sociedad.socio_caracter', 8)->first();
                                $secretario = DB::table('personas')

                                ->join('personas_sociedad', 'personas_sociedad.nro_persona', 'personas.nro_persona')
                                ->where('personas.soc_numero_socio', $id_mutual)->where('personas.soc_sucursal', 0)->where('personas_sociedad.socio_caracter', 6)->first();

        $nombre_mutual = $persona->nombre;
        $tipo_doc = $persona->tipo_doc;
        $nombre_pre = $presidente->nombre;
        $nombre_sec = $secretario->nombre;
        $nro_inaes = $persona->soc_jud_nro_inscripcion;
        $domicilio_mutual = $persona->domicilio_calle;
        $localidad_mutual = $buscar[0]->localidad;
        $provincia_mutual = $buscar[0]->provincia;
        switch ($tipo_doc){
            case 0:
                $tipo = 'No Informado';
            break;
            case 1:
                $tipo = 'DNI';
            break;
            case 2:
                $tipo = 'CI';
            break;  
            case 4:
                $tipo = 'Pasaporte';
            break;  
            case 6:
                $tipo = 'RUT';
            break;
            case 7:
                $tipo = 'CUIL';
            break;
            case 8:
                $tipo = 'CUIT';
            break;  
            case 9:
                $tipo = 'Otro';
            break;    
            
        }

        $opcion = $persona->persona_pep;
        switch($opcion){
            case 0:
                $si = ' ';
                $no = 'X';
            break;
            case 1: 
                $si = 'X';
                $no = ' ';
        }
        $tipo_doc = $tipo;
        $nro_doc = $persona->nro_doc;
        $fecha = $this->fechaCastellano($fecha);
       
        //dd($nombre);
        $view= \View::make('mutuales/pdf/declaracion-jurada', compact('nombre_mutual','tipo_doc','nro_doc','si','no','fecha','nombre_pre','nombre_sec','nro_inaes','domicilio_mutual','localidad_mutual','documentos'))->render();
        $pdf= \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        //return $pdf->stream('reporte_pdf'.'.pdf');
        return $pdf->download('declaracion-jurada-'.$nombre_mutual.'.pdf');
    }

    public function imprimirDatosGenerales()
    {
        $input = request()->all();
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        //dd($input);
        Log::info($input);
        $id_mutual = session()->get('id_mutual');
        $id_sucursal = session()->get('id_sucursal');
        $fecha = date('d-m-Y H:i:s'); 
        
        //dd($fdesde,date('Y-m-d', strtotime($fdesde)),$fd,$fhasta,date('Y-m-d', strtotime($fhasta)),$fh);
        $persona = DB::table('personas')->where('nro_persona', $input['nro_persona_m'])->where('soc_numero_socio', $id_mutual)->where('soc_sucursal', $id_sucursal)->first();

        $sql = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                FROM localidad, provincia
                where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal  = '.$persona->codigo_postal;

        $buscar = DB::connection('mysql')->select($sql);
        //dd($buscar);
        $uif_act = DB::table('uif_actividad')->where('codigo',51)->first();
           

        $presidente = DB::table('personas')
                                ->join('personas_sociedad', 'personas_sociedad.nro_persona', 'personas.nro_persona')
                                ->where('personas.soc_numero_socio', $id_mutual)->where('personas.soc_sucursal', 0)->where('personas_sociedad.socio_caracter', 8)->first();
        $secretario = DB::table('personas')
                                ->join('personas_sociedad', 'personas_sociedad.nro_persona', 'personas.nro_persona')
                                ->where('personas.soc_numero_socio', $id_mutual)->where('personas.soc_sucursal', 0)->where('personas_sociedad.socio_caracter', 6)->first();

        $tesorero = DB::table('personas')
                                ->join('personas_sociedad', 'personas_sociedad.nro_persona', 'personas.nro_persona')
                                ->where('personas.soc_numero_socio', $id_mutual)->where('personas.soc_sucursal', 0)->where('personas_sociedad.socio_caracter', 7)->first();

        $apoderado = DB::table('personas')
                                ->join('personas_sociedad', 'personas_sociedad.nro_persona', 'personas.nro_persona')
                                ->where('personas.soc_numero_socio', $id_mutual)->where('personas.soc_sucursal', 0)->where('personas_sociedad.socio_caracter', 1)->first();

        $nro_adherente = $persona->soc_numero_socio;
        $nombre_mutual = $persona->nombre;
        $sucursal = $persona->soc_sucursal;
        $tipo_doc = $persona->tipo_doc;
        $nro_doc = $persona->nro_cuit;
        $nombre_pre = $presidente->nombre;
        
        $nro_doc_pre = $presidente->nro_doc;
        $nombre_sec = $secretario->nombre;
        
        $nro_doc_sec = $secretario->nro_doc;
        $nombre_tes = $tesorero->nombre;
        
        $nro_doc_tes = $tesorero->nro_doc;
        if($apoderado){
            $nombre = $apoderado->nombre;
            switch ($apoderado->tipo_doc){
                case 0:
                    $tipo_a = 'No Informado';
                break;
                case 1:
                    $tipo_a = 'DNI';
                break;
                case 2:
                    $tipo_a = 'CI';
                break;  
                case 4:
                    $tipo_a = 'Pasaporte';
                break;  
                case 6:
                    $tipo_a = 'RUT';
                break;
                case 7:
                    $tipo_a = 'CUIL';
                break;
                case 8:
                    $tipo_a = 'CUIT';
                break;  
                case 9:
                    $tipo_a = 'Otro';
                break;    
                
            }
            $t_doc = $tipo_a;
            $n_doc = $apoderado->nro_doc;
        }else{
            $nombre = '';
            $t_doc = '';
            $n_doc = '';
        }
        $nombre_apo = $nombre;
        $tipo_doc_apo = $t_doc;
        $nro_doc_apo = $n_doc;
        $nro_inaes = $persona->soc_jud_nro_inscripcion;
        $domicilio_mutual = $persona->domicilio_calle;
        $piso_mutual = $persona->domicilio_nro;
        $cod_postal = $persona->codigo_postal;
        $localidad_mutual = $buscar[0]->localidad;
        $provincia_mutual = $buscar[0]->provincia;
        $fecha_constitucion = $persona->soc_jud_fecha_constitucion;
        $inscripcion_inaes = $persona->soc_jud_nro_inscripcion;
        $actividad_principal = $uif_act->nombre;
        $telefono_ofi = $persona->telefono1;
        $telefono_cel = $persona->telefono_celular;
        if($persona->cond_iva == 5){
            $r_iva = 'Exento';
        }else{
            $r_iva = 'Otro';
        }
        $cond_iva = $r_iva;
        $correo = $persona->email;

        switch ($presidente->tipo_doc){
            case 0:
                $tipo_p = 'No Informado';
            break;
            case 1:
                $tipo_p = 'DNI';
            break;
            case 2:
                $tipo_p = 'CI';
            break;  
            case 4:
                $tipo_p = 'Pasaporte';
            break;  
            case 6:
                $tipo_p = 'RUT';
            break;
            case 7:
                $tipo_p = 'CUIL';
            break;
            case 8:
                $tipo_p = 'CUIT';
            break;  
            case 9:
                $tipo_p = 'Otro';
            break;    
            
        }

        $tipo_doc_pre = $tipo_p;
        switch ($secretario->tipo_doc){
            case 0:
                $tipo_s = 'No Informado';
            break;
            case 1:
                $tipo_s = 'DNI';
            break;
            case 2:
                $tipo_s = 'CI';
            break;  
            case 4:
                $tipo_s = 'Pasaporte';
            break;  
            case 6:
                $tipo_s = 'RUT';
            break;
            case 7:
                $tipo_s = 'CUIL';
            break;
            case 8:
                $tipo_s = 'CUIT';
            break;  
            case 9:
                $tipo_s = 'Otro';
            break;    
            
        }
        $tipo_doc_sec = $tipo_s;
        switch ($tesorero->tipo_doc){
            case 0:
                $tipo_t = 'No Informado';
            break;
            case 1:
                $tipo_t = 'DNI';
            break;
            case 2:
                $tipo_t = 'CI';
            break;  
            case 4:
                $tipo_t = 'Pasaporte';
            break;  
            case 6:
                $tipo_t = 'RUT';
            break;
            case 7:
                $tipo_t = 'CUIL';
            break;
            case 8:
                $tipo_t = 'CUIT';
            break;  
            case 9:
                $tipo_t = 'Otro';
            break;    
            
        }
        $tipo_doc_tes = $tipo_t;

        switch ($persona->soc_ejercicio_economico){
            case 1:
                $ejer = 'Enero';
            break;
            case 2:
                $ejer = 'Febrero';
            break;
            case 3:
                $ejer = 'Marzo';
            break;  
            case 4:
                $ejer = 'Abril';
            break;  
            case 5:
                $ejer = 'Mayo';
            break;
            case 6:
                $ejer = 'Junio';
            break;
            case 7:
                $ejer = 'Julio';
            break;  
            case 8:
                $ejer = 'Agosto';
            break;
            case 9:
                $ejer = 'Septiembre';
            break;    
            case 10:
                $ejer = 'Octubre';
            break;
            case 12:
                $ejer = 'Noviembre';
            break;
            case 12:
                $ejer = 'Diciembre';
            break;
                       
        }
        $ejer_eco = $ejer;
       
        $fecha = $this->fechaCastellano($fecha);
       
        //dd($nombre);
        $view= \View::make('mutuales/pdf/datos-generales', compact('nro_adherente','nombre_mutual','sucursal','nro_doc','fecha','nombre_pre','tipo_doc_pre','nro_doc_pre','nombre_sec','tipo_doc_sec','nro_doc_sec',
                                                                   'nombre_tes','tipo_doc_tes','nro_doc_tes','nombre_apo','tipo_doc_apo','nro_doc_apo','nro_inaes','domicilio_mutual','localidad_mutual','provincia_mutual',
                                                                   'fecha_constitucion','inscripcion_inaes','actividad_principal','piso_mutual','cod_postal','telefono_ofi','telefono_cel','cond_iva','ejer_eco','correo'))->render();
        $pdf= \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        //return $pdf->stream('reporte_pdf'.'.pdf');
        return $pdf->download('datos-generales-'.$nombre_mutual.'.pdf');
    }

    public function fechaCastellano ($fecha) {
        $fecha = substr($fecha, 0, 10);
        $numeroDia = date('d', strtotime($fecha));
        $dia = date('l', strtotime($fecha));
        $mes = date('F', strtotime($fecha));
        $anio = date('Y', strtotime($fecha));
        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
        $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
        return $nombredia." ".$numeroDia." de ".$nombreMes." de ".$anio;
    }

    public function guardarDocumento(Request $request){
        $input = request()->all();
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        Log::info('archivo js');
        Log::info($input);
        $archivo = $_FILES['archivo']['tmp_name']; 
        $tamanio = $_FILES['archivo']['size'];
        $tipo    = $_FILES["archivo"]["type"];
        $nombre  = $_FILES['archivo']['name'];
        //Log::info($archivo);
         
        $id_mutual = session()->get('id_mutual');
        $id_sucursal = session()->get('id_sucursal');
        $fecha = date('d-m-Y H:i:s'); 
        $cod = $input['cod'];
        
        if (!isset($_FILES['archivo']) || $_FILES['archivo']['error'] > 0){
            //return view('mutuales.mutual')->with('error','Ocurrio un error a guardar el archivo, debe seleccionar uno.');
            //dd($html_contenido);
            return response()->json(['success' => false,  'error' => 'archvio no permitido.']);
        }else{

            $permitidos = array("application/pdf");
            $limite_kb = 16384;
            
            if (in_array($_FILES['archivo']['type'], $permitidos) && $_FILES['archivo']['size'] <= $limite_kb * 1024){
                DB::beginTransaction();
                try {
                    $imagen_temporal = $_FILES['archivo']['tmp_name'];
                    $tipo = explode('application/',$_FILES['archivo']['type']);
                    $tipo = $tipo[1];
                    $fp = fopen($archivo, "rb");
                    $contenido = fread($fp, $tamanio);
                    $contenido = addslashes($contenido);
                    fclose($fp); 
                    //falta preguntar si existe , se actualiza y mostrar el que ya existe ne la vista
                    $cod_doc = DB::table('documentacion_mutuales')->where('id_mutual',$id_mutual)->where('cod_doc',$cod)->first();
                    if($cod_doc){
                        Log::info('update');
                        $datos_up =[
                            'descripcion' => $nombre,
                            'archivo'     => $contenido,
                            'tipo'        => $tipo,
                        ];



                        /*DB::table('documentacion_mutuales')->where('id_mutual',$id_mutual)->where('cod_doc',$cod)->update($datos_up);
                        $salida = 'Se Actualizo correctamente el documento '.$cod;*/ //funciona pero no deja visualizar luego el archivo
                        //$sql ='delete from documentacion_mutuales where id_mutual ='.$id_mutual.' and cod_doc ='.$cod;
                        //DB::connection('mysql')->delete($sql);
                        DB::table('documentacion_mutuales')->where('id_mutual',$id_mutual)->where('cod_doc',$cod)->delete();

                        $sqlI ="insert into documentacion_mutuales (id_mutual,cod_doc,descripcion,archivo,tipo) values ('$id_mutual','$cod','$nombre','$contenido', '$tipo')"; //CAST('$data' as varbinary(max))
                        DB::connection('mysql')->insert($sqlI);
                       

                        //DB::connection('sqlsrv')->table('mysql')->where('id_mutual', $id_mutual)->where('cod_doc',$cod)
                                                                //->update(array('descripcion' => $nombre, 'archivo' => $contenido, 'tipo' => $tipo));

                        $salida = 'Se Actualizo correctamente el documento '.$cod;
                        
                    }else{
                        Log::info('insert');
                        $sql ="insert into documentacion_mutuales (id_mutual,cod_doc,descripcion,archivo,tipo) values ('$id_mutual','$cod','$nombre','$contenido', '$tipo')"; //CAST('$data' as varbinary(max))
                        DB::connection('mysql')->insert($sql);
                        $salida = 'Se Guardo correctamente el documento '.$cod;
                    }

                        DB::commit();
                        $documentos = DB::table('documentacion_mutuales')->where('id_mutual',$id_mutual)->where('cod_doc',$cod)->select('descripcion')->first();
                        Log::info('luego de operación');
                        Log::info($documentos->descripcion);
                        return response()->json(['success' => true, 'documento' => $documentos->descripcion, 'salida' => $salida]);
                    
                    }catch(\Illuminate\Database\QueryException $ex){ 
                        DB::rollback();
                       
                        return response()->json(['success' => false,  'error' => $ex->getMessage()]); 		 
                    }
            }else{
                return response()->json(['success' => false,  'error' => 'archivo no permitido.']);
            }
        }
               
    }


    public function updateSucursal(Request $request)
    {
        Log::info('up sucursal');
        $mutuales = DB::table('personas')->where('soc_sucursal',1)->where('es_socio',1)->orderby('soc_numero_socio','asc')->get();
        if ($request->isMethod('post')) {
            $input = $request->all();
            Log::info($input);
            $rules = [
               'nombre' => 'required',
                
               
            ];

            $messages = [
              
                'nombre.required' => 'Ingrese el nombre de la Sucursal.',
              
            ];

            $v = Validator::make($input, $rules, $messages);

            if ($v->fails()) {
                $msg = $v->errors()->all()[0];

                return response()->json(['success' => false, 'error' => $msg]);
            }

            try {
                DB::beginTransaction();

                   
                    $datos = [
                        'nombre' => $input['nombre'],
                        'codigo_postal' => $input['codigo_postal_suc'],
                        'soc_codigo_baja' => $input['habilitado'],
                                             
                    ];
                    DB::table('personas')->where('soc_numero_socio',$input['id_mutual'])->where('soc_sucursal',$input['id_sucursal'])->update($datos);
                    $accion = 'Sucursal Actualizada con exito.';
                   
                   
                    DB::commit();
               
                    
                    return response()->json(['success' => true, 'salida' => $accion]);

                } catch (\Illuminate\Database\QueryException $ex) {
                    DB::rollback();
                    Log::info('Error: '.$ex->getMessage());
                           
                    return response()->json(['success' => false, 'error' => 'error al Actualizarla Sucursal.']);
                }


            }
  
        return view('mutuales.modificar-sucursal')->with('mutuales', $mutuales);
    }

    public function buscarSucMutual(Request $request)
    {
        Log::info('buscarSucMutual');
        $mutuales = DB::table('personas')->where('soc_sucursal',1)->where('es_socio',1)->get();
        if ($request->isMethod('post')) {
            $input = $request->all();
            Log::info($input);
            $sucursales = DB::table('personas')->where('soc_numero_socio',$input['id_mutual'])->where('soc_sucursal','>',1)->get();
            if(count($sucursales) > 0){
                $sucursales = $sucursales->toJson();
                $sucursales = json_decode($sucursales, true);
                //$op = "<option value=" .$sel_dh->tipoCbte.">".$sel_dh->NombreCbte. "</option>";
                //dd($sel_dh);
                return response()->json(['success'=>true, 'sel' =>  $sucursales]);
            }else{
                return response()->json(['success'=>false, 'error' => 'No existen Sucursales.']);
            }
        }
    }

    public function buscarSucursal(Request $request){
        Log::info('buscarSucursal');
        $input = $request->all();
        Log::info($input);

        if($input['nro_sucursal'] == NULL || $input['mutual'] == NULL){
            return response()->json(['success' => false, 'error' => 'No existe Sucursal para buscar.']);
        }

        $mutual = DB::table('personas')->where('soc_numero_socio',$input['mutual'])->where('soc_sucursal',1)->first();
        $sucursal = DB::table('personas')->where('soc_numero_socio',$input['mutual'])->where('soc_sucursal',$input['nro_sucursal'])->get();
        if($sucursal){
            $sql_s = 'SELECT localidad.codigo_postal as cod_postal,localidad.codigo_provincia as cod_prov,localidad.nombre_localidad as localidad,provincia.nombre_provincia as provincia,provincia.codigo_GP as cod_prov_gp 
                      FROM localidad, provincia
                      where provincia.codigo_provincia=localidad.codigo_provincia and localidad.Codigo_Postal  = '.$sucursal[0]->codigo_postal;
    
            $buscar_s = DB::connection('mysql')->select($sql_s);
            Log::info($buscar_s);
        
            $sucursal[0]->localidad = $buscar_s[0]->localidad;
            $sucursal[0]->provincia = $buscar_s[0]->provincia;
            Log::info([$sucursal]);

            switch ($mutual->confirmada){
                case 0:
                    $estado_actual = 'En carga de datos.';
                break;
                case 1:
                    $estado_actual = 'Datos Confirmados, en revisión por FAUM.';
                break;
                case 2:
                    $estado_actual = 'Datos Aprobados por FAUM.';
                break;  
                case 3:
                    $estado_actual = 'Rechazada por FAUM, actualizar datos.';
                break;  
                
                
            }

            $mutual->estado_actual = $estado_actual;

            $html_contenido = view('mutuales.actualizar-sucursal')->with('sucursal',$sucursal)->render();
            return response()->json(['success' => true, 'datos' => $html_contenido, 'error' => '','salida' => $mutual]);

        }else{
            return response()->json(['success' => false,  'error' => 'No existen Mutuales','salida' => $mutual]);
        }
        

    }


    public function imprimirDeclaraAhorro()
    {
        $nombre = 'Declaración Jurada sobre conformación de Ahorros';
        $view= \View::make('mutuales/pdf/declara-ahorro', compact('nombre'))->render();
        $pdf= \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        //return $pdf->stream('reporte_pdf'.'.pdf');
        return $pdf->download('declara-ahorro.pdf');
    }

    public function imprimirCertificacionAhorro()
    {
        $nombre = 'Certificación sobre conformación Ahorros';
        $view= \View::make('mutuales/pdf/certificacion-ahorro', compact('nombre'))->render();
        $pdf= \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        //return $pdf->stream('reporte_pdf'.'.pdf');
        return $pdf->download('certificacion-ahorro.pdf');
    }

    public function imprimirAyudaEconomica()
    {
        $nombre = 'Declaración Jurada sobre recursos Específicos del Servicio de Ayuda Economica con Fondos Propios';
        $view= \View::make('mutuales/pdf/declara-ayuda-economica', compact('nombre'))->render();
        $pdf= \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        //return $pdf->stream('reporte_pdf'.'.pdf');
        return $pdf->download('declara-ayuda-economica.pdf');
    }

    public function imprimirCertificacionAyudaEconomica()
    {
        $nombre = 'Certificación sobre Recursos Servicio Ayuda Economica';
        $view= \View::make('mutuales/pdf/certificacion-ayuda-economica', compact('nombre'))->render();
        $pdf= \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        //return $pdf->stream('reporte_pdf'.'.pdf');
        return $pdf->download('certificacion-ayuda-economica.pdf');
    }

    
}
